package Ch_11_Prog_Exer;



/**
 * Class description: Ch 11-9, Prog Exer. Child class Health and Life uses Insurance abstract class.  Main application is UseInsurance
 * 
 * The Insurance class contains a 
 * 		String representing the type of insurance and a 
 * 		double that holds the monthly price. 
 * 
 * The Insurance class constructor requires a String argument indicating the type of insurance, 
 * but the Life and Health class constructors require no arguments. 
 * The Insurance class contains a get method for each field; it also contains two abstract methods named setCost() and display(). 
 * The Life class setCost() method sets the monthly fee to $36, and the Health class sets the monthly fee to $196.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */

public abstract class Insurance 
{
	protected String type;
    protected double cost;
    
    public Insurance(String lh)
    {
        type = lh;		//lh will be either life or health (lh)
        setCost();		//all fields except cost has been set, therefore implement method setCost() to set the cost of each Insurance
    }
    public String getType()
    {
        return type;
    }
    public double getCost()
    {
        return cost;
    }
    public abstract void setCost();		//both life and health classes will override setCost()
    public abstract void display();		//both life and health classes will override display

}
