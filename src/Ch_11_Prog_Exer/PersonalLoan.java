package Ch_11_Prog_Exer;

/**
 * Class description: Ch 10-13, Prog Exer.   Uses main 'CreateLoans', abstract class 'Loan' which implements 'LoanConstants' and classes 'BusinessLoan' and 'PersonalLoan'
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class PersonalLoan extends Loan
{
	public PersonalLoan (int num, String name, double amt, int yrs, double prime)
	{
		super(num, name, amt, yrs);
		rate = prime + .02;
	}

}
