package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-4, Prog Exer: Child class DomesticDivision and InternationalDivision use abstract parent class Division and UseDivision as the application
 * 
 * Create two subclasses named InternationalDivision and DomesticDivision. 
 * The InternationalDivision includes 
 * 		a field for the country (country) in which the division is located and 
 * 		a field for the language spoken (language); 
 * 	its constructor requires both. 
 * 
 * The DomesticDivision includes a field for the state (state) in which the division is located; a value for this field is required by the constructor.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sep 2021
 */
public class DomesticDivision extends Division
{
	protected String state;
    public DomesticDivision(String title, int acct, String st)
    {
        super(title, acct);
        state = st;
    }
    public void display()
    {
        System.out.println("Domestic Division: " + divisionTitle + "  Acct #" + acctNum + 
        		"\nLocated in " + state);
    }
}
