package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-2, Prog Exer:  The Talk-A-Lot Cell Phone Company provides phone services for its customers. 
 * 
 * Create an abstract class named PhoneCall that includes:
 * 		String field for a phone number 
 * 		double field for the price of the call. 
 * 	Also include a constructor that requires a phone number parameter and that sets the price to 0.0. 
 * 		Include a set method for the price. 
 * 	Also include three abstract get methods�
 * 		one that returns the phone number, 
 * 		another that returns the price of the call, 
 * 		and a third that displays information about the call.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public abstract class PhoneCall 
{
	String phoneNumber;
    double price;
    
    public PhoneCall(String num)
    {
        phoneNumber = num;
        price = 0.0;
        
    }
    public void setPrice(double pr)
    {
        price = pr;
    }
    public abstract String getPhoneNumber();
    public abstract double getPrice();
    public abstract void getInfo();

}
