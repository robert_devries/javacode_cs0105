package Ch_11_Prog_Exer;

/**
 * Class description:  Ch 11-4, Prog Exer: Create an abstract Division class with fields for a 
 * 		company�s division name (divisionTitle) and 
 * 		account number (acctNum), 
 * 		and an abstract display() method. 
 * 
 * Use a constructor in the superclass that requires values for both fields.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  4 Sep 2021
 */
public abstract class Division 
{
	protected String divisionTitle;
    protected int acctNum;
    
    public Division(String title, int acct)
    {
        divisionTitle = title;
        acctNum = acct;
    }
    public abstract void display();

}
