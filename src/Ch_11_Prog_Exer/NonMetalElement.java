package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-5, Prog Exer: Create two extended classes named MetalElement and NonMetalElement. 
 * 
 * Each contains a describeElement() method that 
 * 		displays the details of the element and a 
 * 		brief explanation of the properties of the element type. 
 * 
 * For example, metals are good conductors of electricity, while nonmetals are poor conductors.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class NonMetalElement extends Element
{
	public NonMetalElement(String s, int an, double aw)
	{
		super(s, an, aw);
	}
	
	public void describeElement()			//must have same signature as abstract parent method for describeElement() since its abstract method
	{
		System.out.println("Symbol is: " + getSymbol() + ", Atomic Wt is: " + getAtomicWeight() + 
				"\n, Atomic Num: " + getAtomicNumber() + ", Nonmetals are poor conductors.");
	}

}
