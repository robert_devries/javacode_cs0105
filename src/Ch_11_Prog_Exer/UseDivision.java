package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-4, Prog Exer: Application UseDivision using 'abstract' parent class Division, child classes 'DomesticDivision', 'InternationalDivision'
 * 
 * Create two subclasses named InternationalDivision and DomesticDivision. 
 * The InternationalDivision includes 
 * 		a field for the country (country) in which the division is located and 
 * 		a field for the language spoken (language); 
 * 	its constructor requires both. 
 * 
 * The DomesticDivision includes a field for the state (state) in which the division is located; a value for this field is required by the constructor.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sep 2021
 */

public class UseDivision 
{
	public static void main(String[] args) 
	{
		DomesticDivision abcDomDiv = new DomesticDivision("Sales", 123, "California");
	        DomesticDivision xyzDomDiv = new DomesticDivision("Technology", 234, "Kansas");
	        InternationalDivision abcIntDiv = new InternationalDivision("Technology", 345, "Germany", "German");
	        InternationalDivision xyzIntDiv = new InternationalDivision("Marketing", 456, "Japan", "Japanese");
	        abcDomDiv.display();
	        xyzDomDiv.display();
	        abcIntDiv.display();
	        xyzIntDiv.display();

	}

}
