package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-1 Prog Exer: This is the application to run the abstract Book class used with child classes Fiction and NonFiction 
 * 
 * Create two child classes of Book: Fiction and NonFiction. 
 * Each must include a setPrice() method that sets the price for all Fiction Books to $24.99 and for all NonFiction Books to $37.99. 
 * Write a constructor for each subclass, and include a call to setPrice() within each. 
 * Write an application demonstrating that you can create both a Fiction and a NonFiction Book, and display their fields.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 
 */
public class UseBookDemo 
{
	public static void main(String[] args) 
	{
		Fiction aNovel = new Fiction("Huckelberry Finn");
	    NonFiction aManual = new NonFiction("Elements of Style");
	    System.out.println("Fiction " +
	        aNovel.getTitle() + " costs $" +
	        aNovel.getPrice());
	    System.out.println("Non-Fiction " +
	        aManual.getTitle() + " costs $" +
	        aManual.getPrice());

	}

}
