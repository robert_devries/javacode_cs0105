package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-6; Prog Exer: Create a class named Blanket with fields for a blanketís:
 * 		size, 
 * 		color, 
 * 		material, and 
 * 		price. 
 * 
 * Include a constructor that sets default values for the fields as Twin, white, cotton, and $30.00, respectively. 
 * 
 * Include a set method for each of the first three fields. 
 * 
 * The method that sets size adds $10 to the base price for a double blanket, $25 for a queen blanket, and $40 for a king. 
 * 
 * The method that sets the material adds $20 to the price for wool and $45 to the price for cashmere.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Blanket 
{
	protected String size;
    protected String color;
    protected String material;
    protected double price;
    protected double sizePremium;
    protected double materialPremium;
    private final String SIZE1 = "Twin";
    private final String SIZE2 = "Double";
    private final String SIZE3 = "Queen";
    private final String SIZE4 = "King";
    protected final double BASE_PRICE = 30;
    private final double S_PRICE2 = 10;
    private final double S_PRICE3 = 25;
    private final double S_PRICE4 = 40;
    private final String MAT1 = "cotton";
    private final String MAT2 = "wool";
    private final String MAT3 = "cashmere";
    private final double M_PRICE2 = 20;
    private final double M_PRICE3 = 45;
    
    public Blanket()
    {
        setDefaults();
    }
    private void setDefaults()		//defaults are Twin, white, cotton and $30.00
    {
        size = SIZE1;
        color = "white";
        material = MAT1;
        price = BASE_PRICE;
        
    }
    public String getSize()
    {
        return size;
    }
    public String getColor()
    {
        return color;
    }
    public String getMaterial()
    {
        return material;
    }
    public void setSize(String s)		//method that sets size, adds various amounts to base price
    {
        if (s == SIZE2)
        	price = BASE_PRICE + S_PRICE2;
        else if (s == SIZE3)
        	price = BASE_PRICE + S_PRICE3;
        else if (s == SIZE4)
        	price = BASE_PRICE + S_PRICE4;
        else {
			setDefaults();
		}
    }
    public void setMaterial(String m)
    {
        if (m.equalsIgnoreCase(MAT2))
        {
        	price = BASE_PRICE + M_PRICE2;
        	material = MAT2;
        }
        	
        else if (m.equalsIgnoreCase(MAT3))
        {
        	price = BASE_PRICE + M_PRICE3;
        	material = MAT3;
        }
        	
        else {
			setDefaults();
		}
    }
    public String toString()
    {
        String details = ("Size: " + size + " color: " + color + " Material: " + material + " Price: " + price);
        return details;
    }

}
