package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-6; Prog Exer: Create a child class named ElectricBlanket that extends Blanket and includes two additional fields: 
 * 		one for the number of heat settings (settings) and 
 * 		one for whether the electric blanket has an automatic shutoff feature (hasAutoShutoff). 
 * 
 * The constructor should set the default values for settings and hasAutoShutoff to 
 * 		1 heat setting and false for no automatic shutoff. 
 * 
 * Do not allow the number of settings to be fewer than 1 or more than 5; if it is, use the default setting of 1. 
 * 
 * Add a $5.75 premium to the price if the blanket has the automatic shutoff feature. 
 * 
 * Include get and set methods for the settings and hasAutoShutofffields. 
 * 
 * Include a toString() method that calls the parent class toString() method and combines the returned value 
 * with data about the new fields to return a complete description of features.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ElectricBlanket extends Blanket
{
	private int settings;
    private boolean hasAutoShutoff;
    private final int MAX_SETTINGS = 5;
    private double shutoffPremium;
    private final double S_PRICE = 5.75;
    public ElectricBlanket()
    {
        settings = 1;
        hasAutoShutoff = false;
    }
    public int getSettings()
    {
        return settings;
    }
    public boolean getHasAutoShutoff()
    {
        return hasAutoShutoff;
    }
    public void setSettings(int s)
    {
        if ((s > 0) && (s <= MAX_SETTINGS))
        		settings = s;
        else {
			settings = 1;
		}
    }
    public void setHasAutoShutoff(boolean h)
    {
        hasAutoShutoff = h;
        if (hasAutoShutoff)
        	price = price + S_PRICE;        
    }
    public String toString()
    {
        String blanket = super.toString();
        String eBlanket = (" Settings: " + settings + " HasAutoShutoff: " + hasAutoShutoff);
        return (blanket + eBlanket);
    }

}
