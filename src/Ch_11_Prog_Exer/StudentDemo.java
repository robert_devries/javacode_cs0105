package Ch_11_Prog_Exer;

/**
 * Class description: Uses abstract parent class 'Student', child classes StudentAtLarge, UndergraduateStudent, GraduateStudent
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */
public class StudentDemo 
{
	public static void main(String[] args) 
	{
		Student students[] = new Student[6];		//Student is abstract class, cannot instantiate abstract class variables, but can assign references
        int i;
        students[0] = new UndergraduateStudent("111", "Lambert");		//can instantiate child class variables to each reference object
        students[1] = new UndergraduateStudent("122", "Lembeck");
        students[2] = new GraduateStudent("233", "Miller");
        students[3] = new GraduateStudent("256", "Marmon");
        students[4] = new StudentAtLarge("312", "Nichols");
        students[5] = new StudentAtLarge("376", "Nussbaum");
        for(i = 0; i < students.length; ++i)
            System.out.println("\nStudent # " + students[i].getId() + 
            		" Name: " + students[i].getLastName() + 
            		"  Tuition: " + students[i].getTuition() + " per year");
		
	}

}
