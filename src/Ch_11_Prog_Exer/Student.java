package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-7, Prog Exer: Abstract Student class for Parker University. 
 * The class contains fields for 
 * 		student ID number (id), 
 * 		last name (lastName), and
 * 		annual tuition (tuition). 
 * 
 * Include a constructor that requires parameters for the ID number and last name. 
 * Include get and set methods for each field; the setTuition() method is abstract.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sept 2021
 */

public abstract class Student
{
	private String id;
    private String lastName;
    protected double tuition;
    
    public Student(String id, String name)
    {
        this.id = id;
        lastName = name;
        setTuition();				//required within this constructor to set tuition for each child class
    }
    public void setId(String idNum)
    {
        id = idNum;
    }
    public void setLastName(String name)
    {
        lastName = name;
    }
    public String getId()
    {
        return id;
    }
    public String getLastName()
    {
        return lastName;
    }
    public double getTuition()
    {
        return tuition;
    }
    public abstract void setTuition();

}
