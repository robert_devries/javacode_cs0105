package Ch_11_Prog_Exer;

/**
 * Class description: "OnlineNewspaperSubscription" and "PhysicalNewspaperSubscription" are child classes to NewspaperSubscription. DemoSubscriptions is application
 *
 * Create two subclasses named PhysicalNewspaperSubscription and OnlineNewspaperSubscription. 
 * The parameter for the setAddress() method of the PhysicalNewspaperSubscription class must contain at least one digit; otherwise, an error message is displayed 
 * and the subscription rate is set to 0. If the address is valid, the subscription rate is assigned $15. 
 * 
 * The parameter for the setAddress() method of the OnlineNewspaperSubscription class must contain an at sign (@) or an error message is displayed. 
 * If the address is valid, the subscription rate is assigned $9.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class PhysicalNewspaperSubscription extends NewspaperSubscription
{
	
	public void setAddress(String a)
    {
		boolean isValidAdd = false;
		char chekString;				//check String for a valid number in the address parameter 'a'
        for(int x = 0; x < a.length() - 1; x++)
        {
        	chekString = a.charAt(x);
        	if (Character.isDigit(chekString))
        	{
        		isValidAdd = true;
        		address = a;			//access to abstract parent class, address at this point is valid since contains a digit
        		rate = 15.00;
        	}
        }
        if (!isValidAdd)
        {
        	address = "Error, not valid address";
        	rate = 0.0;
        }

        
    }
}
