package Ch_11_Prog_Exer;


/**
 * Class description: Ch 11-10, Prog Exer. An interface named 'SidedObject' has been added that contains a method called displaySides()
 * 
 * Modify the displaySides() method so that it displays the number of sides the object possesses
 * 
 * Create an abstract class called 'GeometricFigure2'. Main application "UseGeometric2"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square2' and 'Triangle2'. Define the constructor and figureArea method of each subclass.
 * Square2 class should output "This figure has four sides" and the Triangle2 should output "This figure has three sides"
 *
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date 5 Sep 2021
 */

public class Square2 extends GeometricFigure2 implements SidedObject
{
	private double area;
    
	public Square2(int h, int w, String f)
    {
        super(h, w, f);
        //figureArea(w, h);
      
    }
	@Override
    public double figureArea(int w, int h)
    {
        area = (double)(w * h);			//added the (double) instead of 1.0 as a double to ensure area is double as opposed to promoting 2 * ints 
        return area;
    }
	
	@Override						//overrides interface named SidedObject (Chapter 11, page 46, para 11-9 (Don't Do It)
	public void displaySides()
	{
		System.out.println("This figure has four sides");
	}

}
