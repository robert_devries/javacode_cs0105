package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-12, Prog Exer.  Create an interface called 'Runner'.  The interface has an abstract method called run( ) that displays a message
 * 		describing the meaning of run to the class.
 * 
 * Create classes called 'Machine', 'Athlete', and 'PoliticalCandidate' that all implement 'Runner'
 *
 * The run() should print the following in each class:
 * 		Machine - When a machine is running, it is operating.
 * 		Athlete - An athlete might run in a race, or in a game like soccer.
 * 		PoliticalCandidate - A political candidate runs for office.
 * 
 * Main application is:  'DemoRunners'
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class PoliticalCandidate implements Runner 
{
	public void run()
	{
		System.out.println("A political candidate runs for office.");
	}

}
