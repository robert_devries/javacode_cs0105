package Ch_11_Prog_Exer;

/**
 * Class description: Prog Exer: Create three Student subclasses named UndergraduateStudent, GraduateStudent, and StudentAtLarge, 
 * 
 * each with a unique setTuition() method. 
 * 
 * Tuition for an UndergraduateStudent is $4,000 per semester, 
 * 
 * tuition for a GraduateStudent is $6,000 per semester, and 
 * 
 * tuition for a StudentAtLarge is $2,000 per semester.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class GraduateStudent extends Student
{
	public static final double GRAD_TUITION = 6000;
	
    public GraduateStudent(String id, String name)
    {
        super(id, name);
        
    }
   
    @Override			//not a requirement to insert @Override, but is good practice to indicate since it's automatic as referenced by superclass
    public void setTuition()
    {
        tuition = GRAD_TUITION;
    }

}
