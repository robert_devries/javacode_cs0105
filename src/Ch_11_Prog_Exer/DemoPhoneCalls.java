package Ch_11_Prog_Exer;

/**
 * Class description: DemoPhoneCalls is the application that uses PhoneCall (Parent class) Incoming and OutgoingPhoneCall child classes
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 04 Sept 2021
 *
 */
public class DemoPhoneCalls 
{
	public static void main(String[] args) 
	{
		IncomingPhoneCall inCall = new IncomingPhoneCall("212-555-3096");
        OutgoingPhoneCall outCall = new OutgoingPhoneCall("312-874-0232", 10);
        inCall.getInfo();
        outCall.getInfo();

	}

}
