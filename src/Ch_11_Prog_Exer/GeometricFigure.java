package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-10, Prog Exer. Create an abstract class called 'GeometricFigure'. Main application "UseGeometric"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square' and 'Triangle'. Define the constructor and figureArea method of each subclass.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */
public abstract class GeometricFigure 
{
	private int height;
	private int width;
	private String figure;
	   
    public GeometricFigure(int h, int w, String f)
    {
        height = h;
        width = w;
        figure = f;
    }
    public int getHeight()
    {
        return height;
    }
    public int getWidth()
    {
        return width;
    }
    public String getFigure()
    {
        return figure;
    }
    public abstract double figureArea(int width, int height);

}
