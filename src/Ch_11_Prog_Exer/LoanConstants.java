package Ch_11_Prog_Exer;

/**
 * Class description: Ch 10-13, Prog Exer.   Uses main 'CreateLoans', abstract class 'Loan' which implements 'LoanConstants' and classes 'BusinessLoan' and 'PersonalLoan'
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public interface LoanConstants 
{
	public static final int MAXLOAN = 100000;
    public static final int SHORT_TERM = 1;
    public static final int MEDIUM_TERM = 3;
    public static final int LONG_TERM = 5;
    public static final String COMPANY = "Sanchez Construction";

}
