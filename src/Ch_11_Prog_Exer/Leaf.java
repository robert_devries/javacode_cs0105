package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-8, Prog Exer: Create an interface named Turner, with a single method named turn(). 
 * 
 * Create a class named Leaf that implements the Turner interface to display Changing colors.. 
 * 
 * Create a class named Page that implements the Turner interface to display Going to the next page.. 
 * 
 * Create a class named Pancake that implements the Turner interface to display Flipping..
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class Leaf implements Turner
{
	@Override			//not mandatory to include @Override annotation .... good practice since method will be Overridden as declared in interface (Turner)
	public void turn()
	{
		System.out.println("Changing colors.");
	}


}
