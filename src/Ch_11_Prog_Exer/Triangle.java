package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-10, Prog Exer. Create an abstract class called 'GeometricFigure'. Main application "UseGeometric"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square' and 'Triangle'. Define the constructor and figureArea method of each subclass.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date 5 Sept 2021
 */
public class Triangle extends GeometricFigure 
{
	private double area;
	
    public Triangle(int w, int h, String f)
    {
        super(h, w, f);
        //figureArea(w, h);
    }
    
    @Override
    public double figureArea(int w, int h)
    {
        area = (w/2.0)*h;
        return area;
    }

}
