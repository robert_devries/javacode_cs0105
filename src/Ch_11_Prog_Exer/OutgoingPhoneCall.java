package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-2 Prog Exer.  Incoming and OutgoingPhoneCall are child classes of PhoneCall
 *
 * The OutgoingPhoneCall class includes an additional field that holds the time of the call in minutes. 
 * The constructor requires both a phone number and the time. 
 * The price is 0.04 per minute, and the display method shows the details of the call, including the phone number, the rate per minute, the number of minutes, and the total price.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sept 2021
 */

public class OutgoingPhoneCall extends PhoneCall
{
	public final static double RATE = .04;
    private int minutes;
    
    public OutgoingPhoneCall(String telNum, int mins)
    {
        super(telNum);
        minutes = mins;
    }
    
    public void getInfo()
    {
        System.out.println("Phone #: " + getPhoneNumber() + ", the rate per minute is: " + RATE + ", # of minutes: " + minutes + " the price is: " + getPrice());
    }
    
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    
    public double getPrice()
    {
        double price = minutes * RATE;			//price is varied depending on minutes used for outgoing calls
        return price;
    }

}
