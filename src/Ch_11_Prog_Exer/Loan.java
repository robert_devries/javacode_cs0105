package Ch_11_Prog_Exer;

import java.math.*;

/**
 * Class description: Ch 10-13, Prog Exer.   Uses main 'CreateLoans', abstract class 'Loan' which implements 'LoanConstants' and classes 'BusinessLoan' and 'PersonalLoan'
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public abstract class Loan implements LoanConstants
{
	protected int loanNum;
    protected String lastName;
    protected double amount;
    protected double rate;
    protected int term;
    
    public Loan(int num, String name, double amt, int yrs)
    {
        loanNum = num;
        lastName = name;
        if (amt <= MAXLOAN)													//MAXLOAN set by LoanConstants
        	amount = amt;
        else amount = MAXLOAN;
        if (yrs == SHORT_TERM || yrs == MEDIUM_TERM || yrs == LONG_TERM)	//terms set by LoanConstants
        	term = yrs;
        else term = SHORT_TERM;												//if not previously identified with LoanConstants, then SHORT_TERM
    }
    @Override
    public String toString()							// *** could not get the output to show 9% and 10%. after the BigDecimal format, shows 9% and 1E+1%
    {															//which is 10% but couldn't figure out how to display the 10%
    	double newRate = rate * 100;
    	BigDecimal formatRate = new BigDecimal(newRate);
    	BigDecimal formatRate1 = formatRate.stripTrailingZeros();
    	
        String loanData = "Loan #" + loanNum + "   Name: " + lastName + "  " + amount +
        		"\n for " + term + " year(s) at " + formatRate1 + "% interest";
        return loanData;
        
    }

    //public boolean equals(Loan loan)				//couldn't figure a use for this method .... commented this out instead!
    //{
        // write your code here
    //}

}
