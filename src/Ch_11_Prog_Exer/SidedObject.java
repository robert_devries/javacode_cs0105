package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-10, Prog Exer. An interface named 'SidedObject' has been added that contains a method called displaySides()
 * 
 * Modify the displaySides() method so that it displays the number of sides the object possesses
 * 
 * Create an abstract class called 'GeometricFigure2'. Main application "UseGeometric2"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square2' and 'Triangle2'. Define the constructor and figureArea method of each subclass.
 * Square2 class should output "This figure has four sides" and the Triangle2 should output "This figure has three sides"
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 5 Sept 2021
 */

public interface SidedObject 
{
	public abstract void displaySides();		//need to include the 'abstract' notation to ensure its included in classes which implement this interface
}															//abstract inclusion as noted in Chapter 11, page 47, para 11-10b, 4th bullet
