package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-10, Prog Exer. An interface named 'SidedObject' has been added that contains a method called displaySides()
 * 
 * Modify the displaySides() method so that it displays the number of sides the object possesses
 * 
 * Create an abstract class called 'GeometricFigure2'. Main application "UseGeometric2"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square2' and 'Triangle2'. Define the constructor and figureArea method of each subclass.
 * Square2 class should output "This figure has four sides" and the Triangle2 should output "This figure has three sides"
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */

public abstract class GeometricFigure2 implements SidedObject
{
	private int height;
	private int width;
	private String figure;
	   
    public GeometricFigure2(int h, int w, String f)
    {
        height = h;
        width = w;
        figure = f;
    }
    public int getHeight()
    {
        return height;
    }
    public int getWidth()
    {
        return width;
    }
    public String getFigure()
    {
        return figure;
    }
    public abstract double figureArea(int h, int w);

}
