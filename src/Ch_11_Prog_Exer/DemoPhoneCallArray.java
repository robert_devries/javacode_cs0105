package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-2 Prog Exer.  DemoPhoneCallArray is the application that uses PhoneCall (Parent class) Incoming and OutgoingPhoneCall child classes
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sep 2021
 */
public class DemoPhoneCallArray 
{
	public static void main(String[] args) 
	{
		PhoneCall calls[] = new PhoneCall[8];					//note:  the abstract parent class (PhoneCall) is used to create each array instance
        int i;
        calls[0] = new IncomingPhoneCall("345-094-8372");		//note:  //each child class (IncomingPhoneCall and OutgoingPhoneCall) are used to populate each array object
        calls[1] = new OutgoingPhoneCall("644-564-8572", 4);
        calls[2] = new IncomingPhoneCall("343-194-3372");
        calls[3] = new OutgoingPhoneCall("655-999-6372", 10);
        calls[4] = new IncomingPhoneCall("545-065-2362");
        calls[5] = new OutgoingPhoneCall("655-999-6372", 30);
        calls[6] = new IncomingPhoneCall("125-345-4857");
        calls[7] = new OutgoingPhoneCall("235-955-1371", 3);
        for(i = 0; i < calls.length; ++i)
            calls[i].getInfo();

	}

}
