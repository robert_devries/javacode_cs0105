package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-5, Prog Exer: Create two extended classes named MetalElement and NonMetalElement. 
 * 
 * Each contains a describeElement() method that 
 * 		displays the details of the element and a 
 * 		brief explanation of the properties of the element type. 
 * 
 * For example, metals are good conductors of electricity, while nonmetals are poor conductors.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sep 2021
 */

public class MetalElement extends Element
{
	public MetalElement(String s, int an, double aw)
    {
        super(s, an, aw);			//could also use symbol = s;  atomicNumber = an and so on
    }
    public void describeElement()
    {
        System.out.println("Symbol: " + getSymbol() + ", Atomic Wt: " + getAtomicWeight() + 
        		"\nAtomic Num: " + getAtomicNumber() + ", Metals are good conductors of electricity.");
    }
}
