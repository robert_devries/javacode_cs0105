package Ch_11_Prog_Exer;

/**
 * Class description:  Ch 11-1, Prog Exer, This is an abstract class used with BookArray, Fiction, NonFiction, UseBook application 
 * 
 * Create an abstract class named Book. 
 * Include:
 * 		String field for the book�s title 
 * 		double field for the book�s price. 
 * 
 * Within the class, include a constructor that requires the book title, 
 * add two get methods�
 * 		one that returns the title
 * 		one that returns the price. 
 * 
 * Include an abstract method named setPrice().
 * * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 03 Sep 2021
 **/
public abstract class Book 
{
	private String title;
	protected double price;
			
	public Book(String title1)
	{
		title = title1;
		setPrice();
	}
	
	public String getTitle()
	{
		return title;
	}
	public double getPrice()
	{
		return price;
	}
	
	public abstract void setPrice();		//note the semicolon at the end of method - this is because each child class must define the respective price values

}
