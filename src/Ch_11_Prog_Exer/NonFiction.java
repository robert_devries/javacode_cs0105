package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-1, Prog Exer, This is an child class of abstract Book class used with BookArray, NonFiction, UseBook application 
 * 
 * Create two child classes of Book: Fiction and NonFiction. 
 * Each must include a setPrice() method that sets the price for all Fiction Books to $24.99 and for all NonFiction Books to $37.99. 
 * Write a constructor for each subclass, and include a call to setPrice() within each. 
 * Write an application demonstrating that you can create both a Fiction and a NonFiction Book, and display their fields.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sept 2021
 */
public class NonFiction extends Book
{
	public NonFiction (String titleName)
	{
		super(titleName); 
	}
	 
	@Override				//overrides abstract Book class price method since each child class must define the price
	public void setPrice()
	{
		final double PRICE = 37.99;
		price = PRICE;
	}

}
