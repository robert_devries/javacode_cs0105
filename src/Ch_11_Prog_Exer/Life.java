package Ch_11_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 11-9, Prog Exer. Child class Health and Life uses Insurance abstract class.  Main application is UseInsurance
 * 
 * The Insurance class contains a 
 * 		String representing the type of insurance and a 
 * 		double that holds the monthly price. 
 * 
 * The Insurance class constructor requires a String argument indicating the type of insurance, 
 * but the Life and Health class constructors require no arguments. 
 * The Insurance class contains a get method for each field; it also contains two abstract methods named setCost() and display(). 
 * The Life class setCost() method sets the monthly fee to $36, and the Health class sets the monthly fee to $196.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 5 Sep 2021
 */

public class Life extends Insurance
{
	public final static double costL = 36;
	
	public Life()
    {
        super("Life");
    }
	
	@Override
    public void setCost()
    {
        cost = costL;
    }
	
	@Override
    public void display()
    {
        System.out.println("Life insurance costs $" + cost + " per month");
    }

}
