package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-8, Prog Exer: Create an interface named 'Turner', with a single method named turn(). 
 * 
 * Create a class named 'Leaf' that implements the Turner interface to display Changing colors.. 
 * 
 * Create a class named 'Page' that implements the Turner interface to display Going to the next page.. 
 * 
 * Create a class named 'Pancake' that implements the Turner interface to display Flipping..
 *
 * Added 2 more classes (Milk and Wheel) that use Turner interface with single method named turn()
 * *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */

public class DemoTurners2 
{
	public static void main(String[] args) 
	{
		Leaf aLeaf = new Leaf();
        Page aPage = new Page();
        Pancake aCake = new Pancake();

        // write your code here

        aLeaf.turn();
        aPage.turn();
        aCake.turn();
        
        // write your code here (this will include the Milk and Wheel class that implements Turner interface
        
        Wheel aCar = new Wheel();
        Milk aJug = new Milk();
        
        aCar.turn();
        aJug.turn();

	}

}
