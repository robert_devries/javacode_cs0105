package Ch_11_Prog_Exer;


/**
 * Class description: Ch 11-10, Prog Exer. Create an abstract class called 'GeometricFigure'. Main application "UseGeometric"
 * 		Add height, 
 * 		width, and 
 * 		figure attributes. 
 * 
 * Include an abstract method figureArea to determine the area of the figure. 
 * 
 * Create two subclasses called 'Square' and 'Triangle'. Define the constructor and figureArea method of each subclass.
 *
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date 5 Sep 2021
 */

public class Square extends GeometricFigure
{
	private double area;
    
	public Square(int w, int h, String f)
    {
        super(h, w, f);
        //figureArea(w, h);
    }
	
	@Override
    public double figureArea(int w, int h)
    {
        area = w * h * 1.0;			//added the 1.0 as a double to ensure area is double as opposed to promoting 2 * ints 
        return area;
    }

}
