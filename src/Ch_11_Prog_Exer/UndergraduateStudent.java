package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-7, Prog Exer: Create three Student subclasses named UndergraduateStudent, GraduateStudent, and StudentAtLarge, 
 * 
 * each with a unique setTuition() method. 
 * 
 * Tuition for an UndergraduateStudent is $4,000 per semester, 
 * 
 * tuition for a GraduateStudent is $6,000 per semester, and 
 * 
 * tuition for a StudentAtLarge is $2,000 per semester.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sept 2021
 */

public class UndergraduateStudent extends Student
{
	public static final double UNDERGRAD_TUITION = 4000;
	
    public UndergraduateStudent(String id, String name)
    {
        super(id, name);
    }
    
    @Override				//not a requirement to insert @Override, but is good practice to indicate since it's automatic as referenced by superclass
    public void setTuition()
    {
        tuition = UNDERGRAD_TUITION;
    }
}
