package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-1 Prog Exer: This is the application to run the abstract Book class used with child classes Fiction and NonFiction 
 * 
 * Create two child classes of Book: Fiction and NonFiction. 
 * Each must include a setPrice() method that sets the price for all Fiction Books to $24.99 and for all NonFiction Books to $37.99. 
 * Write a constructor for each subclass, and include a call to setPrice() within each. 
 * Write an application demonstrating that you can create both a Fiction and a NonFiction Book, and display their fields.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class BookArrayDemo 
{
	public static void main(String[] args) 
	{
		Book someBooks[] = new Book[10];		//note:  the abstract parent class (Book) is used to create each instance
	    int x;												//each child class (Fiction and NonFiction) are used to populate each array object
	    someBooks[0] = new Fiction("Scarlet Letter");
	    someBooks[1] = new NonFiction("Introduction to Java");
	    someBooks[2] = new Fiction("Mill on the Floss");
	    someBooks[3] = new NonFiction("The Road Not Taken");
	    someBooks[4] = new Fiction("A Tale of Two Cities");
	    someBooks[5] = new NonFiction("Europe on $5 a Day");
	    someBooks[6] = new Fiction("War and Peace");
	    someBooks[7] = new Fiction("A Simple Plan");
	    someBooks[8] = new Fiction("Disclosure");
	    someBooks[9] = new Fiction("Nancy Drew");
	    for(x = 0; x < someBooks.length; ++x)
	       System.out.println("Book: " + 
	         someBooks[x].getTitle() + " costs $" +
	         someBooks[x].getPrice());

	}

}
