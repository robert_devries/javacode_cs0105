package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-8, Prog Exer: Create an interface named Turner, with a single method named turn(). 
 * 
 * Create a class named Leaf that implements the Turner interface to display Changing colors.. 
 * 
 * Create a class named Page that implements the Turner interface to display Going to the next page.. 
 * 
 * Create a class named Pancake that implements the Turner interface to display Flipping..
 * 
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 05 Sep 2021
 */

public interface Turner 
{
	public abstract void turn();		//must include the 'abstract' keyword in interfaces
}
