package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-5, Prog Exer: Create an abstract class named 'Element' that holds properties of  (child classes are MetalElement and NonMetalElement
 * 		elements, including 
 * 		their symbol, 
 * 		atomic number, and 
 * 		atomic weight. 
 * 
 * Include a constructor that requires values for all three properties, a get method for each value, and an abstract method named describeElement().
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 4 Sep 2021
 */
public abstract class Element 
{
	String symbol;
    int atomicNumber;
    double atomicWeight;
    
    public Element(String s, int an, double aw)
    {
        symbol = s;
        atomicNumber = an;
        atomicWeight = aw;
    }
    public String getSymbol()
    {
        return symbol;
    }
    public int getAtomicNumber()
    {
        return atomicNumber;
    }
    public double getAtomicWeight()
    {
        return atomicWeight;
    }
    public abstract void describeElement();			//Note this is an 'abstract' method and ends with semicolon
}
