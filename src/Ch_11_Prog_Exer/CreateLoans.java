package Ch_11_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 11-13, Prog Exer. Uses main 'CreateLoans' and 'LoanSanchez Construction Loan Co. makes loans of up to $100,000 for construction projects. 
 * There are two categories of Loans�
 * 		those to businesses and 
 * 		those to individual applicants.
 * 
 * Write an application that tracks all new construction loans. 
 * The application also must calculate the total amount owed at the due date (original loan amount + loan fee). 
 * 		(Above part was not needed as part of the code) 
 * The application should include the following classes:
 * 		Loan � A public abstract class that implements the LoanConstants interface. 
 * 			A Loan includes a loan number, customer last name, amount of loan, interest rate, and term. 
 * 			The constructor requires data for each of the fields except interest rate. 
 * 			Do not allow loan amounts greater than $100,000. 
 * 			Force any loan term that is not one of the three defined in the LoanConstants class to a short-term, 1-year loan. 
 * 			Create a toString() method that displays all the loan data.
 * 		LoanConstants�A public interface class. 
 * 			includes constant values for short-term (1 year), medium-term (3 years), and long-term (5 years) loans. 
 * 			It also contains constants for the company name and the maximum loan amount.
 * 		BusinessLoan�A public class that extends Loan. 
 * 			The BusinessLoan constructor sets the interest rate to 1% more than the current prime interest rate.
 * 		PersonalLoan�A public class that extends Loan. 
 * 			The PersonalLoan constructor sets the interest rate to 2% more than the current prime interest rate.
 * 
 * CreateLoans� An application that creates an array of five Loans. 
 * 		Prompt the user for the current prime interest rate. 
 * 		Then, in a loop, prompt the user for a loan type and all relevant information for that loan. 
 * 		Store the created Loan objects in the array. When data entry is complete, display all the loans. 
 * 		For example, the program should accept input similar to the sample program execution below:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class CreateLoans 
{
	public static void main (String[] args)
	{
		final int MAX_INPUTS = 5;
		Scanner kb = new Scanner(System.in);
		
		Loan[] eachLoan = new Loan[MAX_INPUTS];
		String name;
		String loanDetails;
		double prime;
		int loanType, accountNum, loanAmt, term;
		int i, j;
		System.out.println("Welcome to " + LoanConstants.COMPANY + 
				"\nEnter the current prime interest rate as a decimal number, for example, .05");
		String userInput = kb.next();
		prime = Double.parseDouble(userInput);
		
		for (i = 0; i < MAX_INPUTS; i++)
		{
			System.out.println("Is this a (1) Business loan or (2) Personal loan");
			userInput = kb.next();
			loanType = Integer.parseInt(userInput);
			System.out.println("Enter account number");
			userInput = kb.next();
			accountNum = Integer.parseInt(userInput);
			System.out.println("Enter name");
			name = kb.next();
			System.out.println("Enter loan amount");
			userInput = kb.next();
			loanAmt = Integer.parseInt(userInput);
			System.out.println("Enter term");
			userInput = kb.next();
			term = Integer.parseInt(userInput);
						
			if (loanType == 1)
			{
				//System.out.println("So far so good in Business Loan.");
				BusinessLoan businessType = new BusinessLoan(accountNum, name, loanAmt, term, prime);
				eachLoan[i] = businessType;
			}
			if (loanType ==2)
			{
				//System.out.println("So far so good in Personal Loan");
				PersonalLoan personalType = new PersonalLoan(accountNum, name, loanAmt, term, prime);
				eachLoan[i] = personalType;
			}
		}
		
		System.out.println(Loan.COMPANY);
		
		for(j = 0; j < MAX_INPUTS;j++)
		{
			loanDetails = eachLoan[j].toString();	//could have used System.out.println(eachLoan[j].toString()); used for ease of future review
			System.out.println(loanDetails);
		}
	}
}
