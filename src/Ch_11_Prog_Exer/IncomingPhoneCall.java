package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-2 Prog Exer.  Incoming and OutgoingPhoneCall are child classes of PhoneCall
 * 
 * The IncomingPhoneCall constructor passes its phone number parameter to its parent�s constructor and sets the price of the call to 0.02. 
 * The method that displays the phone call information displays the phone number, the rate, and the price of the call (which is the same as the rate).
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class IncomingPhoneCall extends PhoneCall
{
	public final static double RATE = .02;
    
	public IncomingPhoneCall(String telNum)
    {
        super(telNum);
        price = 0.02;				//price of call is same as rate.  since it's incoming, you only pay for the line, no additional charges
    }
    public void getInfo()
    {
        System.out.println("Phone #: " + getPhoneNumber() + ", the rate is: " + RATE + ", at a price of: " + getPrice()); 		
    }
    public String getPhoneNumber()
    {
        return phoneNumber;			//the phoneNumber is taken from Parent class set by line 16 constructor
    }
    public double getPrice()
    {
        return price;
    }
}
