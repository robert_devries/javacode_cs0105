package Ch_11_Prog_Exer;

/**
 * Class description: Ch 11-3, Prog Exer:  This is the "abstract" parent class for "OnlineNewspaperSubscription" and "PhysicalNewspaperSubscription". DemoSubscriptions is application
 * 
 * Create an abstract NewspaperSubscription class with fields for the subscriber name, address, and rate. 
 * Include get and set methods for 
 * 		name field and get methods for the address and subscription rate; the setAddress() method is abstract.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public abstract class NewspaperSubscription 
{
	protected String name;
    protected String address;
    protected double rate;
    
    public String getName()
    {
        return name;
    }
    public void setName(String n)
    {
       name = n;
    }
    public String getAddress()
    {
        return address;
    }
    public double getRate()
    {
        return rate;
    }
    public abstract void setAddress(String s);

}
