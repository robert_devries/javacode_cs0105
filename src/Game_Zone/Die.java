package Game_Zone;

/**
 * Class description:  Game Zone #1 #2, goes with TwoDice main () and FiveDice main ()
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 May 2021
 */
public class Die 
{
	private static final int LOWEST_DIE_VALUE = 1;
	private static final int HIGHEST_DIE_VALUE = 6;
	private int randomValue;
	
	public int Die1 ()
	{
		randomValue = ((int)(Math.random() *100) % HIGHEST_DIE_VALUE + LOWEST_DIE_VALUE);
		
		return randomValue;
	}
	

}
