package Game_Zone;

public class Character 

{
	private String eyeColor;
	private String species;
	private int fangNum;
	
	public Character()
	{
		eyeColor = "Bloodstain";
		species = "Alien";
		fangNum = 0;
	}
	
	public void setEyeColor (String color)
	{
		eyeColor = color;
	}
	
	public void setSpecies (String family)
	{
		species = family;
	}
	
	public void setFangNum (int teeth)
	{
		fangNum = teeth;
	}
	public String getEyeColor ()
	{
		return eyeColor;
	}
	
	public String getSpecies ()
	{
		return species;
	}
	
	public int getFangNum ()
	{
		return fangNum;
	}
}
