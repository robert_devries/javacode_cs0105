package Game_Zone;

/* was used in Chapter 3 with EventDemo. This class is now used in Ch 4 with modifications.
 * 
 */

public class Event 
{
	public final static double pricePerGuest = 35.0;
	public final static int guestLargeEvent = 50;
	private String eventNumAssigned;		//assigns an event number, usually alpha-numeric
	private int guestNums;			//total number of guests attending event
	private double eventPrice;		//total event price
		
	public Event(String eventNum, int guests)	//first overloaded constructor to fill both setEventNumber(), setGuests()
	{
		setEventNumber(eventNum);
		setGuests(guests);
	}
	
	public Event()						//default constructor
	{
		this ("A000", 0);				//this calls 2-parameter constructor, 

		//eventNumAssigned = "NoEvent";  	used with original Event class Ch3
		//guestNums = 1;
		//eventPrice = 35.0;
	}
	
	public void setEventNumber(String num)
	{
		eventNumAssigned = num;
	}
		
	public void setGuests(int guests)
	{
		double totalPrice;
		guestNums = guests;
		totalPrice = guests * pricePerGuest;
		eventPrice = totalPrice;
						
	}
	public String getEventNum ()
	{
		return eventNumAssigned;
	}
	public int getGuestNums ()
	{
		return guestNums;
	}
	public double getEventPrice()
	{
		return eventPrice;
	}

}
