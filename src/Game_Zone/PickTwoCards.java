package Game_Zone;

import java.util.Scanner;

/**
 * Class description:  Lesson 3, Game Zone; playing cards
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 19 May 2021
 */
public class PickTwoCards 
{

	public static void main(String[] args) 
	{
		Card firstCard;
		Card secondCard;
		
		firstCard = fillCard();
		secondCard = fillCard();
		showCard(firstCard);
		showCard(secondCard);
				
	}
	public static Card fillCard()
	{
		Card newCard = new Card();
		String pickSuit;
		final int CARDS_IN_SUIT = 13;
		int myValue;
		Scanner keyboard = new Scanner (System.in);
						
		System.out.println("Pick a card suit (h=(Hearts), s=(Spades, d=(Diamonds), c=(Clubs)>> ");
		pickSuit = keyboard.nextLine();
		newCard.setSuit(pickSuit);
		
		myValue = ((int)(Math.random() * 100) % CARDS_IN_SUIT + 1);
		newCard.setCardValue(myValue);
		
		return newCard;
	}
	public static void showCard(Card eachCard)
	{
		System.out.println("Your playing card is >> " + eachCard.getCardValue() + 
				" of " + eachCard.getSuit());
	}

}
