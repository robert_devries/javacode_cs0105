package Game_Zone;

import java.util.Scanner;

/**
 * Class description: Lesson 4, Case Problem 1 using Event class
 * user input number of guests attending event and computes total cost
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class EventDemo
{
	public static void main(String[] args) 
	{
		//create event obj
		Event reservation1;		//initialized 1st Event object
		Event reservation2 = new Event();	//initialized 2nd Event object
		
		reservation1 = fillEvent();		//populates user data
		
		motto();
		showDetails(reservation1);		//displays user input data
		motto();
		showDetails(reservation2);		//displays default constructor data
					
	}
	public static Event fillEvent()
	{
		Event eachEvent = new Event();
		eachEvent.setEventNumber(eventNumber());
		eachEvent.setGuests(nGuests());
				
		return eachEvent;
	}
	
	public static String eventNumber()
	{
		Event eachEvent = new Event();
		String eventIdentifier;
		Scanner keyboard = new Scanner (System.in);
				
		System.out.println("Enter event number >> ");
		eventIdentifier = keyboard.nextLine();
		
		return eventIdentifier;
		
	}
	public static int nGuests()
	{
		//variables used
		int numGuests;
		Scanner keyboard = new Scanner (System.in);
		
		//acquire user data
		System.out.println("How many guests >> ");
		numGuests = keyboard.nextInt();
		keyboard.nextLine();   //retrieve keyboard buffer "Enter" value following nextInt
		
		return numGuests;
	}
	public static void motto()
	{
		System.out.println("\n*************************************************");
		System.out.println("*                                               *");
		System.out.println("* Carly's makes the food that makes it a party. *");
		System.out.println("*                                               *");
		System.out.println("*************************************************\n");
	}
	public static void showDetails(Event carlys)
	{
		boolean isLargeEvent;
		isLargeEvent = carlys.getGuestNums() >= carlys.guestLargeEvent;
		System.out.println("Event details >> " +
				"\nNumber of guests >> " + carlys.getGuestNums() + 
				"\nPrice per Guest >>  " + carlys.pricePerGuest + 
				"\nTotal price >>      " + carlys.getEventPrice() +
				"\nIs large Event >>   " + isLargeEvent);
	}
	
}
