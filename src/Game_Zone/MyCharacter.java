package Game_Zone;

import java.util.Scanner;

/**
 * Class description: Lesson 3 Game Zone: gaming app using Character.java constructor
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  19 May 2021
 */
public class MyCharacter 
{
	public static void main(String[] args) 
	{
		Character firstCharacter;
		//Character secondCharacter;
		
		firstCharacter = buildCharacter();
		//secondCharacter = buildCharacter();
		
		showCharacter (firstCharacter);
		//showCharacter (secondCharacter);

	}
	
	public static Character buildCharacter()
	{
		Character buildAppearance = new Character();
		String eyeColor;
		String species;
		int teethNum;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter eye color (black), (white), (orange) >> ");
		eyeColor = keyboard.nextLine();
		buildAppearance.setEyeColor(eyeColor);
		
		System.out.println("Enter species (Orc), (Minion), (Human) >> ");
		species = keyboard.nextLine();
		buildAppearance.setSpecies(species);
		
		System.out.println("Enter number of fangs/teeth >> ");
		teethNum = keyboard.nextInt();
		buildAppearance.setFangNum(teethNum);
		
		return buildAppearance;
	}
	public static void showCharacter(Character eachOne)
	{
		System.out.println("Character has following characteristics >> " + 
				"\nEye Color:       " + eachOne.getEyeColor() + 
				"\nSpecies Type:    " + eachOne.getSpecies() +
				"\nNumber of Fangs: " + eachOne.getFangNum());
	}

}
