package Game_Zone;

/**
 * Class description:  GameZone #1, throwing a dice which also uses Die Class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 May 2021
 */
public class TwoDice 
{

	public static void main(String[] args) 
	{
		int firstDieValue;
		int secondDieValue;
		
		Die firstDice = new Die();
		Die secondDice = new Die();
		
		firstDieValue = firstDice.Die1();
		secondDieValue = secondDice.Die1();
		
		System.out.println("First Toss is >> " + firstDieValue);
		System.out.println("Second Toss is >> " + secondDieValue);
		

	}

}
