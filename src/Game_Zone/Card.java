package Game_Zone;

public class Card 
{

	private String suit;
	private int cardValue;
	
	public Card()	//constructor to initialize below default values
	{
		suit = "XXX";
		cardValue = 50;
	}
	
	public void setSuit(String s)
	{
		suit = s;
	}
	
	public void setCardValue(int cardNum)
	{
		cardValue = cardNum;
	}
	
	public String getSuit()
	{
		return suit;
	}
	
	public int getCardValue()
	{
		return cardValue;
	}
}
