package Game_Zone;

import java.util.Scanner;

/**
 * Class description:  GameZone #2, throwing a dice using Die class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 May 2021
 */
public class FiveDice 
{
	
	public static void main(String[] args) 
	{
		String playerOne = "You have";
		String playerTwo = "Computer has";
		int winner;
		Scanner keyboard = new Scanner(System.in);
				
		eachPlayer(playerOne);
		eachPlayer(playerTwo);
		
		System.out.println("Winner is, (Enter 1 - player, 2 - computer, 3 - Tie) >>");
		winner = keyboard.nextInt();
						
	}
	public static void eachPlayer (String player)
	{
		Die firstDice = new Die();
		Die secondDice = new Die();
		Die thirdDice = new Die();
		Die fourthDice = new Die();
		Die fifthDice = new Die();
		
		int firstDieValue = firstDice.Die1();
		int secondDieValue = secondDice.Die1();
		int thirdDieValue = thirdDice.Die1();
		int fourthDieValue = fourthDice.Die1();
		int fifthDieValue = fifthDice.Die1();
		
		System.out.println(player + " tossed the following dice >>");	
		showDie(firstDieValue);
		showDie(secondDieValue);
		showDie(thirdDieValue);
		showDie(fourthDieValue);
		showDie(fifthDieValue);
	}
	public static void showDie(int eachValue)
	{
		System.out.println("Die Toss is >> " + eachValue);
	}

}
