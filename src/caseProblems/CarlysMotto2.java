package caseProblems;

/** From lesson 1, after review, called 
 *  Case Problems #1
 * 
 * Class description: CarlysMotto2
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class CarlysMotto2
{

	public static void main(String[] args) 
	{
		System.out.println("*************************************************");
		System.out.println("*                                               *");
		System.out.println("* Carly's makes the food that makes it a party. *");
		System.out.println("*                                               *");
		System.out.println("*************************************************");
		
	}

}
