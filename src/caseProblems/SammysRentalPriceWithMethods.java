package caseProblems;

import java.util.Scanner;

/** class description: Lesson 2, Case Problem 2 SammysRentalPrice
 * prompts user for the number of minutes he rented a piece of sports equip
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * datge: 09 May 2021
 */

public class SammysRentalPriceWithMethods
{

	public static void main(String[] args) 
	{
		//variables needed for user inputs
		int userRentedMinutes;
		
		userRentedMinutes = minutesRented();
		displayMotto();
		calcDisplayReceipt(userRentedMinutes);
	}
	
	public static int minutesRented()
	{
		int userMinutes;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("How many minutes was the equipment rented >> ");
		userMinutes = keyboard.nextInt();
		
		return userMinutes;
	}
	
	public static void displayMotto()
	{
		//motto output 
		System.out.println("");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs");
		System.out.println("Ss                                  Ss");
		System.out.println("Ss Sammy's makes it fun in the sun. Ss");
		System.out.println("Ss                                  Ss");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs\n");
	}
	
	//gets minutes equipment was rented
	public static void calcDisplayReceipt(int userTimeInMinutes)
	{
		//variables used for rental minutes/pricing
		int hours, minutes, userRentedMinutes;
		final double COST_PER_HOUR = 40;		//dollars per hour
		final double COST_PER_MINUTE = 1;		//dollar per minute
		double totalCost;
		userRentedMinutes = userTimeInMinutes;
		
		hours = userRentedMinutes / 60;
		minutes = userRentedMinutes % 60;
		totalCost = ((hours * COST_PER_HOUR) + (minutes * COST_PER_MINUTE));
	
		System.out.println("You have rented the equipment for a total of >> " + 
				hours + " hours, " + 
				minutes + " minute(s) " + 
				"at a total cost of: " + totalCost);
	}
}
