package caseProblems;

import java.util.Scanner;

/**
 * Class description: lesson 3 caseProblem Student GPA
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 17 May 2021
 */
public class ShowStudent2 
{
	public static void main(String[] args) 
	{
		Student myStudent = new Student();				//Student is declared.Student is data type similar to int, float. myStudent is identifier
		
		System.out.println("Initialized ID field >> " + myStudent.getID() + 
				"\nCredit hours >> " + myStudent.getCreditHours() + "\nPoints earned >> " +
				myStudent.getPoints() + "\nGPA >> " + myStudent.getGPA());
				
	}
	
	

}
