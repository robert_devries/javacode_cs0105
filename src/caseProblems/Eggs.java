package caseProblems;

import java.util.Scanner;
/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #11
 * declare constants which represents provided number of inches, to feet and inches.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Eggs 
{
	public static void main(String[] args) 
	{
		final double dozenCost = 3.25;
		final double singleCost = 0.45;		
		int totalOrder;
		int totalDozens;
		int singleEggs;
		double totalCost; 
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many eggs do you need? >> ");
		totalOrder = userInput.nextInt();
		totalDozens = totalOrder / 12;
		singleEggs = totalOrder % 12;
		totalCost = ((totalDozens * dozenCost) + (singleEggs * singleCost));
		
		System.out.println("You have ordered " + totalOrder + 
				". That's " + totalDozens + " dozen at $3.25 per dozen and " +
				singleEggs + " loose eggs at 45 cents each for a total of $" + totalCost);
		userInput.nextLine();		// purge Keyboard buffer of "Enter" value stored
		
	}

}
