package caseProblems;

/** From lesson 1, after review, called 
 *  Case Problems 
 * 
 * 
 * Class description: SammysMotto
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class SammysMotto2 
{

	public static void main(String[] args) 
	{
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs");
		System.out.println("Ss                                  Ss");
		System.out.println("Ss Sammy's makes it fun in the sun. Ss");
		System.out.println("Ss                                  Ss");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs");

	}

}
