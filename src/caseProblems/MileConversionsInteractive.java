package caseProblems;

import java.util.Scanner;

/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #7
 * declare constants representing num of inches, feet, and yards in mile.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  08 May 2021
 */
public class MileConversionsInteractive 
{


	public static void main(String[] args) 
	{
		final int inches = 63360;
		final int feet = 5280;
		final int yards = 1760;
		int miles;
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Please provide exact number of miles for calculation in standard measurement >> ");
		miles = userInput.nextInt();
		
		System.out.println("The total number of miles provided is " + miles + ".");
		System.out.println("The miles converted to yards is " + 
				(miles * yards) + " which is also " + 
				(miles * feet) + " feet or " +
				(miles * inches) + " inches.");
		userInput.nextLine();		//consume Keyboard buffer "Enter" held in the queue
		
	}

}
