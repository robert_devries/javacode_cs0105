package caseProblems;

public class Student 
{
	public int ID;		//ID student number
	public int creditHours;		// hours per class with credit
	public int pointsEarned;	// not sure
	public int GPA;		//divides points by creditHours
	
	public Student()    //constructor created. Note parentheses/same identifier to class to initialize variable fields (not using default) 
	{
		ID = 9999;
		creditHours = 12;
		pointsEarned = 3;
		GPA = 4;
	}
	
	public void setGPA(int pointsEarned, int creditHours)
	{
		int result;
		result = pointsEarned/creditHours;
		//System.out.println("This is GPA from field calcGPA >> " + result);
		
	}
	public int getGPA ()
	{
		return GPA;
	}
	public void setID(int ID)
	{
		ID = ID;
		
	}
	public int getID()
	{
		
		return ID;
	}
	public void setCreditHours(int creditHours)
	{
		creditHours = creditHours;
		
	}
	public int getCreditHours()
	{
		
		return creditHours;
	}
	public void setPoints(int points)
	{
		
		pointsEarned = points;
	}
	public int getPoints()
	{
		
		return pointsEarned;
	}

}
