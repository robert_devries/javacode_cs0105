package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #12
 * Number of Chili meals per adult/child and displays subtotals for each as well as
 * total cost.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 8 may 2021
 */
public class ChiliToGo 
{


	public static void main(String[] args) 
	{
		//cost per meals
		int adultCost = 7;
		int adultQty;
		int adultTotal;
		int childCost = 4;
		int childQty;
		int childTotal;
		int totalCost;
		Scanner userInput = new Scanner (System.in);
		
		System.out.println("How many adult meals would you like >> ");
		adultQty = userInput.nextInt();
		userInput.nextLine();	//purge keyboard buffer of "Enter" value stored
		System.out.println("How many child's meals would you like >> ");
		childQty = userInput.nextInt();
		userInput.nextLine();	//purge keyboard buffer of "Enter" value stored
		
		adultTotal = adultQty * adultCost;
		childTotal = childQty * childCost;
		System.out.println("Right then, you have ordered a total cost of $" + 
				adultTotal + " for adult meals $" + childTotal + " for child meals, with "
				+ "a sum total of $" + (adultTotal + childTotal));

	}

}
