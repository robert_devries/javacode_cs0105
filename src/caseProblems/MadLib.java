package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 2: Game Zone #1
 * four or five words from user and input into rhyme
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 9 May 2021
 */
public class MadLib 
{
	public static void main(String[] args) 
	{
		//each variable per word
		String firstWord;
		String secondWord;
		String thirdWord;
		String fourthWord;
		Scanner userInput = new Scanner(System.in);
		
		//output instructions
		System.out.println("This game involves you providing four words, and I will insert into a poem. \n" +
				"Ready....Let's begin!\n");
		System.out.println("Please type the first word below that is in outer object: ");
		firstWord = userInput.next();
		
		System.out.println("2nd word should be another word for our planet: ");
		secondWord = userInput.next();
		
		System.out.println("3rd word should be some type of precious gem (like, Ruby, Topaz): ");
		thirdWord = userInput.next();
				
		System.out.println("4th word is a word that means 'SMALL': ");
		fourthWord = userInput.next();
		
		
		System.out.println("Twinkle, twinkle little " + "\'" + firstWord + "\'");
		System.out.println("How I wonder what you are. ");
		System.out.println("Up above the " + "\'" + secondWord + "\'" + " so high");
		System.out.println("Like a " + "\'" + thirdWord + "\'" + " in the sky!");
		System.out.println("Twinkle, twinkle " + "\'" + fourthWord + "\'" + " Star!");
		

	}

}
