package caseProblems;

import java.util.Scanner;

/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #14
 * Takes a dollar value and breaks down into 20s, 10s, 5s, and 1s
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  09 May 2021
 */
public class Dollars 
{
	
	public static void main(String[] args) 
	{
		int dollarQty;
		int totalTwenties;
		int totalTens;
		int totalFives;
		int carryOver;
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Enter a dollar amount as an integer >> ");
		dollarQty = userInput.nextInt();
		userInput.nextLine();	//reset keyboard buffer
		
		totalTwenties = dollarQty / 20;
		carryOver = dollarQty % 20;
		totalTens = carryOver / 10;
		carryOver = carryOver % 10;
		totalFives = carryOver / 5;
		carryOver = carryOver % 5;    //carryOver will be lowest denomination (ones)
		
		System.out.println("\nHere's the breakdown of bills from your input of $" + dollarQty +
				"\n" + totalTwenties + " x 20 = $" + (totalTwenties * 20) + 
				"\n" + totalTens + " x 10 = $" + (totalTens * 10) +
				"\n" + totalFives + " x 5 = $" + (totalFives * 5) +
				"\n" + carryOver + " x 1 = $" + (carryOver * 1));

	}

}
