package caseProblems;


/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #10
 * takes three person's initials and displays each followed by a period
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  08 May 2021
 */
public class Initials 
{


	public static void main(String[] args) 
	{
		char firstInitial = 'R';		//first initial
		char secondInitial = 'L';		//second initial
		char thirdInitial = 'D';		//third initial
		
		// Display initials to output
		System.out.println(firstInitial + "." + secondInitial + "." + thirdInitial + ".");
		
	}

}
