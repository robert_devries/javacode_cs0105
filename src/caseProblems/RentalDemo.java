package caseProblems;

import java.util.Scanner;

/** class description: Lesson 2, Case Problem 2 SammysRentalPrice
 * prompts user for the number of minutes he rented a piece of sports equip
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * datge: 09 May 2021
 */

public class RentalDemo
{

	public static void main(String[] args) 
	{
		//variables needed for user inputs
		int userRentedMinutes;
		String contractNumber;
					
		Rental firstRental;  					//1st instance Rental object created
		Rental secondRental = new Rental();		//2nd instance Rental object created
		firstRental = equipmentRented();		//object populated with user input
		
		displayMotto();
		showFullContractDetails(firstRental);	//displays populated data with user inputs
		
		displayMotto();
		showFullContractDetails(secondRental);		//displays default data from Rental class constructor
		
		
	}
	// object created with all details
	public static Rental equipmentRented ()
	{
		Rental newRental = new Rental();
		newRental.setContractNumber(contract());
		newRental.setHoursAndMinutes(minutesRented());
		
		return newRental;
	}	
	// contract number
	public static String contract ()
	{
		String contractDetails;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter contract number >> ");
		contractDetails = keyboard.nextLine();
		return contractDetails;
	}
	
	public static int minutesRented()
	{
		int userMinutes;
		Scanner keyboard = new Scanner(System.in);
		System.out.println("How many minutes was the equipment rented >> ");
		userMinutes = keyboard.nextInt();
		
		return userMinutes;
	}
		
	public static void displayMotto()
	{
		//motto output 
		System.out.println("");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs");
		System.out.println("Ss                                  Ss");
		System.out.println("Ss Sammy's makes it fun in the sun. Ss");
		System.out.println("Ss                                  Ss");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs\n");
	}
	
	//gets minutes equipment was rented
	public static void showFullContractDetails(Rental sammysRentals)
	{
		//variables used for rental minutes/pricing
			
		System.out.println("The following details apply to this contract >> " + 
				"\nContract Number >> " + sammysRentals.getContractNumber() + 
				"\nTotal hours of >> " + sammysRentals.getRentalHours() +
				" hours and " + sammysRentals.getLeftOverMinutes() + " minutes" +
				"\nAt an hourly rate of >> $" + sammysRentals.hourlyRentalRate +
				"\n  with total price >> $" + sammysRentals.getCost());
	}
}
