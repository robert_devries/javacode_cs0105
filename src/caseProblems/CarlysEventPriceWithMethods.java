package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 3, Case Problem 1a
 * user input number of guests attending event and computes total cost
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CarlysEventPriceWithMethods
{
	public static void main(String[] args) 
	{
		//variables number of guests
		int guests;
		
		//method calls/returns
		guests = nGuests();
		motto();
		partyDetails(guests);
			
	}
	public static int nGuests()
	{
		//variables used
		int numGuests;
		Scanner keyboard = new Scanner (System.in);
		
		//acquire user data
		System.out.println("How many guests require booking-in >> ");
		numGuests = keyboard.nextInt();
		keyboard.nextLine();   //retrieve keyboard buffer "Enter" value following nextInt
		
		return numGuests;
	}
	public static void motto()
	{
		System.out.println("\n*************************************************");
		System.out.println("*                                               *");
		System.out.println("* Carly's makes the food that makes it a party. *");
		System.out.println("*                                               *");
		System.out.println("*************************************************\n");
	}
	public static void partyDetails(int numGuests)
	{
		//variable used
		int guests = numGuests;
		int calcEventPrice;
		boolean isLargeEvent;
		final int LARGE_EVENT = 50;
		
		final int COST_PER_GUEST = 35;
		
		//calculations
		calcEventPrice = COST_PER_GUEST * guests;
		isLargeEvent = (numGuests >= LARGE_EVENT);
		
		//display results
		System.out.println("Total event cost: $" + (calcEventPrice));
		System.out.println("Is this a large event >> " + isLargeEvent);
	}

}
