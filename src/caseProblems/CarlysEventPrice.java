package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 2, Assignment 2
 * user input number of guests attending event and computes total cost
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CarlysEventPrice 
{
	public static void main(String[] args) 
	{
		//variables number of guests
		int numGuests;
		final int LARGE_EVENT = 50;
		final int COST_PER_GUEST = 35;
		boolean isLargeEvent;
		Scanner userInput = new Scanner (System.in);
		
		System.out.println("How many guests require booking-in >> ");
		numGuests = userInput.nextInt();
		userInput.nextLine();   //retrieve keyboard buffer "Enter" value following nextInt
		isLargeEvent = (numGuests >= LARGE_EVENT);
				
		System.out.println("\n*************************************************");
		System.out.println("*                                               *");
		System.out.println("* Carly's makes the food that makes it a party. *");
		System.out.println("*                                               *");
		System.out.println("*************************************************\n");
		System.out.println(" Total Guests: " + numGuests);
		System.out.println(" Price per Guest: " + COST_PER_GUEST);
		System.out.println(" Total Cost: $" + (COST_PER_GUEST*numGuests));
		System.out.println("Is this a large event >> " + isLargeEvent);
		
	}

}
