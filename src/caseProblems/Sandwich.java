package caseProblems;

public class Sandwich 
{
	public String MainIngredient;		//such as tuna.  Each word capitalized as per Programming Lesson 3-11 on MindTap website
	public String BreadType;		//such as wheat or rye
	public double Price;			//include some arbitrary cost
	
	public void setMainIngredient(String ingredient)
	{
		MainIngredient = ingredient;
	}
	public String getMainIngredient()
	{
		return MainIngredient;
	}
	public void setBreadType(String flourType)
	{
		BreadType = flourType;
	}
	public String getBreadType()
	{
		return BreadType;
	}
	public void setPrice(double cost)
	{
		Price = cost;
	}
	public double getPrice()
	{
		return Price;
	}
}
