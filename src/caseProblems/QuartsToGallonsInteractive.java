package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #5
 * Convert quarts to gallons, display how much paint needed.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 08 May 2021
 */
public class QuartsToGallonsInteractive 
{
	public static void main(String[] args) 
	{
		final int QUARTS_TO_GAL = 4;
		int quartsPaintingJob = 0;
		int remainderQuarts;
		int totalQuarts;
		Scanner userInput = new Scanner(System.in);	//userInput declared as variable for input
		
		System.out.println("Please provide the number of quarts needed >> ");
		quartsPaintingJob = userInput.nextInt();
		userInput.nextLine();						//after line 23, this line consumes the "Enter" keystroke
		totalQuarts = quartsPaintingJob / QUARTS_TO_GAL;
		remainderQuarts = quartsPaintingJob % QUARTS_TO_GAL;	//capture remaining quarts
		
		
		System.out.println("This job will require " + totalQuarts + 
				" gallons and an additional " + remainderQuarts + " quarts.");
					
	}

}
