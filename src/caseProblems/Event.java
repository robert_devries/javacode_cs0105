package caseProblems;

public class Event 
{
	public final static double pricePerGuest = 35.0;
	public final static int guestLargeEvent = 50;
	private String eventNumAssigned;
	private int guestNums;
	private double eventPrice;
	
	
	public Event()		//Constructor created for empty objects
	{

		eventNumAssigned = "NoEvent";
		guestNums = 1;
		eventPrice = 35.0;
	}
	
	public void setEventNumber(String num)
	{
		eventNumAssigned = num;
	}
		
	public void setGuests(int guests)
	{
		double totalPrice;
		guestNums = guests;
		totalPrice = guests * pricePerGuest;
		eventPrice = totalPrice;
						
	}
	public String getEventNum ()
	{
		return eventNumAssigned;
	}
	public int getGuestNums ()
	{
		return guestNums;
	}
	public double getEventPrice()
	{
		return eventPrice;
	}

}
