package debugExercises;

import java.util.Scanner;

/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #16
 * Records 3 x political parties and number of votes output as percentage
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 09 May 2021
 */
public class ElectionStatistics 
{
	public static void main(String[] args) 
	{
		// table of variables
		//String namedPoliticalPartiesInput = "";
		String firstPartyName = "";
		String secondPartyName = "";
		String thirdPartyName = "";
		//int numVotes = 0;
		float votesFirstParty = 0;
		float votesSecondParty = 0;
		float votesThirdParty = 0;
		float totalVotes = 0;
		float percentFirstParty = 0;
		float percentSecondParty = 0;
		float percentThirdParty = 0;
		Scanner namedPoliticalPartyInput = new Scanner(System.in);
		Scanner numVotes = new Scanner(System.in);
		
		//get inputs
		System.out.println("Provide the name of 1st Political Party (max 3 will be recorded) >> ");
		firstPartyName = namedPoliticalPartyInput.nextLine();
		
		System.out.println("How many votes for: " + firstPartyName);
		votesFirstParty = numVotes.nextInt();
		numVotes.nextLine();
		
		System.out.println("Name of 2nd Party >> ");
		secondPartyName = namedPoliticalPartyInput.nextLine();
		
		System.out.println("How many votes for: " + secondPartyName);
		votesSecondParty = numVotes.nextInt();
		numVotes.nextLine();
		
		System.out.println("Name of 3rd and last Party >> ");
		thirdPartyName = namedPoliticalPartyInput.nextLine();
		
		System.out.println("How many votes for: " + thirdPartyName);
		votesThirdParty = numVotes.nextInt();
		numVotes.nextLine();
				
		totalVotes = (votesFirstParty + votesSecondParty + votesThirdParty);
		percentFirstParty = ((votesFirstParty/totalVotes)*100);
		percentSecondParty = ((votesSecondParty/totalVotes)*100);
		percentThirdParty = ((votesThirdParty/totalVotes)*100);
				
		System.out.println("Please see below for percentage of votes awarded to each party:\n");
		System.out.println(firstPartyName + ": " + percentFirstParty + "\n");
		System.out.println(secondPartyName + ": " + percentSecondParty + "\n");
		System.out.println(thirdPartyName + ": " + percentThirdParty);

	}

}
