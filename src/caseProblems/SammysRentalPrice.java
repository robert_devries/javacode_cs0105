package caseProblems;

import java.util.Scanner;

/** class description: Lesson 2, Case Problem 2 SammysRentalPrice
 * prompts user for the number of minutes he rented a piece of sports equip
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * datge: 09 May 2021
 */

public class SammysRentalPrice 
{

	public static void main(String[] args) 
	{
		//variables for userinputs
		int hours, minutes, userRentedMinutes;
		final double COST_PER_HOUR = 40;
		final double COST_PER_MINUTE = 1;
		double totalCost;
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many minutes was the piece of equipment >> ");
		userRentedMinutes = userInput.nextInt();
		userInput.nextLine();    //retrieve keyboard buffer "Enter" value
		
		//calculations
		hours = userRentedMinutes / 60;
		minutes = userRentedMinutes % 60;
		totalCost = ((COST_PER_HOUR * hours) + (COST_PER_MINUTE * minutes));
		
		//output to user
		System.out.println("");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs");
		System.out.println("Ss                                  Ss");
		System.out.println("Ss Sammy's makes it fun in the sun. Ss");
		System.out.println("Ss                                  Ss");
		System.out.println("SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSs\n");
		System.out.println("You have rented the equipment for a total of: \n" + 
				hours + " hours, " + minutes + " minute(s) " + "at a total cost of: " + 
				totalCost);

	}

}
