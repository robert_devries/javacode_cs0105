package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #13
 * Number of Chili meals per adult/child and displays subtotals, with profits
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 8 may 2021
 */
public class ChiliToGoProfit 
{


	public static void main(String[] args) 
	{
		// cost per meals
		// adultNetCost = 4.35;
		// adultGrossCost = 7;
		double adultProfit = 2.65;
		double adultOverallProfit;
		int adultQty;
				
		// childNetCost = 3.10;
		// childGrossCost = 4;
		double childProfit = 0.90;
		double childOverallProfit;
		int childQty;
		Scanner userInput = new Scanner (System.in);
		
		System.out.println("How many adult meals would you like >> ");
		adultQty = userInput.nextInt();
		userInput.nextLine();	//purge keyboard buffer of "Enter" value stored
		System.out.println("How many child's meals would you like >> ");
		childQty = userInput.nextInt();
		userInput.nextLine();	//purge keyboard buffer of "Enter" value stored
		
		adultOverallProfit = (adultQty * adultProfit);
		childOverallProfit = (childQty * childProfit);
		System.out.println("Total profit for adult meal is $" + adultOverallProfit +
				" \nTotal profit for children's meals is $" + childOverallProfit + 
				" \nThe grand total profit is $" + (adultOverallProfit + childOverallProfit));

	}

}
