package caseProblems;


/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #6
 * declare constants representing num of inches, feet, and yards in mile.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  08 May 2021
 */
public class MileConversions 
{


	public static void main(String[] args) 
	{
		final int inches = 63360;
		final int feet = 5280;
		final int yards = 1760;
		final int miles = 3;
		final int totalMiles;
		
		System.out.println("The total number of miles provided is " + miles + ".");
		System.out.println("The miles converted to yards is " + 
				(miles * yards) + " which is also " + 
				(miles * feet) + " feet or " +
				(miles * inches) + " inches.");
		

	}

}
