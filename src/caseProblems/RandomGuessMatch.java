package caseProblems;

import javax.swing.JOptionPane;
import java.util.Scanner;

/**
 * Class description: CaseProblem: Lesson 2, RandomGuessMatch
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 May 2021
 */
public class RandomGuessMatch 
{

	public static void main(String[] args) 
	{
		int random;
		int userGuess;
		int difference;

		Scanner userInput = new Scanner(System.in);
		
		JOptionPane.showMessageDialog(null, "Provide a number between 1 - 5?");
		userGuess = userInput.nextInt();
			
		random = (1 + (int)(Math.random()*5));    // coding for random = MIN + (int)(Math.random() * MAX);
		difference = userGuess - random;
		JOptionPane.showMessageDialog(null, "Difference between random number and your guess is >> " +
				(difference));
		boolean isMatch = (userGuess == random);
		
		JOptionPane.showMessageDialog(null, "The random number is : " + random + "  " + isMatch);

	}

}
