package caseProblems;

import java.util.Scanner;

/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #9
 * declare constants which represents provided number of inches, to feet and inches.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  08 May 2021
 */
public class InchesToFeetInteractive 
{


	public static void main(String[] args) 
	{
		final int feet = 12;
		int convertTotalInches;
		Scanner userInput = new Scanner(System.in);
		
		// Get user input
		System.out.println("Please provide an integer value in total inches for conversion >> ");
		convertTotalInches = userInput.nextInt();
		System.out.println("The integer provided " + convertTotalInches +
				"Converted to feet and inches is " + (convertTotalInches / feet) + 
				" feet, " + (convertTotalInches % feet) + " inches.");
		userInput.nextInt(); 	//consume Keyboard buffer or errors could occur
	}

}
