package caseProblems;

import java.util.Scanner;

/**
 * Class description:  Lesson
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class JobPricing 
{

	public static void main(String[] args) 
	{
		int materialCost, hoursOfWork, travelTime, jobCost;
		String jobName;
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Name of job >> ");
		jobName = keyboard.nextLine();
		System.out.println("Cost of materials $ >> ");
		materialCost = keyboard.nextInt();
		System.out.println("Hours of work required >> ");
		hoursOfWork = keyboard.nextInt();
		System.out.println("Hours travel time >> ");
		travelTime = keyboard.nextInt();
		keyboard.nextLine();
		
		jobCost = jobNameEstimate(materialCost, hoursOfWork, travelTime);
		
		System.out.println(jobName + " total cost is $" + jobCost);

	}

	public static int jobNameEstimate(int materials, int workHours, int travel)
	{
		int estimate;
		final int standardRate = 35;
		final int travelTime = 12;
				
		estimate = (materials + (workHours * standardRate) + (travel * travelTime));
		
		return estimate;
		
		
		
		
	}
}
