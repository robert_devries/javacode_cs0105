package caseProblems;

import java.util.Scanner;

/**
 * Class description: Lesson 3, caseProblem which uses class data fields from Sandwich.java
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  17 May 2021
 */
public class TestSandwich 
{

	public static void main(String[] args) 
	{
		Sandwich mySandwich;
		mySandwich = getSandwichType();
		displaySandwich(mySandwich);
		
	}
	public static Sandwich getSandwichType()	//return type is Sandwich 
	{
		Sandwich tempSandwich = new Sandwich();		// need ingredients, bread type and price
		String mainMeat;
		String flourType;
		double cost;
		Scanner input = new Scanner (System.in);
		System.out.println("Main ingredient >> ");
		mainMeat = input.nextLine();
		tempSandwich.setMainIngredient(mainMeat);
		System.out.println("Type of bread >> ");
		flourType = input.nextLine();
		tempSandwich.setBreadType(flourType);
		System.out.println("Cost of sandwich >> ");
		cost = input.nextDouble();
		tempSandwich.setPrice(cost);
		return tempSandwich;
	}
	public static void displaySandwich(Sandwich aType)
	{
		System.out.println("\nSandwich created is " + aType.getMainIngredient() +
				" with " + aType.getBreadType() + " costing $" + aType.getPrice());	//not finished yet
	}

}
