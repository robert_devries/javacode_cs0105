package caseProblems;

/**
 * Class description: Lesson 2 Reviewing the Basics Quiz #4
 * Convert quarts to gallons, display how much paint needed.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 08 May 2021
 */
public class QuartsToGallons 
{
	public static void main(String[] args) 
	{
		final int QUARTS_TO_GAL = 4;
		int quartsPaintingJob = 31;
		int remainderQuarts;
		int totalQuarts = quartsPaintingJob / QUARTS_TO_GAL;
		remainderQuarts = quartsPaintingJob % QUARTS_TO_GAL;		//capture remaining quarts
		
		System.out.println("This job will require " + totalQuarts + 
				" gallons and an additional " + remainderQuarts + " quarts.");
		
	}

}
