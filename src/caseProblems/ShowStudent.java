package caseProblems;

import java.util.Scanner;

/**
 * Class description: lesson 3 caseProblem Student GPA
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 17 May 2021
 */
public class ShowStudent 
{
	public static void main(String[] args) 
	{
		Student myStudent;				//Student is declared.Student is data type similar to int, float. myStudent is identifier
		myStudent = getStudentData();    //value returned from getStudentData() method is assigned to myStudent object
		displayStudent(myStudent);		//myEmployee object is passed to displayEmployee() method

	}
	public static Student getStudentData()
	{
		Student eachStudent = new Student();
		int ID, creditHours, pointsEarned, GPA, result;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter ID >> ");
		ID = input.nextInt();
		eachStudent.setID(ID);
		input.nextLine();
		
		System.out.println("Enter credit hours >> ");
		creditHours = input.nextInt();
		eachStudent.setCreditHours(creditHours);
		input.nextLine();
		
		System.out.println("Enter points earned >> ");
		pointsEarned = input.nextInt();
		eachStudent.setPoints(pointsEarned);
		result = eachStudent.calcGPA(pointsEarned, creditHours);
		//System.out.println("GPA is >> " + result);
		
		
		return eachStudent;
	}
	public static void displayStudent(Student studentData)
	{
		
	}

}
