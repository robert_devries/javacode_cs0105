package caseProblems;



/**
 * Class description: Lesson 4-5 Programming Exercises (info took from the web)
 * https://github.com/PhilDatoon/CertIV_IntroOOP/blob/master/PortfolioActivity05/BloodData.java
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 26 May 2021
 */
public class BloodData_fm_Web 

//public class BloodData 
{
	private String bloodType;
	private String rhFactor;
	
	BloodData_fm_Web() {
		bloodType = "O";
		rhFactor = "+";
	}
	
	BloodData_fm_Web (String bloodType, String rhFactor) {
		this.bloodType = bloodType;
		this.rhFactor = rhFactor;
	}
	
	public String getBloodType() {
		return bloodType;
	}
	
	public void setBloodType(String type) {
		this.bloodType = type;
	}
	
	public String getRhFactor() {
		return this.rhFactor;
	}
	
	public void setRhFactor(String factor) {
		this.rhFactor = factor;
	}
}