package caseProblems;


/**
 * Class description:Lesson 2: Reviewing the Basics Quiz #8
 * declare constants which represents number of inches to feet and inches.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  08 May 2021
 */
public class InchesToFeet 
{


	public static void main(String[] args) 
	{
		final int feet = 12;
		int inches = 0;
		int convertTotalInches = 86;
		
		
		System.out.println("The total number of given inches " + 
				convertTotalInches + ", converted to feet and inches is " + (convertTotalInches / feet) + 
				" feet, " + (convertTotalInches % feet) + " inches.");
		
	}

}
