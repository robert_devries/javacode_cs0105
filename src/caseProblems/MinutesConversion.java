package caseProblems;

import java.util.Scanner;


/**
 * Class description: Lesson 2: Reviewing the Basics Quiz #15
 * Converts total minutes into hours and days
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 9 May 2021
 */
public class MinutesConversion 
{
	public static void main(String[] args) 
	{
		
		//placeholders for calcs
		final int minutesInDay = 1440;
		int days = 0, hours, minutes, remainMinutes, userData;
		Scanner userInput = new Scanner(System.in);
		
		//get input in total minutes
		System.out.print("Please enter total minutes for calculation >> ");
		userData = userInput.nextInt(); 
		userInput.nextLine();    //capture the Keyboard Buffer "Enter" stroke
		
		System.out.print("Here's what is entered: " + userData);
		
		//calculations
		days = userData / minutesInDay;
		remainMinutes = userData % minutesInDay;
		hours = remainMinutes / 60;
		minutes = remainMinutes % 60;
			
		//output results
		System.out.print("\nNow the breakdown in days/minutes/hours:" + 
				"\n" + days + " day(s)\n" +
				hours + " hour(s)\n" +
				minutes + " minute(s)");

	}

}
