package Ch9_You_Do_It;

/**
 * Class description: Ch 9-6 enumerations, this is needed by Model class, Car class and CarDemo application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

	enum Color {BLACK, BLUE, GREEN, RED, WHITE, YELLOW};


