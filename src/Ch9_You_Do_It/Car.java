package Ch9_You_Do_It;

/**
 * Class description: Ch 9-6 enumerations, this is needed by Color class, Model class and CarDemo application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Car {
	private int year;
	private Model model;
	private Color color;
	
	public Car(int yr, Model m, Color c)
	{
		year = yr;
		model = m;
		color = c;
	}
	public void display()
	{
		System.out.println("Car is a " + year + " " + color + " " + model);
	}

}
