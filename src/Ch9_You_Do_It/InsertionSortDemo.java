package Ch9_You_Do_It;

import java.awt.DisplayMode;
import java.beans.beancontext.BeanContext;
import java.util.Scanner;

/**
 * Class description: Ch 9, You Do It, BubbleSortDemo
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 01 Aug 2021
 */
public class InsertionSortDemo 
{
	public static void main(String[] args) 
	{
		int[] someNums = new int[5];
		
		Scanner keyboard = new Scanner(System.in);
		int a, b, temp;
		
		for(a=0; a < someNums.length; ++a)
		{
			System.out.print("Enter number " + (a + 1) + " >> ");		//prompts user for a value for each array item (max: 5)
			someNums[a]= keyboard.nextInt();
		}
		
		display(someNums, 0);							//iteration 0 (in this case, it is the original input of nums
		
		a = 1;								//sets pointer to element 1 to start
		while(a < someNums.length)				//set to 5 as noted by line 17
		{
			temp = someNums[a];				//temp is now 2nd element [1]
			b = a - 1;							// b (loop control variable) is set to 0 (zero)
			while(b >= 0 && someNums[b] > temp)		//value of element 0 -> if greater than temp (2nd element [1]), then while loop executes
			{
				someNums[b+1] = someNums[b];		//value at 1st element [0] then pushed to 2nd element (which was previously copied by line 33)
				--b;									//b is reset to -1 and while loop exits
			}
			someNums[b +1] = temp;					//value copied is set to 2nd element [1]
			display(someNums, a);						//results displayed after 1st iteration
			++a;											//pointer is set now to 3rd element [2]
		}
		
	}
	public static void display(int[] someNums, int a)
	{
		System.out.print("Iteration " + a + ": ");
		for(int x = 0; x < someNums.length; ++x)
			System.out.print(someNums[x] + " ");
		System.out.println();
	}

}
