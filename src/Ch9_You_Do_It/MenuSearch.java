package Ch9_You_Do_It;

import java.util.*;
import javax.swing.*;
/**
 * Class description: Ch 9-4, Using the Arrays Class.  This app demonstrates the use of the Arrays Class methods
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  03 August 2021
 */
public class MenuSearch 
{
	public static void main(String[] args) 
	{
		String[] menuChoices = new String[10];
		String entry = "", menuString = "";
		int x = 0;
		int numEntered;
		int highestSub = menuChoices.length - 1;
		
		Arrays.fill(menuChoices, "zzzzzzz");		// fills array with "zzzzzz" (lowercase used cause they have higher value than any other letter.
													// **Therefore, when the user's entries are sorted, the zzzzzzz entries will be at the bottom of the list
		menuChoices[x] = JOptionPane.showInputDialog(null, "Enter an item for today's menu, or zzz to quit");
		while(!menuChoices[x].equals("zzz") && x < highestSub)
		{
			menuString = menuString + menuChoices[x] + "\n";
			++x;
			if(x < highestSub)
				menuChoices[x] = JOptionPane.showInputDialog(null, "Enter an item for today's menu, or zzz to quit");
		}
		numEntered = x;
				
		entry = JOptionPane.showInputDialog(null, "Today's menu is:\n" + menuString + "Please make a selection:");
		
		// sort the array from index position 0 to numEntered so that it is in ascending order prior to using the binarySearch( ) method.
		// if you do not sort the entire array, the result of the binarySearch( ) method is unpredictable
		Arrays.sort(menuChoices, 0, numEntered);
		
		// use the Arrays.binarySearch( ) method to search for the requested entry in the previously sorted array.
		//if the method returns a non-negative value that is less than the numEntered value, display the msg Excellent choice, otherwise display an error msg
		x = Arrays.binarySearch(menuChoices,  entry);
		if(x >= 0 && x < numEntered)
			JOptionPane.showMessageDialog(null,  "Excellent choice");
		else 
			JOptionPane.showMessageDialog(null, "Sorry - that item is not on tonight's menu");

	}

}
