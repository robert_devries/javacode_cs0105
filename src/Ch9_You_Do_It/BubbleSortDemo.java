package Ch9_You_Do_It;

import java.awt.DisplayMode;
import java.util.Scanner;

/**
 * Class description: Ch 9, You Do It, BubbleSortDemo
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 01 Aug 2021
 */
public class BubbleSortDemo 
{
	public static void main(String[] args) 
	{
		int[] someNums = new int[10];
		int comparisonsToMake = someNums.length - 1;		//use this line to reduce additional calculation processes
		Scanner keyboard = new Scanner(System.in);
		int a, b, temp;
		
		for(a=0; a < someNums.length; ++a)
		{
			System.out.print("Enter number " + (a + 1) + " >> ");		//prompts user for a value for each array item (max: 5)
			someNums[a]= keyboard.nextInt();
		}
		
		display(someNums, 0);							//iteration 0 (in this case, it is the original input of nums
		
		for(a=0; a < someNums.length - 1; ++a)						//outer loops controls the number of passes through the list
		{
			for(b = 0; b < comparisonsToMake; ++b) {				//inner loop controls the comparisons on each pass through the list
				if (someNums[b] > someNums[b + 1])					//after each pass, the next largest number is sunk to the bottom (hence don't need
				{													//   to calculate sort through entire array, but 1 less comparison
					temp = someNums[b];
					someNums[b] = someNums[b+1];
					someNums[b+1] = temp;
				}
			}
			display(someNums, (a + 1));		//after each iteration from 0 - 4, output is displayed
			--comparisonsToMake;
		}
		
	}
	public static void display(int[] someNums, int a)
	{
		System.out.print("Iteration " + a + ": ");
		for(int x = 0; x < someNums.length; ++x)
			System.out.print(someNums[x] + " ");
		System.out.println();
	}

}
