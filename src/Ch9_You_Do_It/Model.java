package Ch9_You_Do_It;

/**
 * Class description: Ch 9-6 enumerations, this is needed by Color class, Car class and CarDemo application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

	enum Model {SEDAN, CONVERTIBLE, MINIVAN};

