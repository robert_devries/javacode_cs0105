package Ch9_You_Do_It;

/**
 * Class description: Ch 9-6, enumerations, this is needed by Model class, Car class and Color class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 3 Aug 2021
 */
public class CarDemo 
{
	public static void main(String[] args) 
	{
		Car firstCar = new Car(2014, Model.MINIVAN, Color.BLUE);
		Car secondCar = new Car(2017, Model.CONVERTIBLE, Color.RED);
		firstCar.display();
		secondCar.display();

	}

}
