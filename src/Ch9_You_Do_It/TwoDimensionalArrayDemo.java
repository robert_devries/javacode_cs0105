package Ch9_You_Do_It;

import java.util.Scanner;

/**
 * Class description: Ch 9-3d, Two-Dimensional Array
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 03 Aug 2021
 */
public class TwoDimensionalArrayDemo 
{
	public static void main(String[] args) 
	{
		int[][] count = new int [3][3];
		Scanner keyboard = new Scanner(System.in);
		int row, column;
		final int QUIT = 99;
		
		System.out.print("Enter a row or " + QUIT + " to quit >> ");
		row = keyboard.nextInt();
		
		while (row != QUIT)
		{
			System.out.print("Enter a column >> ");
			column = keyboard.nextInt();
			if (row < count.length && column < count[row].length)
			{
				count[row][column]++;
				for(int r = 0; r < count.length; ++r)
				{
					for (int c = 0; c < count[r].length; ++c)
						System.out.print(count[r][c] + " ");
					System.out.println();
				}
			}
			else
				System.out.println("Invalid position selected");
			System.out.print("Enter a row or " + QUIT + " to quit >> ");
			row = keyboard.nextInt();
		}

	}

}
