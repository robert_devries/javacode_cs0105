package Ch_7_Prog_Exer;
import java.util.*;
/**
 * Class description: Ch 7-5, Count Words
 * Write an application that counts the words in a String entered by a user.
 * Words are separated by any combination of spaces, periods, commas,
 * semicolons, question marks, exclamation points, or dashes. Figure 7-17
 * shows a typical execution.
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 30 Jun 2021
 */
public class CountWords 
{
	public static void main(String[] args) 
	{
		String userInput;
		int stringLength = 0;
		int countLetters = 0;
		int wordCount = 0;
		int resetCountLetters = 0;
		final int START = 0;
				
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a string >> ");
		userInput = keyboard.nextLine();		//get user's sentence
		
		stringLength = userInput.length();		//determine length of string
		
		for (int i = START; i < stringLength; ++i)			//check each character of sentence to determine length of word separated by (comma, colon, semicolon, exclamation, period)
		{	
			char ch = userInput.charAt(i);
			if (Character.isLetter(ch))
			{
				countLetters++;
				
				if (i == stringLength -1)
				{
					wordCount++;
				}
			}
			
			else 
			{
				if (countLetters > resetCountLetters)
				{
					wordCount++;
					countLetters = resetCountLetters;
				}
			}
		}
		
		System.out.println("There are " + wordCount + " in the string");

	}

}
