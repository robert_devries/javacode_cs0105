package Ch_7_Prog_Exer;

import java.util.*;

/* Class description: Ch 7, Programming Exer 7-9 Telephone Number formatted to (000) 000-0000
 * Write a program that inserts parentheses, a space, and a dash into a string of
 * 10 user-entered numbers to format it as a phone number. For example,
 * 5153458912 becomes (515) 345-8912. If the user does not enter exactly 10
 * digits, display an error message. Continue to accept user input until the user
 * enters 999
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 10 Jul 2021
 *  
 */

public class PhoneNumberFormat 
{

	public static void main(String[] args) 
	{
		boolean Sentinel = true;
		char leftParentheses = '(', 
				rightParentheses = ')', 
				spacing = ' ', 
				dash = '-';
		String userQuits = "999";
		String errorMsg = "Insufficient numbers";
		String userMsg = "Enter 10 digits for number conversion or 999 to quit >> ";
		String userInputStr = "";
		int userNumLength;
		int minLength = 10;
		Scanner keyboard = new Scanner(System.in);
		
		while (Sentinel)
		{
			System.out.println(userMsg);
			userInputStr = keyboard.nextLine();
			userNumLength = userInputStr.length();
			
			if (userInputStr.equalsIgnoreCase(userQuits))
			{
				Sentinel = false;
				break;
			}
			
			while (userNumLength < minLength)
			{
				System.out.println(errorMsg);
				System.out.println(userMsg);
				userInputStr = keyboard.nextLine();
				userNumLength = userInputStr.length();				//length() is a method and returns the length of the string
				
				if (userInputStr.equalsIgnoreCase(userQuits))		//don't use == as this compares addresses of variable assignments
				{
					Sentinel = false;
					break;
				}
			}
			if (userInputStr.equalsIgnoreCase(userQuits))
			{
				Sentinel = false;
				break;
			}
			else 
			{
				String reconstructedNum = (leftParentheses + userInputStr.substring(0, 3) + rightParentheses +
						spacing + userInputStr.substring(3,6) + dash + userInputStr.substring(6));
				System.out.println("Formatted telephone number " + reconstructedNum);
				
			}
		}
		System.out.println("Thanks for watching!");
		
	}

}
