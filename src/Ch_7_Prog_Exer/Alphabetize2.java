package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Lesson 7-6a, Programming Exercise
 * Write an application that accepts three Strings from the user and,
 * without regard to case, appropriately displays a message that indicates
 * whether the Strings were entered in alphabetical order. 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 30 June 2021
 */
public class Alphabetize2 
{
	public static void main(String[] args) 
	{
		String origFirstStr, origSecondStr, origThirdStr;
		String lowercaseFirstStr, lowercaseSecondStr, lowercaseThirdStr;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter first string >> ");
		origFirstStr = keyboard.nextLine();
		System.out.println("Second string >> ");
		origSecondStr = keyboard.nextLine();
		System.out.println("Third string >> ");
		origThirdStr = keyboard.nextLine();
		
		//case sensitivity is not a factor - force strings to lower case
		lowercaseFirstStr = origFirstStr.toLowerCase();
		lowercaseSecondStr = origSecondStr.toLowerCase();
		lowercaseThirdStr = origThirdStr.toLowerCase();
		
		if ((lowercaseFirstStr.compareTo(lowercaseSecondStr)< 0) && (lowercaseSecondStr.compareTo(lowercaseThirdStr) < 0))   //check to confirm strings entered in alphabetical order
		{	
			System.out.println(origFirstStr + "\n" + origSecondStr + "\n" + origThirdStr);
		}
		else if ((lowercaseFirstStr.compareTo(lowercaseThirdStr) < 0)&& (lowercaseThirdStr.compareTo(lowercaseSecondStr) < 0))
		{
			System.out.println(origFirstStr + "\n" + origThirdStr + "\n" + origSecondStr);
		}
		else if ((lowercaseSecondStr.compareTo(lowercaseThirdStr) < 0)&& (lowercaseThirdStr.compareTo(lowercaseFirstStr) < 0))
		{
			System.out.println(origSecondStr + " " + origThirdStr + " " + origFirstStr);			
		}
		else if ((lowercaseSecondStr.compareTo(lowercaseFirstStr) < 0)&& (lowercaseFirstStr.compareTo(lowercaseThirdStr)< 0))
		{
			System.out.println(origSecondStr + " " + origFirstStr + " " + origThirdStr);
		}
		else if ((lowercaseThirdStr.compareTo(lowercaseFirstStr)< 0) && (lowercaseFirstStr.compareTo(lowercaseSecondStr) < 0))
		{
			System.out.println(origThirdStr + " " + origFirstStr + " " + origSecondStr);
		}
		else if ((lowercaseThirdStr.compareTo(lowercaseSecondStr) < 0) && (lowercaseSecondStr.compareTo(lowercaseFirstStr) < 0))
		{
			System.out.println(origThirdStr + " " + origSecondStr + " " + origFirstStr);				
		}
	}
	
   
}
