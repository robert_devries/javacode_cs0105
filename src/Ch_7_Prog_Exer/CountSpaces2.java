package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 7-3B, Counts the total number of spaces contained in a quote entered by a user
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CountSpaces2 
{
	public static void main(String[] args) 
	{
		String inspirationalQuote = "";
		int results = 0;
		System.out.println("Enter a quote >> ");
		Scanner keyboard = new Scanner(System.in);
		inspirationalQuote = keyboard.nextLine();
		
		results = calculateSpaces(inspirationalQuote);
		System.out.println("Total number of spaces is >> " + results);

	}
	public static int calculateSpaces(String inString)
	{
		int len = inString.length();
		int count = 0;
		
		for (int i = 0; i < len; i++)			//interesting: charAt(i) must start at beginning of string at posn 0
		{
			if (inString.charAt(i) == ' ')
				count++;
		}
		return count;
		

	}

}
