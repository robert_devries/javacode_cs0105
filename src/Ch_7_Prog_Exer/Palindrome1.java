package Ch_7_Prog_Exer;

import java.util.*;;

/**      This Palindrome (submitted and 100% passed) is more refined that the original Palindrome created. 
 * Class description: Prog Exer 7-10.
 * Write an application that determines whether a phrase entered by the user is a palindrome. 
 * A palindrome is a phrase that reads the same backward and forward without regarding capitalization 
 * or punctuation. For example, �Dot saw I was Tod�, �Was it a car or a cat I saw�, and �Madam Im Adam� are palindromes. 
 * Display the appropriate feedback: You entered a palindrome or You did not enter a palindrome.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 12 Jul 2021
 */
public class Palindrome1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		String userInput;
		StringBuilder reverseUserInput = new StringBuilder();
		StringBuilder userInputWithoutSpace =  new StringBuilder();
		String msgPalidrome = "You entered a palindrome";
		String msgNotPalidrome = "You did not enter a palindrome";
		String msgUserInput = "Enter a string";
		//String userInputConvertToString;
		//String reverseUserInputConvertToString;
		int lengthStr = 0, end = 0, start = 0;
		char ch;
		
		System.out.println(msgUserInput);
		Scanner keyboard = new Scanner(System.in);
		userInput = keyboard.nextLine();
		lengthStr = userInput.length(); 
				
		for (int i = start; i < lengthStr; i++)		//remove all spaces from user input
		{
			ch = userInput.charAt(i);
			ch = Character.toLowerCase(ch);
			if (Character.isLetter(ch))
			{
				userInputWithoutSpace = userInputWithoutSpace.append(ch);
			}
		}
		
		for (int i = lengthStr-1; i >= end; i--)	//reverse word from user input. start at end to beginning of word
		{
			ch = userInput.charAt(i);
			ch = Character.toLowerCase(ch);
			if (Character.isLetter(ch))
			{
				reverseUserInput = reverseUserInput.append(ch);
			}
		}
		
		//userInputConvertToString = userInputWithoutSpace.toString();		//converts StringBuilder word to String
		//reverseUserInputConvertToString = reverseUserInput.toString();	//converts StringBuilder word to String
				
		if (userInputWithoutSpace.toString().equals(reverseUserInput.toString()))
		{
			System.out.println(msgPalidrome);
		}
		else
		{
			System.out.println(msgNotPalidrome);
		}
	}
}
