package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Chap 7-3A, Programming Exercises
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CountSpaces 
{
	public static void main(String[] args) 
	{
		String inspirationalQuote = "Many hands make light work!";
		int results = 0;
		results = calculateSpaces(inspirationalQuote);
		System.out.println("Total number of spaces is >> " + results);

	}
	public static int calculateSpaces(String inString)
	{
		int len = inString.length();
		int count = 0;
		
		for (int i = 0; i < len; i++)			//interesting: charAt(i) must start at beginning of string at posn 0
		{
			if (inString.charAt(i) == ' ')
				count++;
		}
		return count;
	}

}
