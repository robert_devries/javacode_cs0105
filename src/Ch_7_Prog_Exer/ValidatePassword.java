package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 7-4, Programming Exercise
 * Write an application that prompts the user for a password that contains at
 *least two uppercase letters, at least three lowercase letters, and at least one
 *digit. Continuously reprompt the user until a valid password is entered.
 *Display a message indicating whether the password is valid; if not, display the
 *reason the password is not valid.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 Jun 2021
 */
public class ValidatePassword 
{
	public static void main(String[] args) 
	{
		String passWord = " ";
		boolean passwordNotValid = false, validPW = false;
		
		do
		{
			Scanner keyboard = new Scanner(System.in);
			System.out.println("Enter password >> ");
			passWord = keyboard.nextLine();
			if (validPW = isValid(passWord))
			{				
				passwordNotValid = false;
			}
			else
			{
				passwordNotValid = true;
			}
		}while(passwordNotValid);
		
		System.out.println("Valid password");
	}
	
	public static boolean isValid(String pWord )
	{
		boolean passwordStatus = false;
		final int MIN_LOWER = 3, MIN_UPPER = 2, MIN_DIGIT = 1;
		String fullMessage = "Password did not have enough of the following:";
		String threeLowerCase = "lowercase letters";
		String twoUpperCase = "uppercase letters";
		String oneDigit = "digits";
		final int START = 0;
		int countLower = 0, countUpper = 0, countDigit = 0;
		int pWordLength = pWord.length();
		
		for (int i = START; i < pWordLength; i++)
		{
			if (Character.isLowerCase(pWord.charAt(i)))
				countLower++;
			if (Character.isUpperCase(pWord.charAt(i)))
				countUpper++;
			if (Character.isDigit(pWord.charAt(i)))
				countDigit++;	
		}
		if ((countLower >= MIN_LOWER) && (countUpper >= MIN_UPPER) && (countDigit >= MIN_DIGIT))
		{
			passwordStatus = true;
		}
			
		else
			if (countLower < MIN_LOWER || countUpper < MIN_UPPER || countDigit < MIN_DIGIT)
			{
				System.out.println(fullMessage);
				if (countLower < MIN_LOWER)
					System.out.println(threeLowerCase);
				if (countUpper < MIN_UPPER)
					System.out.println(twoUpperCase);
				if (countDigit < MIN_DIGIT)
					System.out.println(oneDigit);
				passwordStatus = false;
			}
		return passwordStatus;
	}
}
 