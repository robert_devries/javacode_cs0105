package Ch_7_Prog_Exer;

import java.io.CharConversionException;
import java.util.*;

/**
 * Class description: Ch 7, Programming Exercise 7-8. PigLatin
 * Write an application that accepts a word from a user and converts it to Pig Latin. 
 * If a word starts with a consonant, the Pig Latin version removes all consonants 
 * from the beginning of the word and places them at the end, followed by ay (Step 1).
 * 
 * For example, cricket becomes icketcray (Step 1). If a word starts with a vowel, the Pig Latin 
 * version is the original word with ay added to the end (Step 2). For example, apple becomes appleay. 
 * If y is the first letter in a word, it is treated as a consonant (Step 3); otherwise, it is treated as a vowel. 
 * For example, young becomes oungyay, but system becomes ystemsay (Step 4). For this program, assume that the 
 * user will enter only a single word consisting of all lowercase letters.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class PigLatin 
{

	public static void main(String[] args) 
	{
		int endOfString = 0;
		int begin = 0;
		int vowelLocation = -1;		//use -1 (ch7,page 12 indexOf()), concludes does not exist in string
		String userInput = "", userPigLatin = "";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a word >> ");
		userInput = keyboard.nextLine();
		
		endOfString = userInput.length();
		
		//iterate through userInput word
		for (int pointer = begin; pointer < endOfString; pointer++)
		{
			//check if first letter is a vowel (Step 2 and Step 3)
			if (pointer < 1)
			{
							
				if (isVowel(userInput.charAt(pointer)))
				{
					if (userInput.charAt(pointer)== 'y')
					{
						userPigLatin = userInput.substring(1, endOfString) + "yay";
						break;
					}
					else
					{
						userPigLatin = userInput.substring(0, endOfString) + "ay";
						break;
					}
				}	
				//find first instance of vowel (Step 1/4)
			}
			else if (isVowel(userInput.charAt(pointer)))
			{
				vowelLocation = pointer; 
				if (userInput.charAt(pointer)== 'y')		//step 4
				{
					userPigLatin = userInput.substring(vowelLocation, endOfString) + userInput.substring(begin, vowelLocation) + "ay";
					break;
				}
				else						//step 1
						userPigLatin = userInput.substring(vowelLocation, endOfString) + userInput.substring(begin, vowelLocation) + "ay";
						break;		//break used to stop iteration
				}
						
			}
	
		System.out.println(userPigLatin);

	}
	public static boolean isVowel(char c)
	{
		char vowel = c;
				
		if (vowel == 'a' || vowel == 'e' || vowel == 'i' || vowel == 'o' || vowel == 'u' || vowel == 'y')
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}

}
