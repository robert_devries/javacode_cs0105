package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ValidPasswordTims {

	
	public static void main(String[] args) 
	{
		String message = "The password did not have enough of the following:";
	    String warnOne = "lowercase letters", warnTwo = "uppercase letters";
	    String warnThree = "digits";
	    String passWord;
	    final int MIN_LOWER = 3, MIN_UPPER = 2, MIN_DIGIT = 1;
	    int len;
	    boolean isNotOK = true;
	    Scanner input = new Scanner(System.in);
	    System.out.println("Enter a new password."); // Program in course didn't like the extra prompts.
//	    System.out.print("Valid passwords must contain at least" + 
//	      "\n 2 uppercase letters" + "\n 3 lowercase letters and " + 
//	      "\n at least 1 digit >> ");
	    while(isNotOK)
	    {
	      int lower = 0, upper = 0, digit = 0;
	      passWord = input.nextLine();
	      len = passWord.length();
	      for(int x = 0; x < len; x++) 
	      {
	        char c = passWord.charAt(x);
	        if(Character.isUpperCase(c))
	        {
	          upper++;
	        }
	        if(Character.isLowerCase(c))
	        {
	          lower++;   
	        }
	        if(Character.isDigit(c))
	        {
	          digit++;
	        }
	      }
	      if(lower < MIN_LOWER || upper < MIN_UPPER || digit < MIN_DIGIT)
	      {
	        System.out.println(message);
	        if(upper < MIN_UPPER)
	        {
	          System.out.println(warnTwo);
	        }
	        if(lower < MIN_LOWER)
	        {
	          System.out.println(warnOne);
	        }
	        if(digit < MIN_DIGIT)
	        {
	          System.out.println(warnThree);
	        }
	        isNotOK = true;
	        System.out.print("Try entering another password >> ");
	      }
	      else
	      {
	        isNotOK = false;
	      }  
	    }
	    System.out.println("Valid password");

	}

}
