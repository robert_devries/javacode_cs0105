package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Lesson 7-6a, Programming Exercise
 * Write an application that accepts three Strings from the user and,
 * without regard to case, appropriately displays a message that indicates
 * whether the Strings were entered in alphabetical order. 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 30 June 2021
 */
public class Alphabetize 
{
	public static void main(String[] args) 
	{
		String origFirstStr, origSecondStr, origThirdStr;
		String lowerFirstStr, lowerSecondStr, lowerThirdStr;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter first string >> ");
		origFirstStr = keyboard.nextLine();
		System.out.println("Second string >> ");
		origSecondStr = keyboard.nextLine();
		System.out.println("Third string >> ");
		origThirdStr = keyboard.nextLine();
		
		//case sensitivity is not a factor - force strings to lower case
		lowerFirstStr = origFirstStr.toLowerCase();
		lowerSecondStr = origSecondStr.toLowerCase();
		lowerThirdStr = origThirdStr.toLowerCase();
		
		if ((lowerFirstStr.compareTo(lowerSecondStr)< 0) && (lowerFirstStr.compareTo(lowerThirdStr) < 0))   //check to confirm strings entered in alphabetical order
		{
			System.out.println("Strings Are entered in alphabetical order!");
		}
		else
			System.out.println("Strings are NOT in alphabetical order!");
		
		
	}
	
   
}
