package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 7, Programming Exercise 7-7,
 * Three-letter acronyms are common in the business world. For example, in
 * Java you use the IDE (Integrated Development Environment) in the JDK (Java
 * Development Kit) to write programs used by the JVM (Java Virtual Machine)
 * that you might send over a LAN (local area network). Programmers even use
 * the acronym TLA to stand for three-letter acronym. Write a program that
 * Step 1: allows a user to enter three words, and 
 * Step 2: display the appropriate three-letter acronym 
 * Step 3: in all uppercase letters. 
 * Step 4: If the user enters more than three words, ignore the extra words.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 9 Jul 2021
 */
public class ThreeLetterAcronym 
{
	public static void main(String[] args) 
	{
		String userInput = "", firstWord = "", secondWord = "", thirdWord = "";
		int start = 0, stringLength = 0, wordsRequired = 3; 
		int spaces = 0, updatePointerLocation = 0;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter 3 words for 3-letter acronym >> ");
		userInput = keyboard.nextLine();
		
		//use indexOf(' ') which is white space and returns the value where this occurs (userInput.indexOf(' ')) and find next instance. this will be iterated
		stringLength = userInput.length();
				
		for (int i = start; i < stringLength; i++)
		{			
			if (userInput.charAt(i) == ' ' || i == (stringLength - 1) )
			{
				char c;
				spaces++; 

				if (wordsRequired == 3)
				{
					firstWord = userInput.substring(0, i);
					c = firstWord.charAt(0);
					c = Character.toUpperCase(c);
					firstWord = c + firstWord.substring(1, i);
					wordsRequired -= 1;
					updatePointerLocation = firstWord.length() + spaces;		//move past whitespace for second word start posn
				}
				else if (wordsRequired == 2)
				{
					secondWord = userInput.substring(updatePointerLocation, i);
					c = secondWord.charAt(0);
					c = Character.toUpperCase(c);
					secondWord = c + secondWord.substring(1);
					wordsRequired -= 1;
					updatePointerLocation += secondWord.length() + 1;		//move past whitespace for third word start posn
				}
				else if (wordsRequired == 1)
				{
					thirdWord = userInput.substring(updatePointerLocation);		//substring from pointer location to end of line
					c = thirdWord.charAt(0);
					c = Character.toUpperCase(c);
					thirdWord = c + thirdWord.substring(1);
					wordsRequired -= 1;
					
				}
			}
				
		}
		//the first line below was not required but used to verify character extrapolation
		System.out.println("First Word >>  " + firstWord +
				"\nSecond Word >>  " + secondWord +
				"\nThird Word >>  " + thirdWord);
		System.out.println(firstWord.substring(0, 1) + secondWord.substring(0, 1) + thirdWord.substring(0, 1));
		
		

	}

}
