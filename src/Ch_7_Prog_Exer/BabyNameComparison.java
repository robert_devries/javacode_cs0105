package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 7 Programming Exercise
 * Prompts user for three first names and concatenates them in every possible
 * two-name combination so that new parents can easily compare them to find the 
 * most pleasing baby name.  
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 June 2021
 */
public class BabyNameComparison 
{
	public static void main(String[] args) 
	{
		String firstName = "";
		String secondName = "";
		String thirdName = "";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a name >> ");
		firstName = keyboard.nextLine();
		System.out.println("Enter a second name >> ");
		secondName = keyboard.nextLine();
		System.out.println("Enter a third name >> ");
		thirdName = keyboard.nextLine();
		
		System.out.println("The combinations are:");
		
		System.out.println(firstName + " " + secondName);
		System.out.println(firstName + " " + thirdName);
		System.out.println(secondName + " " + firstName);
		System.out.println(secondName + " " + thirdName);
		System.out.println(thirdName + " " + firstName);
		System.out.println(thirdName + " " + secondName);
		
		 

	}

}
