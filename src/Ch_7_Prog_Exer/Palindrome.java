package Ch_7_Prog_Exer;

import java.util.*;;

/**
 * Class description: Prog Exer 7-10.
 * Write an application that determines whether a phrase entered by the user is a palindrome. 
 * A palindrome is a phrase that reads the same backward and forward without regarding capitalization 
 * or punctuation. For example, �Dot saw I was Tod�, �Was it a car or a cat I saw�, and �Madam Im Adam� are palindromes. 
 * Display the appropriate feedback: You entered a palindrome or You did not enter a palindrome.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 12 Jul 2021
 */
public class Palindrome 
{
	public static void main(String[] args) 
	{
		String userInput = ("Was it a car or a cat I saw");
		StringBuilder reverseUserInput = new StringBuilder();
		StringBuilder userInputWithoutSpace =  new StringBuilder();
		String msgPalidrome = "You entered a palindrome";
		String msgNotPalidrome = "You did not enter a palindrome";
		String msgUserInput = "Enter a string";
		String userInputConvertToString;
		String reverseUserInputConvertToString;
		int lengthStr = userInput.length(), end = 0, start = 0;
		char ch;
		Scanner keyboard = new Scanner(System.in);
		
		for (int i = lengthStr-1; i >= end; i--)
		{
			ch = userInput.charAt(i);
			ch = Character.toLowerCase(ch);
			if (Character.isLetter(ch))
			{
				System.out.print(i + " (" + ch + ") " + "is a letter\n");
				reverseUserInput = reverseUserInput.append(ch);
			}
			else
			{
				System.out.print(i + " is not a letter\n");
				
			}
			
		}
		for (int i = start; i < lengthStr; i++)
		{
			ch = userInput.charAt(i);
			ch = Character.toLowerCase(ch);
			if (Character.isLetter(ch))
			{
				userInputWithoutSpace = userInputWithoutSpace.append(ch);
			}
		}
		System.out.println(userInputWithoutSpace + " ----- " + reverseUserInput);
		userInputConvertToString = userInputWithoutSpace.toString();
		reverseUserInputConvertToString = reverseUserInput.toString();
		
		
		if (userInputConvertToString.equals(reverseUserInputConvertToString))
		{
			System.out.println(msgPalidrome);
		}
		else
		{
			System.out.println(msgNotPalidrome);
		}
	}
}
