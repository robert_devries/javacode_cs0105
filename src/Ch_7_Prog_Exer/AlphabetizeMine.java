package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Lesson 7-6a, Programming Exercise
 * Write an application that accepts three Strings from the user and,
 * without regard to case, appropriately displays a message that indicates
 * whether the Strings were entered in alphabetical order. 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 30 June 2021
 */
public class AlphabetizeMine
{
	public static void main(String[] args) 
	{
		String origFirstStr, origSecondStr, origThirdStr;
		int valueFirstStr, valueSecondStr, valueThirdStr;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter first string >> ");
		origFirstStr = keyboard.nextLine();
		System.out.println("Second string >> ");
		origSecondStr = keyboard.nextLine();
		System.out.println("Third string >> ");
		origThirdStr = keyboard.nextLine();
		
		//all things equal, convert strings to lowercase
		String lowerFirstStr = origFirstStr.toLowerCase();
		String lowerSecondStr = origSecondStr.toLowerCase();
		String lowerThirdStr = origThirdStr.toLowerCase();
		System.out.println(origFirstStr + " " + lowerFirstStr + " " + origSecondStr + " " +lowerSecondStr);
		
		valueFirstStr = lowerFirstStr.charAt(0);		//ASCII value of each starting string value
		valueSecondStr = lowerSecondStr.charAt(0);
		valueThirdStr = lowerThirdStr.charAt(0);
		System.out.println("Value of strings >> " + 
				valueFirstStr + " | " + 
				valueSecondStr + " | " + 
				valueThirdStr);
		
		if (valueFirstStr < valueSecondStr && valueFirstStr < valueThirdStr)	//check to confirm strings entered in alphabetical order
		{
			System.out.println("Strings Are entered in alphabetical order!");
		}
		else																	//else
			System.out.println("Strings are NOT in alphabetical order!");
		//check to see if values are entered in alphabetical order (regardless of case sensitivity)
		
	}
	
   
}
