package Ch_7_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 7-4, Programming Exercise
 * Write an application that prompts the user for a password that contains at
 *least two uppercase letters, at least three lowercase letters, and at least one
 *digit. Continuously reprompt the user until a valid password is entered.
 *Display a message indicating whether the password is valid; if not, display the
 *reason the password is not valid.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 29 Jun 2021
 */
public class ValidatePassword_Mine
{
	public static void main(String[] args) 
	{
		String passWord = " ";
		boolean isPasswordValid = false, validPW = false;
		
		do
		{
			passWord = EnterPassWord();
			if (validPW = isValid(passWord))
			{
				System.out.println("Valid password");
				passwordIsValid = false;
			}
			else
			{
				isPasswordValid = true;
			}
		}while(isPasswordValid);
	}
	public static String EnterPassWord()
	{
		String passWord = "";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter password >> ");
		passWord = keyboard.nextLine();
		return passWord;
	}
	public static boolean isValid(String pWord )
	{
		boolean passwordIsValid = false;
		final int REQ_UPPER = 2, REQ_LOWER = 3, REQ_DIGIT = 1;
		final int START = 0;
		int countLower = 0, countUpper = 0, countDigit = 0;
		int pWordLength = pWord.length();
		
		for (int i = START; i < pWordLength; i++)
		{
			if (Character.isLowerCase(pWord.charAt(i)))
				countLower++;
			if (Character.isUpperCase(pWord.charAt(i)))
				countUpper++;
			if (Character.isDigit(pWord.charAt(i)))
				countDigit++;	
		}
		if ((countLower >= REQ_LOWER) && (countUpper >= REQ_UPPER) && (countDigit >= REQ_DIGIT))
		{
			passwordIsValid = true;
		}
			
		else
			if (countLower < REQ_LOWER || countUpper < REQ_UPPER || countDigit < REQ_DIGIT)
			{
				System.out.println("Password did not have enough of the following:");
				if (countLower < REQ_LOWER)
					System.out.println("lowercase letters");
				if (countUpper < REQ_UPPER)
					System.out.println("uppercase letters");
				if (countDigit < REQ_DIGIT)
					System.out.println("digits");
				passwordIsValid = false;
			}
		return passwordIsValid;
	}
}
 