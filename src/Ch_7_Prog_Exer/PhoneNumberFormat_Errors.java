package Ch_7_Prog_Exer;

import java.util.*;

/*
 * Class description: Ch 7, Programming Exer 7-9 Telephone Number formatted to (000) 000-0000
 * Write a program that inserts parentheses, a space, and a dash into a string of
 * 10 user-entered numbers to format it as a phone number. For example,
 * 5153458912 becomes (515) 345-8912. If the user does not enter exactly 10
 * digits, display an error message. Continue to accept user input until the user
 * enters 999
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 10 Jul 2021
 */
public class PhoneNumberFormat_Errors 
{
	public static void main(String[] args) 
	{
		boolean insufficientPhone = true;
		boolean userTerminates = true;

		int minRequiredNums = 10;
		int phoneNumLength;
		char parenthesesLeft = '(', parenthesesRight = ')', space = ' ', dash = '-';
		String originalUserInput = "", userFinished = "999";

		String promptNumber = "Enter 10-digit phone number (includes area code) >> ";
		String errorMessage = "Insufficient digits";
			
		Scanner keyboard = new Scanner(System.in);
		System.out.println(promptNumber);
		originalUserInput = keyboard.nextLine();
		phoneNumLength = originalUserInput.length();
		
		while (insufficientPhone || userTerminates)
		{
			while (phoneNumLength != minRequiredNums)
			{
				System.out.println(errorMessage);
				System.out.println(promptNumber);
				originalUserInput = keyboard.nextLine();
				phoneNumLength = originalUserInput.length();
				insufficientPhone = false;
			}
			
			while (userTerminates)
			{
				String reconstructedNum = (parenthesesLeft + originalUserInput.substring(0, 3) + parenthesesRight +
						space + originalUserInput.substring(3,6) + dash + originalUserInput.substring(6));
				System.out.println(reconstructedNum);
				System.out.println("Enter another 10-digit number or 999 to exit >>");
				originalUserInput = keyboard.nextLine();
				phoneNumLength = originalUserInput.length();
				
				if (originalUserInput == userFinished)
				{
					userTerminates = false;
				}
				else
				{
					System.out.println(promptNumber);
					originalUserInput = keyboard.nextLine();
					phoneNumLength = originalUserInput.length();
				}
			}
		}
		
		System.out.println("All good for now >>");

	}

}
