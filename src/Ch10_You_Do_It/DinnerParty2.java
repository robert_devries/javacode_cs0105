package Ch10_You_Do_It;

/**
 * Class description:  Ch 10-2, Demonstrating Inheritance.  this file is a child class or derived class of the Party class (note line 9 with the additions)
 *
 * This class will be used with PartyWithConstructor
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  13 Aug 2021
 */
public class DinnerPartyWithConstructor2 extends PartyWithConstructor		//added this change (extends PartyWithConstructor)
{
	private int dinnerChoice;
	
	public void setDinnerChoice(int choice)
	{
		dinnerChoice = choice;
	}
	public int getDinnerChoice()
	{
		return dinnerChoice;
	}
	//changes include the addition of the @Override method - this will override the parent Party class (parent class)
	@Override
	public void displayInvitation()
	{
		System.out.println("Please come to my dinner party!"); 		//parent class had "Please come to my party!"
	}
}
