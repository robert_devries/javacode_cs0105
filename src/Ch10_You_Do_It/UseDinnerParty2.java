package Ch10_You_Do_It;

import java.awt.Menu;
import java.util.Scanner;

import com.sun.javafx.scene.EnteredExitedHandler;

/**
 * Class description: Ch 10-2, Demonstrating Inheritance.  Uses Party class (Parent class) and DinnerParty2 (child class) to override parent class.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  13 Aug 2021
 */
public class UseDinnerParty2 
{
	public static void main(String[] args) 
	{
		int guests;
		int choice;
		Party aParty = new Party();
		DinnerParty2 aDinnerParty = new DinnerParty2();		//aDinnerParty is the obj that uses DinnerParty2 as a datatype and DinnerParty2() constructor
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Enter number of guests for the party >> ");
		guests = kb.nextInt();
		aParty.setGuests(guests);
		System.out.println("The party has " + aParty.getGuests() + " guests");
		aParty.displayInvitation();
		
		System.out.print("Enter number of guests for the dinner party >> ");
		guests = kb.nextInt();
		aDinnerParty.setGuests(guests);
		
		System.out.print("Enter the menu option -- 1 for chicken or 2 for beef >> ");
		choice = kb.nextInt();
		aDinnerParty.setDinnerChoice(choice);
		System.out.println("The dinner party has " + aDinnerParty.getGuests() + " guests");
		System.out.println("Menu option " + aDinnerParty.getDinnerChoice() + " will be served");
		aDinnerParty.displayInvitation();

	}

}
