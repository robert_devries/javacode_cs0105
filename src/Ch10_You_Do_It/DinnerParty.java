package Ch10_You_Do_It;

/**
 * Class description:  Ch 10-2, Demonstrating Inheritance.  this file is a child class or derived class of the Party class (note line 9 with the additions)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  13 Aug 2021
 */
public class DinnerParty extends Party
{
	private int dinnerChoice;
	
	public void setDinnerChoice(int choice)
	{
		dinnerChoice = choice;
	}
	public int getDinnerChoice()
	{
		return dinnerChoice;
	}
}
