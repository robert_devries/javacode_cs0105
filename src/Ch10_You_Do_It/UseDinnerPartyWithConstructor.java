package Ch10_You_Do_It;

/**
 * Class description:  Uses both DinnerPartyWithConstructor class and PartyWithConstructor class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 14 Aug 2021
 */
public class UseDinnerPartyWithConstructor 
{
	
	
	public static void main(String[] args) 
	{
		DinnerPartyWithConstructor aDinnerPartyWithConstructor = new DinnerPartyWithConstructor();

	}

}
