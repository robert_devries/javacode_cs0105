package Ch10_You_Do_It;

import java.util.Scanner;

import com.sun.javafx.scene.EnteredExitedHandler;

/**
 * Class description: Ch 10-2, Demonstrating Inheritance.  Uses Party class.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  13 Aug 2021
 */
public class UseParty 
{
	public static void main(String[] args) 
	{
		int guests;
		Party aParty = new Party();
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Enter number of guests for the party >> ");
		guests = kb.nextInt();
		aParty.setGuests(guests);
		System.out.println("The party has " + aParty.getGuests() + " guests");
		
		aParty.displayInvitation();

	}

}
