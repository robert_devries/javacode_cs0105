package Ch10_You_Do_It;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Party 
{
	private int guests;
	
	public int getGuests()
	{
		return guests;
	}
	public void setGuests(int numGuests)
	{
		guests = numGuests;
	}
	public void displayInvitation()
	{
		System.out.println("Please come to my party!");
	}
}
