
public class Rental 
{
	public final static int hourlyMinutes = 60;
	public final static double hourlyRentalRate = 40;
	private String contractNumber;
	private int rentalHours;
	private int residualRentedMinutes;
	private double price;
	
	public void setContractNumber(String contract)
	{
		contractNumber = contract;
	}
	
	public void setHoursAndMinutes (int userInputMinutes)
	{
		//local variables
		int totalMinutes, costPerMinute;
		totalMinutes = userInputMinutes;
		costPerMinute = 1;
		
		rentalHours = totalMinutes / hourlyMinutes;			//extract full hours rented
		residualRentedMinutes = totalMinutes % 60;		//extract remainder minutes
		price = ((hourlyRentalRate * rentalHours) + (costPerMinute * residualRentedMinutes));
	}
	public String getContractNumber()
	{
		return contractNumber;
	}
	
	public int getRentalHours ()
	{
		return rentalHours;
	}
	public int getLeftOverMinutes()
	{
		return residualRentedMinutes;
	}
	public double getCost()
	{
		return price;
	}
}


