package Ch_6_Prog_Exer;

import java.time.LocalDate;

/**
 * Class description: Ch 6, Prog Exer 6-8 Factory worker increases production by 6% on a monthly basis
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 20 Jun 2021
 */
public class IncreasedProduction 
{
	public static void main(String[] args) 
	{
		int months = 1;
		double partsProduced = 4000;
		final double monthlyProdRate = 0.06;
		boolean exceeded7000 = true;
		LocalDate startDate = LocalDate.now();
		
		for (months = 1; months <= 24; months++)
		{
			partsProduced += (partsProduced * monthlyProdRate);
			if ((partsProduced >= 7000) && (exceeded7000))		//'one-shot' if greater than 7000, returns false and 'if' never used again
			{
				exceeded7000 = exceedQuota(partsProduced);		//will reset exceeded7000 to false, 'if' statement no longer accessible
				LocalDate endDate = startDate.plusMonths(months);
				System.out.println(partsProduced + " The month in which production exceeds 7000.0 is month " + months + ".");
			}
			else
			{
				LocalDate endDate = startDate.plusMonths(months);
				System.out.println(partsProduced);	
			}
		}
	}
	public static boolean exceedQuota(double parts)
	{
		if (parts > 7000)
		{
			return false;
		}
		else
			return true;
	}
}
