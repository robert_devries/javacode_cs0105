package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Prog Exer 6-15 
 * Create a class named Purchase. Each Purchase contains an invoice number, amount of sale, and amount of sales tax. 
 * Include set methods for the invoice number and sale amount. 
 * Within the set() method for the sale amount, calculate the sales tax as 5% of the sale amount. 
 * Also include a display method that displays a purchase�s details.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CreatePurchase 
{
	public static void main(String[] args) 
	{
		int invoiceNum = 0,
				saleAmount = 0;
		Purchase custPurchase = new Purchase();
		Scanner sc = new Scanner(System.in);
		boolean badInvoice = true;
		boolean badSaleAmount = true;
				
		while (badInvoice)
		{
			
			if (invoiceNum > 1000 && invoiceNum < 8000)
			{
				custPurchase.setInvoiceNumber(invoiceNum);
				badInvoice = false;
			}
			else if ((invoiceNum < 1000) || (invoiceNum > 8000))
			{
				System.out.println("Enter invoice number (between 1,000 - 8,000 >> ");
				invoiceNum = sc.nextInt();
			}
		}
				
		while (badSaleAmount)
		{
			System.out.println("Enter sale amount  >> ");
			saleAmount = sc.nextInt();
			
			if (saleAmount >= 0)
			{
				custPurchase.setSaleAmount(saleAmount);
				badSaleAmount = false;				
			}
			else if (saleAmount < 0)
			{
				System.out.println("Sale amount must be greater than zero >> ");
			}
		}
		
		custPurchase.display();
	}
}
