package Ch_6_Prog_Exer;

/**
 * Class description: Ch 6 programming exercise 
 * Count by fives
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 16 Jun 2021
 */
public class CountByFives 
{
	
	public static void main(String[] args) 
	{
		int endValue = 501,
				incrementValue = 5;
		final int newLineMultipleOf50 = 50;
		
		for (int startValue = 5; startValue < endValue; startValue += incrementValue)
		{
			System.out.print(startValue + " ");
			if (startValue % newLineMultipleOf50 == 0)
				System.out.println("");
		}

	}

}
