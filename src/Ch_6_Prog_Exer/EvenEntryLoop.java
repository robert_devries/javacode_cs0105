package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 6, programming exercise 2 "user enters an even number or the sentinel '999' to stop.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 16 Jun 2021
 */
public class EvenEntryLoop 
{
	
	public static void main(String[] args) 
	{
		int sentinel = 999;
		int userNum = 0, result = 0;
		
		String msgGood = "Good job!";
		String msgGoAgain = "Another input >> ";
		String errorMsg = " is not an even number >> ";
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Enter even number of 999 to quit >> ");
		userNum = keyboard.nextInt();
						
		while (userNum != sentinel)
		{
			if (userNum % 2 == 0)
			{
				System.out.println(msgGood);
				System.out.println(msgGoAgain);
				userNum = keyboard.nextInt();
			}
			else
			{
				System.out.println(userNum + errorMsg);
				System.out.println(msgGoAgain);
				userNum = keyboard.nextInt();
			}
		System.out.println("Done!");
		}
	}
}
