package Ch_6_Prog_Exer;

/**
 * Class description:
 *
 * @author Robert deVries (UoOttawa)
 *
 */
public class Triangle 
{

	public static void main(String[] args) 
	{
		char letterT = 'T';
		int height = 7;			//height can be changed through user input
		int leadingSpaces = (height -1);		//leading spaces is height - 1
		int count = 6;
		
		//pyramid number of rows noted by height
		for (int i = 1; i < height; i++)
		{
			//leading spaces prior to each 'T' output, travelling down pyramid there are less leading spaces
			for (int j = i; j < (leadingSpaces); j++)
			{
				System.out.print(" ");
			}
				//with leading spaces identified, output 'T's which is always 2 more than above line, except line 1
				for (int k = 1; k < (i*2); k++)
				{
					System.out.print(letterT);
				}
			System.out.println();	//new line for next iteration
		}

	}

}
