package Ch_6_Prog_Exer;

/**
 * Class description: Prog Ex 6-12 Population
 * Assume the population in Mexico is 128 million and that the population increases 1.01 percent annually.
 * Assume that the population of the United States is 323 million and that the population is reduced 0.15 percent
 * annually. Write an application that displays the populations for the two countries every year until the population
 * of Mexico exceeds that of the United States, and display the number of years it took.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 22 Jun 2021
 */
public class PopulationMine
{
	public static void main(String[] args) 
	{
		double setPopulationMexico = 128; 
		double setPopulationStates = 323;
		double annualMexicoIncreaseRate = 0.0101,   //was 0.0101
				annualStatesDecreaseRate = 0.0015;		//was 0.0015
		double accumulateMexico = setPopulationMexico;
		double accumulateStates = setPopulationStates;
		int count = 0;
		
		do
		{
			count++;
			accumulateMexico += (accumulateMexico * annualMexicoIncreaseRate);
			accumulateStates -= (accumulateStates * annualStatesDecreaseRate);
			
			if (accumulateMexico < accumulateStates)
			{				
				System.out.println("After " + count + " years, US population is " + accumulateStates + " Million ");
		        System.out.println("Mexico population is " + accumulateMexico + " Million");
			}
            else
            {
            	System.out.println("The population of Mexico will exceed the U.S. " +
                        "population in " + count + " years. Mexico population: " + accumulateMexico + " Million " +  
                        ", U.S. population: " + accumulateStates + " Million");
            }
					
		}while (accumulateMexico < accumulateStates);
	}
}
