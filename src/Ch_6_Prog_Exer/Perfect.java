package Ch_6_Prog_Exer;


/**
 * Class description:  Ch 6-5 
 * Write an application that displays every perfect number from 1 through 1,000.
 * A perfect number is one that equals the sum of all the numbers that divide
 * evenly into it. For example, 6 is perfect because 1, 2, and 3 divide evenly into
 * it, and their sum is 6; however, 12 is not a perfect number because 1, 2, 3, 4,
 * and 6 divide evenly into it, and their sum is greater than 12.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 17 Jun 2021
 */
public class Perfect 
{
	public static void main(String[] args) 
	{
		final int START = 1, FINISH = 1000;
										
		for (START; START <= FINISH; START++ )	// counter to 1000
		{
			boolean isPerfect = perfect(START);
			if (isPerfect)
				System.out.println(START + " is a perfect number");
		}
				
	}
	public static boolean perfect(int n) 
	{
        int initialNum;
        int sum = 0;
        int counter = 1;
        
        for (initialNum = n; counter < initialNum; ++counter)
        {
        	if (initialNum % counter == 0)
        		sum += counter;
        }
        if (sum == initialNum)
        	return true;
        else
        	return false;
    }
}
