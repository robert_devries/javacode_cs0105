package Ch_6_Prog_Exer;
import java.util.*;

/**
 * Class description: Ch 6 programming exercise 
 * Count by fives
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 16 Jun 2021
 */
public class CountByAnything 
{
	
	public static void main(String[] args) 
	{
		int endValue = 501, userCountBy;
		int lineCounter = 0;
			
		System.out.println("Enter integer value to increment by >> ");
		Scanner keyboard = new Scanner (System.in);
		userCountBy = keyboard.nextInt();
		//keyboard.nextLine();
		
		for (int startValue = userCountBy; startValue < endValue; startValue += userCountBy)
		{
			lineCounter += 1;
			System.out.print(startValue + " ");
			if (lineCounter == 10)
			{
				System.out.println("");
				lineCounter = 0;	//resets counter
			}
		
			
		}
		

	}

}
