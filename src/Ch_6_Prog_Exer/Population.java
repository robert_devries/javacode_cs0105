package Ch_6_Prog_Exer;

/**
 * Class description: Prog Ex 6-12 Population
 * Assume the population in Mexico is 128 million and that the population increases 1.01 percent annually.
 * Assume that the population of the United States is 323 million and that the population is reduced 0.15 percent
 * annually. Write an application that displays the populations for the two countries every year until the population
 * of Mexico exceeds that of the United States, and display the number of years it took.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 22 Jun 2021
 */
public class Population 
{
	public static void main(String[] args) 
	{
		double mexicoPop = 128,
	      	usPop = 323;
	    //double annualMexicoIncreaseRate = 0.0101,   //was 0.0101			100% success when the rates were not associated with variables ??
		//			annualStatesDecreaseRate = 0.0015;		//was 0.0015
		int year = 0;
	      
	      do
	      {
	    	  if (mexicoPop < usPop)
	    	  {   
	    		  year++;
	    		  mexicoPop += (mexicoPop * 0.0101);
	    		  usPop -= (usPop * 0.0015);
	
	    		  if (mexicoPop < usPop)
	    		  {
	    			  System.out.println("After " + year + " years, US population is " + usPop + " million");
	    			  System.out.println("Mexico population is " + mexicoPop + " million");
	    		  }
	    		  //else if (mexicoPop > usPop)
	    		  //{
	    		//	  System.out.println("The population of Mexico will exceed the U.S. population in " + year + " years");
	    			//  System.out.println("The population of Mexico will be " + mexicoPop + " million");
	    			//  System.out.println("and the population of the U.S. will be " + usPop + " million");
	    		  //}
	    	  }
	      }while (mexicoPop < usPop);
	      
	      System.out.println("The population of Mexico will exceed the U.S. population in " + year + " years");
		  System.out.println("The population of Mexico will be " + mexicoPop + " million");
		  System.out.println("and the population of the U.S. will be " + usPop + " million");
	      
	   }
	}
