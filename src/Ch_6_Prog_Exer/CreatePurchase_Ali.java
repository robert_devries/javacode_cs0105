package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Goes with Purchase_Ali class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 23 Jun 2021
 */
public class CreatePurchase_Ali 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
	      Purchase_Ali purch = new Purchase_Ali();
	      int num;
	      double amount;
	      String entry;
	      final int LOW = 1000, HIGH = 8000;
	      System.out.println("Enter invoice number");
	      entry = input.next();
	      num = Integer.parseInt(entry);
	      while(num <= LOW || num >= HIGH)
	      {
	         System.out.println("Invoice number must be between " +
	         LOW + " and " + HIGH + "\nEnter invoice number");
	         entry = input.next();
	         num = Integer.parseInt(entry);
	      }
	      
	      System.out.println("Enter sale amount");
	      entry = input.next();
	      amount = Double.parseDouble(entry);
	      while(amount < 0)
	      {
	         System.out.println("Enter sale amount");
	         entry = input.next();
	         amount = Double.parseDouble(entry);
	      }
	      purch.setInvoiceNumber(num);
	      purch.setSaleAmount(amount);
	      purch.display();
	   }
	}
