package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Prog Exer 6-8 Student Quiz Scores until 99 entered
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 20 Jun 2021
 */
public class QuizScoreStatistics 
{
	public static void main(String[] args) 
	{
		int inputScore, higherScore = 0, lowerScore = 0, sentinel = 99, inputCounter = 0;
		double totalSum = 0.0, avgScore = 0.0;
		String initialMsg = ("Enter a score >>");
		String continueMsg = ("Enter another score or " + sentinel + " to quit >> ");
		String invalidMsg = ("Score must be between 10 and 0");
		
		System.out.println(initialMsg);
		Scanner sc = new Scanner (System.in);
		inputScore = sc.nextInt();
		higherScore = inputScore;	//initialize first entry as highest score for later comparisons
		
		//get scores until sentinel '99'
		while (sentinel != inputScore)
		{
			//get range conditions (inputScore parameters, sentinel)
			if ((inputScore >= 1 && inputScore <= 10))		//first condition
			{
				//perform operations 
				inputCounter++;
				totalSum += inputScore;
				
				//typical range for data entry	(high score init = 0)	
				if ((inputScore >= higherScore))
				{
					if ((inputScore > lowerScore) && (lowerScore != 0))
					{
						higherScore = inputScore;
						avgScore = totalSum/inputCounter;
						System.out.println(continueMsg);
						inputScore = sc.nextInt();
					}
					else
					{
						lowerScore = inputScore;
						avgScore = totalSum / inputCounter;
						System.out.println(continueMsg);
						inputScore = sc.nextInt();
					}
				}
				//alternate range for data entry
				else if (inputScore < higherScore && inputScore >= lowerScore)
				{
					if (inputScore <= lowerScore)
					{
						lowerScore = inputScore;
						avgScore = totalSum / inputCounter;
						System.out.println(continueMsg);
						inputScore = sc.nextInt();
					}
					else
					{
						lowerScore = inputScore;
						avgScore = totalSum / inputCounter;
					    System.out.println(continueMsg);
					    inputScore = sc.nextInt();
					}
				}
				// last correction made to absorb the lower int 7 after first int 9 initiated
				else
				{
					lowerScore = inputScore;
					avgScore = totalSum / inputCounter;
					System.out.println(continueMsg);
					inputScore = sc.nextInt();
				}
			}
			// outside boundary ranges
			else if (inputScore <= 0 || inputScore >10)
			{
				System.out.println(invalidMsg);
				System.out.println(continueMsg);
				inputScore = sc.nextInt();
			}
			//terminate application successfully
			else
				sentinel = 99;
		}
		//capture message output for only one data score entry
		if (inputCounter == 1)
		{
			System.out.print(inputCounter + " valid scores were entered " + 
					"\nHighest was " + higherScore +  
					"\nLowest was not recorded" +
					"\nAverage was " + avgScore);
		}		
		//final msg, outside all loops
		else
		{
			System.out.print(inputCounter + " valid scores were entered " + 
					"\nHighest was " + higherScore +  
					"\nLowest was " +lowerScore +
					"\nAverage was " + avgScore);
		}
	}
}
