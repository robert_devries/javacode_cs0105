package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description:  Ch 6-4 programming exercise (2 numbers entered and displays all integers between the 2)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 16 Jun 2021
 */
public class Inbetween 
{
	public static void main(String[] args) 
	{
		int firstInt, secondInt, largest, smallest, lcv;	//lcv (loop control variable)
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Enter 1st integer >> ");
		firstInt = keyboard.nextInt();
		System.out.println("Enter 2nd integer >> ");
		secondInt = keyboard.nextInt();
		
		//determine the largest & smallest integer provided by user
		if (firstInt > secondInt)
		{
			largest = firstInt - 1;		//decrement by 1 to confirm a gap exists between first/second number
			smallest = secondInt;
		}
		else
		{
			largest = secondInt - 1;	//decrement by 1 to confirm a gap exists between first/second number
			smallest = firstInt;
		}
		
		//loop control variable
		lcv = largest - smallest;
		
		//case if no integers exist between integers provided.
		if (lcv > 0)		
		{
			while (lcv > 0)		//output the integers between the largest and smallest
			{
				System.out.println(largest + " ");
				--largest;
				--lcv;
			}
		}
		else
			System.out.println("There are no integers between " + firstInt + " and " + secondInt);
	}

}
