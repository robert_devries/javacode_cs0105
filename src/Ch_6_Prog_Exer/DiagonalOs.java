package Ch_6_Prog_Exer;

/**
 * Class description: Ch 6-6 outputs a diagonal line of "O"s
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DiagonalOs 
{
	public static void main(String[] args) 
	{
		char letterO = 'O';
				
		for (int i = 0; i < 10; ++i)
		{
			switch (i)
			{
			case 0:
				System.out.println(letterO);
				break;
			case 1:
				System.out.println(" " + letterO);
				break;
			case 2:
				System.out.println("  " + letterO);
				break;
			case 3:
				System.out.println("   " + letterO);
				break;
			case 4:
				System.out.println("    " + letterO);
				break;
			case 5:
				System.out.println("     " + letterO);
				break;
			case 6:
				System.out.println("      " + letterO);
				break;
			case 7:
				System.out.println("       " + letterO);
				break;
			case 8:
				System.out.println("        " + letterO);
				break;
			case 9:
				System.out.println("         " + letterO);	
				break;
			}
		}
	}

}
