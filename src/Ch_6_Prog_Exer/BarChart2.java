package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Prog Ex 6-12 BarCharts
 * The Huntington High School basketball team has five players named Art, Bob, Cal, Dan, and Eli. 
 * Implement the BarChart class which accepts the number of points scored by each player in a game 
 * and displays a bar chart that illustrates the points scored by displaying an asterisk for each point. 
 * The output looks similar to the chart in Figure 6-34.
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 22 Jun 2021
 */
public class BarChart2 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int artPoints;
        int bobPoints;
        int calPoints;
        int danPoints;
        int eliPoints;
        final int AMT_EACH_ASTERISK = 10;
        
        
        System.out.println("Enter points earned for the season");
        System.out.print("    by Art >> ");
        artPoints = input.nextInt();
        System.out.print("    by Bob >> ");
        bobPoints = input.nextInt();
        System.out.print("    by Cal >> ");
        calPoints = input.nextInt();
        System.out.print("    by Dan >> ");
        danPoints = input.nextInt();
        System.out.print("    by Eli >> ");
        eliPoints = input.nextInt();
        System.out.println("\nPoints for Season (each asterisk represents " +
                           AMT_EACH_ASTERISK + " points)\n");
        
        drawChart("Art", artPoints, AMT_EACH_ASTERISK);
        drawChart("Bob", bobPoints, AMT_EACH_ASTERISK);
        drawChart("Cal",calPoints, AMT_EACH_ASTERISK);
        drawChart("Dan", danPoints, AMT_EACH_ASTERISK);
        drawChart("Eli",eliPoints, AMT_EACH_ASTERISK);

	}
	 public static void drawChart(String name, int points, int amt_each)
	{
		 int calculate = points;
		 //final int START = 1, FINISH = 1000;
		 int resultantPoints;
		 resultantPoints = calculate / amt_each;
		 System.out.print(name + "  ");
		 
		 //for (int i = START; i<= FINISH; ++i)
		 //for (int x =0; x < points/amt_each; ++x)
		 for (int start = 1; start <= resultantPoints; start++)
		 {
			 System.out.print("*");
		 }
		 System.out.println();
	}

}
