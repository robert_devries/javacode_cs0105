package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description: Prog Ex 6-12 BarCharts
 * The Huntington High School basketball team has five players named Art, Bob, Cal, Dan, and Eli. 
 * Implement the BarChart class which accepts the number of points scored by each player in a game 
 * and displays a bar chart that illustrates the points scored by displaying an asterisk for each point. 
 * The output looks similar to the chart in Figure 6-34.
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 22 Jun 2021
 */
public class BarChart 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int artPoints;
        int bobPoints;
        int calPoints;
        int danPoints;
        int eliPoints;
        String art = "Art";
        String bob = "Bob";
        String cal = "Cal";
        String dan = "Dan";
        String eli = "Eli";

        System.out.print("Enter points earned by Art >> ");
        artPoints = input.nextInt();
        System.out.print("Enter points earned by Bob >> ");
        bobPoints = input.nextInt();
        System.out.print("Enter points earned by Cal >> ");
        calPoints = input.nextInt();
        System.out.print("Enter points earned by Dan >> ");
        danPoints = input.nextInt();
        System.out.print("Enter points earned by Eli >> ");
        eliPoints = input.nextInt();
        System.out.println("\nPoints for Game\n");
        
        drawChart(art, artPoints);
        drawChart(bob, bobPoints);
        drawChart(cal,calPoints);
        drawChart(dan, danPoints);
        drawChart(eli,eliPoints);

	}
	public static void drawChart(String name, int points)
	{
		System.out.print(name + "  ");
		for (int start = 1; start <= points; start++)
		{
			System.out.print("*");
		}
		System.out.println();
	}

}
