package Ch_6_Prog_Exer;

/**
 * Class description: Prog Exer 6-11 Pickering Trucking Company
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 21 Jun 2021
 */
public class DrugTests 
{
	public static void main(String[] args) 
	{
		final int START = 1, FINISH = 52;
		int testEmployeeNum;
		int columnCounter = 0;
		
		//testEmployeeNum = randomEmployeeNum();
		
		//52 week iteration
		for (int week = START; week <= FINISH; week++)
		{			
			columnCounter++;
			testEmployeeNum = randomEmployeeNum();
			System.out.printf("week " + week + " Emp # " + testEmployeeNum + "     ");
			
			//4 random employees per line, 
			if (columnCounter == 4)
			{				
				columnCounter = 0;		//reset columns to next line
				System.out.println();
			}
		}
	}
	//will need to create a method here for each employee number
	public static int randomEmployeeNum()
	{
		int testedEmployee = (1 + (int)(Math.random() * 30));
		return testedEmployee;
	}
}
