package Ch_6_Prog_Exer;

import java.util.*;

/**
 * Class description:  Programming Exercise 6-2 Factorials
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 * date: 16 Jun 2021
 */
public class Factorials 
{
	public static void main(String[] args) 
	{
		int endPoint = 10;		//no factorials for one (repeater) or below (eliminate unnecessary operations)
        int result = 1;			//start factorial with 1 since 0 will always produce a zero result with multiplication
		
		for (int factorial = 1; factorial <= endPoint; ++factorial)	//starting point was 10 and looking for final o/p result
		{
			result *= factorial;
            System.out.println("The factorial of " + factorial + " is " + result);
		}
		
	}

}
