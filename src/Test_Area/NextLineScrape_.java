package Test_Area;

import java.util.*;
/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class NextLineScrape_ 
{
	public static void main(String[] args) 
	{
		int firstVar;
		int secondVar;
		String firstName;
		String secondName;
		int thirdVar;
		String capture;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("First number >> ");
		firstVar = keyboard.nextInt();
		System.out.println("Second number >> ");
		secondVar = keyboard.nextInt();
		System.out.println("Line scrape no action! ");
		capture = keyboard.nextLine();
		System.out.println("First name >> ");
		firstName = keyboard.nextLine();
		System.out.println("Last name >> ");
		secondName = keyboard.nextLine();
		System.out.println("Final int >> ");
		thirdVar = keyboard.nextInt();
		
		System.out.println("First number >> " + firstVar);
		System.out.println("Second number >> " + secondVar);
		System.out.println("Here would be the line scrape left behind by int values! ");
		System.out.println("First name >> " + firstName);
		System.out.println("Last name >> " + secondName);
		System.out.println("Last number >> " + thirdVar);
		

	}

}
