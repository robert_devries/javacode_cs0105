package Test_Area;

import java.util.*;

/**
 * Class description: This was extracted from the net (youtube.com/watch?v=xzstcj3Cuso) excellent description
 * Requirements:  start with 1 * and continue with stars for the number of rows
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * 18 Jun 2021
 */
public class StarPatterns1 
{
	public static void main(String[] args) 
	{	
		int userRows;		//how many rows do you want to see the pattern on
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many rows of '*' >> ");
		userRows = userInput.nextInt();
			
		firstQuadrant(userRows);									//dissect with 2 triangles (one with spaces, one with *)
		System.out.println("-----.----- 1st quadrant above");		//resets line for next for loop of *'s
		
		secondQuadrant(userRows);									//dissect with 2 triangles (one with spaces, one with *)
		System.out.println("-----.----- 2nd quadrant above");		//resets line for next for loop of *'s
		
		thirdQuadrant(userRows);									//dissect with 2 triangles (one with spaces, one with *)
		System.out.println("-----.----- 3rd quadrant above");
		
		fourthQuadrant(userRows);									//dissect with 2 triangles (one with spaces, one with *)
		System.out.println("-----.----- 4th quadrant above");
		
		hillPattern(userRows);										//dissect with '3' triangles (one with spaces, one with * (less one row), one with *
		System.out.println("-----.----- Hill Pattern above");
		
		reverseHillPattern(userRows);
		System.out.println("-----.----- Reverse Hill Pattern above");	//dissect with '3' triangles (one with spaces, one with * (less one row), once with *
		
		diamondPattern(userRows);
		System.out.println("-----.----- Diamond Pattern above");		//will use both the Hill and Reverse Hill for loops nested in the main for loop
																		//when added, noted is extra full line in middle - make Hill Pattern for loop < userRows ^^^^^
	}
	public static void firstQuadrant(int userRows)
	{
		for (int counter = 1; counter <= userRows; counter++)		//i is the counter and userRow the limit
		{
			//starts with * and increases # of stars
			//1st for loop starts with *
			for (int j = 1; j <= counter; j++ )			//first line starts with *'s, so this for loop reflects same requirements
			{
				System.out.print("* ");				//all done!  (no println statement, want to continue on the same line until line below
			}
		System.out.println();		//resets 1st for loop to next line down
		}
	}
	public static void secondQuadrant(int userRows)
	{
		//starts with *'s and decreases the number of *'s on each line
		for (int counter1 = 1; counter1 <= userRows; counter1++)		//counter and userRow the limit
		{
			//first row starts with all stars and decreases
			for (int j = counter1; j <= userRows; j++)
			{
				System.out.print("* ");						//*'s taken care of
			}
			System.out.println();							//reset line to next line
		}
	}
	public static void thirdQuadrant(int userRows)
	{
		for (int i = 1; i <= userRows; i++)		//number of columns to execute
		{
			for (int j = 1; j < i; j++)		//number of spaces needed on each line in above for loop (changed j <= i to j < i to make sure 1st space doesn't exe
			{
				System.out.print("  ");
			}
			for (int j = i; j <= userRows; j++)
			{
				System.out.print("* ");
			}
		System.out.println();
		}
	}
	public static void fourthQuadrant(int userRows)
	{
		//starts with spaces and ends with *'s
		for (int i = 1; i <= userRows; i++)		//initiate for loop
		{
			//although starts with *'s, the remaining lines start with spaces (spaces are the driver)
			for (int j = i; j < userRows; j++)	//remove the equals (=) from j <= userRows which eliminate 5 spaces and makes 4 spaces prior to adding *
			{
				System.out.print("  ");		
			}
			for (int k = 1; k <= i; k++)
			{
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	public static void hillPattern(int userRows)
	{
		//there are 3 triangle patterns (1 empty spaces - decreasing, 1 stars - increasing, 1 stars - increasing on the right side)
		for (int i = 1; i <= userRows; i++)		//this is the overall height of the triangle in rows
		{
			//all the for loops below will output on the same line until the println at the bottom - which resets to the next line
			//since there are 3 triangles, there will be 3 for loops
			for (int j = i; j <= userRows; j++)
			{
				System.out.print("  ");
			}
			for (int j = 1; j < i; j++)		//condition phrase changed from <= to just < to eliminate the peak *
			{
				System.out.print("* ");
			}
			for (int j = 1; j <= i; j++)
			{
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	public static void reverseHillPattern(int userRows)
	{
		//there are 3 triangle patterns (1 empty spaces - increasing, 1 stars - decreasing, 1 stars - decreasing on the right side)
		for (int i = 1; i <= userRows; i++)		//this is the overall height of the triangle in rows
		{
			//all the for loops below will output on the same line until the println at the bottom - which resets to the next line
			//since there are 3 triangles, there will be 3 for loops
			for (int j = 1; j <= i; j++)
			{
				System.out.print("  ");
			}
			for (int j = i; j < userRows; j++)		//condition phrase changed from <= to just < to eliminate the peak * (in essence 1 column)
			{
				System.out.print("* ");
			}
			for (int j = i; j <= userRows; j++)			//added this line for decreasing star triangle
			{
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	public static void diamondPattern(int userRows)
	{
		//this is the Hill Pattern
		for (int i = 1; i < userRows; i++)		//^^^^^ changed from i <= userRows to i < userRows to eliminate last line of ****
		{
			//all the for loops below will output on the same line until the println at the bottom - which resets to the next line
			//since there are 3 triangles, there will be 3 for loops
			for (int j = i; j <= userRows; j++)
			{
				System.out.print("  ");
			}
			for (int j = 1; j < i; j++)		//condition phrase changed from <= to just < to eliminate the peak *
			{
				System.out.print("* ");
			}
			for (int j = 1; j <= i; j++)
			{
				System.out.print("* ");
			}
			System.out.println();
		}
		//this is the Reverse Hill Pattern
		for (int i = 1; i <= userRows; i++)		//this is the overall height of the triangle in rows
		{
			//all the for loops below will output on the same line until the println at the bottom - which resets to the next line
			//since there are 3 triangles, there will be 3 for loops
			for (int j = 1; j <= i; j++)
			{
				System.out.print("  ");
			}
			for (int j = i; j < userRows; j++)		//condition phrase changed from <= to just < to eliminate the peak * (in essence 1 column)
			{
				System.out.print("* ");
			}
			for (int j = i; j <= userRows; j++)			//added this line for decreasing star triangle
			{
				System.out.print("* ");
			}
			System.out.println();
		}
		
	}
}
