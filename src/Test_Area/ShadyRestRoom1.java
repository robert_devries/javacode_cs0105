package Ch_Exercises;
import java.util.Scanner;

public class ShadyRestRoom 
{
    public static void main(String[] args)
    {
        final int INVALID = 0;
        final int QUEEN = 1;
        final int KING = 2;
        final int KING_PULLOUT = 3;
        final double ROOM_QUEEN = 125;
        final double ROOM_KING = 139;
        final double ROOM_KING_PULLOUT = 165;
        String queen = "Queen";
        String king = "King";
        String kingPullout = "King with pullout";
        int userInput, view;
        
        System.out.println("Choose (1 - Queen), (2-King), (3-King_Pullout) >> ");
        Scanner keyboard = new Scanner(System.in);
        userInput = keyboard.nextInt();
        
        switch (userInput)
        {
            case 0:
            {
                System.out.println("Invalid selection made.");
                System.out.println("Cost >> $0");
                break;
            }
            case 1:
            {
                roomSelection(queen, ROOM_QUEEN);
                break;
            }
            case 2:
            {
                roomSelection(king, ROOM_KING);
                break;
            }
            case 3:
            {
                roomSelection(kingPullout, ROOM_KING_PULLOUT);
                break;
            }
            //default setting for any other user input 
            default:
                System.out.println("\nUnknown entry. Application gracefully terminated.");
        }
    }
    public static int viewType()
    {
        int roomView;
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Would like a lake view (1), or a park view (2) >>");
        roomView = keyboard.nextInt();
        return roomView;

    }
    public static void roomSelection(String roomType, double cost)
    {
            int view;

            System.out.println("Selected >> " + roomType + ", Cost >> $" + cost);

            view = viewType();
            if (view == 2)
                cost = (cost + 15);
            else if ((view == 0) || (view > 2))
            {
                cost = (cost + 15);
                System.out.println("You have made an incorrect entry, cost increased as per lake view ($15).");
            }
            System.out.println("Total cost is >> " + cost);
    }
}
