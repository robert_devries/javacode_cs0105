package Test_Area;

import java.util.*;

/**
 * Class description: This was extracted from the net (youtube.com/watch?v=xzstcj3Cuso) excellent description
 * Requirements:  start with 1 * and continue with stars for the number of rows
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * 18 Jun 2021
 */
public class StarsPattern 
{
	public static void main(String[] args) 
	{	
		int userRows;		//how many rows do you want to see the pattern on
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("How many rows of '*' >> ");
		userRows = userInput.nextInt();
		
		firstQuadrant(userRows);
		System.out.println("- - - - - . - - - - -");		//resets line for next for loop of *'s
		secondQuadrant(userRows);
		System.out.println("- - - - - . - - - - -");		//resets line for next for loop of *'s
		fourthQuadrant(userRows);
						
		
			
	}
	public static void firstQuadrant(int userRows)
	{
		for (int counter = 1; counter <= userRows; counter++)		//i is the counter and userRow the limit
		{
			//starts with * and increases # of stars
			//1st for loop starts with *
			for (int j = 1; j <= counter; j++ )			//first line starts with *'s, so this for loop reflects same requirements
			{
				System.out.print("* ");				//all done!  (no println statement, want to continue on the same line
			}
		System.out.println();		//resets 1st for loop to next line down
		}
	}
	public static void secondQuadrant(int userRows)
	{
		//starts with *'s and decreases the number of *'s on each line
		for (int counter1 = 1; counter1 <= userRows; counter1++)		//counter and userRow the limit
		{
			//first row starts with all stars and decreases
			for (int j = counter1; j <= userRows; j++)
			{
				System.out.print("* ");						//*'s taken care of
			}
			System.out.println();							//reset line to next line
		}
	}
	public static void fourthQuadrant(int userRows)
	{
		//starts with spaces and decreases and ends with *'s on each line
		for (int i = 1; i <= userRows; i++)
		{
			//although starts with *'s, the remaining lines start with spaces (spaces are the driver)
			for (int j = i; j < userRows; j++)	//remove the equals (=) from j <= userRows which eliminate 5 spaces and makes 4 spaces prior to adding *
			{
				System.out.print("  ");		
			}
			for (int j = 1; j <= i; j++ )
			{
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
