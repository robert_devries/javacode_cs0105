package Ch_10_Prog_Exer;

public class HorseAli
{
   // Define the Horse class here
   String name;
   String color;
   int birthYear;

   public void setName(String horseName){
       name = horseName;
   }

   public void setColor(String horseColor){
       color = horseColor;
   }

   public void setBirthYear(int year){
       birthYear = year;
   }

   public String getName(){
       return name;
   }

   public String getColor(){
       return color;
   }

   public int getBirthYear(){
       return birthYear;
   }
}





// horse1.setName("Old Paint");
//        horse1.setColor("brown");
//        horse1.setBirthYear(2009);
//        horse2.setName("Champion");
//        horse2.setColor("black");
//        horse2.setBirthYear(2011);
//        horse2.setRaces(4);
