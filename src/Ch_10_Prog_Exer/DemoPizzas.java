package Ch_10_Prog_Exer;

/**
 * Class description:  Ch 10-7 Prog Exer.  Uses Pizza class (parent) and DeliveryPizza class (child class)
 * 
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DemoPizzas 
{
	public static void main(String[] args) 
	{
		final int standardFee = 5;
		final int reducedFee = 3;
		final int costsBulkOrder = 15;
		String pizzaIngredients, address;
		double pizzaCost, deliveryCost;
		
		Pizza custOrder;
		DeliveryPizza custOrder1;
		//orderItem = ("sausage and onion pizza", 14.99, 5.00, "22 Acorn Street, APT 882");
		
		pizzaIngredients = ("bacon, pepperoni, hamburger");
		address = ("1741B Embleton Cres");
		pizzaCost = 18.99;
				
		if (pizzaCost > 15)
		{
			deliveryCost = reducedFee;
		}
		else 
			deliveryCost = standardFee;
		
		custOrder1 = new DeliveryPizza(pizzaIngredients, pizzaCost, address);
		
		custOrder1.display();
		
		pizzaIngredients = "Canadian bacon and pineapple";
		pizzaCost = 20.99;
		custOrder = new Pizza(pizzaIngredients, pizzaCost);
		custOrder.display();
		

	}

}
