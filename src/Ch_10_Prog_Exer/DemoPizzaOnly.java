package Ch_10_Prog_Exer;

public class DemoPizzaOnly {

	public static void main(String[] args) 
	{
		//final int standardFee = 5;
		//final int reducedFee = 3;
		//final int costsBulkOrder = 15;
		String pizzaIngredients;
		double pizzaCost;
		
		Pizza custOrder1;
		//orderItem = ("sausage and onion pizza", 14.99, 5.00, "22 Acorn Street, APT 882");
		
		pizzaIngredients = ("Canadian bacon and pineapple");
		pizzaCost = 20.99;
		
		custOrder1 = new Pizza(pizzaIngredients, pizzaCost);
		
		custOrder1.display();

	}

}

