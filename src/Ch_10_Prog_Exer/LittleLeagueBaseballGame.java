package Ch_10_Prog_Exer;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class LittleLeagueBaseballGame extends BaseballGame
{
	private int INNINGS = 6;
	
	@Override
	public int getInnings()
	{
		return INNINGS;
	}

}
