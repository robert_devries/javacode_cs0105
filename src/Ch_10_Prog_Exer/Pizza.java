package Ch_10_Prog_Exer;

/**
 * Class description: This is the parent class and goes with DeliveryPizza class (parent) and DemoPizzas application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Pizza 
{
	private String description;
	private double price;
	
	public Pizza (String desc, double cost)
	{
		description = desc;
		price = cost;
	}
	
	public void display()
	{
		System.out.println(description + " pizza   Price: $" + price);		//must have 3 spaces between pizza and price
	}

}
