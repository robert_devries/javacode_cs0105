package Ch_10_Prog_Exer;

import java.util.Scanner;

/**
 * Ch 10, Prog Exer 10-10.  This is the child class for CollegeEmployee (child class) descending from Person class
 * 
 * Person
 *     	-> CollegeEmployee
 *        	-> Faculty
 *   	-> Student 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  30 August 2021
 */
public class Faculty extends CollegeEmployee
{
    private boolean isTenured = false;
    Scanner input = new Scanner(System.in);
    @Override
        public void setData()
        {
            // write your code here
            super.setData();
       System.out.print("Is Tenured :");
       isTenured = input.nextBoolean();
        }
        
    @Override
        public void display()
        {
            // write your code here
            super.display();
       if(isTenured)
       System.out.println("Faculty memeber is tenured.");
       else
       System.out.println("Faculty memeber is not tenured.");
        }
}