package Ch_10_Prog_Exer;


/**
 * Class description: Ch 10-8 Prog Exer.  This is the parent class to the child class LabCourse and UseCourse application
 * 
 * Create a class named CollegeCourse that includes the following data fields:
 * 		dept (String) - holds the department (for example, ENG)
 * 		id (int) - the course number (for example, 101)
 * 		credits (double) - the credits (for example, 3)
 * 		price (double) - the fee for the course (for example, $360).
All of the fields are required as arguments to the constructor, except for the fee, which is calculated at $120 per credit hour. 
Include a display() method that displays course data. For example, if dept is SE, id is 225, and credits is 3, output from display() method should be:
 *		SE225
 *		Non-lab course
 *		3.0 credits
 *		total fee is $360.0
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date; 27 August 2021
 */
public class CollegeCourse 
{
	protected String dept;
	protected int id;
	protected double credits;
	private double price;
	
	public CollegeCourse(String department, int ID, double creds)
	{
		dept = department;
		id = ID;
		credits = creds;
		price = 120 * creds;		//$120 per credit hour
	}
	public void display()
	{
		System.out.println(dept + id + "\nNon-lab course" + "\n" + credits + " credits" + "\nTotal fee is $" + price);
	}
}
