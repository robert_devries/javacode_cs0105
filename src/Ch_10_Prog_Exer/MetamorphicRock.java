package Ch_10_Prog_Exer;

/**
 * Class description: Ch 10-9, Prog Exer.  this is a child class to Rock class and DemoRock application
 * Create three child classes named IgneousRock, SedimentaryRock, and MetamorphicRock. 
 * The constructors for these classes require parameters for the sampleNumber and weight. 
 * Search the Internet for a brief description of each rock type and assign it to the description field 
 * using a method named setDescription inside of the constructor.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class MetamorphicRock extends Rock
{
	public MetamorphicRock(int sampleNum, double wt) 
	{
		super(sampleNum, wt);
		super.setDescription("Any rock can become a metamorphic rock \r\n" + 
				"if the rock is moved into an environment in which the \r\n" + 
				"minerals which make up the rock become unstable and \r\n" + 
				"out of equilibrium with the new environmental conditions.");
	}

}
