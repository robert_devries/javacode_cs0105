package Ch_10_Prog_Exer;

import java.util.Scanner;

/**
 * Class description: Ch 10-8, Prog Exer.  This application uses both the CollegeCourse (parent class) and LabCourse (child class)
 * 
 * Write an application named UseCourse that prompts the user for course information. 
 * If the user enters a class in any of the following departments, create a LabCourse object: BIO, CHM, CIS, or PHY. 
 * If the user enters any other department, create a CollegeCourse that does not include the lab fee. Then display the course data.
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class UseCourse 
{

	public static void main(String[] args) 
	{
		Scanner kb = new Scanner(System.in);
		String dept, inStr;
		String[] labCourses = {"BIO", "CHM", "CIS", "PHY"};
		int id, credits;
		int found = 0;
		int x;
		
		CollegeCourse course;
		LabCourse course2;
		System.out.println("Enter course information >> ");
		dept = kb.nextLine();
		System.out.println("Enter course id >> ");
		id = kb.nextInt();
		System.out.println("Enter credits >> ");
		credits = kb.nextInt();
		
		kb.close();

		if (dept.length() > 2)
		{
			inStr = dept.substring(0, 3);
			for (x = 0; x < 4;x++)
			{
				if (labCourses[x].equalsIgnoreCase(inStr))
				{
					found = 1;
				}
			}
		}
		
		if (found == 1)			//indicates this will be a LabCourse
		{
			course2 = new LabCourse(dept, id, credits);
			course2.display();
				
		}
		else					//will be a CollegeCourse
		{
			course = new CollegeCourse(dept, id, credits);
			course.display();
			
		}
		
	

	}

}
