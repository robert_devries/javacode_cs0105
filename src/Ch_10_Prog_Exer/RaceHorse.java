package Ch_10_Prog_Exer;

/**
 * Class description: Class description: Ch 10-1, Prog Exer:  this class is associated with Horse class (parent class) and DemoHorses application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class RaceHorse extends Horse				//note the keyword 'extends Horse' to show which is the parent class and methods now avail
{
	private int races;
	//private int countIncrement;
	
	public RaceHorse()
	{
		races = 0;
		//countIncrement = 0;
	}
	
	public void setRaces(int numRaces)
	{
		//countIncrement++;
		races += numRaces;
	}
	public int getRaces ()
	{
		return races;
	}
	//public int getCounts()
	//{
	//	return countIncrement;
	//}
}
