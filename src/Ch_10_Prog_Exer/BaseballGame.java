package Ch_10_Prog_Exer;



/**
 * Class description: Ch 10-6, Prog Exer BaseballGame class (this is the parent class)
 * Create a class named BaseballGame that contains data fields for 
 * 		two team names and scores for each team in each of nine innings. (String[] names = new String [2])  (this will be each baseball game teams)
 * 		names should be an array of two strings and scores should be a two-dimensional array of type int; 
 * 				the first dimension indexes the team (0 or 1) and the second dimension indexes the inning.  scores = new int[2][INNINGS]
 * Create get and set methods for each field. 
 * The get and set methods for the scores should require a parameter that indicates 
 * 		which inning�s score is being assigned or retrieved. 
 * 		Do not allow an inning score to be set if all the previous innings have not already been set. 
 * 		If a user attempts to set an inning that is not yet available, issue an error message.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 25 August 2021
 */

public class BaseballGame 
{
	private String[] names = new String[2];		//String name for each team(2 teams)
	protected int[][] scores;				//first index is the team, 2nd is the inning
	public static final int UNPLAYED = -99;		
	private final int INNINGS = 9;			//9 innings, suggest use this num to countdown to 0 innings - "game over"
	
	public BaseballGame()
	{
	   scores = new int[2][INNINGS];			//declares a matrix (2 rows x 9 columns composed of value of -99 in each index)
	   for(int t = 0; t < scores.length; ++t)
	      for(int i = 0; i < scores[t].length; ++i)
	         scores[t][i] = UNPLAYED;		//default constructor shows -99 to indicate unassigned gameplay at each index value
	}
	   
	public int getInnings()
	{
	   return INNINGS;
	}
	
	public void setNames (String teamName1, String teamName2)
	{
		names[0] = teamName1;
		names[1] = teamName2;
	}
	public String getName(int indexTeamName)
	{
		return names[indexTeamName];
	}
	public boolean setScore(int teamNum, int inningDone, int runs)
	{
		boolean valid;
	       if((inningDone == 0) || (inningDone < scores[teamNum].length && scores[teamNum][inningDone - 1] != UNPLAYED))
	       {
	            scores[teamNum][inningDone] = runs;
	            valid = true;
	        }
	        else
	        {
	            System.out.println("Need to play the earlier inning");
	            valid = false;
	        }
	       return valid;
	}
    public int getScore(int teamNum, int inningDone)
	{
	    if(scores[teamNum][inningDone] == UNPLAYED)
	    {
	        System.out.println("Inning hasn't been played");
	        return 0;
	    }
	    else
	    {
	        return scores[teamNum][inningDone];
	    } 
	}
    public void gameWinner()
    {
    	int totalTeam1 = 0, totalTeam2 = 0;
    	//3 possibilities, team0 wins, team1 wins, tie
    	for (int i = 0; i < getInnings(); i++)
    	{
    		totalTeam1 += scores[0][i];
    		totalTeam2 += scores[1][i];
    	}
    	if (totalTeam1 > totalTeam2)
    	{
    		System.out.println(names[0] + " win!");
    	}
    	else if (totalTeam1 < totalTeam2)
    	{
    		System.out.println(names[1] + " win!");
    	}
    	else
    		System.out.println("Game is tie!");
    }
}
