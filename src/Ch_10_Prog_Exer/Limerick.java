package Ch_10_Prog_Exer;

/**
 * Class description:  Prog Exer 10-4, DemoPoems application uses Poem (parent class) and 3 subclasses (Couplet, Limerick, Haiku) 
 * Create a class named Poem that contains the following fields:
 * 		title - the name of the poem (of type String)
 * 		lines - the number of lines in the poem (of type int)
 * Include a constructor that requires values for both fields. Also include get methods to retrieve field values. 
 * 
 * Create three subclasses: Couplet, Limerick, and Haiku. 
 * The constructor for each subclass requires only a title; the lines field is set using a constant value. 
 * A couplet has two lines, a limerick has five lines, and a haiku has three lines.
 * An example of the DemoPoems program is shown below:
 * Poem: The Raven   Lines: 84
 * Poem: True Wit   Lines: 2
 * Poem: There was an Old Man with a Beard   Lines: 5
 * Poem: The Wren   Lines: 3
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  19 August 2021
 *
 */

public class Limerick extends Poem
{
	public Limerick(String title3)
	{
		super(title3,  5);		//Limerick has 5 lines, isn't this hardcoded?
	}
}
