package Ch_10_Prog_Exer;



/**
 * Class description: Ch 10-2, Prog Exer  Uses ScentedCandle class and DemoCandles application
 * Create a class for the business named Candle that contains the following data fields:
 * 
 * color of type String
 * height of type int
 * price of type double
 * 
 * create get methods for all 3 and set methods for color and height but not price. Instead, when height is set, determine the price as $2 per inch
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  18 Aug 2021
 */
public class Candle 
{
	private String color;
	public int height;
	public double price;
		
	public void setColor(String c)
	{
		color = c;
	}
	public void setHeight(int h)
	{
		height = h;
		price = h * 2;
	}
	public String getColor()
	{
		return color;
	}
	public int getHeight()
	{
		return height;
	}
	public double getPrice()
	{
		return price;
	}
}
