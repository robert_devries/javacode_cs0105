package Ch_10_Prog_Exer;

import java.util.Scanner;

/**
 * Ch 10, Prog Exer 10-10.  This is the parent class for CollegeEmployee (child class) and
 * 
 * Person class 
 *     	-> CollegeEmployee class
 *        	-> Faculty class
 *   	-> Student class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class CollegeEmployee extends Person
{   
    private String ssn;
    private double annualSalary;
    private String dept;
    Scanner input = new Scanner(System.in);
    
    @Override
        public void setData()
        {            
            super.setData();
        System.out.print("Enter SSN :");
        ssn = input.next();

        System.out.print("Enter Annual Salary :");
        annualSalary = input.nextDouble();

        System.out.print("Enter Department :");
        dept = input.next();
        }
    @Override
        public void display()
        {
            // write your code here
            super.display();
        System.out.print("SSN :" + ssn);
        System.out.print(" Salary $" + annualSalary);
        System.out.println(" Department :" + dept);
        }
}
