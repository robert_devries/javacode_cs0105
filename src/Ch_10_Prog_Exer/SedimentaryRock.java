package Ch_10_Prog_Exer;

/**
 * Class description: Ch 10-9, Prog Exer.  this is a child class to Rock class and DemoRock application
 * Create three child classes named IgneousRock, SedimentaryRock, and MetamorphicRock. 
 * The constructors for these classes require parameters for the sampleNumber and weight. 
 * Search the Internet for a brief description of each rock type and assign it to the description field 
 * using a method named setDescription inside of the constructor.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class SedimentaryRock extends Rock
{
	public SedimentaryRock(int sampleNum, double wt) 
	{
		super(sampleNum, wt);
		super.setDescription("Sedimentary rocks are called secondary, \r\n"
				+ "because they are often the result of the accumulation \r\n"
				+ "of small pieces broken off of pre-existing rocks.");
	}

}
