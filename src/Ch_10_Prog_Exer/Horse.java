package Ch_10_Prog_Exer;

/**
 * Class description: Ch 10-1, Prog Exer:  this class is associated with RaceHorse (derived class or child class) and DemoHorses application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Horse 					//this is parent class and RaceHorse class implemented using 'extends Horse' in the header
{
	private String name;
	private String color;
	private int birthYear;
	
	public void setName(String horseName)
	{
		name = horseName;
	}
	public void setColor(String horseColor)
	{
		color = horseColor;
	}
	public void setBirthYear(int yr)
	{
		birthYear = yr;
	}
	public String getName()
	{
		return name;
	}
	public String getColor()
	{
		return color;
	}
	public int getBirthYear()
	{
		return birthYear;
	}
}
