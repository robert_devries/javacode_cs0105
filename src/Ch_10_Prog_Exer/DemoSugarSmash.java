package Ch_10_Prog_Exer;

import java.util.*;

import javax.security.auth.x500.X500Principal;

/**
 * Class description: Ch 10-5, Prog Exer DemoSugarSmash application uses PremiumSugarSmashPlayer class (child class) and SugarSmashPlayer class (parent class)
 * 
 * The developers of a free online game named Sugar Smash have asked you to develop a class named SugarSmashPlayer that holds data 
 * about a single player. The class contains the following fields:
 * 		idNumber - the player�s ID number (of type int)
 * 		name - the player's screen name (of type String)
 * 		scores - an array of integers that stores the highest score achieved in each of 10 game levels
 * Include get and set methods for each field. The get method for scores should require the game level to retrieve the score for. 
 * The set method for scores should require two parameters�one that represents the score achieved and 
 * 		one that represents the game level to be retrieved or assigned.
 * 
 * Display an error message if the user attempts to assign or retrieve a score from a level that is out of range for the array of scores. 
 * Additionally, no level except the first one should be set unless the user has earned at least 100 points at each previous level. 
 * If a user tries to set a score for a level that is not yet available, issue an error message.
 *
 * Create a class named PremiumSugarSmashPlayer that descends from SugarSmashPlayer. 
 * This class is instantiated when a user pays $2.99 to have access to 40 additional levels of play. 
 * As in the free version of the game, a user cannot set a score for a level unless the user has earned at least 100 points at all previous levels.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 19 August 2021
 */

public class DemoSugarSmash 
{
	public static void main(String[] args) 
	{
		final int MAX_RANGE = 10;		//reset this back to 10 scores per level
		final int EXTENDED_RANGE = 40;	//reset this back to 50 scores per level
		
		final int MIN_VALUE_SCORE = 100;
		SugarSmashPlayer player1 = new SugarSmashPlayer();
		SugarSmashPlayer player2 = new SugarSmashPlayer();

		PremiumSugarSmashPlayer premiumPlayer = new PremiumSugarSmashPlayer();
		
		String errorMsg = "Score outside range";
		String upgradeMsg = "Pay $2.99 to upgrade to PremiumSugarSmashPlayer (Y) for Yes, (N) for No >> ";
		final double paidUpgrade = 2.99;
		boolean full = false;
		int i, j, k;
		String inputIDMsg = "Enter ID number >> ";
		String inputUserNameMsg = "Enter Player Name >> ";
		String enterScoreMsg = "Enter score >> ";
		String enterLevelMsg = "Enter level >> ";
		
		
		int gamerID, scoreAchieved, levelAchieved = 0;
		int choice;
		int counter = 0;
		Scanner kb = new Scanner(System.in);
		
		//once paid (2.99) will need to create new player1 for PremiumSugarSmashPlayer
		
		
		//for regular SugarSmashPlayer mode
		while (!full)
		{
			player1.setIDNumber(100);
			player1.setName("Gamer1");
			player1[] = [110, 120, 130, 140, 150, 160, 170, 180, 190, 200];
			//get score and level
			for (j = 0; j < MAX_RANGE; j++)
			{
				System.out.print(enterScoreMsg);
				scoreAchieved = kb.nextInt();
				kb.nextLine();
				System.out.print(enterLevelMsg);
				levelAchieved = kb.nextInt();
				kb.nextLine();
				//set score and level
				if (levelAchieved == 1 || player1.getScores(levelAchieved - 1) > 100)			//first gamelevel achieved
				{
					player1.setScore(scoreAchieved, levelAchieved);
				}
				else 
					if (player1.getScores(levelAchieved - 1) < 100)
					{
						System.out.println("Need to score 100 or more in previous level");
					}
					else {
						System.out.println("Level is not available, play previous level");
					}
					
				
					
					
					
			}
		}
			System.out.println("All IDs, userNames, scores and levels set!");
		
		else
		{
			for (k = 0; k < MAX_PREM_PLAYERS; k++)		//max players is 3
			{
				premiumPlayer[k] = new PremiumSugarSmashPlayer();
				System.out.print(inputIDMsg);
				idNum = kb.nextInt();
				kb.nextLine();
				premiumPlayer[k].setIDNumber(idNum);
				System.out.print(inputUserNameMsg);
				playerName = kb.nextLine();
				premiumPlayer[k].setName(playerName); 
														//get score and level
				for (j = 0; j < EXTENDED_RANGE; j++)
				{
					System.out.print(enterScoreMsg);
					scoreAchieved = kb.nextInt();
					kb.nextLine();
					System.out.print(enterLevelMsg);
					levelAchieved = kb.nextInt();
					kb.nextLine();
					//set score and level
					premiumPlayer[j].setScores(scoreAchieved, levelAchieved);
				}
			}
			System.out.println("All IDs, userNames, scores and levels set!");
		}
		
		players[0].display();
		System.out.println("");
		premiumPlayer[0].display();
	}
}
