package Ch_10_Prog_Exer;


/**
 * Class description: Ch 10-2, Prog Exer.  uses Candle class (parent class) and DemoCandles application
 * this class overrides Candle class setHeight() method to set the price of a ScentedCandle object at $3 per inch.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ScentedCandle extends Candle
{
	private String scent;
	
	@Override								//this overrides parent class (super class) method to setHeight since different values for cost
	public void setHeight(int h)
	{
		this.height = h;
		this.price = h * 3;
	}
	
	public void setScent(String odour)
	{
		scent = odour;
	}
	public String getScent()
	{
		return scent;
	}
}
