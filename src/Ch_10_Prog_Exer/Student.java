package Ch_10_Prog_Exer;

import java.util.Scanner;

/**
 * Ch 10, Prog Exer 10-10.  This is the child class descending from Person class (parent class) 
 * 
 * Person
 *     	-> CollegeEmployee
 *        	-> Faculty
 *   	-> Student 
 *
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Student extends Person
{
    private String major;
    private double gpa;
    Scanner input = new Scanner(System.in);
    @Override
        public void setData()
        {
            // write your code here
            super.setData();
       System.out.print("Enter Major :");
       major = input.next();
       System.out.print("Enter GPA :");
       gpa = input.nextDouble();
        }

    @Override
        public void display()
        {
            // write your code here
            super.display();
       System.out.print("Major :" + major);
       System.out.println(" GPA :" + gpa);
        }
}
