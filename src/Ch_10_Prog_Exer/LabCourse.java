package Ch_10_Prog_Exer;


/**
 * Class description: Ch 10-8, Prog Exer.  this is child class to CollegeCourse and UseCourse application
 *  Create a subclass named LabCourse that adds $50 fee to the course price. 
 *  Override the parent class display() method to indicate that the course is a lab course and to display all the data. 
 *  For example, if dept is 'BIO', id is 201, and credits is 4, the output from the display() method should be:
 *  
 *  BIO201
 *  Lab Course
 *  4.0 credits
 *  Lab fee is $50
 *  Total fee is $530.0
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class LabCourse extends CollegeCourse
{
	final private double labFee = 50.0;
	final private double price;
	
	public LabCourse(String department, int ID, double creds) 
	{
		super(department, ID, creds);
		price = 120* creds + labFee;
	}
		
	@Override
	public void display()
	{
		System.out.println(super.dept + id + 
				"\nLab course" + 
				"\n" + super.credits + " credits" + 
				"\nLab fee is $" + labFee + 
				"\nTotal Fee is $" + price);
	}
}
