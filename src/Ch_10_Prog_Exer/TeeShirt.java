package Ch_10_Prog_Exer;

/**
 * Class description:  Prog Exer 10-3, this class (child class) extends parent class (TeeShirt) and is used by DemoTees
 * 
 * Create a TeeShirt class for Toby�s Tee Shirt Company. Fields include:
 * 		orderNumber - of type int
 * 		size - of type String
 * 		color - of type String
 * 		price - of type double
 * 
 * Create set methods for the order number, size, and color and get methods for all four fields. 
 * The price is determined by the size: $22.99 for XXL or XXXL, and $19.99 for all other sizes.
 * 
 * Create a subclass named CustomTee that descends from TeeShirt and includes a field named slogan 
 *	(of type String) to hold the slogan requested for the shirt, and include get and set methods for this field.
 *
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class TeeShirt {
	   private int orderNumber;
	   private String size;
	   private String color;
	   private double price;
	   
	   public void setOrderNumber(int num)
	   {
	       orderNumber = num;
	   }
	   public void setSize(String sz)
	   {
	       size = sz;
	       if ((sz.equalsIgnoreCase("XXL")) || (sz.equalsIgnoreCase("XXXL")))
	    		   {
	    	   			price = 22.99;
	    		   }
	       else 
	       {
	    	   price = 19.99;
	       }
	   }
	   public void setColor(String color)
	   {
	       this.color = color;
	   }
	   public int getOrderNumber()
	   {
	       return orderNumber;
	   }
	   public String getSize()
	   {
	       return size;
	   }
	   public String getColor()
	   {
	       return color;
	   }
	   public double getPrice()
	   {
	       return price;
	   }
}
