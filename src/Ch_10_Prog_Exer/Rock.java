package Ch_10_Prog_Exer;


/**
 * Class description: Ch 10-9, Prog Exer.  This is parent class.  
 * DemoRock application uses this parent class and 
 * Igneous, MetamorphicRock and SeimentaryRock are child classes
 * Create a class named Rock that acts as a superclass for rock samples collected and catalogued by a natural history museum. The Rock class contains the following fields:
 * 		sampleNumber - of type int
 * 		description - A description of the type of rock (of type String)
 * 		weight - The weight of the rock in grams (of type double)
 * Include a constructor that accepts parameters for the sampleNumber and weight. 
 * The Rock constructor sets the description value to Unclassified. Include the following methods:
 *		getSampleNumber
 *		getDescription
 *		setDescription
 *		getWeight
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  27 August 2021
 */
public class Rock 
{
	public int sampleNumber;
	public String description;
	public double weight;
	
	public Rock(int sampleNum, double mass)
	{
		sampleNumber = sampleNum;
		weight = mass;
		description = "Unclassified";
	}
	public int getSampleNumber()
	{
		return sampleNumber;
	}
	public String getDescription()
	{
		return description;
	}
	public double getWeight()
	{
		return weight;
	}
	public void setDescription(String desc)
	{
		description = desc;
	}
}
