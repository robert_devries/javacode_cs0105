import java.util.*;
public class Faculty extends CollegeEmployee
{
    private boolean isTenured = false;
    Scanner input = new Scanner(System.in);
    @Override
        public void setData()
        {
            // write your code here
            super.setData();
       System.out.print("Is Tenured :");
       isTenured = input.nextBoolean();
        }
        
    @Override
        public void display()
        {
            // write your code here
            super.display();
       if(isTenured)
       System.out.println("Faculty memeber is tenured.");
       else
       System.out.println("Faculty memeber is not tenured.");
        }
}
