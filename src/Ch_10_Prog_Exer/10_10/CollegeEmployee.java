import java.util.*;
public class CollegeEmployee extends Person
{   
    private String ssn;
    private double annualSalary;
    private String dept;
    Scanner input = new Scanner(System.in);
    @Override
        public void setData()
        {

            // write your code here
            super.setData();
        System.out.print("Enter SSN :");
        ssn = input.next();

        System.out.print("Enter Annual Salary :");
        annualSalary = input.nextDouble();

        System.out.print("Enter Department :");
        dept = input.next();
        }
    @Override
        public void display()
        {
            // write your code here
            super.display();
        System.out.print("SSN :" + ssn);
        System.out.print(" Salary $" + annualSalary);
        System.out.println(" Department :" + dept);
        }
}
