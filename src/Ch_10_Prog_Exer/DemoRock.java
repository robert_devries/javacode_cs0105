package Ch_10_Prog_Exer;


/**
 * Class description: Ch 10-9, Prog Exer.  DemoRock application uses Rock class (Parent class)
 * Create three child classes named IgneousRock, SedimentaryRock, and MetamorphicRock. 
 * The constructors for these classes require parameters for the sampleNumber and weight. 
 * Search the Internet for a brief description of each rock type and assign it to the description field 
 * using a method named setDescription inside of the constructor.
 * 
 * Sample is shown below:
 * Sample # 123 weighs 4.5
 * Description:   Unclassified
 * 
 * Sample # 234 weighs 14.9
 * Description:   Igneous rocks are crystalline solids 
 * which form directly from the cooling of magma.
 *
 * Sample # 345 weighs 18.3
 * Description:   Sedimentary rocks are called secondary,
 * because they are often the result of the accumulation 
 * of small pieces broken off of pre-existing rocks.
 * 
 * Sample # 456 weighs 7.0
 * Description:   Any rock can become a metamorphic rock
 * if the rock is moved into an environment in which the 
 * minerals which make up the rock become unstable and 
 * out of equilibrium with the new environmental conditions.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 28 August 2021
 */
public class DemoRock 
{

	public static void main(String[] args) 
	{
		Rock rock = new Rock(123, 4.5);
		IgneousRock rockIg = new IgneousRock(234, 14.9);
		SedimentaryRock rockSed = new SedimentaryRock(345, 18.3);
		MetamorphicRock rockMet = new MetamorphicRock(456, 7.0);
		
		System.out.println("Sample # " + rock.getSampleNumber() + " weighs " + rock.getWeight() + "\nDescription:   " + rock.getDescription());
		System.out.println("");
		System.out.println("Sample # " + rockIg.getSampleNumber() + " weighs " + rockIg.getWeight() + "\nDescription:   " + rockIg.getDescription());
		System.out.println("");
		System.out.println("Sample # " + rockSed.getSampleNumber() + " weighs " + rockSed.getWeight() + "\nDescription:   " + rockSed.getDescription());
		System.out.println("");
		System.out.println("Sample # " + rockMet.getSampleNumber() + " weighs " + rockMet.getWeight() + "\nDescription:   " + rockMet.getDescription());
		
	}

}
