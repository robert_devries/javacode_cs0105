package Ch_10_Prog_Exer;

import java.util.Scanner;

/** Description:  Ch 10, Prog Exer 10-10.  This is the parent class for CollegeEmployee (child class) and
 * 
 * Person
 *     	-> CollegeEmployee
 *        	-> Faculty
 *   	-> Student 
 * 
 */

public class Person
{
    private String firstName;
    private String lastName;
    public String address;
    private String zip;
    private String phone;
    Scanner input = new Scanner(System.in);
    public void setData()
    {
        // write your code here
         System.out.print("Enter first name :");
       firstName = input.next();

       System.out.print("Enter last name :");
       lastName = input.next();

       input.nextLine();
      
       System.out.print("Enter address :");
       address = input.nextLine();

       System.out.print("Enter zip :");
       zip = input.next();

       System.out.print("Enter phone :");
       phone = input.next();
    }
    public void display()
    {
        // write your code here
        System.out.println(firstName+" "+lastName+" "+address+" "+zip+" "+phone);
    }
}
