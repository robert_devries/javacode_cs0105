package Ch_10_Prog_Exer;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class HighSchoolBaseballGame extends BaseballGame
{
	private int INNINGS = 7;
	
	public int getInnings()
	{
		return INNINGS;
	}
	
}
