package Ch_10_Prog_Exer;



/**
 * Class description: Ch 10-7, This is the child class and goes with Pizza class (parent) and DemoPizzas application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DeliveryPizza extends Pizza
{
	double deliveryFee;
	String address;
	
	
	public DeliveryPizza(String desc, Double cost, String addr)
	{
		super(desc, cost);		//default parent class constructor
		address = addr;
		if (cost > 15)
		{
			deliveryFee = 3;
		}
		else 
			deliveryFee = 5;
	}
	public void display()
	{
		super.display();		//default parent class method
		System.out.println("Deliver to: " + address + ". " + "Fee is " + deliveryFee);
	}

}
