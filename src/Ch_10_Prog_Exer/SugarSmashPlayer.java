package Ch_10_Prog_Exer;

import java.util.Scanner;

/**
 * Class description: Ch 10-5, Prog Exer DemoSugarSmash application uses PremiumSugarSmashPlayer class (child class) and SugarSmashPlayer class (parent class)
 * 
 * The developers of a free online game named Sugar Smash have asked you to develop a class named SugarSmashPlayer that holds data 
 * about a single player. The class contains the following fields:
 * 		idNumber - the player�s ID number (of type int)
 * 		name - the player's screen name (of type String)
 * 		scores - an array of integers that stores the highest score achieved in each of 10 game levels
 * Include get and set methods for each field. The get method for scores should require the game level to retrieve the score for. 
 * The set method for scores should require two parameters�one that represents the score achieved and 
 * 		one that represents the game level to be retrieved or assigned.
 * 
 * Display an error message if the user attempts to assign or retrieve a score from a level that is out of range for the array of scores. 
 * Additionally, no level except the first one should be set unless the user has earned at least 100 points at each previous level. 
 * If a user tries to set a score for a level that is not yet available, issue an error message.
 *
 * Create a class named PremiumSugarSmashPlayer that descends from SugarSmashPlayer. 
 * This class is instantiated when a user pays $2.99 to have access to 40 additional levels of play. 
 * As in the free version of the game, a user cannot set a score for a level unless the user has earned at least 100 points at all previous levels.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 19 August 2021
 *
 */

public class SugarSmashPlayer 
{
	int idNumber;
	String name;
	int[] scores = new int[10];
	
	public void setIDNumber(int id)
	{
		idNumber = id;
	}
	public void setName(String userName)
	{
		name = userName;
	}
	public void setScore(int score, int gameLevel)		//game level is between 1 - 10, value will be reduced by 1 for indexing
	{	
		Scanner kb = new Scanner(System.in);
		boolean withinScore = false;
		int indexPosition = gameLevel - 1;			
		if ((indexPosition >= 0) && (indexPosition < 10))
		{
			while (!withinScore)
			{
				if (indexPosition == 0 || scores[indexPosition - 1] >= 100)					//this captures level 1 score only
				{
					scores[indexPosition] = score;
					withinScore = true;
				}
				else {
						
					System.out.println("Previous level score must be greater than 100 >> ");
					score = kb.nextInt();
					kb.nextLine();						
				} 
					
			}
			
		}
		//error outside range of array (in this case MAX_RANGE)
		else {
			System.out.print("You are attempting to assign a score that is outside the range");
		}
		kb.close();
		
	}
	public int getIDNumber()
	{
		return idNumber;
	}
	public String getName()
	{
		return name;
	}
	public int getScores(int gameLevelIndex)
	{
		return scores[gameLevelIndex -1];
	}
	public void display()
	{
		System.out.println("User ID# " + idNumber + ", Online Name: " + name + ",  Stats are:");
		for (int i = 0; i < scores.length; i++)
			System.out.print("  Level " + (i + 1) + " score: " + scores[i] + "\n");
	}

	
}
