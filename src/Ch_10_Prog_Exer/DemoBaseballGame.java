package Ch_10_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 10-6, Prog Exer uses BaseballGame class
 * Also include a method named display in DemoBaseballGame.java that determines 
 * 		the winner of the game after scores for the last inning have been entered. (For this exercise, assume that a game might end in a tie.)
 *
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DemoBaseballGame 
{
	public static void main(String[] args) 
	{
		//create baseball instance
		BaseballGame game1 = new BaseballGame();
		boolean gameOver = false;
		boolean inningValid = false;
		Scanner kb = new Scanner(System.in);
		final int TEAM_NAME1 = 0, TEAM_NAME2 = 1;			//reqmts stated that team1 is indexed at 0, team2 indexed at 2
		
		//game triggered by # of innings played
		int inningPlayed = 0, inningUserInput = 0;
		int scoredRuns = 0;
				
		//need input vars for team names
		String teamName1, teamName2;
		
		System.out.print("Enter home team name >> ");
		teamName1 = kb.nextLine();
		
		System.out.print("Enter visiting team name >> ");
		teamName2 = kb.nextLine();
		game1.setNames(teamName1, teamName2);		//BaseballGame field name[0] set
		
		//loop till gameOver
		while (!gameOver)
		{
			//input inning
			System.out.print("Enter inning played >> ");
			inningUserInput = kb.nextInt();
			System.out.print("Enter score for team " + teamName1 + " >> ");
			scoredRuns = kb.nextInt();
			inningValid = game1.setScore(TEAM_NAME1, (inningUserInput - 1), scoredRuns);
			if (inningValid)
			{
				System.out.print("Enter score for team " + teamName2 + " >> ");
				scoredRuns = kb.nextInt();
				inningValid = game1.setScore(TEAM_NAME2, (inningUserInput - 1), scoredRuns);
				inningPlayed = inningUserInput;
			}
			if (inningPlayed == game1.getInnings())
				gameOver = true;
			
					//increment inningPlayed for current inning
		}
		display(game1);
		kb.close();
	}
	 public static void display(BaseballGame bGame)
	{
		 final int NUM_TEAMS = 2;
		 int teamNum = 0, inningScores = 0;
		 for (teamNum = 0; teamNum < NUM_TEAMS; teamNum++)
	     {
			 System.out.print("Team " + bGame.getName(teamNum) + " scored:");
			 System.out.println("");
			 for (inningScores = 0; inningScores < bGame.getInnings(); inningScores++)
			 {
				 System.out.println(bGame.getScore(teamNum, inningScores));
			 }
	     }
		 
		 bGame.gameWinner();
	}
}
