package Ch9_Figures;

import java.util.ArrayList;

/**
 * Class description: Ch 9, Figure 9-5 Using ArrayList's and their methods (Note: the declaration uses brackets instead of [ ] since its a method
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 3 August 2021
 */
public class ArrayListDemo 
{
	public static void main(String[] args) 
	{
		ArrayList<String> names = new ArrayList<String>();
		names.add("Abigail");			//Arraylist was empty, now Abigail is added
		display(names);
		names.add("Brian");				//ArrayList now has 2 names (size = 2)
		display(names);
		names.add("Zachary");			//ArrayList now has 3 names (size = 3)
		display(names);
		names.add("Christy");			//ArrayList now has 4 names (size = 4)
		display(names);
		names.remove(1);				//Arraylist removed 1 name (size = 3)
		display(names);
		names.set(0,  "Annette");		//ArrayList replaced a name with a new name (size = 3)
		display(names);

	}
	public static void display(ArrayList<String> names)
	{
		System.out.println("\nThe size of the list is " + names.size());
		for (int x = 0; x < names.size(); ++x)
			System.out.println("position " + x + " Name: " + names.get(x));
	}

}
