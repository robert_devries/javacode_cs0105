package Ch9_Figures;

import java.util.Scanner;

/**
 * Class description: Ch 9-6, Fig: 9-24 Enumeration types and creation (NOTE: enum is declared within a class, not within main method)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 3 Aug 2021
 */
public class EnumDemo 
{
	enum Month {JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC};		// enum class created in its own method and not within other methods
	
	public static void main(String[] args) 
	{
		Month birthMonth;			// A Month type variable is declared
		String userEntry;
		int position;
		int comparison;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("The months are:");
		for(Month month : Month.values())			// enhanced for loop displays each value
			System.out.print(month + " ");
		System.out.print("\n\nEnter the first three letter of " + "your birth month >> ");
		userEntry = keyboard.nextLine().toUpperCase();
		birthMonth = Month.valueOf(userEntry);		// valueOf() converts string to an enumeration value
		System.out.println("You entered " + birthMonth);
		position = birthMonth.ordinal();					// ordinal () gets position of string
		System.out.println(birthMonth + " is in position " + position);
		System.out.println("So its month number is " + (position + 1));
		comparison = birthMonth.compareTo(Month.JUN);			// compareTo() compares entered string to the JUN constant.
		if(comparison < 0)
			System.out.println(birthMonth + " is earlier in the year than " + Month.JUN);
		else
			if (comparison > 0)
				System.out.println(birthMonth + " is later in the year than " + Month.JUN);
			else 
				System.out.println(birthMonth + " is " + Month.JUN);

	}

}
