package Ch9_Figures;

import java.util.*;
import javax.swing.*;

/**
 * Class description: Ch 9, Figure 9-17, uses Java Array classes including binarySearch 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 03 August 2021
 */
public class VerifyCode 
{
	public static void main(String[] args) 
	{
		char[] codes = {'B', 'E', 'K', 'M', 'P', 'T'};  //Output will display the position (element location) of any of the chars within this array.  
		String entry;
		char userCode;
		int position;
		entry = JOptionPane.showInputDialog(null, "Enter a product code >>");
		userCode = entry.charAt(0);
		position = Arrays.binarySearch(codes, userCode);
		if(position >= 0)
			JOptionPane.showMessageDialog(null,  "Position of " + userCode + " is " + position);
		else
			JOptionPane.showMessageDialog(null,  userCode + " is an invalid code");
		
	}
}
