package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import static java.nio.file.StandardOpenOption.*;

/**
 * Class description: Ch 13-7 Figure 13-30, Create an Employee record based on a KEY field;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 15 Sept 2021
 */

public class CreateOneRandomAccessRecord
{
	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\RandomEmployees.txt");		//Data.txt is in dir
		String s = "002,Newmann,12.25" + System.getProperty("line.separator");		//default record (string value for all new records).  
		final int RECSIZE = s.length();
		byte[] data = s.getBytes();														//*000 - will be key field, followed by 7 spaces (for name), 00.00 for salary
		ByteBuffer buffer = ByteBuffer.wrap(data);
		FileChannel fc = null;					//default ID for KEY and used as array limiting value
		
		try
		{
			fc = (FileChannel)Files.newByteChannel(filePath1,  READ, WRITE);
			fc.position(2 * RECSIZE);			//bcuz employee number is 2, posn is set to 2 times size of each record
			fc.write(buffer);
			fc.close();
		}
		catch(Exception e) 
		{
			System.out.println("IO Exception");
		}
	}
}
