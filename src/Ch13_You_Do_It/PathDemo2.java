package Ch13_You_Do_It;

import java.nio.file.*;	
import java.util.*;

/**
 * Class description: Ch 13-2c, Figure 13-4 (about creating paths to files
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class PathDemo2 
{
	public static void main(String[] args) 
	{
		String name;
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a file name >> ");		//this statement could be included in a try....catch block
		name = keyboard.nextLine();
		Path inputPath = Paths.get(name);				//takes input file name and creates "Relative" path
		System.out.println("Inputpath name is " + inputPath);
		Path fullPath = inputPath.toAbsolutePath();		//Takes above "Relative" path and establishes "Absolute" path from root to destination folder
		System.out.println("Full path is " + fullPath.toString());		//if the input represents a relative Path, program creates an absolute path 
																			//by assigning the file to the current directory.

	}

}
