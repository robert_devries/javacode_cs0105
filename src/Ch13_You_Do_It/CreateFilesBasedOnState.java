package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.text.*;

/**
 * Class description: Ch 13-8b You_Do_it (all instructions to build code are listed there)
 *
 * You write a class that prompts the user for customer data and assigns the data to one of two files depending on the customer�s state of residence. 
 * This program assumes that Wisconsin (WI) records are assigned to an in-state file and that all other records are assigned to an out-of-state file. 
 * First, you will create
 * 		empty files to store the records, and 
 * 		then you will write the code that places each record in the correct file
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 17 Sept 2021
 */
		
public class CreateFilesBasedOnState
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		Path inStateFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\InStateCusts.txt");
		Path outOfStateFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\OutOfStateCusts.txt");
		final String ID_FORMAT = "000";				//3 digits for account number (will end up using Integer.parseInt to convert to 'int')
		
		final String NAME_FORMAT = "          ";			//10 spaces
		final int NAME_LENGTH = NAME_FORMAT.length();		
		final String HOME_STATE = "WI";
		final String BALANCE_FORMAT = "0000.00";			//customer's balance up to 9999.99
		String delimiter = ",";
		String s = ID_FORMAT + delimiter + NAME_FORMAT + delimiter +							//build generic customer string by assembling the pieces
				HOME_STATE + delimiter + BALANCE_FORMAT + System.getProperty("line.separator");		//Record size is calculated from the dummy record.
		final int RECSIZE = s.length();
		
		FileChannel fcInState = null;
		FileChannel fcOutState = null;
		String idString;
		int id;
		String name;
		String state;
		double balance;
		final String QUIT = "999";
		
		createEmptyFile(inStateFile, s);		//method below where blank template files are created. Later, files can be written 
		createEmptyFile(outOfStateFile, s);			//* Method accepts Path for a file and the String that defines the record format.
		
		try
		{
			fcInState = (FileChannel)Files.newByteChannel(inStateFile,  CREATE, WRITE);
			fcOutState = (FileChannel)Files.newByteChannel(outOfStateFile,  CREATE, WRITE);
			System.out.print("Enter customer account number >> ");
			idString = input.nextLine();
			while(!(idString.equals(QUIT)))
			{
				id = Integer.parseInt(idString);
				System.out.print("Enter name of customer >> ");
				name = input.nextLine();
				StringBuilder sb= new StringBuilder(name);
				sb.setLength(NAME_LENGTH);
				name = sb.toString();
				System.out.print("Enter state >> ");
				state = input.nextLine();
				System.out.print("Enter balance >>");
				balance = input.nextDouble();
				input.nextLine();								//absorbs the Enter key value left in the input stream
				DecimalFormat df= new DecimalFormat(BALANCE_FORMAT);	//Decimal format will be 
				s = idString + delimiter + name + delimiter + state + delimiter + df.format(balance) + System.getProperty("line.separator");
				byte data[] = s.getBytes();						//convert the constructed string above to an array of bytes,
				ByteBuffer buffer = ByteBuffer.wrap(data);			//* then wrap the array into a ByteBuffer
				if(state.equals(HOME_STATE)) 
				{
					fcInState.position(id * RECSIZE);
					fcInState.write(buffer);
				}
				else
				{
					fcOutState.position(id * RECSIZE);
					fcOutState.write(buffer);
				}
				System.out.print("Enter next customer account number or " + QUIT + " to quit >>");
				idString = input.nextLine();
			}
			fcInState.close();
			fcOutState.close();
		}
		catch (Exception e)
		{
			System.out.println("Error message: " + e);
		}
		
	}
	public static void createEmptyFile(Path file, String s)		//creates blank file template used later for writing
	{
		final int NUMRECS = 1000;		//creates maximum 1000 records
		try 
		{
			OutputStream outputStr = new BufferedOutputStream(Files.newOutputStream(file, CREATE));		//step 1, buffered stream
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStr));				//step 2, buffered writer
			
			for (int count = 0; count < NUMRECS; ++count)
			{
				writer.write(s, 0, s.length());
			}
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Error message: " + e);
		}
	}
}
