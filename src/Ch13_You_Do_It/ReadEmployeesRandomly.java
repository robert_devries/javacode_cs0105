package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;



/**
 * Class description: Ch 13-8b Figure 13-39, Uses previously created file (RandomEmployees.txt ) and reads employee data randomly
 * 												
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 10 Oct 2021
 */

public class ReadEmployeesRandomly
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\RandomEmployees.txt");		//Data.txt is in dir
		String s = "000,       ,00.00" + System.getProperty("line.separator");
		final int RECSIZE = s.length();
		byte[] data = s.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(data);
		FileChannel fc = null;
		String idString;
		int id;
		final String QUIT = "999";
				
		try
		{
			fc = (FileChannel)Files.newByteChannel(filePath1, READ, WRITE);
			System.out.print("Enter employee ID number or " + QUIT + " to quit >> ");
			idString = keyboard.nextLine();
			while(!idString.equals(QUIT))
			{
				id = Integer.parseInt(idString);
				buffer = ByteBuffer.wrap(data);
				fc.position(id * RECSIZE);
				fc.read(buffer);
				s = new String(data);
				System.out.println("ID #" + idString + "  " + s);
				System.out.print("Enter employee ID number or " + QUIT + " to quit >> ");
				idString = keyboard.nextLine();
			}
			fc.close();
		}
		catch(Exception e) 
		{
			System.out.println("Error message: " + e);
		}
		
	}
}
