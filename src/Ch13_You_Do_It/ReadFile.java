package Ch13_You_Do_It;

import java.io.*;
import java.nio.file.*;


/**
 * Class description: Ch 13-4 Using file systems to output data to screen (**** added more import classes)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ReadFile
{
	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Grades.txt");    //**** added this line
		InputStream input = null;		// **** clears the buffer

		try
		{
			input = Files.newInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			String s = null;		//**** sets s to null to ensure null value was set
			s = reader.readLine();				// *** this line initializes the while 's' variable with data
			while (s != null)
			{
				System.out.println(s);
				s = reader.readLine();
			}
				
			input.close();
		}
		catch (Exception e)
		{
			System.out.println("Message: " + e);
		}
		System.out.println("File called Grades.txt was read.");

	}

}
