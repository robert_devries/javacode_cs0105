package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import static java.nio.file.AccessMode.*;		//verify if file exists and pgm has access to it using the checkAccess() method
import java.io.IOException;

/**
 * Class description: Ch 13-2b Figure 13-2, using the Path information to establish a filename;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sept 2021
 */
public class PathDemo3
{
	public static void main(String[] args) 
	{
		Path filePath = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It");
		System.out.println("Path is " + filePath.toString());	//use toString() method to display to screen (doesn't include \\, only 1)
		
		try
		{
			filePath.getFileSystem().provider().checkAccess(filePath,  READ, WRITE, EXECUTE);
			System.out.println("File can be read, write and executed");
		}
		catch(IOException e)
		{
			System.out.println("File cannot be used for this application");
		}
	}
}
