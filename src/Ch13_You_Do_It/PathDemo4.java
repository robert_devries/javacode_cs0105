package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import static java.nio.file.AccessMode.*;
import java.io.IOException;

/**
 * Class description: Ch 13-2b Figure 13-2, using the Path information to establish a filename;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sept 2021
 */
public class PathDemo4
{
	public static void main(String[] args) 
	{
		Path filePath = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Data5.txt");
				
		try
		{
			Files.delete(filePath);
			System.out.println("File or directory is deleted");
		}
		catch(NoSuchFileException e)
		{
			System.out.println("No such file or directory");
		}
		catch(DirectoryNotEmptyException e)
		{
			System.out.println("Directory is not empty");
		}
		catch(SecurityException e)
		{
			System.out.println("No permission to delete");
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
