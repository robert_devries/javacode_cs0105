package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import java.nio.file.attribute.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.text.*;

/**
 * Class description: Ch 13-8b You_Do_it (instructions to build code is listed there) Creates 2 text files with ID, Name, State, Balance
 * 																		*** One file for "InStateCusts.txt" and the other "OutOfStateCusts.txt"
 *
 * The program uses either of the files just created ("InStateCusts" or "OutOfStateCusts").
 * 		This pgm will prompt the user to enter the filename to be used and setup all necessary variables and constants
 * 		A few statistics about the file will be displayed
 * 		The nondefault contents of the file will be displayed sequentially
 * 		a selected record from the file will be accessed directly
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 18 Sept 2021
 */
		
public class ReadStateFile
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		String fileName;
		System.out.print("Enter name of the file to use >> ");	//use InStateCusts.txt or OutOfStateCusts.txt
		fileName = keyboard.nextLine();							//this line and below line are concatenated. This is file name not path 
		fileName = "C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\" + fileName;	//first slash is java ignore statement
		Path file= Paths.get(fileName);
		final String ID_FORMAT = "000";				//3 digits for account number (will end up using Integer.parseInt to convert to 'int')
		
		final String NAME_FORMAT = "          ";		//10 spaces;		
		final int NAME_LENGTH = NAME_FORMAT.length();			//***variable not used??? so why keep it???
		final String HOME_STATE = "WI";					  //length is 2
		final String BALANCE_FORMAT = "0000.00";			//customer's balance up to 9999.99
		String delimiter = ",";
		String s = ID_FORMAT + delimiter + NAME_FORMAT + delimiter +							//build generic customer string by assembling the pieces
				HOME_STATE + delimiter + BALANCE_FORMAT + 
				System.getProperty("line.separator");		//"line.separator" accesses OS method to determine OS specific line separator chars.
		final int RECSIZE = s.length();					//stores length of each record (ID, name, State, balance = 4 fields)
		
		//next 2 lines below, i added to see the file size length;
		System.out.println("Length of each record is >> " + RECSIZE);
		System.out.println("Filesize is Length of each record x 1000 records, which is >> " + RECSIZE*1000);
		
		byte data[] = s.getBytes();
		final String EMPTY_ACCT = "000";
		String[] array = new String [4];
		double balance;
		double total = 0;
	
		try 
		{
			BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
			System.out.println("\nAttributes of the file:");
			System.out.println("Creation time " + attr.creationTime());
			System.out.println("Size " + attr.size());
		} 
		catch (Exception e) 
		{
			System.out.println("IO Exception - file " + fileName + " Is not found");		//** I added, this will be absolute path to file location
			e.printStackTrace();		//could have used getStackTrace(), but that would only provide an address label - not useful
		}
		
		try 
		{
			InputStream iStream = new BufferedInputStream(Files.newInputStream(file));		//InputStream is an abstract class, hence cannot create object 
			BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));			//instead, used BufferedInputStream called iStream
			System.out.println(("\nAll non-default records:\n"));
			s = reader.readLine();
			
			while(s != null)						//while more data to read
			{
				array = s.split(delimiter);				//split the String using the comma delimiter
				if(!array[0].equals(EMPTY_ACCT))			//test the first split element, the account number and proceed only it is not "000"
				{
					balance = Double.parseDouble(array[3]);		//balance
					System.out.println("ID #" + array[0] + " " + array[1] + array[2] + "  $" + array[3]);
					total += balance;							//increment total balance
				}
				s = reader.readLine();
			}
			System.out.println("Total of all balances is $" + total);
			reader.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Message: " + e);
		}
		
		try 					//declares a FileChannel and ByteBuffer and then prompts the user for and accepts an account to search for in file
		{
			FileChannel fc= (FileChannel)Files.newByteChannel(file, READ);
			ByteBuffer buffer = ByteBuffer.wrap(data);
			int findAcct;
			System.out.print("\nEnter account to seek >> ");
			findAcct = keyboard.nextInt();
			
			fc.position(findAcct * RECSIZE);		//position of desired record in file by multiplying the record # by the file size.
			fc.read(buffer);							//read selected record into the ByteBuffer
			s = new String(data);							//convert the associated byte array to a String that you can display
			System.out.println("Desired record; " + s);
			
			
		} 
		catch (Exception e) 
		{
			System.out.println("Message: " + e);
		}
	
	}
}
