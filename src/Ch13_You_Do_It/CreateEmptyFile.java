package Ch13_You_Do_It;

import java.nio.file.*;
import java.io.*;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;

/**
 * Class description: Fig 13-30:  creates blank template for records
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CreateEmptyFile {


	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\EmptyFile.txt");
		String composition = "000,          ,          ,0000.00" + System.getProperty("line.separator");
		byte[] data1 = composition.getBytes();
		//ByteBuffer buffer = ByteBuffer.wrap(data1);
		final int NUMRECS = 1000;
		
		try 
		{
			OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			for (int count = 0; count < NUMRECS; ++count)
				writer.write(composition, 0, composition.length());
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Error in message formatting: " + e);
		}

	}

}
