package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import static java.nio.file.AccessMode.*;
import java.util.Scanner;

/**
 * Class description: Ch 13-7 Figure 13-30, Uses previously created file (RandomEmployees.txt ) and inserts
 * 												a new record in the positions input into the application based on screen outputs (see line 31);
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 17 Sept 2021
 */

public class ReadEmployeesSequentially
{
	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\RandomEmployees.txt");		//Data.txt is in dir
		String s = "";
		String[] array = new String [3];
		
		String delimiter = ",";
		int id;
		String stringId;
		String name;
		double payRate;
		double gross;
		final double HRS_IN_WEEK = 40;
		double total = 0;
				
		try
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(filePath1));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			System.out.println();
			s = reader.readLine();
			while(s != null)
			{
				array = s.split(delimiter);
				stringId = array[0];
				id = Integer.parseInt(array[0]);
				if(id != 0)
				{
					name = array[1];
					payRate = Double.parseDouble(array[2]);
					gross = payRate * HRS_IN_WEEK;
					System.out.println(("ID# " + stringId + "  " + name + "   $" + payRate + "   $" + gross));
					total += gross;
				}
				s = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception" + e);
		}
		System.out.println("  Total gross payroll is $" + total);
	}
}
