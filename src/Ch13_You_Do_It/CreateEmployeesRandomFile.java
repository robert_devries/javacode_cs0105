package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

/**
 * Class description: Ch 13-7 Figure 13-30, Uses previously created file (RandomEmployees.txt ) and inserts
 * 												a new record in the positions input into the application based on screen outputs (see line 31);
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 15 Sept 2021
 */

public class CreateEmployeesRandomFile
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\RandomEmployees.txt");		//Data.txt is in dir
		String s = "000,       ,00.00" + System.getProperty("line.separator");		//record to be created.  
		final int RECSIZE = s.length();													//*000 - will be key field, followed by 7 spaces (for name), 00.00 for salary
		FileChannel fc = null;					//default ID for KEY and used as array limiting value
		String delimiter = ",";
		String idString;
		int id;
		String name;
		String payRate;
		final String QUIT = "9999";
				
		try
		{
			fc = (FileChannel)Files.newByteChannel(filePath1, READ, WRITE);
			System.out.print("Enter employee ID Number >> ");
			idString = input.nextLine();
			while(!idString.equals(QUIT))
			{
				id = Integer.parseInt(idString);
				System.out.print("Enter name for employee # " + idString + " >> ");
				name = input.nextLine();
				System.out.print("Enter pay rate >> ");
				payRate = input.nextLine();
				s = idString + delimiter + name + delimiter + payRate + System.getProperty("line.separator");
				byte[] data = s.getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(data);
				fc.position(id * RECSIZE);		
				fc.write(buffer);
				System.out.print("Enter next ID number or " + QUIT + " to quit >> ");
				idString = input.nextLine();
			}
			fc.close();
		}
		catch(IOException e) 						//error checking for valid ID numbers and pay rates was not included 
		{												//IOT concentrate on writing of a random access file
			System.out.println("IO Exception");
		}
	}
}
