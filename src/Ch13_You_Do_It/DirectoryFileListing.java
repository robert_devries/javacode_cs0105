package Ch13_You_Do_It;

import java.io.File;

/**
 * Class description: From the net.  https://www.youtube.com/watch?v=bFOUb2OhLIQ
 * 
 * Shows how to determine whether the location is a file or a directory
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DirectoryFileListing {

	
	public static void main(String[] args) 
	{
		File dr = new File("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It");
		File [] allFiles = dr.listFiles();
		
		for (int i = 0; i < allFiles.length; i++)
		{
			if (allFiles[i].isFile())
			{
				System.out.println("File name: " + allFiles[i]);
			}
			else if (allFiles[i].isDirectory())
			{
				System.out.println("Directory name: " + allFiles[i]);
			}
		}
	}

}
