package Ch13_You_Do_It;

import java.nio.file.*;
import java.io.*;
import java.util.Scanner;

/**
 * Class description: Ch 13 -5 Creating and Using Sequential Data Files
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 15 Sept 2021
 */
public class ReadEmployeeFile 
{

	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Employees.txt");
		String s = "";
		
		try
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(file));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			s = reader.readLine();
			while(s != null)
			{
				System.out.println(s);
				s = reader.readLine();
			}
			reader.close();
		}
		catch(Exception e)
		{
			System.out.println("Message: " + e);
		}

	}

}
