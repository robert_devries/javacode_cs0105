package Ch13_You_Do_It;

import java.util.*;

/**
 * Class description: Check to see how StringBuilder compiles a string to be converted to an int format
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ExperimentStringBuilder {

	
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		String accountNum;
		String accountNum1;
		int accountNumberLength = 4;
		int convertAcctNum, division;
		StringBuilder numToString = new StringBuilder();
		
		System.out.print("Enter a number for the account >> ");
		accountNum = keyboard.nextLine();
		
		numToString.setLength(accountNumberLength);
		accountNum1 = accountNum.toString();
		convertAcctNum = Integer.parseInt(accountNum1);
		
		System.out.println("Account number is >> " + accountNum1);
		System.out.println("Account number length is >> " + accountNum1.length());
		
		System.out.println("Account number converted is >> " + convertAcctNum);
		division = convertAcctNum/5;
		System.out.println("After division >> " + division);
		System.out.println("After addition >> " + (convertAcctNum + 25) + " should be 50 ");
		keyboard.close();

	}

}
