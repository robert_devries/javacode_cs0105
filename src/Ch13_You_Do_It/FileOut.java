package Ch13_You_Do_It;

import java.io.*;
import java.nio.file.*;
import static java.nio.file.StandardOpenOption.*;

/**
 * Class description: Ch 13-4 Using file systems to output data to screen (**** added more import classes)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class FileOut
{
	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Grades.txt");    //**** added this line
		String s = "ABCDF";
		byte[] data = s.getBytes();
		OutputStream outputStream = null;
		try
		{
			outputStream = new BufferedOutputStream(Files.newOutputStream(file, CREATE));			//**** added this line to create a file
			outputStream.write(data);
			outputStream.flush();
			outputStream.close();
		}
		catch (Exception e)
		{
			System.out.println("Message: " + e);
		}
		System.out.println("File called Grades.txt was created.");

	}

}
