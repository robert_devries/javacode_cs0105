package Ch13_You_Do_It;

import java.io.*;

/**
 * Class description: From Tim Greenwood for reading/writing files to a text file
 * 						after discussing with Ali on Zoom
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 02 Nov 2021
 */

public class ReadWriteTimFromAli 
{
	private static final String BOOKS_FILE = "books_blank.txt";
	private static final String BOOKS_FILE2 = "bookslist.txt";
	private static final String ACCOUNTS = "C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\InStateCusts.txt";
	private static final String LETTERS = "C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\letters.dat";

	// 9795804215224;813.52 STE;3;6;Of Mice and Men;John Steinbeck;1937;D  (Books record format)
	
	public static void main(String[] args) throws IOException
	{
		
//*************** Read example (Pulls out all the ISBN fields from the BOOKS_FILE2 (bookslist.txt)
//		int lineNumber = 0;
//		
//		BufferedReader br = new BufferedReader(new FileReader(BOOKS_FILE2));	
//		String line = br.readLine();
//
//		while (line != null) {
//			lineNumber++;
//			String[] parts = line.split(";"); 
//			long isbn = Long.parseLong(parts[0]);
//			System.out.println(isbn);
//
//			line = br.readLine();
//		}
//		br.close();
		
//*************** Write example	(Writes a series of text strings to BOOKS_FILE (books_blank file)
//		
//		PrintWriter pw = new PrintWriter(BOOKS_FILE);
//		
//		pw.println("test");
//		pw.println("123");
//		pw.println("456");
//
//		pw.close();
		
//*************** Random Access examples	
		
		long byteNum;
		final int CHAR_SIZE = 2;	// if writing to a .dat file char take two bytes, double takes four etc.
		char ch;
		
		RandomAccessFile randomFile = new RandomAccessFile(ACCOUNTS, "r");	// "r" is for read.  Use "rw" for read write
		
		//find sixth character in LETTERS (letters.dat file)
//		byteNum = CHAR_SIZE * 5;
		
//		randomFile.seek(byteNum);
//		ch = randomFile.readChar();
//		System.out.println(ch);
//		randomFile.close();

		//find 11th character in LETTERS
//		byteNum = CHAR_SIZE  * 10;
//		
//		randomFile.seek(byteNum);
//		ch = randomFile.readChar();
//		System.out.println(ch);
//		randomFile.close();
		
		//Get the 104th record previously filled using CreateFilesBasedOnState prgm in Unit 13
		
		String s = "000,          ,WI,0000.00" + System.getProperty("line.separator"); //  s.getlength() = 31
		final int RECSIZE = s.length();

		int recordNum = RECSIZE * (104 - 1);	//line 103 is line 104 in reality due to index 0 of arrays
		randomFile.seek(recordNum);
		
		String line = randomFile.readLine();
		System.out.println(line);

	}
}
