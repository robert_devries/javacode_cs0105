package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.io.*;

/**
 * Class description: Ch 13-2b Figure 13-7, using the ByteBuffer to access and write data at various locations within the ByteBuffer array;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 15 Sept 2021
 */

public class RandomAccessTest
{
	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Numbers.txt");		//Data.txt is in dir
		String s = "XYZ";
		byte[] data = s.getBytes();
		ByteBuffer out = ByteBuffer.wrap(data);
		FileChannel fc = null;
		
		try
		{
			fc = (FileChannel)Files.newByteChannel(filePath1, READ, WRITE);
			fc.position(0);
			while(out.hasRemaining())
				fc.write(out);
			out.rewind();
			fc.position(22);
			while(out.hasRemaining())
				fc.write(out);
			out.rewind();
			fc.position(12);
			while(out.hasRemaining())
				fc.write(out);
			fc.close();
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
