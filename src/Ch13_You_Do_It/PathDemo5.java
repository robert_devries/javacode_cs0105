package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.AccessMode.*;
import java.io.IOException;

/**
 * Class description: Ch 13-2b Figure 13-2, using the Path information to establish a filename;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sept 2021
 */
public class PathDemo5
{
	public static void main(String[] args) 
	{
		Path filePath = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Data.txt");
				
		try
		{
			BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);
			System.out.println("Creation time " + attr.creationTime());
			System.out.println("Last modified time " + attr.lastModifiedTime());
			System.out.println("Size " + attr.size());
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
