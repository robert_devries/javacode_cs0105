package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import static java.nio.file.AccessMode.*;
import java.io.IOException;

/**
 * Class description: Ch 13-2b Figure 13-2, using the Path information to establish a filename;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sept 2021
 */
public class PathDemo6
{
	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Data.txt");		//Data.txt is in dir
		Path filePath2 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Data1.txt");	//Data1.txt is in dir
		
		try
		{
			BasicFileAttributes attr1 = Files.readAttributes(filePath1, BasicFileAttributes.class);
			BasicFileAttributes attr2 = Files.readAttributes(filePath2, BasicFileAttributes.class);
			FileTime time1 = attr1.creationTime();
			FileTime time2 = attr2.creationTime();
			
			
			System.out.println("file1's creation time is " + time1);
			System.out.println("file1's modified time " + attr1.lastModifiedTime());
			System.out.println("file2's creation time is " + time2);
			System.out.println("file2's modified time is " + attr2.lastModifiedTime());
			
			if (time1.compareTo(time2) < 0)
				System.out.println("file1 was created before file2");
			else
				if(time1.compareTo(time2) > 0)
					System.out.println("file1 was created after file2");
				else 
					System.out.println("file1 and file2 were created at the same time");
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
