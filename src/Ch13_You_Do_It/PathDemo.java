package Ch13_You_Do_It;

import java.nio.file.*;						//This is the required library class to be imported (nio - stands for new input/output)

/**
 * Class description: Ch 13-2b Figure 13-2, using the Path information to establish a filename;
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sept 2021
 */
public class PathDemo 
{
	public static void main(String[] args) 
	{
		Path filePath = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Grades.txt");	//full path to file location
		//Path filePath1 = fs.getPath("C:" + fs.getSeparator() + "Users" + fs.getSeparator())	//this is incomplete and crap
		int count = filePath.getNameCount();								//returns 8 for each folder/subfolder and destination file Grades.txt
		System.out.println("Path is " + filePath.toString());				//full path description to String
		System.out.println("File name is " + filePath.getFileName());		//name of file to retrieve - Grades.txt
		System.out.println("There are " + count + " elements in the file path");	//display number of elements
		
		for (int x = 0; x < count; ++x)										//displays each element with its corresponding name identifier
			System.out.println("Element " + x + " is " + filePath.getName(x));
		

	}

}
