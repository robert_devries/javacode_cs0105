package Ch13_You_Do_It;

import java.io.*;


/**
 * Class description: Ch 13-4 Using file systems to output data to screen
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ScreenOut 
{
	public static void main(String[] args) 
	{
		String s = "ABCDF";
		byte[] data = s.getBytes();
		OutputStream output = null;
		try
		{
//			output = System.out;		//these 4 lines are the same as the active lines 25 - 27
//			output.write(data);
//			output.flush();
//			output.close();
//			output = System.out;
			System.out.println("new");
			System.out.write(data);
			System.out.flush();
			System.out.close();
		}
		catch (Exception e)
		{
			System.out.println("Message: " + e);
		}

	}

}
