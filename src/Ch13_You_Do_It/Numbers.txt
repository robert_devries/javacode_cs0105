012345678901234567890123456789

XYZ345678901XYZ5678901XYZ567890123456789

012         234       234
0 posn      12 posn   22 posn

After RandomAccessTest application, above string was replaced at exact locations (0, 22, 12) and in that order.

at posn 0 with XYZ, at position 22 with XYZ and position 12 with XYZ
