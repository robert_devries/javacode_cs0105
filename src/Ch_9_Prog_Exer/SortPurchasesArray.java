package Ch_9_Prog_Exer;

import java.util.ArrayList;

import Test_Area.NextLineScrape_;

import java.util.*;

/**
 * Class description:  Ch 9, Prog Exer 9-7, uses Purchase class in Ch_6_Prog_Exer as objects in this application
 * In Chapter 6 Exercise 15, you created a class named Purchase. Each Purchase contains 
 * 		an invoice number (invoiceNumber), 
 * 		amount of sale (saleAmount), 
 * 		amount of sales tax (tax). 
 * Add get and set methods for the invoice number and sale amount fields so their values can be used in comparisons. 
 * Include a display method to display the invoice number, sale amount, and tax information.
 * 
 * Next, implement the SortPurchaseArray program that 
 * 		declares an array of five Purchase objects and 
 * 		prompt a user for their values. 
 * 
 * Then, in a loop that continues until a user inputs a 
 * 		sentinel value of Z, 
 * ask the user whether the Purchase objects should be 
 * 		sorted and displayed in invoice number order or sale amount order by entering the values of I and S respectively.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 10 August 2021
 */

public class SortPurchasesArray 
{
	public static void main(String[] args) 
	{
		final int MAX_SIZE = 5;
		Purchase[] partsCost = new Purchase[MAX_SIZE];
		Scanner kb = new Scanner(System.in);
		String invoiceMsg = "Enter invoice number>> ";
		String saleAmtMsg = "Enter sale amount>> ";
		String sortCriteria = "Sort Purchases by (I)nvoice number, or (S)ale amount? ";
				String sortOrQuit = " or enter Z to quit >> ";
		String sortedSaleAmount = "Sorted by sale amount";
		String sortedInvoice = "Sorted by invoice number";
		String inputSortCriteria;
		int inputInvoiceNum; 
		double inputSalesAmount;
		int i, j, k, m;
		char select;
		char Sentinel = 'Z';
		boolean DONE = false;
		
		
		for (i = 0; i < MAX_SIZE; i++)
		{
			System.out.print(invoiceMsg);
			inputInvoiceNum = kb.nextInt();
			System.out.print(saleAmtMsg);
			inputSalesAmount = kb.nextDouble();
			kb.nextLine();
			Purchase item = new Purchase();
			item.setInvoiceNumber(inputInvoiceNum);
			item.setSaleAmount(inputSalesAmount);
			partsCost[i] = item;
		}
		System.out.println("");
		
		System.out.print(sortCriteria);
		inputSortCriteria = kb.nextLine();
		select = inputSortCriteria.charAt(0);
		select = Character.toUpperCase(select);
		
		if (select == 'I')
		{
			System.out.println("");
			sortByInvoice(partsCost);
			display(partsCost, sortedInvoice);
		}
		
		if (select == 'S')
		{
			System.out.println("");
			sortBySaleAmount(partsCost);
			display(partsCost, sortedSaleAmount);
		}
		
		while (!DONE)
		{
			System.out.print(sortCriteria + sortOrQuit);
			inputSortCriteria = kb.nextLine();
			select = inputSortCriteria.charAt(0);
			select = Character.toUpperCase(select);
			
			if (select == 'I')
			{
				System.out.println("");
				sortByInvoice(partsCost);
				display(partsCost, sortedInvoice);
			}
			
			if (select == 'S')
			{
				System.out.println("");
				sortBySaleAmount(partsCost);
				display(partsCost, sortedSaleAmount);
			}
			else if (select == Sentinel)
				DONE = true;
		}
		
	}
	public static void sortBySaleAmount(Purchase[] array) 
	{
        Purchase temp;
        int comparisonsToMake = array.length - 1;
        int i, j;
        
        for (i = 0; i < array.length; i++)
        {
        	for (j = 0; j < comparisonsToMake; j++)
        	{
        		if (array[j].getSaleAmount() > array[j + 1].getSaleAmount())
        		{
        			temp = array[j + 1];
        			array[j + 1] = array[j];
        			array[j] = temp;
        		}
        	}
        	--comparisonsToMake;
        }
    }
    public static void sortByInvoice(Purchase[] array) 
    {
    	Purchase temp;
        int comparisonsToMake = array.length - 1;
        int i, j;
        
        for (i = 0; i < array.length; i++)
        {
        	for (j = 0; j < comparisonsToMake; j++)
        	{
        		if (array[j].getInvoiceNumber() > array[j + 1].getInvoiceNumber())
        		{
        			temp = array[j + 1];
        			array[j + 1] = array[j];
        			array[j] = temp;
        		}
        	}
        	--comparisonsToMake;
        }
    }
    public static void display(Purchase[] p, String msg) 
    {
    	Purchase[] array = p;
    	int i;
        
    	System.out.println(msg);
    	System.out.println("");
        for (i = 0; i < array.length; i++)
        {
        	array[i].display();	
        }
        System.out.println("");
    }

}
