package Ch_9_Prog_Exer;

import java.util.*;
import java.math.*;

import Ch_6_Prog_Exer.CountByAnything;

/**
 * Class description: Ch 9, Prog Exer 9-2A, 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * The mean of a list of numbers is its arithmetic average. 
 * The median of a list is its middle value when the values are placed in order. 
 * For example, if an ordered list contains 1, 2, 3, 4, 5, 6, 10, 11, and 12, then the mean is 6, 
 * 		and their median is 5. 
 * 
 * Write an application that allows you to enter nine integers and displays the values, their mean, and their median.
 */
public class MeanMedian 
{
	
	public static void main(String[] args) 
	{
		final int MAX_INPUTS = 9;
		int[] userList = new int[MAX_INPUTS];
		int userInput, median = 0;
		int comparisonsToMake = userList.length - 1;			//end of for statement, include --comparisonsToMake to reduce counter
		int i, outerCount, innerCount, tempHold;												//since each pass will sink next higher value to the bottom of list
		double meanAvg = 0.0, mean = 0.0; 					//*** mean is average, median is the middle value in   the list
		
		Scanner keyboard = new Scanner(System.in);
		//System.out.println("Enter 9 integers to determine mean and median >> ");
		
		//get userInput for all 9 values
		for(i = 0; i < MAX_INPUTS; i++)
		{
			System.out.print("Enter number ");
			userInput = keyboard.nextInt();			//keep word case and sort by value of first letter
			userList[i] = userInput;				//results will be unsorted, merely inserted into array
			System.out.println(userList[i]);
		}
				
		//Need to sort array first before finding median (center of list), could have used ArrayList but wanted to have the bubblesort code
		for(outerCount = 0; outerCount < userList.length - 1; outerCount++)
		{
			for(innerCount = 0; innerCount < comparisonsToMake; innerCount++)
			{
				if(userList[innerCount] > userList[innerCount + 1])
				{
					tempHold = userList[innerCount];
					userList[innerCount] = userList[innerCount + 1];
					userList[innerCount + 1] = tempHold;
				}
			}
			--comparisonsToMake;		//reduces the number of comparisons in sorting of array
		}
		
		for (i = 0; i < MAX_INPUTS; i++)
		{
			mean += userList[i];
		}
		meanAvg = (mean/userList.length);		//mean avg is the total divided by number of input numbers
		//System.out.println(meanAvg);
		median = userList.length/2;
		median = userList[median];
		
		System.out.print("You entered: ");
		for (int j = 0; j < userList.length; j++)
			System.out.print(userList[j] + ", ");
		System.out.println();
		System.out.print("The mean is: " + meanAvg + " and the median is: " + median);
	
	}
}
