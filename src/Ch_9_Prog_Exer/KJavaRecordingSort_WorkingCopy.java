package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 9, Prog Exer 9-3A (there is no (B), this application uses Recording.java class as setters/getters
 * Radio station KJAVA wants a class to keep track of recordings it plays. 
 * Create a class named Recording that contains fields to hold methods for:
 * 		setting and getting a Recording�s title, artist, and playing time in seconds.
 *
 * Implement the RecordingSort application that instantiates five Recording objects and prompts the user for values for the data fields. Then prompt the user to enter which field the Recordings should be sorted by�(S)ong title, (A)rtist, or playing (T)ime. Perform the requested sort procedure, and display the Recording objects.
 *
 * The getter and setter methods for the song, artist, and playTime variables must be defined in the Recording class.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class KJavaRecordingSort_WorkingCopy 
{
	public static void main(String[] args) 
	{
		final int MAX_INPUTS = 3;		// ***** Reset back to 5
		Recording[] soundTrack = new Recording[MAX_INPUTS];			//Recording is an object array
		String inputEachArtist, inputEachSong;
		String userSongRecordingMsg = "Enter a song for recording ";					//need to include the iter # at end of msg
		String userArtistNameMsg = "Enter an artist for recording ";					//need to include the iter # at end of msg
		String userSongDurationMsg = "Enter the time for the recording in seconds ";		//need to include the iter # at end of msg
		String userFieldSort = ("by which field do you want to sort?\n(S)ong, (A)rtist, or (T)ime");
		String userSortSelection;
		int inputSongDuration;
		Scanner keyboard = new Scanner(System.in);
		int i, j, k;			//iter variables for arrays ...
		
		for(i = 0; i < soundTrack.length; i++)
		{
			System.out.println(userSongRecordingMsg + (i + 1));		// inputs for first iteration
			inputEachSong = keyboard.nextLine();			//
			System.out.println(userArtistNameMsg + (i + 1));			//
			inputEachArtist = keyboard.nextLine();			//
			System.out.println(userSongDurationMsg + (i + 1));		//
			inputSongDuration = keyboard.nextInt();			// inputs for first iteration complete
			keyboard.nextLine();							// clear keyboard buffer
		
			Recording temp = new Recording();				//new temp obj
			temp.setArtist(inputEachArtist);					//set field instances
			temp.setPlayTime(inputSongDuration);				//
			temp.setSong(inputEachSong);						//
			soundTrack[i] = new Recording();				//new soundTrack
			soundTrack[i] = temp;								//populate albums[i] array with temp instance (holds all song values)
			
		}
		System.out.println(userFieldSort);
		userSortSelection = keyboard.nextLine().toUpperCase();
		if (userSortSelection.equals("A"))
			sortByArtist(soundTrack);
		if (userSortSelection.equals("S"))
			sortBySong(soundTrack);
		if (userSortSelection.equals("T"))
			sortByTime(soundTrack);
		
		
	}// ************ end of main ******************
	public static void sortByArtist(Recording[] array) 
	{
        Recording swapHolder;
        int comparisonsToMake = array.length - 1;
        int k, m;
        
        for (k = 0; k < comparisonsToMake; k++)		//sort album by the artist field
        {
        	for (m = 0; m < comparisonsToMake; m++)
        	{
        		if(array[m].getArtist().charAt(0) > array[m + 1].getArtist().charAt(0))
        		{
        			swapHolder = array[m];
        			array[m] = array[m + 1];
        			array[m + 1] = swapHolder;
        		}
        	}
        }
        for (k = 0; k < array.length; k++)
        	System.out.println("artist: " + array[k].getArtist() + "  song: " + array[k].getSong() + "  time: " + array[k].getPlayTime());  
    }

    public static void sortBySong(Recording[] array) 
    {
        Recording swapHolder;		//
        int comparisonsToMake = array.length - 1;
        int k, m;
        
        for (k = 0; k < comparisonsToMake; k++)		//sort album by the artist field
        {
        	for (m = 0; m < comparisonsToMake; m++)
        	{
        		if(array[m].getSong().charAt(0) > array[m + 1].getSong().charAt(0))
        		{
        			swapHolder = array[m];
        			array[m] = array[m + 1];
        			array[m + 1] = swapHolder;
        		}
        	}
        }
        for (k = 0; k < array.length; k++)
        	System.out.println("artist: " + array[k].getArtist() + "  song: " + array[k].getSong() + "  time: " + array[k].getPlayTime());  
    }

    public static void sortByTime(Recording[] array) 
    {
    	Recording swapHolder;		//
        int comparisonsToMake = array.length - 1;
        int k, m;
        
        for (k = 0; k < comparisonsToMake; k++)		//sort album by the artist field
        {
        	for (m = 0; m < comparisonsToMake; m++)
        	{
        		if(array[m].getPlayTime() > array[m + 1].getPlayTime())
        		{
        			swapHolder = array[m];
        			array[m] = array[m + 1];
        			array[m + 1] = swapHolder;
        		}
        	}
        }
        for (k = 0; k < array.length; k++)
        	System.out.println("artist: " + array[k].getArtist() + "  song: " + array[k].getSong() + "  time: " + array[k].getPlayTime());  
    }

}
