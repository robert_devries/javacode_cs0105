package Ch_9_Prog_Exer;

import java.util.*;		//includes Scanner lib
import java.util.ArrayList;

/**
 * Class description: Ch 9, Prog Exer 9-9,
Create a class named Majors that includes an enumeration for the six majors offered by a college as follows: 
	1. ACC,
	2. CHEM,
	3. CIS,
	4. NG,
	5. HIS,
	6. PHYS.

Display the enumeration values for the user, and 
	then prompt the user to enter a major. 

Display the college division in which the major falls. 
	ACC and CIS are in the Business Division, 
	CHEM and PHYS are in the Science Division, and 
	ENG and HIS are in the Humanities Division. 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 August 2021
 */

public class Majors 
{
	enum Major {ACC, CIS, CHEM, PHYS, ENG, HIS};
	enum BusinessDiv {ACC, CIS};				//kind of useless since couldn't figure out how to use another enum for comparisons/equality
	enum ScienceDiv {CHEM, PHYS};
	enum HumanitiesDiv {ENG, HIS};
	
	public static void main(String[] args) 
	{
		Major discipline;
		BusinessDiv business;
		ScienceDiv science;
		HumanitiesDiv humanity;
		Scanner kb = new Scanner(System.in);
		String enterMajorMsg = "Enter your major >> ";
		String userInput;		 
		int i = 1, j = 3, k = 5, position;
		
		for(Major eachMajor : Major.values())
		{
			System.out.print(eachMajor + " ");
		}
		System.out.println("\n");
		System.out.print(enterMajorMsg);
		userInput = kb.nextLine().toUpperCase();
	
		discipline = Major.valueOf(userInput);
		position = discipline.ordinal();			//get location within enum of user selection (similar to element value...0, 1, 2, 3..)
		
		//System.out.println("Value of userInput >> " + userInput);
		//System.out.println("Value of discipline >> " + discipline);
		System.out.println("Value of position >> " + position);
		
		//System.out.print(" In main, Value of position is >> " + position + ", " + "Value of discipline.compareTO...>> " + discipline.compareTo(Major.CHEM));
		
		System.out.println("");
		
		if (position <= i)		//takes care of ACC and CIS (values 0, 1)
		{
			System.out.println("You enrolled in: " + discipline + " which is in the Business Division");
		}
		else if (position <= j)		//takes care of CHEM and PHYS (values 2, 3)
		{
			System.out.println("You enrolled in: " + discipline + " which is in the Science Division");
		}
		else if (position <= k)		//takes care of ENG and HIS (values 4, 5)
		{
			System.out.println("You enrolled in: " + discipline + " which is in the Humanities Division");
		}
		

	}

}
