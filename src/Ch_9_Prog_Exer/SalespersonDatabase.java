package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description:Ch 9 (Arrays and ArrayLists), Prog Exer 9-5, this application uses Salesperson class (contained in Ch_8_Prog_Exer)
 * 
 * Now, create an application that allows you to store an array that acts as a database of any number of Salesperson objects up to 20. 
 * While the user decides to continue, offer four options: 
 * 		to (A)dd a record to the database, 
 * 		to (D)elete a record from the database, 
 * 		to (C)hange a record in the database, 
 * 		or to (Q)uit. Then proceed as follows:
 * If the user selects the add option, issue an error message, Sorry - array is full -- cannot add a record, if the database is full. 
 * Otherwise, prompt the user for an ID number. 
 * 		If the ID number already exists in the database, issue the error message Sorry -- ID number already exists. 
 * 		Otherwise, prompt the user for a sales value and add the new record to the database.
 *
 * If the user selects the delete option, issue the error message, Cannot delete - no records in database, if the database is empty.
 * 		Otherwise, prompt the user for an ID number. 
 * 		If the ID number does not exist, issue the error message, Sorry - ID number #xxx does not exist in the database. 
 * 		Otherwise, do not access the record for any future processing.
 * 
 * If the user selects the change option, issue the error message, Database is empty -- cannot change record, if the database is empty. 
 * 		Otherwise, prompt the user for an ID number. 
 * 		If the requested record does not exist, issue the error message, Sorry - ID number #xxx does not exist in the database. 
 * 		Otherwise, prompt the user for a new sales value and change the sales value for the record.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 06 Aug 2021
 */

public class SalespersonDatabase 
{
	public static void main(String[] args) 
	{
		final int MAX_INPUTS = 10;		//***** chg this to "20" prior to upload
		boolean QUIT = false;
		Salesperson[] employee = new Salesperson[MAX_INPUTS];	//don't need to include capacity but it is available
		//Salesperson tempHolder;
		
		Scanner keyboard = new Scanner(System.in);
		String userMainMsg = "Do you want to (A)dd, (D)elete, or (C)hange a record or (Q)uit >> ";
		String userSelection;
		char choice;
		int count = 0;
				
		//System.out.println(userMainMsg);
				
		while (!QUIT) 		//&& (count != MAX_INPUTS)), was used for application termination
		{
			System.out.print(userMainMsg);
			userSelection = keyboard.nextLine().toUpperCase();
			choice = userSelection.charAt(0);
			if ((choice == 'A') || (choice == 'D') || (choice == 'C') || (choice == 'Q'))
			{
				if (choice == 'Q')
					QUIT = true;
				else if (choice == 'A' || choice == 'C')
				{
					if (choice == 'A')
					{						
						if (count == MAX_INPUTS)
						{
							System.out.println("Sorry - array is full - cannot add a record");							
						}
						else
							count = addOption(employee, count);			//count incremented on return of addOption method
					}
					else
					{
						if (count == 0)
						{
							System.out.println("Database is empty - cannot change record");
						}
						else
							changeOption(employee, count);
					}
				}
				else if (choice == 'D')
				{
					if (count == 0)
					{
						System.out.println("Cannot delete - no records in database");
					}
					else
					{
						count = deleteOption(employee, count);
					}
				}
			}
			//sort array display o/p (initial employee addition needs to be accounted for IOT sort)
			
			display(employee, count);
			
			//System.out.println();
		}
		//System.out.println("Terminated. Remove before upload");		// ***** Remove before uploading
	}
	public static int addOption(Salesperson[] array, int count) 	//returns the number of Salesperson's in array
	{
		String idMsg = "Enter salesperson ID >> ", SalesMsg = "Enter sales >> ";
		boolean match = false;
		int j;
		int inputID;
		double inputSales;
		Scanner keyboard = new Scanner(System.in);
		System.out.print(idMsg);
		inputID = keyboard.nextInt();
		
		for (j = 0; j < count; j++)
		{
			if (array[j].getId() == inputID)
			{
				System.out.println("Sorry - ID number already exists.");
				match = true;
			}
		}
		if(!match)
		{
			System.out.print(SalesMsg);
			inputSales = keyboard.nextDouble();
			array[count] = new Salesperson(inputID, inputSales);	
			count+= 1;
		}
	
		return count;
	}
    public static int deleteOption(Salesperson[] array, int count) 		//returns the number of Salesperson's in array
    {
    	int inputID;
		double inputSales;
		boolean match = false;
		String idMsg = "Enter ID to delete >> "; 
		//String SalesMsg = "Enter sales >>";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print(idMsg);
		inputID = keyboard.nextInt();
		keyboard.nextLine();					
				
		for (int m = 0; m < count && !match; m++)
		{
			if(array[m].getId() == inputID)
        	{
				//System.out.print("            Found the ID .. get rid of this statement before upload");
        		for (int r = m; r < count; r++)
        		{
        			array[r] = array[r+1];		//shift all Salesperson objects to the left to fill deleted array (this will not work for last element)
        											//a Salesperson obj from array (seems practical ID is not avail and count is decremented
        		}
        		match = true;								
        		count--;
        	}
		}
		if (!match)
		{
			System.out.println("Sorry - ID number # " + inputID + " does not exist in the database");
		}
		return count;
		
    }
    public static void changeOption(Salesperson[] array, int count) 	//edits a current record in Salesperson
    {    	
		int inputID;
		double inputSales;
		boolean match = false;
		String idMsg = "Enter ID to change >> ", SalesMsg = "Enter sales >> ";
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print(idMsg);
		inputID = keyboard.nextInt();
		keyboard.nextLine();					
		System.out.print(SalesMsg);
		inputSales = keyboard.nextDouble();		//locate ID field in array, change Sales to new sales (Salesperson.setSales(inputSales);
		keyboard.nextLine();
		
		for (int m = 0; m < count; m++)
		{
			if(array[m].getId() == inputID)
        	{
        		array[m].setSales(inputSales);
        		match = true;
        	}
		}
		if (!match)
			System.out.println("Sorry - ID number #" + inputID + " does not exist in the database");
				
    }
    public static void display(Salesperson[] array, int count) 			//displays the array contents
    {
    	Salesperson tempHolder;
    	int comparisonsToMake = count -1;
    	if (count == 1)
		{
			System.out.println("\nCurrent database:");
			System.out.println("ID #" + array[count - 1].getId() + "  $" + array[count - 1].getSales());
		}
		else
		{
			for (int m = 0; m < count; m++)
			{
				for (int j = 0; j < comparisonsToMake; j++)
				{
					if (array[j].getId() > array[j + 1].getId())
					{
						tempHolder = array[j];
						array[j] = array[j + 1];
						array[j + 1] = tempHolder;
					}
				}
				--comparisonsToMake;			//will reduce # of iters since largest ID will be at the end of array after inner loop exits the 1st time
			}
			System.out.println("\nCurrent database:");
	    	for (int k = 0; k < count; k++)
			{
				System.out.println("ID #" + array[k].getId() + "  $" + array[k].getSales());
			}
		}
    	//  Arrays.sort(array);				//could have used this method to sort the entire array - I wasn't aware of Arrays.sort() could sort objects
    	System.out.println("");
    }
}
