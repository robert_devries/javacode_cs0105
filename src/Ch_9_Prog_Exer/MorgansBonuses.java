package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 9, Prog Exer 9-10,
 * Morgan�s Department Store distributes bonuses to its salespeople after the holiday rush. Table 9-6 shows the bonuses, which are based on full weeks worked during the season and the number of positive online customer reviews. Write a program that allows a user to continuously enter values for the number of weeks worked and the number of positive reviews received and displays the appropriate bonus until the user enter a sentinel value: 99.
 *
 *An example of the program is shown below:

Enter number of full weeks worked or 99 to quit >> 2
Enter number of positive reviews received >> 3
Bonus for 2 weeks and 3 positive reviews is 42.0
Enter number of full weeks worked or 99 to quit >> 99

 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class MorgansBonuses 
{
	public static void main(String[] args) 
	{
		double[][] bonus = {{5.00, 9.00, 16.00, 22.00, 30.00},
                {10.00, 12.00, 18.00, 24.00, 36.00},
                {20.00, 25.00, 32.00, 42.00, 53.00},
                {32.00, 38.00, 45.00, 55.00, 68.00},
                {46.00, 54.00, 65.00, 77.00, 90.00},
                {60.00, 72.00, 84.00, 96.00, 120.00},
                {85.00, 100.00, 120.00, 140.00, 175.00}};
		
		String numWeeksMsg = "Enter number of full weeks worked or 99 to quit >> ";
		String posReviewsMsg = "Enter number of positive reviews received >> ";
		boolean isSentinel = false;
		final int MAX_WEEKS = 6; final int MAX_REVIEWS = 4;
		int sentinel = 99;
		int inputWeeks, inputReviews;
		int i, j, k;
		Scanner kb = new Scanner(System.in);
				
		while (!isSentinel)
		{
			System.out.print(numWeeksMsg);
			inputWeeks = kb.nextInt();
			kb.nextLine();
						
			if (inputWeeks == sentinel)
			{
				isSentinel = true;
			}
			else
			{				
				System.out.print(posReviewsMsg);
				inputReviews = kb.nextInt();
				kb.nextLine();
				if (inputWeeks > MAX_WEEKS)
				{
					inputWeeks = MAX_WEEKS;
				}
				if (inputReviews > MAX_REVIEWS)
				{
					inputReviews = MAX_REVIEWS;
				}
				System.out.print("Bonus for " + inputWeeks + " and " + inputReviews + " positive reviews is " + bonus[inputWeeks][inputReviews]);
				System.out.println("");			//new line and start again
			}
			
		}
		System.out.println("All done!");
	
	}
}
