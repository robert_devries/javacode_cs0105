package Ch_9_Prog_Exer;

/**
 * Class description: Ch 9, Prog Exer 9-3A (there is no B).  This is needed by KJavaRecording application
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  05 Aug 2021
 */
public class Recording {
	private String song;
    private String artist;
    private int playTime;
    public void setSong(String title) {
    	song = title;
    }
    public void setArtist(String name) {
    	artist = name;
    }
    public void setPlayTime(int time) {
    	playTime = time;
    }
    public String getSong() {
    	return song;
    }
    public String getArtist() {
    	return artist;
    }
    public int getPlayTime() {
    	return playTime;
    }

}
