package Ch_9_Prog_Exer;

/**
 * Class description: Ch 9, Prog Exer 9-6, CollegeCourseTimes class used by TimesAndInstructors application
 *
 * 
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 09 August 2021
 */
public class CollegeCourseTimes 
{
	private String courseID;
	private String time;
	private String professor;
	
	//did not create constructor - instead left it to java to implement default java constructor
	
	public String getCourseID() {
		return courseID;
	}
	public String getTime() {
		return time;
	}
	public String getProfessor() {
		return professor;
	}
	public void setCourseID (String crse) {
		courseID = crse;
	}
	public void setTime(String mtgTime) {
		time = mtgTime;
	}
	public void setProfessor(String prof) {
		professor = prof;
	}

}
