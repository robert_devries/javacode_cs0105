package Ch_9_Prog_Exer;

import java.util.*;			//util includes most prewritten array methods/classes



/**
 * Class description: Ch 9, Prog Exer 9-1, String Sort
 * Write an application containing an array of 15 String values 
 * (that are not originally in alphabetical order), and display them in ascending order. Save the file as StringSort.java.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class StringSort 
{

	
	public static void main(String[] args) 
	{
		final int MAX_VALUE = 15;
		String[] name = {"Bill", "Fred", "Max", "Harry", "Pat", "Tom", "Henry", "Rob", "Lloyd", "Gerry", "Perry", "Jeff", "Al", "Jo", "Kylie"}; 
		int i = 0;
		
		String[] values = {"mouse", "dog", "cat", "horse", "cow",
		         "moose", "tiger", "lion", "elephant", "bird", "hamster",
		         "guinea pig", "leopard", "aardvark", "hummingbird"};
		
		//sort array
		Arrays.sort(values, 0, MAX_VALUE - 1);		//strings are objects, this sort method uses (objects, start index, end index)
		Arrays.sort(name, 0, MAX_VALUE - 1);
		
		for(String eachName : name)
		{
			//i++;
			System.out.print(eachName + ", ");
		}
		System.out.println();

		for (String eachValue : values)
		{
			System.out.print(eachValue + ", ");
		}
				
		
	}

}
