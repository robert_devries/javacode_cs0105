package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 9, Prog Exer 9-7,
 * Create an application that contains an enumeration that represents the days of the week. 
 * Display a list of the days, and then prompt the user for a day. 
 * Display business hours for the chosen day. 
 * Assume that the business is open from 
 * 		11 - 5 on Sunday, 
 * 		9 - 9 on weekdays, and 
 * 		10 - 6 on Saturday.
 * 
 * The program should accept input values in the format SUN, MON, TUE, WED, THU, FRI, SAT.
 * 
 * An example of the program is shown below:
 * 
 * The days are:
 * SUN MON TUE WED THU FRI SAT
 * 
 * Enter the day code to find our hours >> MON
 * You entered MON
 * On MON our hours are 9 - 9
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 10 August 2021
 */

public class DayOfWeek_Enums
{
	enum Day {SUN, MON, TUE, WED, THU, FRI, SAT};	//enum is a data type, Day is the class (uppercase letter like all classes), enum constants which
														//are all caps since they are values that are constant and are not Strings
	public static void main(String[] args) 
	{
		//Day start;			//start variable declared (can also use Day start = Day.MON;)
		//start = Day.MON;			//enum constant (Mon) assigned to start variable (Day is a class, enum MON act like objects instantiated 
		Day selection;
		Scanner kb = new Scanner(System.in);					// from the class and can access all methods of the class (start.toString() is "MON"
		String initMsg = "The days are:";		//Nonstatic enum methods start.toString() is "MON", 
		String daysMsg = "Enter the day code to find our hours >> ";	//start.ordinal() is 1 (constant's posn), 
		String confirmInput = "You entered ";							//start.equals(Day.MON) is true
		String sundayHrs = "11 - 5";
		String weekdayHrs = "9 - 9";
		String saturdayHrs = "10 - 6";
		String userInput;   										//start.equals(Day.TUE) is false,
																	//start.compareTo(Day.FRI) is negative (since TUE is before FRI - in essence a lesser value), 			 
																	//start.compareTo(Day.SUN) is positive (because SUN is enum constant value of 0, and MON is 1),
		System.out.println(initMsg);								//start.compareTo(MON) is 0																
		for(Day eachDay : Day.values())					//Static enum methods DAY.valueOf("TUE") returns the TUE enum constant, 
		{													//Day.values() returns an array with 7 elements that contain the enum constants
			System.out.print(eachDay + " ");
		}
		System.out.println("\n");								//Day.values() returns an array with 7 elements that contain the enum constants
		System.out.print(daysMsg);
		userInput = kb.nextLine().toUpperCase();
		selection = Day.valueOf(userInput);
		System.out.println(confirmInput + selection);
		
		if (selection == Day.SUN)
		{
			System.out.println("On " + selection + " our hours are " + sundayHrs);
		}
		else if (selection == Day.SAT)
		{
			System.out.println("On " + selection + " our hours are " + saturdayHrs);
		}
		else
			System.out.println("On " + selection + " our hours are " + weekdayHrs);
	}
}
