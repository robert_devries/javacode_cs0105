package Ch_9_Prog_Exer;

import java.util.*;			//util includes most prewritten array methods/classes and includes ArrayList
import java.util.Collections;

/**
 * Class description: Ch 9, Prog Exer 9-1, String Sort
 * Write an application containing an array of 15 String values 
 * (that are not originally in alphabetical order), and display them in ascending order. Save the file as StringSort.java.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class StringSort2 
{
	public static void main(String[] args) 
	{
		final int MAX_VALUE = 5;		// *** the String[] name was mine before i went to the portal and then noted Ali's Strings
		ArrayList<String> name = new ArrayList<String>(); 		//ArrayLists are not restricted to max elements, will need to check limits
		int i, count = 0;
		
		final String QUIT = "zzz";
		String userInput;
		boolean isDone = false;
		Scanner keyboard = new Scanner(System.in);
		
		//first condition to check is userInput for "QUIT"
				
		while((!isDone) && (name.size() < MAX_VALUE))
		{
			System.out.println("Enter word >> ");
			userInput = keyboard.nextLine();			//keep word case and sort by value of first letter
			if (userInput.equals(QUIT))
			{
				isDone = true;
				System.out.print("Terminated");
				break;
			}
					
			if(name.size() != MAX_VALUE)
			{
				name.add(userInput);
			}
		}
	
		Collections.sort(name);			//this is from Ch 9-5, page 27
		
		for (String word : name)
			System.out.print(word + " ");
		
	}
}
