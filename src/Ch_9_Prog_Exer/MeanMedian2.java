package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 9 , Prog Exer 9-2B, 
 * Revise the MeanMedian2 class from Chapter 9 Exercise 2A so that the user can enter any number of values up to 20. 
 * If the list has an even number of values, the median is the numeric average of the values in the two middle positions. 
 * Allow the user to enter 9999 to quit entering numbers.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 05 Aug 2021
 */
public class MeanMedian2 
{
	
	public static void main(String[] args) 
	{
		final int MAX_INPUT = 20;
		final int QUIT = 9999;
		int[] nums = new int[MAX_INPUT];
	    int num = 0;
	    String list = "You entered: ";
	    int a = 0, b;
	    String entry = "Enter number " + a + " or 9999 to quit";
	    int temp;
	    double median = 0, mean = 0;
	    int total = 0;
	    int size=0, size1 = 0;
	    Scanner keyboard = new Scanner(System.in);
	    	    
	    for(a = 0; num != QUIT && a < nums.length; ++a)		//populate array with user inputs
	    {
	       System.out.println(entry);
	       num = keyboard.nextInt();
	       if (num != QUIT)
	       {
	    	   nums[a] = num;
	    	   total += nums[a];		//total all inputs for mean average
	       }  								
	    }
	    System.out.println("size is " + a);
	    if(num != QUIT)
	       size = nums.length;
	    else
	       size = a - 1;
	    System.out.println("size is " + size);
	    System.out.println(list);
	    for (int j = 0; j < size; j++)
			System.out.print(nums[j] + ", ");
	    int comparisonsToMake = size - 1;
	    for(a = 0; a < size - 1; ++a)						//sort array
	    {
	       for(b = 0; b < comparisonsToMake; ++b)
	       {
	          if(nums[b] > nums[b + 1])
	          {
	             temp = nums[b];
	             nums[b] = nums[b + 1];
	             nums[b + 1] = temp;
	          }
	       }
	       --comparisonsToMake;
	    }
	        
		if (size % 2 == 0)		//indicates even number
		{
			size1 = (size/2);	//provides left index locator in array, right index will be plus 1 as per below
			median = nums[size1] + nums[size1 - 1]; 	//add index[size1] + index[size1 + 1}
			median /= 2;							//divide by 2 to get the numeric average between the two middle positions
		}
		else
		{
			temp = size/2;		//this would be element 4 which is correct 
			median = nums[temp];
		}
		
		mean = (total*1.0/size);		//mean avg is the total divided by number of input numbers
		System.out.println();
		
		System.out.println("The mean is " + mean +
	         " and the median is " + median);

	}

}
