package Ch_9_Prog_Exer;

/**
 * Class description:Ch 9, (Arrays), Prog Exer 9-4, this class is needed by SalespersonSort.java application
 * In Chapter 8, Programming Exercise 6 you created a Salesperson class with fields for an:
 * 		 ID number and sales values. 
 * Now, create an application that allows a user to enter values for an array of seven Salesperson objects. 
 * Offer the user the choice of displaying the objects in ascending order by either (I)D number or (S)ales value.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 06 Aug 2021
 */
public class Salesperson 
{
		private int id;
		private double sales;
		Salesperson(int idNum, double amt)	//this now becomes the default construc
		{
			id = idNum;
			sales = amt;
		}
		public int getId()
		{
			return id;
		}
		public double getSales()
		{
			return sales;
		}
		public void setId(int idNum)
		{
			id = idNum;
		}
		public void setSales(double amt)
		{
			sales = amt;
		}
}



