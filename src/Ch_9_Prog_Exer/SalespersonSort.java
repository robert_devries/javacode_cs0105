package Ch_9_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 9, Prog Exer 9-4, this application uses Salesperson class to populate fields
 *
 *In Chapter 8, Programming Exercise 6 you created a Salesperson class with fields for an:
 * 		 ID number and sales values. 
 * Now, create an application that allows a user to enter values for an array of seven Salesperson objects. 
 * Offer the user the choice of displaying the objects in ascending order by either (I)D number or (S)ales value.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 06 Aug 2021
 */
public class SalespersonSort 
{
	public static void main(String[] args) 
	{
		final int MAX_VALUES = 3;
		Salesperson[] employeeStats = new Salesperson[MAX_VALUES];		//simply declare the new object, populate in array
		int userInputID;
		double userInputSales;
		String userIDMsg = "Enter an ID number ";
		String userSalesMsg = "Enter sales value";
		String userFieldSortMsg = "By which field do you want to sort?\n(I)d number of (S)ales";
		String inputUserSelection, inputSortID = "I", inputSortSales = "S";
		Scanner keyboard = new Scanner(System.in);
		int i, j, k, m;
		
		//populate the 7 Salespersons with ID and Salesamount
		for(i = 0; i < MAX_VALUES; i++)
		{
			System.out.println(userIDMsg);
			userInputID = keyboard.nextInt();
			System.out.println(userSalesMsg);
			userInputSales = keyboard.nextDouble();
			employeeStats[i] = new Salesperson(userInputID, userInputSales);		//user default constructor to populate fields
		}
		keyboard.nextLine();
		
		//get user info for sorting
		System.out.println(userFieldSortMsg);
		inputUserSelection = keyboard.nextLine().toUpperCase();
		if (inputUserSelection.equals(inputSortID))
			sortByID(employeeStats);
		else
			sortBySales(employeeStats);
		

	}
	public static void sortByID(Salesperson[] array)
	{
		Salesperson swap;
		int comparisonsToMake = array.length - 1;
        int k, m;
        
        for (k = 0; k < comparisonsToMake; k++)		//sort album by the artist field
        {
        	for (m = 0; m < comparisonsToMake; m++)
        	{
        		if(array[m].getId() > array[m + 1].getId())
        		{
        			swap = array[m];
        			array[m] = array[m + 1];
        			array[m + 1] = swap;
        		}
        	}
        }
        for (k = 0; k < array.length; k++)
        	System.out.println("ID " + array[k].getId() + "  sales: " + array[k].getSales());  
		System.out.println();
	}
	public static void sortBySales(Salesperson[] array)
	{
		Salesperson swap;
		int comparisonsToMake = array.length - 1;
        int k, m;
        
        for (k = 0; k < comparisonsToMake; k++)		//sort album by the artist field
        {
        	for (m = 0; m < comparisonsToMake; m++)
        	{
        		if(array[m].getSales() > array[m + 1].getSales())
        		{
        			swap = array[m];
        			array[m] = array[m + 1];
        			array[m + 1] = swap;
        		}
        	}
        }
        for (k = 0; k < array.length; k++)
        	System.out.println("sales " + array[k].getSales() + "  ID: " + array[k].getId());  
		System.out.println();
	}

}
