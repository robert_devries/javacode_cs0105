package Ch_9_Prog_Exer;

import java.util.*;

import javax.management.monitor.Monitor;

/**
 * Class description: Ch 9, Prog Exer 9-6, TimeAndInstructors
 * Write the TimesAndInstructors application that 
 * 		stores at least five different college courses (such as CIS101), 
 * 		the time it first meets in the week (such as Mon 9 am), and the instructor (such as Farrell) in a two-dimensional array.
 * Allow the user to enter a course name and display the corresponding time and instructor. 
 * 		If the course exists twice, display details for both sessions. 
 * 		If the course does not exist, display Invalid Entry: No Such Course.
 *
 *	Use the following values:				Example of program is shown below
 *
 * 	Course	Time			Instructor		Enter a course:
 *	CIS101	Mon 9 am		Farrell			MKT100
 * 	CIS210	Mon 11 am		Patel			Course: MKT100 Time: Tues 8:30 am Instructor: Wong
 * 	MKT100	Tues 8:30 am	Wong			
 * 	ACC150	Tues 6 pm		Deitrich			
 * 	CIS101	Fri 1 pm		Lennon				
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 09 August 2021
 */

public class TimesAndInstructors 
{
	public static void main(String[] args) 
	{
		final int MAX_ROWS = 5;				//****** set this to 10 for upload
		
		String[][] courseInfo = {{"CIS101", "Mon 9 am", "Farrell"},
								{"CIS210", "Mon 11 am", "Patel"},
								{"MKT100", "Tues 8:30 am", "Wong"},
								{"ACC150", "Tues 6 pm", "Deitrich"},
								{"CIS101", "Fri 1 pm", "Lennon"}		 
								};									//array would be courseInfo[5][]
		String courseMsg = "Enter a course: ";
		String courseStr = "Course: ";
		String timeStr = " Time: ";
		String instructorStr = " Instructor: ";
		String notValidMsg = "Invalid Entry: No Such Course.";
		String inputCrseName;
		boolean QUIT = false;
		boolean isValid = false;
		Scanner kb = new Scanner(System.in);
		int i;
		
		System.out.println("  Let's start ...... ");
		
		while(!QUIT)
		{					
			System.out.println(courseMsg);
			inputCrseName = kb.nextLine();
			for (i = 0; i < MAX_ROWS; i++)
			{
				if (inputCrseName.equals(courseInfo[i][0]))
				{
					System.out.println(courseStr + courseInfo[i][0] + " " + 
										timeStr + courseInfo[i][1] + 
										instructorStr + courseInfo[i][2]);
					QUIT = true;
					isValid = true;
				}
				
			}
			if(!isValid)
			{
				System.out.println(notValidMsg);
			}
		}
	}
}
