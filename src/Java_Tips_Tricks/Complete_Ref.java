package Java_Tips_Tricks;

import java.util.PrimitiveIterator.OfDouble;

import org.omg.CORBA.SystemException;
import org.w3c.dom.css.Counter;

/**
 * Class description: Publication: Java The Complete Reference Tenth Edition. Author: Herbert Schildt
 * Book from Vancouver Island Regional Library (#33119909305772)
 * Provides some interesting ways to increase speed, shortcuts and other tips and tricks!
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 30 July 2021
 */
public class Complete_Ref {

	public static void main(String[] args) 
	{
		//Declaration area
		int [][] twoD = new int [2][4];		//Page 56, [2] is number of rows, [4] is number of columns(declared and initialized - but not created since no values)
		int i = 100, j = 300, midPoint;		//Page 92, find mid-point using simple 'while' statement
		
		
				
		
		
		//Working area
		TwoDArray(twoD);
		System.out.println("Inside main after return from method >> " + (midPoint = MidPointOfTwoNums (i, j)));
		System.out.print("The values are the same - right?  Of course they are!");
		
		midPoint = MidPointOfTwoNums(i, j);
		System.out.println("Midpoint of 100 and 300 is >> " + midPoint);
		
	}
	public static void TwoDArray(int[][] anArray){

		int[][] arrayOut = anArray;
		int setValue = 100;
		int counter = 0;
		System.out.println("Setting up a 2-day array:");
		for (int x = 0; x < 2; x++)		//number of rows
		{
			for (int y = 0; y < 4; y++)		//number of columns
			{
				arrayOut[x][y] = setValue;	//inserts 100 at [0][0], then 101 @ [0][1], 102 @ [0][2], 103 @[0][3]...107[1][4]
				System.out.print(arrayOut[x][y] + " ");
				setValue++;
				counter++;
			}
			System.out.println(); 		//newline
			
		}
		System.out.println();
		System.out.println("Counter for times in loop is >> " + counter);
		
	}
	public static int MidPointOfTwoNums(int numOne, int numTwo)
	{
		while (++numOne < --numTwo);
		System.out.print("Midpoint between 100 and 300 is >> " + numOne);
		System.out.println();
		System.out.println();
		return numOne;		//can also use numTwo since it is the midpoint
		
	}
	
}
