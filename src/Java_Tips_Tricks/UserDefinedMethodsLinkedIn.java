package Java_Tips_Tricks;

import java.util.Scanner;

/**
 * Class description: LinkedIn Learning "Java Essential Training for Students (2. Basics Review, Creating User-Defined Methods 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class UserDefinedMethodsLinkedIn {

	
	static Scanner in = new Scanner(System.in);				//good place for the Scanner input statement
	
	
	
	public static void main(String[] args) 
	{
		double total = getTotal();
		System.out.print(total);
		
	}
	public static double getTotal()
	{
		double total = 0;
		boolean moreItems = true;
		char response;
		while (moreItems)
		{
			total += getItemPrice(getItemName());
			System.out.println("More items? (y/n)");
			response = in.next().charAt(0);
			if (response != 'y' && response != 'Y')
				moreItems = false;
			in.nextLine();
		}
		return total;
	}
	public static String getItemName()
	{
		String name;
		System.out.println("Enter item name: ");
		name = in.nextLine();
		return name;
	}
	public static double getItemPrice(String value)
	{
		double price = 0;
		try 
		{
			System.out.println("Enter price for " + value + ":");
			price = in.nextDouble();
		} 
		catch (Exception e) 
		{
			System.out.println("Invalid data type entered.");
			e.printStackTrace();
		}
		int quantity = getItemQuantity();
		return quantity * price;
	}
	public static int getItemQuantity()
	{
		System.out.println("Enter the quantity for this item: ");
		int quantity = in.nextInt();
		return quantity;
	}
	public static void print(double total)
	{
		String storeName = "Giant";
		System.out.printf("The total for your grocery items is: $%5.2f, " 
					+ "Thanks for shopping with us at %s!\n\n", total, storeName);				//*** the $%5.2f didn't work as expected - why?
	}
	

}
