package Ch_8_Prog_Exer;

import java.util.*;

import javax.security.auth.x500.X500Principal;

/*
 * Class description: Ch_8 Prog Exer # 2.
 * Allow a user to enter any number of double values up to 15. The user should
 * enter 99999 to quit entering numbers. Display an error message if the user
 * quits without entering any numbers; otherwise, display each entered value
 * and its distance from the average
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 18 July 2021
 */

public class DistanceFromAverage_Orig
{
	public static void main(String[] args) 
	{
		final int MAX_INDEXES= 5;
		final int QUIT = 99999;
		double[] userInput = new double[MAX_INDEXES];
		double accumulatedSum = 0.0;
		double userInputConverted;
		int countInputs = 0;
		int start = 0;
		int sub = 0;
		boolean terminate = false;
		String initialUserMsg = "Enter any number of doubles up to a max 15 >> ";
		String errorMsg = "Error msg for not entering any values, application terminated.";
		String continueOrQuit = "Enter 99999 to quit";
		String userInputToDouble = "";
		Scanner keyboard = new Scanner(System.in);
		
		//get user first double input and convert to double
		System.out.println(initialUserMsg);
		System.out.println(continueOrQuit);
		userInputToDouble = keyboard.nextLine();
		userInputConverted = Double.parseDouble(userInputToDouble);
		
		//capture first input value for quitting without an initial input double
		if (userInputConverted == QUIT)
		{
			System.out.println(errorMsg);
			terminate = true;
		}
		
		//executes if at least one double value has been entered
		while (!terminate)
		{
			//update array with input value
			userInput[countInputs] = userInputConverted;
			accumulatedSum += userInputConverted;		//accumulate each value
			countInputs++;								//increase array sub value (called countInputs
			double avg = accumulatedSum/countInputs;
						
			//user message for next input
			if (countInputs < MAX_INDEXES && userInputConverted != QUIT )
			{
				System.out.println(initialUserMsg);
				System.out.println(continueOrQuit);
				userInputToDouble = keyboard.nextLine();
				userInputConverted = Double.parseDouble(userInputToDouble);
			}
			//output array totals and each value entered distance to average
			if (userInputConverted == QUIT || countInputs == MAX_INDEXES )
			{
				for (sub = start; sub < countInputs; ++sub )
				{
					double originalUserValue = userInput[sub];
					System.out.println("Input value of " + originalUserValue + 
							", is " + (avg - originalUserValue) + " distance from average");
					terminate = true;
				}
			}
		}
	}
}
