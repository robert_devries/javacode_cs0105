package Ch_8_Prog_Exer;

import java.util.*;
import java.util.regex.Matcher;

/**
 * Class description: Ch 8, Prog Exer 8-3a.
 * Write an application for Cody�s Car Care Shop that shows a user a list
 * of available services: oil change, tire rotation, battery check, or brake
 * inspection. Allow the user to enter a string that corresponds to one of
 * the options, and display the option and its price as $25, $22, $15, or $5,
 * accordingly. Display an error message if the user enters an invalid item.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 20 Jul 2021
 */

public class CarCareChoice_InstructorSolution
{
	public static void main(String[] args) 
	{		
		Scanner input = new Scanner(System.in);
	    boolean isMatch = false;
	    String[] items =  { "oil change", "tire rotation",
	         "battery check", "brake inspection"};
	    int[] prices = {25, 22, 15, 5};
	    int x;
	    int matchIndex = 0;
	    String menu = "Enter selection:";
	    for(x = 0; x < items.length; ++x)
	    {
	        menu += "\n   " + items[x];
	    }
	    System.out.println(menu);
	    String selection = input.nextLine();
	    for (x = 0; x < items.length; x++)
	      if(selection.equals(items[x]))
	      {
	    	  isMatch = true;
	    	  matchIndex = x;
	      }
	    if(isMatch)
	    	System.out.println(selection + " price is $" + prices[matchIndex]);
	    else
	        System.out.println("Invalid Entry");
		
	}

}
