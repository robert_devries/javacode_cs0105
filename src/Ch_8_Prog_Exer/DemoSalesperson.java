package Ch_8_Prog_Exer;

/**
 * Class description: Ch 8, Prog Exer 8-6
 * Write an application named DemoSalesperson that declares an array of 10 Salesperson objects. 
 * Set each ID number to 9999 and each sales value to 0. Display the 10 Salesperson objects.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 21 July 2021
 */
public class DemoSalesperson 
{

	public static void main(String[] args) 
	{
		final int NUM_PERSONS = 10;
		Salesperson[] salesPersonObj = new Salesperson[NUM_PERSONS];	//declared/created 10 Salesperson objects
		int defaultID = 9999;
		double defaultAnnualSales = 0.0;
		//System.out.println();
		int start = 0;
		int sub;
		
		for(sub = start; sub < NUM_PERSONS; sub++)
		{
			salesPersonObj[sub]= new Salesperson(defaultID, defaultAnnualSales);
		}
		for (sub = start; sub < NUM_PERSONS; sub++)
		{
			System.out.println(salesPersonObj[sub].getId() + ", " + salesPersonObj[sub].getSales());

		}

	}

}
