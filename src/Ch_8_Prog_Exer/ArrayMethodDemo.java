package Ch_8_Prog_Exer;

/**
 * Class description: Ch 8 Prog Exercise 8-4
 * Implement the ArrayMethodDemo application containing an array that stores eight integers. 
 * The application should call the following five methods:
 * 1. display - should display all the integers
 * 2. displayReverse - should display all the integers in reverse order
 * 3. displaySum - should display the sum of the integers
 * 4. displayLessThan - should display all values less than a limiting argument
 * 5. displayHigherThanAverage - should display all values that are higher than the calculated average value.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 21 July 2021
 */
public class ArrayMethodDemo 
{
	public static void main(String[] args) {
		int[] numbers = {12, 15, 34, 67, 4, 9, 10, 7};
        int limit = 12;
        display(numbers);		//done using Enhanced foreach loop 
        displayReverse(numbers);
        displaySum(numbers);
        displayLessThan(numbers, limit);
        displayHigherThanAverage(numbers);

	}
	public static void display(int[] numbers) 
	{
        for (int num : numbers)						//implements the Enhanced 'for' loop also called 'foreach' loop
        {
        	System.out.print(num + " ");	//NOTE: the num usage which takes the value of each element and not used as a counter
        	
        }
        System.out.println();
    }
    public static void displayReverse(int[] numbers) 
    {
        for (int start = numbers.length-1; start >= 0; start--)
        {
        	System.out.print(numbers[start] + " ");
        }
        System.out.println();
    }
    public static void displaySum(int[] numbers) 
    {
    	int accumulate = 0;
        for (int i = 0; i < numbers.length; i++)
        {
        	accumulate += numbers[i];
        }
        System.out.print("Sum of all numbers >> " + accumulate);
        System.out.println();
    }
    public static void displayLessThan(int[] numbers, int limit) 
    {
        for (int eachNum:numbers)
        {
        	if (eachNum < limit)
        		System.out.print(eachNum + " ");
        }
        System.out.println();
    }
    public static void displayHigherThanAverage(int[] numbers) 
    {
    	int count = 0;
    	int accumulate = 0;
    	double avg;
    	double convertEachNum;
        for(int eachNum: numbers)
        {
        	count++;
        	accumulate += eachNum;
        }
        
        avg = (accumulate * 1.0/count);
        if (count > 0)
        {
        	for (int eachNum : numbers)
        	{
        		convertEachNum = eachNum/1.0;
        		if (convertEachNum > avg)
        		{
        			System.out.print(eachNum + " ");
        		}
        	}
        	System.out.println();
        }
        else
        	System.out.print("there are no values higher than zero");
    }

}
