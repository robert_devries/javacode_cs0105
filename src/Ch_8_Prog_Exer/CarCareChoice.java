package Ch_8_Prog_Exer;

import java.util.*;
import java.util.regex.Matcher;

/**
 * Class description: Ch 8, Prog Exer 8-3a.
 * Write an application for Cody�s Car Care Shop that shows a user a list
 * of available services: oil change, tire rotation, battery check, or brake
 * inspection. Allow the user to enter a string that corresponds to one of
 * the options, and display the option and its price as $25, $22, $15, or $5,
 * accordingly. Display an error message if the user enters an invalid item.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 20 Jul 2021
 */

public class CarCareChoice 
{
	public static void main(String[] args) 
	{		
		int[] selectionCost = {25, 22, 15, 5};		//array declared and created
		String[] availMaintenance = {"oil change", "tire rotation", "battery check", "brake inspection"};		//array declared and created
		int sub, start = 0;
		String userChoice = "";
		String userMessage = "Enter selection: "
				+ "\n   oil change"
				+ "\n   tire rotation"
				+ "\n   battery check"
				+ "\n   brake inspection ";
		String errorMsg = "Error message - incorrect selection made";
		boolean match = false;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println(userMessage);
		userChoice = keyboard.nextLine();
		
		for(sub = start; sub < availMaintenance.length; sub++)
		{
			if (userChoice.equals(availMaintenance[sub]))
			{
				System.out.print(userChoice + " price is $" + selectionCost[sub]);
				match = true;				
			}
		}
		if (!match)
		{
			System.out.println(errorMsg);
		}
		

	}

}
