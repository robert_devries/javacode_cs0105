package Ch_8_Prog_Exer;

import java.util.*;

/*
 * Class description: Ch_8 Prog Exer # 2.
 * Allow a user to enter any number of double values up to 15. The user should
 * enter 99999 to quit entering numbers. Display an error message if the user
 * quits without entering any numbers; otherwise, display each entered value
 * and its distance from the average
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 18 July 2021
 */

public class DistanceFromAverage
{
	public static void main(String[] args) 
	{
		final int MAX_INPUTS = 15;
		final int QUIT = 99999;		//ints can be elevated to double 
		double distance[] = new double[MAX_INPUTS];
		double accumulateInputs = 0;
		double userInputToDouble;
		int countInputs = 0;
		int sub;
		int start = 0;
		double average;
		boolean finished = false;
		String userInput;
		String userMsg = "Enter any double values up to a maximum of 15 >> ";
		String continueOrQuit = ("Enter " + QUIT + " to quit.");
		String errorMsg = "Error message created for not entering any values.";
		Scanner keyboard = new Scanner(System.in);

		System.out.println(userMsg + "\n" + continueOrQuit);
		userInput = keyboard.nextLine();
		userInputToDouble = Double.parseDouble(userInput);
		
		if (userInputToDouble == QUIT)
		{
			System.out.println(errorMsg);
			finished = true;
		}
		while(!finished)
		{
			if(countInputs != MAX_INPUTS && userInputToDouble != QUIT)
			{
				distance[countInputs] = userInputToDouble;
				accumulateInputs += distance[countInputs];
				countInputs++;
			}
			if((countInputs == MAX_INPUTS) || (userInputToDouble == QUIT))
			{
				for(sub = start; sub < countInputs; sub++)
				{
					average = accumulateInputs/countInputs;
					System.out.println("Input value of " + distance[sub] + ", is " + (average - distance[sub]) + " distance from average.");
				}				
				finished = true;
			}
			else
			{
				System.out.println(userMsg + "\n" + continueOrQuit);
				userInput = keyboard.nextLine();
				userInputToDouble = Double.parseDouble(userInput);
			}
			
		}
		
	}
}
