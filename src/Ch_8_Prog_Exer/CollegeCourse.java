package Ch_8_Prog_Exer;

import javafx.scene.media.VideoTrack;

/**
 * Class description: Ch 8, Prog Exer 8-7, CollegeCourse class and Student class are used by InputGrades application
 *
 * For this class, the following applies:
 * 1. contains fields for the course ID (for example, CIS 210), 
 * 2. credit hours (for example, 3), and a letter grade (for example, A). 
 * Include get and set methods for each field.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 23 July 2021
 */
public class CollegeCourse 
{
	private String courseID;
	private int credits;
	private char grade;
	
	//did not create constructor - instead left it to java to implement default java constructor
	
	public String getID() {
		return courseID;
	}
	public int getCredits() {
		return credits;
	}
	public char getGrade() {
		return grade;
	}
	public void setID (String idNum) {
		courseID = idNum;
	}
	public void setCredits(int cr) {
		credits = cr;
	}
	public void setGrade(char gr) {
		grade = gr;
	}

}
