package Ch_8_Prog_Exer;

import java.util.Scanner;


/**
 * Class description: Ch 8, Prog Exer 8-8, Birthday Reminder
 * Write an application that allows a user to enter the names and birth dates of up to 10 friends. 
 * Continue to prompt the user for names and birth dates until the user
 * 		1. enters the sentinel value ZZZ for a name or 
 * 		2. has entered 10 names, whichever comes first. 
 * When the user is finished entering names, produce a count of how many names were entered, and then display the names. 
 * In a loop, continuously ask the user to type one of the names and display the corresponding birth date or "Sorry, no entry for name" 
 * if the name has not been previously entered. The loop continues until the user enters ZZZ for a name.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class BirthdayReminder 
{
	public static void main(String[] args) 
	{
		final String inputSentinel = ("ZZZ");
		final int MAX_LENGTH = 4;				// ***** requirement was for 10 entries *****
		String[] names = new String[MAX_LENGTH];
		String[] birthdate = new String[MAX_LENGTH];
		boolean sentinel = false;
		boolean isMatch = false;
		String msgIntro = ("Enter a name");
		String msgintroPlusQuit = (msgIntro + " or " + inputSentinel + " to quit");
		String msgBirthdate = ("Enter birthdate with slashes between the month, day,");
		String inputName = "";
		String inputBirthdate;
		int counter = 0;
		int searchCounter = 0;
		int reset = 0;
		
		Scanner keyboard = new Scanner(System.in);		
		System.out.println(msgIntro);
					
		while ((counter < (MAX_LENGTH)) && (!sentinel))
		{
			inputName = keyboard.nextLine();
			//System.out.println("Value of counter " + counter + " value of names.length " + names.length);
			if (!(inputName.equalsIgnoreCase(inputSentinel)))		//use this feature for comparing obj strings in Java
			{
				names[counter] = inputName;
				System.out.println(msgBirthdate);
				inputBirthdate = keyboard.nextLine();
				birthdate[counter] = inputBirthdate;
					
				System.out.println(msgintroPlusQuit);
				counter++;
			}
			else
				sentinel = true;
		}
		System.out.println("Count of names entered: " + counter);
		for (int j = reset; j < counter; j++)
		{
			System.out.println(names[j]);
		}
		System.out.println();
		sentinel = false;										//reset after line 56
		System.out.println("Enter a name to search for");
		do 
		{
			searchCounter = 0;		//reset search counter
			inputName = keyboard.nextLine();
			for (int k = reset; k < MAX_LENGTH; k++ )
			{
				searchCounter++;
				if (inputName.equalsIgnoreCase(inputSentinel))
				{
					sentinel = true;
					break;
				}
				if (inputName.equalsIgnoreCase(names[k]))
				{
					System.out.println(names[k] + " " + birthdate[k]);
					System.out.println("Enter a name");
					//System.out.println(msgintroPlusQuit);
					k = MAX_LENGTH;
					isMatch = true;
				}
				else if ((searchCounter == MAX_LENGTH) && (!inputName.equalsIgnoreCase(inputSentinel)))
				{
					System.out.println("Sorry, no entry for " + inputName);
					System.out.println(msgintroPlusQuit);
					k = MAX_LENGTH;
				}
				
			}
		}while (!sentinel);
	}
}
