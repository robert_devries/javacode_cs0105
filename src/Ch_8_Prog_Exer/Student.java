package Ch_8_Prog_Exer;

/**
 * Class description: Ch 8, Prog Exer 8-7, Student and CollegeCourse classes used by InputGrades application
 * Details to Student class as follows;
 * 
 * contains an ID number and an array of five CollegeCourse objects. 
 * Create a get/set method for the student's ID number. 
 * Create a get method that returns one of the student's CollegeCourses; 
 * 		the method takes an integer argument and returns the CollegeCourse in that position (0 through 4). 
 * Create a set method that sets the value of one of the Student�s CollegeCourse objects; 
 * 		the method takes two arguments � a CollegeCourse and an integer representing the CollegeCourse�s position (0 through 4).
 * 
 *  * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 23 July 2021
 */

public class Student 
{
	private int stuID;
    private CollegeCourse[] course = new CollegeCourse[5];		//***** change this back to 5

    public int getID() 
    {
    	return stuID;
    }
    public CollegeCourse getCourse(int x) 
    {
    	return course[x];
    }

    public void setID(int idNum) 
    {
    	stuID = idNum;
    }
    public void setCourse(CollegeCourse c, int x) 
    {
    	course[x] = c;
    }

}
