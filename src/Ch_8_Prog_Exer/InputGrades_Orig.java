package Ch_8_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 8, Prog Exer 8-7, uses both CollegeCourse and Student classes 
 * Details for main as follows:
 * 		Write an application that prompts a professor to enter grades for five different courses each for 10 students. 
 * Prompt the professor to enter data for one student at a time, 
 * 		including student ID and course data for five courses. 
 * Use prompts containing the number of the student whose data is being entered and the course number�for example, 
 * Enter ID for student #1, and Enter course ID #5. 
 * Verify that the professor enters only A, B, C, D, or F for the grade value for each course. 
 * After all the data has been entered, display all the information in order by student then course as shown:
 * Student #1  ID #101
 * CS1 1  -- credits. Grade is A
 * CS2 2  -- credits. Grade is B
 * CS3 3  -- credits. Grade is C
 * CS4 4  -- credits. Grade is D
 * CS5 5  -- credits. Grade is F
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class InputGrades_Orig
{
	public static void main(String[] args) 
	{
		final int NUM_STUDENTS = 2;		//***** Change this back to 10
		final int MAX_COURSE_IDS = 2;	//***** change this back to 5
		boolean isMatch = false;						//confirm professor input matches allowable grade values
		char[] gradeValues = {'A', 'B', 'C', 'D', 'F'};	//avail grade values
		char grade;
		String courseStringID = "";						//identifier ie: CS 1010
		int studentID = 0;	
		int x;
		int courseIDCounter = 0;
		int start = 0;
		
		Student[] eachStudent = new Student[NUM_STUDENTS];
		CollegeCourse[] courseBluePrint = new CollegeCourse[MAX_COURSE_IDS];
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter data for each 10 students, one student at a time. Follow prompts");

		//get student ids created
		for (x = start; x < NUM_STUDENTS; x++)
		{
			System.out.print("Enter ID for student #" + (x+1) + " ");
			studentID = keyboard.nextInt();
			eachStudent[x] = new Student();
			eachStudent[x].setID(studentID);
			keyboard.nextLine();				//forgot to include the 'clear buffer' on next line following the nextInt
			
			for (int i = start; i < MAX_COURSE_IDS; i++)	//enters max 5 courses per student
			{	
				System.out.println("Enter course ID #5");
				courseBluePrint[i] = new CollegeCourse();
				courseStringID = keyboard.nextLine();
				courseBluePrint[i].setID(courseStringID);		//sets courseID to user college crse identifier
				eachStudent[x].setCourse(courseBluePrint[i], i);		//  **** may need to be last code line to populate student's curriculum
								
				System.out.println("Enter Grade Score for this course (A, B, C, D, F)");
				grade = keyboard.next().charAt(0);
				grade = Character.toUpperCase(grade);
				
				System.out.println("Enter Grade Score for this course (A, B, C, D, F)");
				//keyboard.nextLine();				does this need to be here to clear the buffer??
			
				isMatch = false;	//resets Grade comparison, ensures Grade criteria
				int count = 0;
				while (!isMatch)
				{
					for (int z = start; z < gradeValues.length; z++)
					{
						count++;
						char temp = gradeValues[z];  	//this may need to be included as gradeValues[z] might provide the memory address
						if (grade == (temp))
						{
							isMatch = true;
							courseBluePrint[i].setGrade(grade);
							z = gradeValues.length;
						}
						if (grade != temp && count == 5) 
						{
							System.out.println("Incorrect grade score provided, try again >> " + grade);
							grade = keyboard.next().charAt(start);
							grade = Character.toUpperCase(grade);
							count = 0;
						}
					}
					keyboard.nextLine();
				}
				eachStudent[x].getCourse(i).setGrade(grade);
			}
		}
		for (x = start; x < NUM_STUDENTS; x++)
		{
			int count1 = 0;
			System.out.println("Student #" + (x+1) + "  ID #" + eachStudent[x].getID());
			for (int y = start; y < MAX_COURSE_IDS; y++)
			{				
				count1++;
				System.out.println (eachStudent[x].getCourse(y).getID() + " " + count1 + "  -- credits." + " Grade is " + 
						eachStudent[x].getCourse(y).getGrade());
			}
			System.out.println();
		}
	}
}
