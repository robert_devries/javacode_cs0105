package Ch_8_Prog_Exer;

import java.util.*;

import javax.crypto.KeyGenerator;

/**
 * Class description: Ch 8, Prog Exer 8-7, uses both CollegeCourse and Student classes 
 * Details for main as follows:
 * 		Write an application that prompts a professor to enter grades for five different courses each for 10 students. 
 * Prompt the professor to enter data for one student at a time, 
 * 		including student ID and course data for five courses. 
 * Use prompts containing the number of the student whose data is being entered and the course number�for example, 
 * Enter ID for student #1, and Enter course ID #5. 
 * Verify that the professor enters only A, B, C, D, or F for the grade value for each course. 
 * After all the data has been entered, display all the information in order by student then course as shown:
 * Student #1  ID #101
 * CS1 1  -- credits. Grade is A
 * CS2 2  -- credits. Grade is B
 * CS3 3  -- credits. Grade is C
 * CS4 4  -- credits. Grade is D
 * CS5 5  -- credits. Grade is F
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class InputGrades 
{
	public static void main(String[] args) 
	{
		final int NUM_STUDENTS = 2;		//***** Change this back to 10
		final int MAX_COURSE_IDS = 5;	//***** change this back to 5
		boolean isMatch = false;						//confirm professor input matches allowable grade values
		char[] gradeValues = {'A', 'B', 'C', 'D', 'F'};	//avail grade values
		String grade;
		String courseStringID = "";						//identifier ie: CS 1010
		char alphaGrade;
		int studentID = 0, creditHours;
		int x, i, z, count = 0;
		
		
		Student[] eachStudent = new Student[NUM_STUDENTS];
		
		Scanner keyboard = new Scanner(System.in);

		//get student ids created
		for (x = 0; x < NUM_STUDENTS; x++)
		{
			System.out.println("Enter ID for student >> ");
			studentID = keyboard.nextInt();
			keyboard.nextLine();						//clears the buffer from nextInt() line 48
			eachStudent[x] = new Student();
			eachStudent[x].setID(studentID);
			
			for (i = 0; i < MAX_COURSE_IDS; i++)	//enter max 5 courses per student
			{	
				System.out.println("Enter course ID >> ");
				courseStringID = keyboard.nextLine();
				
				System.out.println("Enter credit hours >> ");
				creditHours = keyboard.nextInt();
				keyboard.nextLine();
				
				System.out.println("Enter Alpha Grade score for course >> ");
				grade = keyboard.nextLine();
				
				alphaGrade = grade.charAt(0);
				alphaGrade = Character.toUpperCase(alphaGrade);
		
				//check grade score within parameters
				do {
					for (z = 0; z < gradeValues.length; z++)
					{
						count++;
						char temp = gradeValues[z];
						if (alphaGrade == (temp))
						{
							isMatch = true;
							z = gradeValues.length;
						}
						else if (alphaGrade != temp && count == 5) 
						{
							System.out.println("Incorrect grade score provided," + alphaGrade + " try again >> ");
							System.out.println("Enter A, B, C, D or F >>");
							grade = keyboard.nextLine();
							alphaGrade = grade.charAt(0);
							alphaGrade = Character.toUpperCase(alphaGrade);
							count = 0;
						}
					}
					count = 0;		//resets for next course id 'grade' score iteration
				} while (!isMatch);
				CollegeCourse courseLayout = new CollegeCourse() ;		//declare new college course each iteration since not an array
				courseLayout.setID(courseStringID);
				courseLayout.setCredits(creditHours);
				courseLayout.setGrade(alphaGrade);					
				eachStudent[x].setCourse(courseLayout, i);		//  **** last line to populate student's curriculum
			}
		}
		//output Student (#1-5) each student's ID 
		for (x = 0; x < NUM_STUDENTS; x++)
		{
			System.out.println("Student #" + (x+1) + "  ID #" + eachStudent[x].getID());
			for (int y = 0; y < MAX_COURSE_IDS; y++)
			{				
				System.out.println (eachStudent[x].getCourse(y).getID() + "  " + 
						eachStudent[x].getCourse(y).getCredits() + " credits. Grade is " + 
						eachStudent[x].getCourse(y).getGrade());
			}
			System.out.println();
		}
	}
}
