package Ch_8_Prog_Exer;

/**
 * Class description: Ch 8, Prog Exer 8-6 which is needed by DemoSalesperson
 * Data fields for Salesperson include:
 * 1. an integer ID number and
 * 2. a double annual sales amount. 
 * 3. Methods include a constructor that requires values for both data fields, as well as get and set methods for each of the data fields. 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 21 July 2021
 */

public class Salesperson 
{
	private int id;
	private double annualSales;
	
	public Salesperson (int idNum, double annualSalesAmt)		//constructor built - a must include
	{
		id = idNum;
		annualSales = annualSalesAmt;
	}
	public void setId (int idNum)
	{
		id = idNum;
	}
	public int getId()
	{
		return id;
	}
	public void setSales(double someMoney)
	{
		annualSales = someMoney;
	}
	public double getSales()
	{
		return annualSales;
	}
}
