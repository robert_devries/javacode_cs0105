package Ch_8_Prog_Exer;

import java.util.*;

/**
 * Class description: Ali Moussa (Java Instructor) his solution (nicely done!!!)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 28 July 2021
 */
public class InputGrades_Instructor 
{
	public static void main(String[] args) 
	
	{
		final int MAX_STUDENT = 10, MAX_CRSE = 5;
        Student[] studentList = new Student[MAX_STUDENT];
        String crseId, ltrGrade;
        char e;
        int i, j, creditHours, studentId;
        Scanner input = new Scanner(System.in);

        for(i = 0; i < studentList.length; i++)
        {
            studentList[i] = new Student();     // Required Student class only has a default constructor
            System.out.print("Enter ID for student >> ");
            studentId = input.nextInt();
            input.nextLine();
            studentList[i].setID(studentId);
            for(j = 0; j < MAX_CRSE; j++)
            {
                System.out.print("Enter course ID >> ");
                crseId = input.nextLine();
                System.out.print("Enter credit hours >> ");
                creditHours = input.nextInt();
                input.nextLine();
                System.out.print("Enter Letter Grade for course >> ");
                ltrGrade = input.nextLine();
                e = Character.toUpperCase(ltrGrade.charAt(0));
                while((e != 'A') && (e != 'B') && (e != 'C') && (e != 'D') && (e != 'F'))
                {
                    System.out.println("Please try again");
                    System.out.print("Enter A, B, C, D or F >> ");
                    ltrGrade = input.nextLine();
                    e = Character.toUpperCase(ltrGrade.charAt(0));
                }
                // CollegeCourse course = new CollegeCourse(crseId, creditHours, e);
                CollegeCourse course = new CollegeCourse();
                course.setID(crseId);
                course.setCredits(creditHours);
                course.setGrade(e);
                studentList[i].setCourse(course, j);
            }
        }
        for(i = 0; i < studentList.length; i++)
        {
            System.out.println("Student #" + (i + 1) + "  ID #" + studentList[i].getID());
            for(j = 0; j < MAX_CRSE; j++)
            {
                System.out.println(studentList[i].getCourse(j).getID() + "  " + studentList[i].getCourse(j).getCredits() + " credits. Grade is " + studentList[i].getCourse(j).getGrade());
            }
            System.out.println();
        }
	}
}
