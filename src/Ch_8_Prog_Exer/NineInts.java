package Ch_8_Prog_Exer;

/**
 * Class description:  Prog Exercise #1 an application that stores Nine Integers into an array
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  18 July 2021
 */
public class NineInts 
{
	public static void main(String[] args) 
	{
		//int[] nineInts;								//declares array and below is the inclusion of memory capacity
		//nineInts = new int[9];						//now includes memory capacity
		//int[] nineInts = new int[9];						//declare array including memory capacity (this declares, creates, and commits memory - all in one)
		int[] nineInts = {10, 15, 19, 23, 26, 29, 31, 34, 38};		//declared and initialized in one statement (in other words - array created)
		final int START = 0;
		final int NUMBER_OF_INTS = 9;
		int sub;

		for (sub = START; sub < NUMBER_OF_INTS; sub++)		//incremental counter/display o/p 
		{
			System.out.print(nineInts[sub] + " " );
		}
		System.out.println();
		for (sub = (NUMBER_OF_INTS - 1); sub >= START; sub--)		//decremental counter/display o/p 
		{			
			System.out.print(nineInts[sub] + " ");
		}
	}

}
