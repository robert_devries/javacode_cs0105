package Ch_8_Prog_Exer;

import java.util.*;
import java.time.*;

/**
 * Class description: Ch 8, Prog Exer 8-10 
 * Implement the CertOfDepositArray application to accept data for:
 * 		1. an array of five CertOfDeposit objects, 
 * 		2. then display the data.
 * 
 * The program should prompt the user for the following information (in order):
 * 		Certificate #
 *	 	Last name
 *		Balance
 * 		Month of issue
 * 		Day of issue
 * 		Year of issue
 * 
 * The program should display the year the certificate matures, which is one year after it was issued, in the format YYYY-MM-DD.
 * 	Enter certificate number			
 *		95								Certificate 95
 *	Enter last name of owner			
 *		Wayne							Name: Wayne  Balance: $123.0
 *	Enter balance
 *		123
 *	Enter month of issue				Issued: 1998-09-08
 *		9
 *	Enter day of issue
 *		8
 *	Enter year of issue
 *		1998							Matures: 1999-09-08
 *	Enter certificate number
 *		96								Certificate 96
 *	Enter last name of owner
 *		Allen							Name: Allen  Balance: $345.56
 *	Enter balance
 *		345.56
 *	Enter month of issue				Issued: 1999-10-11
 *		10
 *	Enter day of issue
 *		11
 *	Enter year of issue
 * 		1999							
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class CertOfDepositArray 
{
	public static void main(String[] args) 
	{	
		//define needed variables
		final int MAX_DEPOSIT = 2;		// ********  this could be set to 2 for application functionality (return back to 5)
		double balance;
		int issueMon, issueDay, issueYr;
		String certNumMsg = ("Enter certificate number"), lastNameMsg = ("Enter last name of owner"), 
				balanceMsg = ("Enter balance"), issueMonMsg = ("Enter month of issue"), 
				issueDayMsg = ("Enter day of issue"), issueYrMsg = ("Enter year of issue");
		String certNum, lastName;
		LocalDate dateOfIssue;
		
		CertOfDeposit[] investment = new CertOfDeposit[MAX_DEPOSIT];
		int i, j, start = 0;
		
		//output msgs and inputs from user x 5 certificates
						
		for (i = start; i < investment.length; i++)		
		{
			Scanner keyboard = new Scanner(System.in);
			System.out.println(certNumMsg);
			certNum = keyboard.nextLine();
			System.out.println(lastNameMsg);
			lastName = keyboard.nextLine();
			System.out.println(balanceMsg);
			balance = keyboard.nextDouble();
			System.out.println(issueMonMsg);
			issueMon = keyboard.nextInt();
			System.out.println(issueDayMsg);
			issueDay = keyboard.nextInt();
			System.out.println(issueYrMsg);
			issueYr = keyboard.nextInt();
			dateOfIssue = LocalDate.of(issueYr, issueMon, issueDay);
			
			// *** This will be the last line of code to populate each investment default constructor
			investment[i] = new CertOfDeposit(certNum, lastName, balance, dateOfIssue);					
		}
		
		for (j = start; j < investment.length; j++)
		{
			display(investment[j]);
		}

	}
	public static void display(CertOfDeposit cd)
    {
        System.out.println("Certificate " + cd.getCertNum() +
        "\nName: " + cd.getName() + " Balance: $" + cd.getBalance() +
        "\nIssued: " + cd.getIssueDate() +
        "\nMatures: " + cd.getMaturityDate());
    }

}
