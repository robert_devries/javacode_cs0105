package Ch_8_Prog_Exer;

import java.time.*;

/**
 * Class description: Ch 8, Prog Exer 8-10 Class.  This belongs to CertOfDepositArray.java application
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 29 July 2021
 */
public class CertOfDeposit 
{
	private String certNum;
    private String lastName;
    private double balance;
    private LocalDate issueDate;
    private LocalDate maturityDate;
    public CertOfDeposit(String num, String name, double bal, LocalDate issue) {
    	certNum = num;
    	lastName = name;
    	balance = bal;
    	issueDate = issue;
    	maturityDate = issueDate.plusYears(1);
    }
    public void setCertNum(String n) {
    	certNum = n;
    }
    public void setName(String name) {
    	lastName = name;
    }
    public void setBalance(double bal) {
    	balance = bal;
    }
    public void setIssueDate(LocalDate date) {
    	issueDate = date;
    	maturityDate = issueDate.plusYears(1);
    }
    public String getCertNum() {
    	return certNum;
    }
    public String getName() {
    	return lastName;
    }
    public double getBalance() {
    	return balance;
    }
    public LocalDate getIssueDate() {
    	return issueDate;
    }
    public LocalDate getMaturityDate() {
    	return maturityDate;
    }
}
