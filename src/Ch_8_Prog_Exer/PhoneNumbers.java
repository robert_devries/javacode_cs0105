package Ch_8_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 8, Prog Exer 8-9 Directory Phone Listing
 * A personal phone directory contains room for first names and phone numbers for 30 people. 
 * Assign names and phone numbers for the first 10 people. 
 * Prompt the user for a name, and if the name is found in the list, display the corresponding phone number. 
 * If the name is not found in the list, 
 * 		prompt the user for a phone number, 
 * 		and add the new name and phone number to the list.
 * 
 * Continue to prompt the user for names until the user enters quit. 
 * After the arrays are full (containing 30 names), do not allow the user to add new entries.
 *
 * Gina	    (847) 341-0912
 * Marcia	(847) 341-2392
 * Rita	    (847) 354-0654
 * Jennifer	(414) 234-0912
 * Fred	    (414) 435-6567
 * Neil	    (608) 123-0904
 * Judy	    (608) 435-0434
 * Arlene	(608) 123-0312
 * LaWanda	(920) 787-9813
 * Deepak	(930) 412-0991
 *
 *Enter name to look up. Type 'quit' to quit.
 * Gina
 * Gina's phone number is (847) 341-0912
 * Enter name to look up. Type 'quit' to quit.
 * Jorge
 * Enter phone number for Jorge
 * 555-555-0184
 * Enter name to look up. Type 'quit' to quit.
 * quit
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 27 July 2021
 */
public class PhoneNumbers 
{
	public static void main(String[] args) 
	{
		final int MAX_LISTINGS = 12;		// ***** change back to requirements of 30 (first 10 will be auto-populated)
		String[] directoryNames = new String[MAX_LISTINGS];
		String[] directoryPhone = new String[MAX_LISTINGS];
		
		String[] testNameArray = {"Gina", "Marcia", "Rita", "Jennifer", "Fred", "Neil", "Judy", "Arlene", "LaWanda", "Deepak"};
		String[] testPhoneArray = {"(847) 341-0912", "(847) 341-2392", "(847) 354-0654", "(414) 234-0912", "(414) 435-6567", 
				"(608) 123-0904", "(608) 435-0434", "(608) 123-0312", "(920) 787-9813", "(930) 412-0991"};
		String lookUp;
		String quit = "quit";
		boolean isQuit = false;
		boolean isMaxListing = false;
		boolean foundMatch = false;
		boolean isFullDirectory = false;
		Scanner kb= new Scanner(System.in);
		int start = 0;
		int searchCounter = 0;
		int arrayLoading = 10;
		
		//preloads both arrays
		for (int i = start; i < (arrayLoading); i++)
		{				
			
			directoryNames[i] = testNameArray[i];
			directoryPhone[i] = testPhoneArray[i];
		}
				
		while (!isMaxListing && !isQuit)
		{
			System.out.println("Enter name to look up. Type 'quit' to quit.");
			lookUp = kb.nextLine();
			if (lookUp.equalsIgnoreCase(quit))
			{
				isQuit = true;
			}
			else
			{
				for (int j = start; j < directoryNames.length; j++)
				{
					searchCounter++;
					if (lookUp.equalsIgnoreCase(directoryNames[j]))
					{
						System.out.println(lookUp + "\'s phone number is " + 
								"(" + directoryPhone[j].substring(1, 4) + ")" + " " +
								directoryPhone[j].substring(6, 9) + "-" + directoryPhone[j].substring(10));
						foundMatch = true;
						j = directoryNames.length;
					}
					else if ((searchCounter == MAX_LISTINGS) && (!foundMatch) && (!isFullDirectory))
					{
						//addMissingItem += nameArray.length + 1;
						System.out.println("Enter phone number for " + lookUp + " ");
						directoryNames[arrayLoading] = lookUp;
						lookUp = kb.nextLine();
						directoryPhone[arrayLoading] = lookUp;
						arrayLoading += 1;
						if (arrayLoading == (MAX_LISTINGS))
						{
							System.out.println("Directory full! Additional names cannot be added!\n");
							isFullDirectory = true;		
						}
					}	
				}
				searchCounter = start;
			}
			isMaxListing = false;  // ************* ****************** this needs to go away
			foundMatch = false;	
		}
	}		
}
