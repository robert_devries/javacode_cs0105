package Ch_8_Prog_Exer;

import java.util.*;
import java.util.regex.Matcher;

import javax.security.auth.Subject;

/**
 * Class description: Ch 8, Prog Exer 8-3a.
 * Write an application for Cody�s Car Care Shop that shows a user a list
 * of available services: oil change, tire rotation, battery check, or brake
 * inspection. Allow the user to enter a string that corresponds to one of
 * the options, and display the option and its price as $25, $22, $15, or $5,
 * accordingly. Display an error message if the user enters an invalid item.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 20 Jul 2021
 */

public class CarCareChoice2
{
	public static void main(String[] args) 
	{		
		Scanner input = new Scanner(System.in);
	    boolean isMatch = false;
	    String[] items =  { "oil change", "tire rotation",
	         "battery check", "brake inspection"};
	    int[] prices = {25, 22, 15, 5};
	    int x;
	    int sub = 0;
	    int start = 0;
	    int matchIndex = 0;
	    int lengthChars = 3;
	    String menu = "Enter selection:";
	    for(x = 0; x < items.length; ++x)
	    {
	        menu += "\n   " + items[x];
	    }
	    System.out.println(menu);		
	    String selection = input.nextLine();
	    
	    //start here for the three letter user input
	    String compareItem;
	    //String selection = "oil change";
	    String abbreviatedSelection = selection.substring(start, lengthChars);		//have the first 3 chars from original input
	    
	    //3 letters from each element in menu items and compare to abbreviatedSelection
	    while ((!isMatch) && (sub < items.length))
	    {
	    	for (sub = start; sub < items.length; sub++)
		    {
		    	compareItem = items[sub].substring(start, lengthChars);
		    	if (abbreviatedSelection.equals(compareItem))						//****** need to use equals when dealing with strings ******
		    	{
		    		selection = items[sub];
		    		isMatch = true;
		    		matchIndex = sub;
		    	}
		    }
	    }
	    
	    if(isMatch)
	    	System.out.println(selection + " price is $" + prices[matchIndex]);
	    else
	        System.out.println("Invalid Entry");
	}
}
