package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-8, Prog Exer:   main 'TestScore' with 'ScoreException' class that extends Exception and displays exception to user
 * 
 * Write an application that displays a series of at least five student ID numbers (that you have stored in an array) 
 * and asks the user to enter a numeric test score for the student. 
 * Create a ScoreException class that extends the Exception class. 
 * The ScoreException class constructor should accept a single argument of type String which is the message passed to the Exception superclass. 
 * Throw a ScoreException for the class if the user does not enter a valid score (less than or equal to 100). 
 * Catch the ScoreException, display the message Score over 100, and then store a 0 for the studentís score. 
 * At the end of the application, display all the student IDs and scores.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 Sept 2021
 */

public class TestScore
{
	public static void main(String[] args) throws Exception 	//***** this is important to include **********
	{
		Scanner input = new Scanner(System.in);
        int[] ids = {1234, 2345, 3456, 4567, 5678};
        int[] scores = {0, 0, 0, 0, 0};
        String scoreString = new String();
        final int HIGHLIMIT = 100;
        String inString, outString = "";
        for (int x = 0; x < ids.length; ++x) 
        {
            System.out.println("Enter score for student id number: " + ids[x]);
            inString = input.next();
            scores[x] = Integer.parseInt(inString);		//write code below this line
            try 
            {
				if (scores[x] >= HIGHLIMIT)				//decision point
				{
					scores[x] = 0;
					throw new ScoreException("Score over 100");		//throws new ScoreException with string value and stored in super(s)
				}
			} 
            catch (ScoreException e) 	//catches msg from line 40
            {
				System.out.println(e.getMessage());		//e is the instance parameter and .getMessage is retrieved from line 40
			}
            //don't need else statement since checking valid value in line 34 and scores[x] was previously loaded at line 33
        }
        for (int x = 0; x < ids.length; ++x)
            outString = outString + "ID #" + ids[x] + "  Score " +
                        scores[x] + "\n";
        System.out.println(outString);

	}

}
