package Ch_12_Prog_Exer;

import java.util.Scanner;
import java.util.Random;
import java.io.IOException;

/**
 * Class description: Ch 12-12, Prog Exer:  main:  CalculatorDemo2 
 * 
 * In a �You Do It� section of this chapter, you created a CalculatorDemo program that asked the user to solve an arithmetic problem 
 * and provided the system calculator for assistance. Now modify that program to include the following improvements:
 * 
 * Both numbers in the arithmetic problem should be random integers between 1 and 5,000.
 * The program should ask the user to solve five problems.
 * The program should handle any non-integer data entry by displaying an appropriate message and continuing with the next problem.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 Sep 2021
 */
public class CalculatorDemo2 
{
	public static void main(String[] args) throws IOException		//**** note the inclusion of the throws IOException in the header for main
	{
		Scanner input = new Scanner(System.in);
		Process proc = Runtime.getRuntime().exec 
				("cmd /c C:\\Windows\\System32\\calc.exe");

		final int MIN = 1;		//min value set for random generator
		final int MAX = 5000;	//max value set for random generator
		int userAnswer;
		int randomNumAnswer;
		int randomNum1;
		int randomNum2;
		Random random = new Random();
				
		for (int i = 0; i < 5; i++)
		{
			randomNum1 = random.ints(MIN, (MAX + 1)).limit(1).findFirst().getAsInt();		//**** from web: https://mkyong.com/java/java-generate-random-integers-in-a-range/
			randomNum2 = random.ints(MIN, (MAX + 1)).limit(1).findFirst().getAsInt();
			randomNumAnswer = randomNum1 + randomNum2;
			System.out.print("What is the sum of " + randomNum1 + " and " + randomNum2 + "? >> ");
			try 
			{
				userAnswer = input.nextInt();
				if(userAnswer == randomNumAnswer)
				{
					System.out.println("Correct!");
				}
				else {
					System.out.println("Sorry - the answer is " + randomNumAnswer);
				}
			} 
			catch (Exception e) 
			{
				System.out.println("Invalid answer. Try again.");
				input.nextLine();
			}
						
			
		}
	}
}
