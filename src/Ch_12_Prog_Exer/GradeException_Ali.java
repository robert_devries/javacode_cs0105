package Ch_12_Prog_Exer;

public class GradeException_Ali extends Exception 
{
	public static char[] validGrades = {'A', 'B', 'C', 'D', 'I'};
	
    public GradeException_Ali(String string) 
    {
		super(string);	
	}
}
