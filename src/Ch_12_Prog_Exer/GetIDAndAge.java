package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-10, Prog Exer:  main 'GetIDAndAge' and 'DataEntryException' class which extends Exception superclass
 * 
 * Create a DataEntryException class whose getMessage() method returns information about invalid integer data. 
 * 
 * Write a program named GetIDAndAge that continually prompts the user for an ID number and an age until a terminal 0 is entered for both. 
 * If the ID and age are both valid, display the message ID and Age OK.
 *
 * Throw a DataEntryException if the 
 *		ID is not in the range of valid ID numbers (0 through 999), or 
 *		if the age is not in the range of valid ages (0 through 119). 
 *
 * Catch any DataEntryException or InputMismatchException that is thrown, and display the message 
 * 		"Invalid age or ID - DataEntryException - \", where \ is the value of the invalid input. For example:
 * 		Enter ID 1000
 *		Enter age 40
 *		Invalid age or ID - DataEntryException - 1000
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 12 Sept 2021
 */

public class GetIDAndAge 
{
	public static void main(String[] args) throws DataEntryException 		//Did not produce the desired output from the DataEntryException (lack of instructions)
	{
        int id;
        int age;
        final int QUIT = 0;
        int returnVal = QUIT + 1;
        Scanner keyboard = new Scanner(System.in);
      
        while (returnVal != QUIT) 
        {
        	System.out.print("Enter ID ");
        	try 
        	{
        		id = keyboard.nextInt();
            	System.out.print("Enter age ");
            	age = keyboard.nextInt();
            	returnVal = check(id, age);
			} 
        	catch (InputMismatchException e) 
        	{
				System.out.println("Invalid data Type");
			}
        	
        }
    }
    public static int check(int idNum, int ageNum) throws DataEntryException 
    {
        final int MINS = 0;
        final int ID_MAX = 999;
        final int AGE_MAX = 119;
    	try 
        {
			if (idNum < MINS || idNum > ID_MAX) 		//DataEntryException criteria
			{
				throw (new DataEntryException(idNum));		
			}
			else if (ageNum < MINS || ageNum > AGE_MAX) 
			{
				throw (new DataEntryException(ageNum));
			}
			else if (idNum == MINS && ageNum == MINS) 
			{
				return 0;		//will end main/while loop
			}
			else 
			{
				showStatus("ID and Age OK");
				return 1;		//this will continue with the main/while loop
			}
		} 
        catch (DataEntryException e) 
    	{
			System.out.println(e.getMessage());
			return -1;
		}
    	
    }
    public static void showStatus(String msg) 
    {
        System.out.println(msg);
    }

}
