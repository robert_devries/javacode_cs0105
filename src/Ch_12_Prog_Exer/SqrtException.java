package Ch_12_Prog_Exer;

import java.util.*;
import java.lang.Math;

/**
 * Class description: Ch 12-5, Prog Exer
 * 
 * Write an application that throws and catches an ArithmeticException when you attempt to take the square root of a negative value. 
 * Prompt the user for an input value and try the Math.sqrt() method on it. 
 * The application either displays the square root or catches the thrown Exception and 
 * 		displays the message Can't take square root of negative number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 9 Sept 2021
 */

public class SqrtException 
{
	public static void main(String[] args) throws ArithmeticException
	{
		Scanner keyboard = new Scanner(System.in);
		double input;
		double result;
		String userValue;
		
		System.out.print("Enter number for square root value >> ");
		userValue = keyboard.nextLine();
		input = Double.parseDouble(userValue);
		
		try 
		{
			if (input < 0)
			{
				System.out.println("Can't take square root of negative number.");
			}
			else
			{
				System.out.println(Math.sqrt(input));
			}
			
 
		} 
		catch (ArithmeticException e) 
		{
			throw (new ArithmeticException());
		}

	}

}
