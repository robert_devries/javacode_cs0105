package Ch_12_Prog_Exer;

import java.util.*;



/**
 * Class description: Ch 12-3, Prog Exer
 * 
 * In Chapter 2, you created an application named QuartsToGallonsInteractive that accepts a number of quarts from a user and converts the value to gallons. 
 * Now, add exception-handling capabilities to this program and continuously reprompt the user while any nonnumeric value is entered.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 8 Sept 2021
 */

public class QuartsToGallonsWithExceptionHandling 
{
	public static void main(String[] args) 
	{		
		final int QUARTS_IN_GALLON = 4;
	    int quartsNeeded = 18;
	    int gallonsNeeded = 0;
	    int extraQuartsNeeded = 5;				//this is a system calculated variable and will never result in 5
	    Scanner input = new Scanner(System.in);
	    	    
	    while (extraQuartsNeeded == 5)		//5 is a fixed value that can never be achieved by the try block
	    {
	    	
	    	try 
	    	{
	    		System.out.print("Enter quarts needed >> ");
	    	    quartsNeeded = input.nextInt();
	    	    gallonsNeeded = quartsNeeded / QUARTS_IN_GALLON;
	    	    extraQuartsNeeded = quartsNeeded % QUARTS_IN_GALLON;
	    	    input.nextLine(); 		
			} 
	    	
	    	catch (InputMismatchException e) 
	    	{
				System.out.println("Incorrect value entered.");
				input.nextLine();					//********************* need to clear inputStream Buffer from line 33 above ************* 			
	    	}
	    	/*catch (Exception e) 				//this will catch any other error and restart while loop
	    	{
				System.out.println("Must be an integer.");
				input.nextLine();
	    	}*/
	    	
	    }
	    System.out.println("A job that needs " + quartsNeeded +
		         " quarts requires " + gallonsNeeded + " gallons plus " +
		         extraQuartsNeeded + " quarts.");

	}

}
