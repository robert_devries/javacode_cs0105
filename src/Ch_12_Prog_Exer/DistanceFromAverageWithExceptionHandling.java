package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-4, Prog Exer
 * 
 * Using Chapter 8, Exercise 2 named DistanceFromAverage which allowed users to enter up to 15 double values and then displays each entered value and its 
 * distance from the average.
 * Now modify the app to first prompt user to enter an integer that represents the array size. Java generates a "NumberFormatException" if a non-integer value is
 * entered using nextInt(): handle this exception by displaying an appropriate msg.   
 *
 * If the user enters the value 99999 to quit the program before entering all values to fill the array, the program should perform its calculations
 *  on the values already entered before exiting
 *  
 * Create an array using the integer entered as the size. 
 * 		Java generates a NegativeArraySizeException if you attempt to create an array with a negative size; handle this exception by setting the array size 
 * 			to a default value of five. 
 * 		If the array is created successfully, use exception-handling techniques to ensure that each entered array value is a double 
 * 			before the program calculates each elementís distance from the average.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 9 Sept 2021
 * 
 */

public class DistanceFromAverageWithExceptionHandling 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		boolean isNotDouble = false;
	    double[] numbers = new double[15];
	    double entry = 0;
	    double total = 0;
	    double average = 0;
	    final int QUIT = 99999;
	    int x = 0, y;
	    int j = 0, arraySize = 0;
	     
	    try 		//capture noninteger value entered by user
	    {
	    	System.out.print("Please enter a value for the array size >> ");
		    arraySize = input.nextInt();
		    input.nextLine();      			//clear IO Stream buffer
	    } 
	      
	    catch (InputMismatchException e) 			//captures noninteger values
	    {
	    	System.out.println("Value entered was a noninteger.");
	    }
	    	    
	    entry = arraySize;			//use arraySize value to confirm a negative value has not been entered. Above try is for alpha values
	    try 
	    {
    		numbers[arraySize] = arraySize;
	    } 
	    catch (ArrayIndexOutOfBoundsException e) 
	    {
			//default arraySize set to [5]			//default set to 5 as per requirement
	    	arraySize = 5;
		}
	    
	    if (arraySize > 0 && entry != QUIT)
	    {
	    	System.out.print("Enter a numeric value or 99999 to quit >> ");
	    	entry = input.nextDouble();
	    }
	    	    
	    while(j < arraySize && entry != QUIT)
	    {
	    	try 
	    	{
	    		//entry = input.nextDouble();		//moved to line 66 and 87
		    	numbers[j] = entry;
		    	total += numbers[j];
		    	isNotDouble = true;
		    	++j;
			} 
	    	catch (InputMismatchException e) 
	    	{
				input.nextLine();
			}
	    		    	
	    	if(x < numbers.length && j < arraySize)
	    	{
	    		while (isNotDouble)
	    		{
	    			try 
	    			{
	    				System.out.print("Enter next numeric value or " +
	   							QUIT + " to quit >> ");
	   					entry = input.nextDouble();
	   					isNotDouble = false;
	    			} 
	    			catch (InputMismatchException e) 
	    			{
	    				input.nextLine();

	   				}
	   			}
	    		
	    		
	    	}
	    }
	    if(j == 0)
	    	System.out.println("Average cannot be computed because no numbers were entered");
	    else
	    {
	    	average = total / j;
	    	System.out.println("You entered " + j +
	    			  " numbers and their average is " + average);
	    	for(y = 0; y < j; ++y)
	    		System.out.println(numbers[y] + " is " +
	    			  (average - numbers[y]) + " away from the average");
	    }           
	}
}
