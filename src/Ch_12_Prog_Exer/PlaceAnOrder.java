package Ch_12_Prog_Exer;

import java.util.*;


/**
 * Class description: Ch 12-11, Prog Exer:  main:  PlaceAnOrder, OrderMessages class [] 7 messages , OrderException class extends Exception class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 12 Sept 2021
 */

public class PlaceAnOrder 
{
	public static void main(String[] args) throws OrderException
	{
		final int HIGHITEM = 9999;
        final int HIGHQUAN = 12;
        int code = 0;
        int x = 0;
        boolean found = false;
        final int[] ITEM = {111, 222, 333, 444};
        final double[] PRICE = {0.89, 1.47, 2.43, 5.99}; 
        int item =0;
        int quantity = 0;
        double price = 0;
        double totalPrice = 0;
        String message;
        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter item number ");
        String message1 = input.nextLine();
        char checkLetter = message1.charAt(0);
        try 
        {	//Exception: InputMismatchException, if not an int for Item Number
        	if (Character.isLetter(checkLetter))
        	{
        		throw (new OrderException(OrderMessages.message[0]));
        	}
        	item = Integer.parseInt(message1);
        	if (item < 0 || item > HIGHITEM)
            {
            	//Exception: handles item < 0 (message[2])
            	if (item < 0)
            	{
            		throw (new OrderException(OrderMessages.message[2]));
            	}
            	//Exception: handles item > HIGHITEM
            	else
            	{
            		message = OrderMessages.message[3];			
            		throw (new OrderException(message));
            	}
            }
            //handles item not within Item Number listing
            else if (item != 111 && item != 222 && item != 333 && item != 444)
            {	
            	message = OrderMessages.message[6];				//handles item not in values (message[6]
            	throw (new OrderException(message));
            }
		} 
        catch (InputMismatchException e)		//catches incorrect data type (message[0]
        {
        	System.out.println(OrderMessages.message[0]);
        					
        }
        catch (OrderException e) 							//catches all data variables (message[0, 2, 3, 6]
        {
			System.out.println(e.getMessage());
		}
        
        System.out.print("Enter quantity ");
        message1 = input.nextLine();
        checkLetter = message1.charAt(0);
        
        try 
        {
        	if(Character.isLetter(checkLetter))
        	{
        		//InputMismatchException for non-integer value
        		throw (new OrderException(OrderMessages.message[1]));
        	}
        	quantity = Integer.parseInt(message1);
            if (quantity < 0 || quantity > HIGHQUAN)
            {
            	if (quantity < 0)
            	{
            		message = OrderMessages.message[4];		//handles quantity too low (message[4])
            		throw (new OrderException(message));
            	}
            	else
            	{
            		message = OrderMessages.message[5];		//handles quantity too high (message[5])
            		throw new OrderException(message);
            	}
            }            
		} 
        catch (InputMismatchException e)					//catches incorrect data type (message[1]
        {
        	System.out.println(OrderMessages.message[1]);														
        }
        catch (OrderException e) 							//catches all data variables (message[4,5]
        {
			System.out.println(e.getMessage());
		}
        for (x = 0 ; x < ITEM.length; x++)
        {
        	if (item == ITEM[x])
        	{
        		code = x;
        		x = ITEM.length-1;
        		found = true;
        	}
        	if (found)
        	{
        		totalPrice = quantity * PRICE[code];
        	
        		System.out.println("Total price " + totalPrice);
        	}
        }
        
        

	}

}
