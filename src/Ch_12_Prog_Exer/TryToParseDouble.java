package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-2, Prog Exer
 * 
 * The Double.parseDouble() method requires a String argument, but it fails if the String cannot be converted to a floating-point number. 
 * Write an application in which you try accepting a double input from a user and catch a NumberFormatException if one is thrown. 
 * The catch block forces the number to 0 and displays Value entered cannot be converted to a floating-point number. 
 * Following the catch block, display the number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 8 Sept 2021
 */

public class TryToParseDouble 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		String userInput;
		double number;
		
		try 
		{
			System.out.print("Enter a double value >> ");
			userInput = keyboard.nextLine();
			number = Double.parseDouble(userInput);			//if this throws an exception, catch will output reason and reset number to 0
		} 
		
		catch (NumberFormatException e) 
		{
			System.out.println("Value entered cannot be converted to a floating-point number");
			number = 0;
		}
		System.out.println("Number is >> " + number);

	}

}
