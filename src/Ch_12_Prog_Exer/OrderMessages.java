package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-11, Prog Exer:  main:  PlaceAnOrder, OrderMessages class [] 7 messages , OrderException class extends Exception class
 * 
 * A company accepts user orders for its products interactively. Users might make the following errors as they enter data:
 *
 * 		� The item number ordered is not numeric, too low (less than 0), or too high (more than 9999).
 *
 * 		� The quantity is not numeric, too low (less than 1), or too high (more than 12).
 *
 * 		� The item number is not a currently valid item.
 *
 * Although the company might expand in the future, its current inventory consists of the items listed in Table 12-1.
 *    Item Number		Price ($)
 *    111				0.89
 *    222				1.47
 *    333				2.43
 *    444				5.99
 *
 * Create a class that stores an array of usable error messages. 
 * 
 * Create an OrderException class that stores one of the messages. 
 * 
 * Create an application that contains prompts for an item number and quantity. 
 * 
 * Allow for the possibility of non-numeric entries as well as out-of-range entries and entries that do not match any of the currently available item numbers. 
 * 
 * The program should display an appropriate message if an error has occurred. 
 * 
 * The program error messages can be found in the OrderMessages.java file. 
 * 
 * If no errors exist in the entered data, compute the user�s total amount due (quantity times price each) and display it.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 12 Sept 2021
 */

public class OrderMessages 
{
	public final static String[] message = 
	{ "Item number not numeric    ", "Quantity not numeric       ",
	  "Item number too low        ", "Item number too high       ",
	  "Quantity too low           ", "Quantity too high          ",
	  "Item number does not exist "};

}
