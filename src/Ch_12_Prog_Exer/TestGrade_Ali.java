package Ch_12_Prog_Exer;

import java.util.*;

public class TestGrade_Ali
{
    public static void main(String args[]) throws Exception 
    {
        Scanner input = new Scanner(System.in);
        int[] ids = {1234, 1245, 1267, 1278, 2345,
                     1256, 3456, 3478, 4567, 5678
                    };
        char[] grades = new char[10];
        String gradeString = new String();
        final int HIGHLIMIT = 100;
        String inString, outString = "";
        int flag = 0;
        
        for (int x = 0; x < ids.length; ++x) 
        {
        	try 
        	{
        		System.out.println("Enter letter grade for student id number: " + ids[x]);
                inString = input.next();
                for(int j = 0; j < GradeException_Ali.validGrades.length; ++j) 
                {
                	if(inString.charAt(0) == GradeException_Ali.validGrades[j]) 
                	{
                		flag = 1;
                	}
                }
                if(flag == 1) 
                {
                	grades[x] = inString.charAt(0);
                }
                else 
                {
                	throw(new GradeException_Ali("Invalid grade"));
                }
        	}
        	catch(GradeException_Ali e) 
        	{
        		System.out.println(e.getMessage());
        		grades[x] = 'I';	
        	}
        }
        for (int x = 0; x < ids.length; ++x)
            outString = outString + "ID #" + ids[x] + "  Grade " +
                        grades[x] + "\n";
        System.out.println(outString);
    }
}
