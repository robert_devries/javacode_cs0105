package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-1,   Prog Exercise
 * 
 * Write an application in which you declare an array of eight first names. 
 * Write a try block in which you prompt the user for an integer and display the name in the requested position. 
 * Create a catch block that catches the potential ArrayIndexOutOfBoundsException thrown when the user enters a number that is out of range. 
 * The catch block also should display the error message 'Subscript out of range.'
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 8 Sept 2021
 */
public class BadSubscriptCaught 
{
	public static void main(String[] args) 
	{
		String[] names = {"Ariel", "Brad", "Clifford", "Denise", "Emily", "Fred", "Gina", "Henry"};
		Scanner keyboard = new Scanner(System.in);
		int number;
		
		try 
		{
			System.out.println("Enter a number, and I will display a name");
			number = keyboard.nextInt();
			System.out.println("Name is " + names[number]);		//test output doesn't want to revert to array index positions
		} 
		
		catch (ArrayIndexOutOfBoundsException e) 
		{
			System.out.print ("Subscript out of range.");
		}
	}
}
