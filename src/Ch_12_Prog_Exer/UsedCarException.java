package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-7, Prog Exer
 * 
 * Create a UsedCarException class that extends Exception; its constructor receives a value for a vehicle identification number (VIN) 
 * that is passed to the parent constructor so it can be used in a getMessage() call. 
 * 
 * Create a UsedCar class with fields for vin, make, year, mileage, and price. 
 * The UsedCar constructor throws a UsedCarException when the 
 * 		VIN is not four digits; 
 * 		when the make is not Ford, Honda, Toyota, Chrysler, or Other; 
 * 		when the year is not between 1997 and 2017 inclusive; or 
 * 		either the mileage or price is negative.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 10 Sept 2021
 */

public class UsedCarException extends Exception
{
	public UsedCarException(String s)
	{
		super(s);
	}

}
