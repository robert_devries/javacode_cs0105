package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-7, Prog Exer.  main 'ThrowUsedCarException', 'UsedCarException class that extends "Exception' and UsedCar class
 * 
 * Create a UsedCarException class that extends Exception; its constructor receives a value for a vehicle identification number (VIN) 
 * that is passed to the parent constructor so it can be used in a getMessage() call. 
 * 
 * Create a UsedCar class with fields for vin, make, year, mileage, and price. 
 * The UsedCar constructor throws a UsedCarException when the 
 * 		VIN is not four digits; 
 * 		when the make is not Ford, Honda, Toyota, Chrysler, or Other; 
 * 		when the year is not between 1997 and 2017 inclusive; or 
 * 		either the mileage or price is negative.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 10 Sept 2021
 */

public class UsedCar 
{
	String vin;
    String make;
    int year;
    int mileage;
    int price;
    public final static String DEFAULT_VIN = "999";
    final int VIN_NUM_LENGTH = 4;
    final int LOW_YEAR = 1997;
    final int HIGH_YEAR = 2017;
    final String[] MAKES = {"Ford", "Honda", "Toyota", "Chrysler", "Other"};
    
    public UsedCar(String num, String carMake,
                   int carYear, int miles, int pr) throws UsedCarException 		//*** note the throws UsecarException implementation
    {
    	//vehicle vin established
    	if (num.length() != VIN_NUM_LENGTH)
        	throw (new UsedCarException("Not valid VIN"));
        if (num.length() == VIN_NUM_LENGTH)							//either throws exception or creates valid vin
        {
        	boolean invalidLetter;
        	for (int x = 4; x < VIN_NUM_LENGTH;++x)
        	{
        		invalidLetter = Character.isLetter(x);
        		if(invalidLetter)
        			throw (new UsedCarException("Invalid VIN using letter(s)."));
        	}
        	vin = num; 
        }
        
        //vehicle make established
        switch(carMake)
        {
        	case "Ford":
        	{
        		make = carMake;
        		break;
        	}
        	case "Honda":
        	{
        		make = carMake;
        		break;
        	}
        	case "Toyota":
        	{
        		make = carMake;
        		break;
        	}
        	case "Chrysler":       	
        	{
        		make = carMake;
        		break;
        	}
        	case "Other":
        	{
        		make = carMake;
        		break;
        	}
        	default:
        	{
        		throw (new UsedCarException("Invalid make"));
        	}
        }
        
        //year of manufacture ascertained
    	if (carYear < 1997 || carYear > 2017)
    		throw (new UsedCarException("Invalid year"));
    	else 
    	{
			year = carYear;
		}
    	
    	//mileage or price is negative 
    	if(miles < 0 || pr < 0)
    		throw (new UsedCarException("Negative pricing"));
    	else 
    		{
    		price = pr;
    		mileage = miles;
    		}
    }
    
    public UsedCar() 
    {
        vin = DEFAULT_VIN;
        make = "NoMake";
        year = 2200;
        mileage = -1;
        price = -1;
        
    }
    
    public String getVin() 
    {
        return vin;
    }
    
    public String toString() 
    {
        return "VIN " + vin + "  Make: " + make +
               "\n   Year: " + year + "  " + mileage + " miles   $" +
               price;
    }

}
