package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-8, Prog Exer:  main 'TestScore' with 'ScoreException' class that extends Exception and displays exception to user
 * 
 * Write an application that displays a series of at least five student ID numbers (that you have stored in an array) 
 * and asks the user to enter a numeric test score for the student. 
 * Create a ScoreException class that extends the Exception class. 
 * The ScoreException class constructor should accept a single argument of type String which is the message passed to the Exception superclass. 
 * Throw a ScoreException for the class if the user does not enter a valid score (less than or equal to 100). 
 * Catch the ScoreException, display the message Score over 100, and then store a 0 for the studentís score. 
 * At the end of the application, display all the student IDs and scores.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 Sept 2021
 */

public class ScoreException extends Exception
{
	public ScoreException(String s)
	{
		super(s);
	}
}
