package Ch_12_Prog_Exer;

import java.util.*;

/**
 * Class description: Ch 12-9, Prog Exer: main 'TestGrade' with GradeException class which extend Exception class.
 * 
 * Write an application that displays a series of at least 
 * 		eight student ID numbers (that you have stored in an array) and asks the user to enter a test letter grade for the student.
 * 
 * Create an Exception class named GradeException that contains a static public array of valid grade letters (A, B, C, D, F, and I) 
 * 		that you can use to determine whether a grade entered from the application is valid. 
 * 		The GradeException class constructor should accept a single argument of type String which is the message passed to the Exception superclass. 
 * 		In your application, throw a GradeException if the user does not enter a valid letter grade. 
 * 		Catch the GradeException, and then display the message Invalid grade which should be passed to the GradeException class as a parameter. 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 Sept 2021
 */

public class TestGrade 
{
	public static void main(String[] args) throws Exception			//***** this is important to include **********
	{
		Scanner input = new Scanner(System.in);
        int[] ids = {1234, 1245, 1267, 1278, 2345,
                     1256, 3456, 3478, 4567, 5678
                    };
        char[] grades = new char[10];
        //String gradeString = new String(); 		//didn't use this code line from Cengage
        final int HIGHLIMIT = 100;
        boolean validGrade = false;
        String inString, outString = "";
        int flag = 0;								//didn't use this code line from Cengage
        for (int x = 0; x < ids.length; ++x) 
        {
            System.out.println("Enter letter grade for student id number: " + ids[x]);
            inString = input.next();
            grades[x] = inString.charAt(0);
            try 
            {
				for (int y = 0; y < GradeException.gradeLetter.length; y++)
				{
					if (grades[x] == GradeException.gradeLetter[y])				//decision point
					{
						grades[x] = GradeException.gradeLetter[y];
						validGrade = true;						
					}
				}
            	if (!validGrade)
            	{
            		grades[x]= GradeException.gradeLetter[5]; 
            		throw (new GradeException("Invalid grade"));
            	}
			} 
            catch (GradeException e) 	//catches msg from line 40
            {
				System.out.println(e.getMessage());		//e is the instance parameter and .getMessage is retrieved from line 40
			}
            // Write your code here
        }
        for (int x = 0; x < ids.length; ++x)		//Cengage did not have curly braces with for statement
           	outString = outString + "ID #" + ids[x] + "  Grade " +		//Cengage had this line all by itself
                    grades[x] + "\n";
        System.out.println(outString);			//Cengage had this line all by itself - this would only display the last outString within for statement 
        
        	
        

	}

}
