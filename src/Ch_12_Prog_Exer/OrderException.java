package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-11, Prog Exer:  main:  PlaceAnOrder, OrderMessages class [] 7 messages , OrderException class extends Exception class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 12 Sept 2021
 */

public class OrderException extends Exception
{
	public OrderException (String s)
	{
		super(s);			//try this to begin with
	}
}
