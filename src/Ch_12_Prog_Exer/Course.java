package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-6, Prog Exer
 * 
 * Create a CourseException class that extends Exception and whose constructor receives a String that holds a college course�s department 
 * 		(for example, CIS), a course number (for example, 101), and a number of credits (for example, 3).
 * 
 * Create a Course class with the same fields and whose constructor requires values for each field. 
 * Upon construction, throw a CourseException if
 * 		the department does not consist of 3 letters, 
 * 		if the course number does not consist of three digits between 100 and 499 inclusive, or 
 * 		if the credits are less than 0.5 or more than 6.
 * 
 * The ThrowCourseException application has been provided to test your implementation. 
 * 
 * The ThrowCourseException application establishes an array of at least six Course objects with valid and invalid values 
 * and displays an appropriate message when a Course object is created successfully and when one is not.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Course 
{
	String department;
    int courseNumber;
    double credits;
    final int DEPT_LENGTH = 3;
    final int LOW_NUM = 100;
    final int HIGH_NUM = 499;
    final double LOW_CREDITS = 0.5;
    final double HIGH_CREDITS = 6;
    
    public Course() 
    {
        department = "ZZZ";
        courseNumber = 999;
        credits = 0;
    }
    public Course(String dept, int num, double creditValue) throws CourseException
    {
    	char c;				//not used
    	
    	if (dept.length() != DEPT_LENGTH)
    		throw (new CourseException("Department code length not within limits"));
    	else
    		department = dept;
		
    	if (num < LOW_NUM || num > HIGH_NUM)
    		throw (new CourseException("courseNumber out of limits."));
    	else 
    		courseNumber = num;
    	
    	if (creditValue < LOW_CREDITS || creditValue > HIGH_CREDITS )
    		throw (new CourseException("Credits not within range."));
    	else 
    		credits = creditValue;
    }
    public String toString() 
    {
        return ("Department: " + department + " \nCourse Number: " + courseNumber + " \nCredits: " + credits);
    }

}
