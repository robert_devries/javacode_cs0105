package Ch_12_Prog_Exer;

/**
 * Class description: Ch 12-9, Prog Exer: main 'TestGrade' with GradeException class which extend Exception class.
 * 
 * Write an application that displays a series of at least 
 * 		eight student ID numbers (that you have stored in an array) and asks the user to enter a test letter grade for the student.
 * 
 * Create an Exception class named GradeException that contains a static public array of valid grade letters (A, B, C, D, F, and I) 
 * 		that you can use to determine whether a grade entered from the application is valid. 
 * 		The GradeException class constructor should accept a single argument of type String which is the message passed to the Exception superclass. 
 * 		In your application, throw a GradeException if the user does not enter a valid letter grade. 
 * 		Catch the GradeException, and then display the message Invalid grade which should be passed to the GradeException class as a parameter. 
 * 
 * In addition, store an I (for Incomplete) for any student for whom an exception is caught. 
 * 
 * At the end of the application, display all the student IDs and grades.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 11 Sept 2021
 */

public class GradeException extends Exception 
{
	public static char[] gradeLetter = {'A', 'B', 'C', 'D', 'F', 'I'};
	
	public GradeException(String string)
	{
		super (string);
	}
}
