package Ch11_You_Do_It;

import javax.swing.*;

/**
 * Class description:  Ch 11-6a (interfaces) using Vehicle class and InsuredCar class that uses Insured interface
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  1 Sep 2021
 */
public class InsuredCarDemo 
{
	public static void main(String[] args) 
	{
		InsuredCar myCar = new InsuredCar();
		JOptionPane.showMessageDialog(null, myCar.toString());

	}

}
