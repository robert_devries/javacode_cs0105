package Ch11_You_Do_It;

import javax.swing.*;

/**
 * Class description:  Ch 11-2a, arrays of subclass items 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 31 August 2021
 */
public class VehicleDatabase 
{
	public static void main(String[] args) 
	{
		Vehicle[] vehicles = new Vehicle[2];	//creates 5 Vehicle references 
		int x;
		
		for(x = 0; x < vehicles.length; ++x)
		{
			String userEntry;
			int vehicleType;
			userEntry = JOptionPane.showInputDialog(null, "Please select the type of\n" +
					"vehicle you want to enter: \n1 - Sailboat\n" +
					" 2 - Bicycle");
			vehicleType = Integer.parseInt(userEntry);
			if(vehicleType == 1)					//if multiple vehicleTypes, consider using switch statement to choose which type
				vehicles[x] = new Sailboat();
			else
				vehicles[x] = new Bicycle();
		}
			
			StringBuffer outString = new StringBuffer();		//StringBuffer created to hold the list of vehicles
			for(x=0; x < vehicles.length; ++x)
			{
				outString.append("\n#" + (x+1) + " ");			//add newline character, a counter, and vehicle from the array to StringBuffer (outString)
				outString.append(vehicles[x].toString());
			}
			JOptionPane.showMessageDialog(null, "Our available Vehicles include:\n" + outString);
	}

}
