package Ch11_You_Do_It;

/**
 * Class description: Ch 11, You Do It Parent class Vehicle
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 31 August 2021
 */
public abstract class Vehicle 
{
	private String powerSource;
	private int wheels;
	protected int price;		//this is protected so child classes are able to access this field
	
	public Vehicle(String powerSource, int wheels)
	{
		setPowerSource(powerSource);
		setWheels(wheels);
		setPrice();
	}
	
	public String getPowerSource()
	{
		return powerSource;
	}
	
	public int getWheels()
	{
		return wheels;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public void setPowerSource(String source)
	{
		powerSource = source;
	}
	public void setWheels(int numWheels)
	{
		wheels = numWheels;
	}
	public abstract void setPrice();		//setPrice is abstract and will have a unique prompt for the price and a different max allowed price as coded in each subclass
}
