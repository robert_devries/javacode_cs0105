package Ch11_You_Do_It;

import javax.swing.*;

/**
 * Class description: Ch 11, Extends abstract Vehicle class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date 31 August 2021
 */
public class Sailboat extends Vehicle
{
	private int length;
	
	public Sailboat()
	{
		super("wind", 0);			//value for Vehicle arguments powerSource and wheels values
		setLength();				//prompts user for and sets the length of the Sailboat objects
	}
	public void setLength()
	{
		String entry;
		entry = JOptionPane.showInputDialog(null, "Enter sailboat length in feet ");
		length = Integer.parseInt(entry);
	}
	public int getLength()
	{
		return length;
	}
	@Override
	public void setPrice()			//setPrice method must be included as part of the parent class method requirements
	{
		String entry;
		final int MAX = 100000;
		entry = JOptionPane.showInputDialog(null, "Enter sailboat price ");
		price = Integer.parseInt(entry);
		if (price > MAX)
			price = MAX;			//if price is less than MAX, the indicated price from line 38 will apply
	}
	@Override
	public String toString()
	{
		return("The " + getLength() + " foot sailboat is powered by " + getPowerSource() + 
				"; it has " + getWheels() + " wheels and costs $" + getPrice());
	}

}
