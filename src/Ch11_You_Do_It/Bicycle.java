package Ch11_You_Do_It;

import javax.swing.*;

/**
 * Class description: Ch 11 belongs to Sailboat class and Vehicle parent class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 31 August 2021
 */
public class Bicycle extends Vehicle
{
	public Bicycle ()
	{
		super("a person", 2);	//call parent constructor, sending it power source and wheel values
	}
	@Override
	public void setPrice()
	{
		String entry;
		final int MAX = 4000;
		entry = JOptionPane.showInputDialog(null, "Enter bicycle price ");
		price = Integer.parseInt(entry);
		if(price > MAX)
			price = MAX;
	}
	@Override
	public String toString()
	{
		return ("The bicycle is powered by " + getPowerSource() + "; it has " + getWheels() + " wheels and costs $" + getPrice());
	}

}
