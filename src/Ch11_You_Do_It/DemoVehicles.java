package Ch11_You_Do_It;

import javax.swing.*;

import com.sun.javafx.runtime.async.AbstractAsyncOperation;

/**
 * Class description:  Ch 11 main which uses abstract Vehicle parent class and Sailboat child class and Bicycle child class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 31 August 2021
 */
public class DemoVehicles 
{
	
	public static void main(String[] args) 
	{
		Sailboat aBoat = new Sailboat();
		Bicycle aBike = new Bicycle();
		
		JOptionPane.showMessageDialog(null,  "\nVehicle descriptions:\n" + aBoat.toString() + "\n" + aBike.toString());
	}

}
