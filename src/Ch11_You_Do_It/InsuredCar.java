package Ch11_You_Do_It;

import javax.swing.*;

/**
 * Class description: InsureCar class extends Vehicle and implements Insured
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class InsuredCar extends Vehicle implements Insured
{
	private int coverage;
	
	public InsuredCar()		//this is the implemented class
	{
		super("gas",  4);	//this belongs to the Vehicle superclass constructor, passing arguments for the InsuredCar's power source and num of wheels
		setCoverage();		//must have the setCoverage method as required by the implemented class Insured
	}
	
	public void setPrice()		//implement the setPrice() method required by the Vehicle class.  The method accepts the car's price from the user
	{							//and enforces a maximum value of $60000
		String entry;
		final int MAX = 60000;
		entry = JOptionPane.showInputDialog(null, "Enter car price ");
		price = Integer.parseInt(entry);
		if (price > MAX)
			price = MAX;
	}
	
	public void setCoverage()
	{
		coverage = (int)(price * 0.9);
	}
	
	public int getCoverage()
	{
		return coverage;
	}
	
	public String toString()
	{
		return ("The insured car is powered by " + getPowerSource() + 
				"; it has " + getWheels() + " wheels, costs $" + 
				getPrice() + " and is insured for $" + getCoverage());
	}

}
