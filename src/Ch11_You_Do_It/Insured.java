package Ch11_You_Do_It;

/**
 * Class description: Ch 11-6a (interfaces) using Vehicle class and InsuredCar class that uses Insured interface
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 1 Sep 2021
 */
public interface Insured 
{
	public abstract void setCoverage();		//a concrete class that implements Insured will be required to contain setCoverage(), getCoverage() methods
	public abstract int getCoverage();		//because they are defined as abstract in the interface(as all methods in an interface MUST BE).

}
