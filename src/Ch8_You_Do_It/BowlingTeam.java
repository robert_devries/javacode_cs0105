package Ch8_You_Do_It;

/**
 * Class description: Ch 8-4b Class for BowlingTeamDemo
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class BowlingTeam 
{
	
	private String teamName;
	private String[] members = new String[4];

	public void setTeamName(String team)
	{
		teamName = team;
	}
	public String getTeamName()
	{
		return teamName;
	}
	public void setMember(int number, String name)
	{
		members[number] = name;		//array which holds the name in the index set by int number
	}
	public String getMember(int number)
	{
		return members[number];		//returns the member name in the index int number
	}

}
