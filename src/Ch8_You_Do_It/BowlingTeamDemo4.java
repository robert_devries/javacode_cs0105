package Ch8_You_Do_It;

import java.util.*;

/**
 * Class description:  Ch_8-4b which uses BowlingTeam Class to populate the array and display o/p
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 15 Jul 2021
 */
public class BowlingTeamDemo4
{
	public static void main(String[] args) 
	{
		String name;
		//BowlingTeam bowlTeam = new BowlingTeam();
		final int NUM_TEAMS = 2;
		BowlingTeam[] teams = new BowlingTeam[NUM_TEAMS];			//holds 4 bowling team objects in the array
		int x;														//used as subscript to display team member names
		int y;														//used as a subscript to display the teams
		final int NUM_TEAM_MEMBERS = 4;
		Scanner keyboard = new Scanner(System.in);
		
		//lines 26 - 40 were repositioned into the getTeamData array method at line 66 and line 25 included as a call to the method  
		getTeamData(teams);						//array argument passed to method call at line 66
		//for(y = 0; y < NUM_TEAMS; ++y)			//create array of TEAMS
		//{
		//	teams[y] = new BowlingTeam();
				
		//	System.out.print("Enter team name >> ");
		//	name = keyboard.nextLine();
		//	teams[y].setTeamName(name);
		
		//	for(x = 0; x < NUM_TEAM_MEMBERS; ++x)		//create names per team in iteration array 
		//	{
		//		System.out.println("Enter team member's name >> ");
		//		name = keyboard.nextLine();
		//		teams[y].setMember(x, name);			//x is subscript to indicate the team member's position in the array BowlingTeam
		//	}
		//}
		
		//the o/p of team/player data
		for(y = 0; y < NUM_TEAMS; ++y)
		{
			System.out.println("\nMembers of team " + teams[y].getTeamName());
			
			for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
			{
				System.out.print(teams[y].getMember(x) + " ");		//no println on this o/p so all names show up on the same line
			}
			System.out.println();
			
		}
		System.out.println("\n\nEnter a team name to see its roster >> ");		//This was added to demo3
		name = keyboard.nextLine();
		
		for(y = 0; y < teams.length; y++)
		{
			if(name.equals(teams[y].getTeamName()))
				for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
					System.out.println(teams[y].getMember(x) + " ");
			System.out.println();
		}
		System.out.println("This is teams [0] >> " + teams[0].getTeamName());		//I did this as confirmation
	}
	public static void getTeamData(BowlingTeam[] teams)
	{		//below declarations are the same as the main() method
		String name;
		final int NUM_TEAMS = 2;
		int x;														
		int y;														
		final int NUM_TEAM_MEMBERS = 4;
		Scanner keyboard = new Scanner(System.in);
		
		for(y = 0; y < NUM_TEAMS; ++y) 
		{
			teams[y] = new BowlingTeam();
			System.out.print("Enter team name >> ");
			name = keyboard.nextLine();
			teams[y].setTeamName(name);
			for(x = 0; x < NUM_TEAM_MEMBERS; ++x) 
			{
				System.out.print("Enter team member's name >> ");
				name = keyboard.nextLine();
				teams[y].setMember(x,  name);
			}
		}
	}
}
