package Ch8_You_Do_It;

/**
 * Class description:  Ch 8_1 You Do It Exercise on Arrays
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DemoArray2 
{
	public static void main(String[] args) 
	{
		double[] salaries1 = new double[4];		//the 4 indicates the size of the array
		double[] salaries2 = {12.2501, 13.5501, 14.2501, 16.8501};		//declared and initialized
		salaries1[0] = 12.25;					//values for each element of the salaries array
		salaries1[1] = 13.55;
		salaries1[2] = 14.25;
		salaries1[3] = 16.85;
		
		System.out.println("Salaries1 one by one are: ");
		System.out.println(salaries1[0]);
		System.out.println(salaries1[1]);
		System.out.println(salaries1[2]);
		System.out.println(salaries1[3]);
		System.out.println("\nSalaries2 one by one are: ");
		System.out.println(salaries2[0]);
		System.out.println(salaries2[1]);
		System.out.println(salaries2[2]);
		System.out.println(salaries2[3]);
		//System.out.println(salaries[4]);		//this will throw messages indicating out of bounds (ArrayIndexOutOfBoundsException: 4) meaning the index is 4
	}

}
