package Ch8_You_Do_It;

import java.util.*;

/**
 * Class description:  Ch_8-4b which uses BowlingTeam Class to populate the array and display o/p
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 15 Jul 2021
 */
public class BowlingTeamDemo 
{
	public static void main(String[] args) 
	{
		String name;
		BowlingTeam bowlTeam = new BowlingTeam();		//calls method thru use of ( ) and declares bowlTeam
		int x;
		final int NUM_TEAM_MEMBERS = 4;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter team name >> ");
		name = keyboard.nextLine();
		bowlTeam.setTeamName(name);
		
		for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
		{
			System.out.println("Enter team member's name >> ");
			name = keyboard.nextLine();
			bowlTeam.setMember(x, name);
		}
		System.out.println("\nMembers of team " + bowlTeam.getTeamName());
		for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
		{
			System.out.print(bowlTeam.getMember(x) + " ");		//no println on this o/p so all names show up on the same line
			
		}
		System.out.println();
	}

}
