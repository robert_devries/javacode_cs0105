package Ch8_You_Do_It;

import java.util.*;

/**
 * Class description:  Ch_8-4b which uses BowlingTeam Class to populate the array and display o/p
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 15 Jul 2021
 */
public class BowlingTeamDemo2 
{
	public static void main(String[] args) 
	{
		String name;
										//replaced ln 16 BowlingTeam bowlTeam = new BowlingTeam(); with ln 18
		final int NUM_TEAMS = 4;
		BowlingTeam[] teams = new BowlingTeam[NUM_TEAMS];	//declares/creates BowlingTeam with 4 Teams (names not created)
		int x;							//used as subscript to display team member names
		int y;							//used as a subscript to display the teams
		final int NUM_TEAM_MEMBERS = 4;
		Scanner keyboard = new Scanner(System.in);
		
		for(y = 0; y < NUM_TEAMS; ++y)
		{
			teams[y] = new BowlingTeam();			//instantiates each teams[0-4] object, with ln 32 team[0-4].setMember
				
			System.out.print("Enter team name >> ");
			name = keyboard.nextLine();
			teams[y].setTeamName(name);
		
			for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
			{
				System.out.println("Enter team member's name >> ");
				name = keyboard.nextLine();
				teams[y].setMember(x, name);
			}
		}					//below is just the display of team(s) and associated mbr per team
		
		for(y = 0; y < NUM_TEAMS; ++y)
		{
			System.out.println("\nMembers of team " + teams[y].getTeamName());
			
			for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
			{
				System.out.print(teams[y].getMember(x) + " ");		//no println on this o/p so all names show up on the same line
			}
			System.out.println();
			
		}
	}
}
