package Ch8_You_Do_It;

/**
 * Class description:  Ch 8_1 You Do It Exercise on Arrays
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DemoArray 
{
	public static void main(String[] args) 
	{
		double[] salaries = new double[4];
		salaries[0] = 12.25;
		salaries[1] = 13.55;
		salaries[2] = 14.25;
		salaries[3] = 16.85;
		
		System.out.println("Salaries one by one are: ");
		System.out.println(salaries[0]);
		System.out.println(salaries[1]);
		System.out.println(salaries[2]);
		System.out.println(salaries[3]);
		//System.out.println(salaries[4]);		//this will throw messages indicating out of bounds (ArrayIndexOutOfBoundsException: 4) meaning the index is 4
	}

}
