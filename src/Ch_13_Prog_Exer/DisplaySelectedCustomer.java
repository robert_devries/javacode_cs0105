package Ch_13_Prog_Exer;

import java.nio.channels.FileChannel;
import java.nio.file.*;		//included in Cengage
import java.io.*;			//included in Cengage
import java.util.Scanner;	//included in Cengage
import java.nio.ByteBuffer;
import static java.nio.file.AccessMode.*;		//included in Cengage
import static java.nio.file.StandardOpenOption.*;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

/**
 * Class description: Lesson 13, Prog Exer 13-5,    Step 3 in below coding instructions
 * Done		Step 1:  WriteCustomerList main initial program to create a record.
 * 						Create a program in WriteCustomerList.java that allows a user to input customer records 
 * 						(ID number, first name, last name, and balance owed) and save each record to a file named CustomerList.txt. 
 * 						Have the program accept input until ZZZ is input for the ID number. 
 * 						Write the records to CustomerList.txt in CSV format, for example:
 * Done		Step 2:  DisplaySavedCustomerList application reads the file created by this application
 * Below	Step 3:  DisplaySelectedCustomer application allows entry of ID Number, reads the customer data file created in Step 1
 * 						Display "No records found" if the ID Number cannot be found in the input file
 * 			Step 4:  DisplaySelectedCustomersByName allows last name entry and displays all the data for customers with the given last name.
 * 						Display "No records found" if the name cannot be found in the input file
 * 			Step 5:  DisplaySelectedCustomersByBalance allows entry of any purchase amt and displays all the data for customers 
 * 						with balances greater than the desired entered value.  	
 * 						Displays message "No records found" if no customers meet criteria		
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  18 Oct 2021
 */
public class DisplaySelectedCustomer 
{

	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\CustomerList.txt");		//Data.txt is in dir
		String s = "";
		String[] array = new String [4];
		Scanner keyboard = new Scanner(System.in);
		boolean found = false;
		
		String delimiter = ",";
		String storedID;
		String stringID;
		String f_name;
		String l_name;
		String balanceOwed;
							
		try
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(filePath1));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			System.out.println();
			s = reader.readLine();
			
			System.out.print("Enter ID to search >> ");
			stringID = keyboard.nextLine();
			
			while(s != null)
			{
				array = s.split(delimiter);
				storedID = array[0];
				
				if(stringID.equals(storedID))
				{
					f_name = array[1];
					l_name = array[2];
					balanceOwed = array[3];
					System.out.println("ID# " + stringID + "  " + f_name + "  " + l_name + "  " + balanceOwed);
					found = true;
					s = null;
				}
				if (!found)
				{
					s = reader.readLine();
				}				
			}
			reader.close();
			if (!found)
				System.out.print("No records found");
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
