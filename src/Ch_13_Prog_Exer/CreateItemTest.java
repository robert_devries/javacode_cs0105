package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

import javax.management.InstanceNotFoundException;

import javafx.scene.chart.PieChart.Data;

import java.nio.channels.FileChannel;
import java.text.*;

/**
 * Class description: Lesson 13, Prog Exer 13-10b 
 * Write a program named CreateItemFile.java that creates a file of items carried by the company (Items.txt). 
 * 		Include a three-digit item number and up to a 20-character description for each item. 
 * 		Issue an error message if the user tries to store an item number that already has been used. 
 * 		Accept user input until 999 is input for the item number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 27 Oct 2021
 */

public class CreateItemTest {

	public static void main(String[] args) 
	{
		//Path file = Paths.get("/root/sandbox/Items.txt");			//needed for Cengage homepage access to Items.txt
		Scanner keyboard = new Scanner(System.in);
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Items.txt");
		
		
		String itemStr = "000";						//* 3 digits long
		int itemNum;
		String itemDesc = "                    ";	//* 20 characters long
		int descLength = 20;
		String s = "000,                    " + System.getProperty("line.separator"); //separate line in given format
	    String retrieved = "";
		final int RECSIZE = s.length(); 		//length of string in each line of template file

	    //user data into ByteBuffer
		byte [] newData = s.getBytes();
	    ByteBuffer buffer = ByteBuffer.wrap(newData);

	    //retrieved data into ByteBuffer for comparison with new itemNum
	    String[] retrievedArray = new String[2];
	    byte [] retrievedData = retrieved.getBytes();
	    ByteBuffer buffer2 = ByteBuffer.wrap(retrievedData);
	    FileChannel fc = null;  
	    final int NUMRECS = 10; 		//total accounts
	    String delimiter = ","; 		//use ',' as a separator
	    final String QUIT = "999"; 	//quit if user enters 9999
	    String searchItem = "";
	    boolean isFound = false;
	    boolean fileScanned = false;

	    try
	    {	//*Step 1: Create template for Items/Description
	       OutputStream output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
	       BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output)); //use bufferedWriter to write in file
	       for(int count = 0; count < NUMRECS; ++count) //loop till 10000 all account numbers
	          writer.write(s, 0, s.length()); 	//write template for account details
	       writer.close(); 						//close the writer
	    }catch(Exception e)
	    {
	        System.out.println("Error message: " + e); //catch the exception if try fails
	    }
	    
	    try
	    {	//*Step 2: Get item number 
	    	fc = (FileChannel)Files.newByteChannel(file, READ, WRITE); //file descriptor to write account details
	         
	    	System.out.print("Enter 3-digit item number >> ");
	    	itemStr = keyboard.nextLine(); //enter the account number of user as string
	       
	    	while(!itemStr.equals(QUIT)) //loop till 10000
	    	{
	    		itemNum = Integer.parseInt(itemStr); //convert string to integer
	    		System.out.print("Enter 20-letter description for item >> ");
	    		itemDesc = keyboard.nextLine(); //enter the name of user
	    		s = itemStr + delimiter + itemDesc + System.getProperty("line.separator");
	    		
	    		if (itemDesc.length() > 20) 
	    		{
	    			itemDesc = itemDesc.substring(0, descLength);
	    		}
	          
	    		while (!fileScanned)
	    		{
	    			for (int i = 0; i < NUMRECS; i++)
	    			{
	    				fc.position(i * RECSIZE);		//use i to check all item #'s for duplication
	    				fc.read(buffer);
	    				buffer = ByteBuffer.wrap(retrievedData);
	    				String bufferedString = new String(retrievedData);
	    				retrievedArray = bufferedString.split(delimiter);
	    				searchItem = retrievedArray[0];
	    				if (searchItem.equals(itemStr))
	    				{
	    					isFound = true;
	    					break;
	    				}
	    			}
	    			fileScanned = true;		//once here, completed file scanning for duplication
	    		}
	    		
	    		//if item num found, error message, else insert into Item.txt file
	    		if (isFound)
	    		{
	    			System.out.println("Error: Item number previously enterred. Description not changed.");
					System.out.println();
					System.out.print("Enter item number or " + QUIT + " to quit >> ");
					itemStr = keyboard.nextLine();
					keyboard.nextLine();
	    		}
	    		else 
	    		{
	    			fc.read(buffer);
	    			newData = s.getBytes();
	    		    buffer = ByteBuffer.wrap(newData);
	    		    fc.position(itemNum * RECSIZE);
	    		    fc.write(buffer);
	    		    System.out.print("Enter item number or " + QUIT + " to quit >> ");
	    		    itemStr = keyboard.nextLine();
										
					isFound = false;
					fileScanned = false;
				}
	    		    		
	       }
	       fc.close(); //close the file
	    }
	    catch (Exception e)
	    {
	        System.out.println("Error message: " + e); //otherwise catch exception here
	    }
	}
}
