package Ch_13_Prog_Exer;

import java.util.Scanner;
import java.nio.file.*;
import java.io.IOException;

/**
 * Class description: Ch 13-2, Prog Exer: Only main required.  Compares 3 files in same folder and 1 file in other folder
 * 
 * Write an application that determines whether the first two files are located in the same folder as the third one. 
 * The program should prompt the user to provide 3 filepaths. 
 * If the files are in the same folder, display 
 * 		"All files are in the same folder", otherwise display 
 * 		"Files are not in the same folder".
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 20 Sept 2021
 */

public class CompareFolders_Orig1
{
	public static void main(String[] args) 
	{
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData1.txt");
		Path filePath2 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData2.txt");
		Path filePath3 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData3.txt");
		//Path filePath3 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Test_Dir_1\\TestData3.txt");

		final int MAX_FILES = 3;
		int count;
		Scanner kb = new Scanner(System.in);
		//String testFile(i);
		
		/* get 3 file paths
		for (int i = 0; i < MAX_FILES; ++i)
		{
			/*System.out.print("Enter path of File >> ");
			testFile(i) = kb.nextLine();
			Path inputPath(i) = Paths.get(testFile(i));
			Path fullPath1(i) = inputPath(i).toAbsolutePath();
			
		}
		*/
		
		try 
		{
			int count1 = filePath1.getNameCount();
			int count2 = filePath2.getNameCount();
			int count3 = filePath3.getNameCount();
			if (filePath1.getName(count1 - 2).compareTo(filePath2.getName(count2 - 2)) == 0)
			{
				//file 1 and file 2 are in the same location
				if (filePath1.getName(count1 - 2).compareTo(filePath3.getName(count3 - 2)) == 0)
				{
					System.out.println("All files are in the same folder");
				}
				else
					System.out.println("Files are not in the same folder");
			}
			else
			{
				System.out.println("Files are not in the same folder");
				System.out.println("Name count of filePath1: " + count1);
				System.out.println("File path is " + filePath1.getName(count1 - 2).toString());
				System.out.println("Name count of filePath2: " + count1);
				System.out.println("File path is " + filePath2.toString());
				System.out.println("Name count of filePath3: " + count1);
				System.out.println("File path is " + filePath3.toString());
			
			}
				
		} 
		catch (Exception e) 
		{
			System.out.println("IO Exception");
		}
	}

}
