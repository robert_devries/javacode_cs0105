package Ch_13_Prog_Exer;

import java.nio.file.*;		//included in Cengage
import java.io.*;			//included in Cengage
import java.nio.file.StandardOpenOption.*;
import java.util.Scanner;	//included in Cengage


/**
 * Class description: Lesson 13, Prog Exer 13-5, first creates a blank template, then inserts records according to ID Number
 * 			Step 1:  WriteCustomerList main initial program to create a record.
 * 						Create a program in WriteCustomerList.java that allows a user to input customer records 
 * 						(ID number, first name, last name, and balance owed) and save each record to a file named CustomerList.txt. 
 * 						Have the program accept input until ZZZ is input for the ID number. 
 * 						Write the records to CustomerList.txt in CSV format, for example:
 * 			Step 2:  DisplaySavedCustomerList application reads the file created by this application
 * 			Step 3:  DisplaySelectedCustomer application allows entry of ID Number, reads the customer data file created in Step 1
 * 						Display "No records found" if the ID Number cannot be found in the input file
 * 			Step 4:  DisplaySelectedCustomersByName allows last name entry and displays all the data for customers with the given last name.
 * 						Display "No records found" if the name cannot be found in the input file
 * 			Step 5:  DisplaySelectedCustomersByBalance allows entry of any purchase amt and displays all the data for customers 
 * 						with balances greater than the desired entered value.  	
 * 						Displays message "No records found" if no customers meet criteria		 	
 * 		
 * 
 * 		101,John,Smith,107.5
 * 		41,Jill,Green,20.0
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * @since 19 Oct 2021
 */

public class WriteCustomerList1 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\CustomerList.txt");
		String s = "";
		String delimiter = ",";
		String id;
		String f_name;
		String l_name;
		String balanceOwed;
		final String QUIT = "ZZZ";
		try
		{
			OutputStream output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			System.out.print("Enter ID number for customer or " + QUIT + " to QUIT >>");
			id = input.nextLine();
			
			while(!id.equals(QUIT))
			{
				System.out.print("Enter first name >> ");
				f_name = input.nextLine();
				System.out.print("Enter last name >> ");
				l_name = input.nextLine();
				System.out.print("Enter balance owed >> ");
				balanceOwed = input.nextLine();
				s = id + delimiter + f_name + delimiter + l_name + delimiter + balanceOwed;
				writer.write(s, 0, s.length());
				writer.newLine();
				System.out.print("Enter next ID number or " + QUIT + " to QUIT >> ");
				id = input.nextLine();
				id = id.toUpperCase();
			}
			writer.close();
		}
		catch(Exception e)
		{
			System.out.println("Message: " + e);
		}

	}

}
