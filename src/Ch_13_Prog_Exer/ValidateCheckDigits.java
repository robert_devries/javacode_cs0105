package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;

/**
 * Class description: lesson 13, Prog_Exer 13-6:
 *  AcctNumsIn.txt has been provided and contains a list of at least 15 six-digit account numbers. 
 *  Implement the ValidateCheckDigits program to read in each account number and display whether it is valid. 
 *  An account number is valid only if 
 *  	the last digit is equal to the remainder when the sum of the first five digits is divided by 10. 
 *  	For example, the number 223355 is valid because the sum of the first five digits is 15, 
 *  		the remainder when 15 is divided by 10 is 5, and the last digit is 5. 
 *  Write only valid account numbers to an output file (AcctNumsOut.txt), each on its own line.
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 19 Oct 2021
 */
public class ValidateCheckDigits 
{

	public static void main(String[] args) 
	{
		//Path fileIn = Paths.get("/root/sandbox/AcctNumsIn.txt");
        //Path fileOut = Paths.get("/root/sandbox/AcctNumsOut.txt");
       
		Path fileIn = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\AccNumsIn.txt");
		Path fileOut = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\AccNumsOut.txt");
		
		
		String readAccountLine = "";
		String fmArray = "", fmArray1 = "";
		int total = 0, extractNum = 0, extractNum1 = 0, remainder = 0;
		int toInteger = 0;
		final int DIVISION_FACTOR = 10;
		
		
		try 
		{
			//read each line of 15 lines
			InputStream input = new BufferedInputStream(Files.newInputStream(fileIn));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input)); 
			readAccountLine = reader.readLine();
						
			while(readAccountLine != null)
			{
				byte[] data = readAccountLine.getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(data);
				String fmArray2 = new String(data);
				//extractNum = Integer.parseInt(fmArray);
				fmArray1 = String.valueOf(fmArray2);
				
				for (int i = 0; i < readAccountLine.length() - 1; ++i)	//don't include last digit in adding first 5 numbers
				{
					fmArray2 = String.valueOf(data);
					//int j = Character.digit(Number.charAt(i), 10);
					//total += j;
					 
					System.out.println("Each byte >> " + data[i] + " ,string from fmArray variable >> " + fmArray + " buffer " + data);
				}
				System.out.println("New Line");
				readAccountLine = reader.readLine();
			}
			reader.close();
			
		} 
		catch (Exception e) 
		{
			System.out.println("Message for InputStream: " + e);
		}
		
			
		
		
		//read & total the first 5 digits and capture last digit in 6th spot
		
		
		//divide total by 10 and capture remainder integer
		
		
		//is remainder integer equal to last digit of original 6 digits
		
		
		//if remainder and last digit equal:  then valid account number
	}

}
