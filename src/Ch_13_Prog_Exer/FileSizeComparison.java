package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.IOException;

/**
 * Class description: Lesson 13, Prog Exer 13-3.
 * 
 * Write an application that displays the sizes of the files lyric1.txt and lyric2.txt in bytes 
 * 		as well as the ratio of their sizes to each other.
 * 
 * An example of the program running is shown below:
 * 		/root/sandbox/lyric1.txt is 95 bytes long
* 		/root/sandbox/lyric2.txt is 97 bytes long
		The first file is 97.9381443298969% of the size of the second file
 *
 * @author: Robert deVries Stadelaar (UoOttawa)
 * @date: 13 Oct 2021
 */
public class FileSizeComparison 
{
	
	public static void main(String[] args) 
	{
		//** below needed to access Cengage root directory
		//Path textFile = Paths.get("/root/sandbox/lyric1.txt");
        //Path wordFile = Paths.get("/root/sandbox/lyric2.txt");
		
		//** this used to access my system files for testing coding lines
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData1.txt");
		Path filePath2 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData2.txt");
		long sz1 = 0, sz2 = 0;
		
		try
		{
			//BasicFileAttributes used to read file (creationTime(), lastModifiedTime(), size())
			BasicFileAttributes attr1 = Files.readAttributes(filePath1, BasicFileAttributes.class);
			BasicFileAttributes attr2 = Files.readAttributes(filePath2,  BasicFileAttributes.class);
			sz1 = attr1.size();	
			sz2 = attr2.size();	
		}
		catch(IOException e)
		{
			System.out.println("IO Exception");
		}
		
		System.out.println(filePath1.toString() + " is " + sz1 + " bytes long");
		System.out.println(filePath2.toString() + " is " + sz2 + " bytes long");
		
		try
		{
			System.out.println("The first file is " + (((double)sz1 /(double)sz2) * 100) + "% of the size of the second file");
		}
		catch(ArithmeticException e)
		{
			System.out.println("2nd file does not have an attribute size");
			System.out.println(e.fillInStackTrace());
		}
	}

}
