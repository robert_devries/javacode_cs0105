package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

/**
 * Class description: Lesson 13-10c, Prog Exer. Step 3 last file which uses Step 1: CreateCustomerFile.java and Step 2: CreateItem.java)
 * Write an application named CustomerItemOrder.java that takes customer orders. 
 * 		Allow a user to enter:
 * 		1.	customer number and 
 * 		2.	item ordered. 
 * Display an error message if the customer number does not exist in the customer file or the item does not exist in the item file; 
 * 		otherwise, display all the customer information and item information. 
 * Accept user input until 999 is input for the customer ID number. An example program execution is as follows:
 *
 *  Enter customer ID number or 999 to quit >> 1
 *	Enter item number >> 7
 *	Customer: 1   Name: Joe      zip code: 88888
 * 		ordered Item #7 which is a Cake/Brownies
 *
 *	Enter customer ID number or 999 to quit >> 8
 *	Enter item number >> 2
 *	Customer: 8   Name: Jill     zip code: 11111
 * 		ordered Item #2 which is a Cookies
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  29 Oct 2021
 */

public class CustomerItemOrder 
{

	public static void main(String[] args) 
	{
		//Path customers = Paths.get("/root/sandbox/Customers.txt");
        //Path items = Paths.get("/root/sandbox/Items.txt");

		Path customers = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Customers.txt");
		Path items = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Items.txt");
		
		//Create read paths to both files
        
        
        //Enter a customer number and item ordered (display error msg if customer number (in Customer.txt file) or item not exist (Items.txt)
        
        
        //Continue until 999 is input for customer ID number
        
        
        
	}

}
