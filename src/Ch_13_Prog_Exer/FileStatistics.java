package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.File;
import java.io.IOException;

/**
 * Class description: Ch 13-1, Prog Exer: Only main application - but a few new imports
 * 
 * Write an application that displays the 
 * 		name, 
 * 		containing folder, 
 * 		size, and 
 * 		time of last modification for the file FileStatistics.java. 
 * 
 * Your program should utilize the getFileName(), readAttributes(), size(), and creationTime() methods.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 20 Sep 2021
 */

public class FileStatistics 
{
	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch13_You_Do_It\\Grades.txt");	//use this to test app 
		//Path file = Paths.get("/root/sandbox/FileStatistics.java");	//but will need this line for Cengage
		//below was added after checking the net for locating directory
		//File myFile = new File("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData1.txt");
		
		try 
		{
			int count = file.getNameCount();
			BasicFileAttributes attributes = Files.readAttributes(file,  BasicFileAttributes.class);
			System.out.println("File name is " + file.getFileName());
			System.out.println("Folder name is " + file.getName(count - 2));		//looking for the folder name. Although this works, it assumes there is a file at the end - hence nogo
			
			
			System.out.println("File size is " + attributes.size());
			System.out.println("File's creation time is " + attributes.creationTime());
		} 
		catch (IOException e) 
		{
			System.out.println("IO Exception");
		}
	}
}
