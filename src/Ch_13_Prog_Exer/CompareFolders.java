package Ch_13_Prog_Exer;

import java.util.Scanner;
import java.nio.file.*;
import java.io.IOException;

/**
 * Class description: Ch 13-2, Prog Exer: Only main required.  Compares 3 files in same folder and 1 file in other folder
 * 
 * Write an application that determines whether the first two files are located in the same folder as the third one. 
 * The program should prompt the user to provide 3 filepaths. 
 * If the files are in the same folder, display 
 * 		"All files are in the same folder", otherwise display 
 * 		"Files are not in the same folder".
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 13 Oct 2021
 */

public class CompareFolders
{
	public static void main(String[] args) 
	{
		/*  Testing static file paths to remove need for constant inputs
		Path filePath1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData1.txt");
		Path filePath2 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData2.txt");
		
		//**** activate either of the below filePath3's (one above and one below to ensure application works) 
		Path filePath3 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData3.txt");
		//Path filePath3 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Test_Dir_1\\TestData3.txt");
		*/
		
		final int MAX_FILES = 3;
		Scanner kb = new Scanner(System.in);
		String userPath;
		Path [] userInputPath = new Path [MAX_FILES];
		
		
		// get 3 file paths
		for (int i = 0; i < MAX_FILES; ++i)
		{
			System.out.print("Enter file path #" + (i + 1) + ">> ");
			userPath = kb.nextLine();
			userInputPath[i] = Paths.get(userPath);
		}
		
		try 
		{
			int count0 = userInputPath[0].getNameCount();
			int count1 = userInputPath[1].getNameCount();
			int count2 = userInputPath[2].getNameCount();
			
			//if value 0, same folder (if < 0, userInputPath[0] has less folders: if > 0, userInputPath[0] is deeper in the directory
			if (userInputPath[0].getName(count0 - 2).compareTo(userInputPath[1].getName(count1 - 2)) == 0) 	 
			{
				//file 1 and file 2 are in the same location, then compare to file 3
				if (userInputPath[0].getName(count0 - 2).compareTo(userInputPath[2].getName(count2 - 2)) == 0)
				{
					System.out.println("All files are in the same folder");
				}
				else
					System.out.println("Files are not in the same folder");
			}
			else
			{
				System.out.println("Files are not in the same folder");
				/*System.out.println("Name count of filePath1: " + count1);
				System.out.println("File path is " + filePath1.getName(count1 - 2).toString());
				System.out.println("Name count of filePath2: " + count1);
				System.out.println("File path is " + filePath2.toString());
				System.out.println("Name count of filePath3: " + count1);
				System.out.println("File path is " + filePath3.toString());
				*/
			
			}
				
		} 
		catch (Exception e) 
		{
			System.out.println("Exception encountered: " + e);
		}
	}

}
