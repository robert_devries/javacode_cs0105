package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.ByteBuffer;							//IDE says this is not used
import static java.nio.file.StandardOpenOption.*;	//IDE says this is not used
import java.util.Scanner;
import java.nio.channels.FileChannel;				//IDE says this is not used

/**
 * Class description: Lesson 13-10, Prog Exer.  Step 1:  CreateCustomerFile.java that allows
 * 		you to create a file of customers (Customers.txt) for a company. 
 * 		The first part of the program should create an empty file suitable for writing 
 * 			a three-digit ID number, 
 * 			six-character last name, and 
 * 			five-digit zip code for each customer. 
 * 
 * The second half of the program accepts user input to populate the file. Accept user input until 999 is input for the ID number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 25 Oct 2021
 */

public class CreateCustomerFile 
{
	public static void main(String[] args) 
	{
		// Path file = Paths.get("/root/sandbox/Customers.txt");		//this is used for Cengage access of writing text files
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Customers.txt");
		
		Scanner keyboard = new Scanner(System.in);
		String userID, userName, userZip;
		int userIDInt;
		
		String delimiter = ",";
		final String ID_FORMAT = "000";						//3 characters long
		final int ID_FORMAT_LENGTH = ID_FORMAT.length();
		final String NAME_FORMAT = "      ";				//6 characters long
		final int NAME_FORMAT_LENGTH = NAME_FORMAT.length();
		final String ZIP = "00000";							//5 characters long
		final int ZIP_LENGTH = ZIP.length();
		final String QUIT = "999";
		String s = (ID_FORMAT + delimiter + NAME_FORMAT + delimiter + ZIP + System.getProperty("line.separator"));
		String sNewData ="";
		
		final int RECSIZE = s.length();
		
		final int NUM_RCDS = 1000;
		final int MAX_FIELDS = 3;
		String[] array = new String[MAX_FIELDS];
		
		final String MT_ACCT = "000";
		
		FileChannel fc = null;			//initialize filechannel for accessing random ID location
		byte[] data = s.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(data);
		
		//create empty template for customer db of ID, name, zip code *** block comment out to check rest of pgm
		try 
		{
			OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			for (int rcds = 0; rcds < NUM_RCDS; rcds++)
				writer.write(s, 0, RECSIZE);
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.print("Error in creating blank template of records:" + e);
		}
		
						
		try 
		{
			//fc (FileChannel) used to write user data into empty Customer.txt file
			fc = (FileChannel)Files.newByteChannel(file, READ, WRITE);
			//populate the Customers.txt file created from above
			System.out.print("Enter customer ID number or 999 to quit >> ");
			userID = keyboard.nextLine();
			while(!userID.equals(QUIT))
			{
				
												
				userIDInt = Integer.parseInt(userID);				//from line 67 - used to position FileChannel pointer
				//StringBuilder sbUserID = new StringBuilder(userID);		//StringBuilder used to maintain field length
				//sbUserID.setLength(ID_FORMAT_LENGTH);
				
				System.out.print("Enter customer name >> ");
				userName = keyboard.nextLine();
				StringBuilder sbName = new StringBuilder(userName);
				sbName.setLength(NAME_FORMAT_LENGTH);
				
				System.out.print("Enter zip code >> ");
				userZip = keyboard.nextLine();
				//StringBuilder sbZip = new StringBuilder(userZip);
				//sbZip.setLength(ZIP_LENGTH);
				
				//new data to input into file
				sNewData = userID + delimiter + sbName + delimiter + userZip + System.getProperty("line.separator");
				byte [] dataNew = sNewData.getBytes();						//convert the constructed string above to an array of bytes,
				buffer = ByteBuffer.wrap(data);			//* then wrap the array into a ByteBuffer
				userIDInt = userIDInt - 1;				//since file starts at index 000, then all insertions will be 1 minus the user input (User wants location 2, then stored in 001)
				fc.position(userIDInt * RECSIZE);
				
				//used fc.read to read buffer data to confirm first field "000"
				fc.read(buffer);
				s = new String(data);
				array = s.split(delimiter);
				
				if (array[0].equals(MT_ACCT))
				{
					buffer = ByteBuffer.wrap(dataNew);
					fc.write(buffer);
					System.out.print("Enter customer ID number or 999 to quit >> ");
					userID = keyboard.nextLine();
				}
				else
				{
					System.out.println("Error: Customer ID previously added.");
					userID = keyboard.nextLine();
				}		
			}
			fc.close();	
		} 
		catch (Exception e) 
		{
			System.out.print("Error in populating Customers.txt: " + e);
		}
	}
}
