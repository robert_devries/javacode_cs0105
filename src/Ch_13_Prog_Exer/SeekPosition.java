package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

/**
 * Class description: Lesson 13, Prog Exer 13-7:
 * Write an application named SeekPosition that allows a user to enter a filename and an integer representing a character's position (index).
 * Assume that the file is in the same folder as your executing program. 
 * Access the requested position within the file, and display the next 10 characters there.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  23 Oct 2021
 */
public class SeekPosition 
{
	public static void main(String[] args) 
	{
		String userFileName = "", userPosNum = "";
		Scanner keyboard = new Scanner(System.in);		
		int userNum = 0;
		final int TEN_CHARS = 10;
		String tenChars = "";
		String s2 = "";
		
		System.out.print("Enter a file name >> ");			//use input1.txt as file
		userFileName = keyboard.nextLine();
		System.out.print("Enter a position number >> ");
		userPosNum = keyboard.nextLine();
		userFileName = "C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\" + userFileName;
		Path userPath = Paths.get(userFileName);
		userNum = Integer.parseInt(userPosNum);
		
		try 
		{
			InputStream inputData = new BufferedInputStream(Files.newInputStream(userPath));
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputData));
			s2 = reader.readLine();
			byte[] fileContents = s2.getBytes();
			int s2Length = s2.length();
			
			for (int i = 0; i < TEN_CHARS; ++i)
			{
				char outPut = s2.charAt(userNum + i);
				System.out.print(outPut);
			}
			System.out.println();		//**Ali added this line to make it correct
			reader.close();
			
		} 
		catch (Exception e) 
		{
			System.out.println("Error message: " + e);
		}
		


	}

}
