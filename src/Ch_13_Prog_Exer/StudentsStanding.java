package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.text.DecimalFormat;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Class description: Lesson 13-8, Prog Exer:
 * Create an application named StudentsStanding.java that allows you to enter student data that consists of an 
 * 		ID number, 
 * 		first name, 
 * 		last name, and 
 * 		grade point average. 
 * 
 * Have the program accept input until ZZZ is entered for the ID number. 
 * Depending on whether the studentís grade point average is at least 2.0, output each record either to a file 
 * 		of students in good standing (StudentsGoodStanding.txt) or 
 * 		those on academic probation (StudentsAcademicProbation.txt).
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class StudentsStanding 
{
	public static void main(String[] args) 
	{
		//Path goodFile = Paths.get("/root/sandbox/StudentsGoodStanding.txt");
        //Path probFile = Paths.get("/root/sandbox/StudentsAcademicProbation.txt");
		
		Path goodFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\StudentsGoodStanding.txt");
		Path probFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\StudentsAcademicProbation.txt");
		
		Scanner input = new Scanner(System.in);
		final String ID_FORMAT = "000";				//3 digits for account number (will end up using Integer.parseInt to convert to 'int')
		
		final String F_NAME_FORMAT = "       ";			//10 spaces
		final int F_NAME_LENGTH = F_NAME_FORMAT.length();
		final String L_NAME_FORMAT = "       ";				//10 spaces
		final int L_NAME_LENGTH = L_NAME_FORMAT.length();
		final String GPA_FORMAT = "0.0";
		final int GPA_LENGTH = GPA_FORMAT.length();
		String delimiter = ",";
		String s = ID_FORMAT + delimiter + F_NAME_FORMAT + delimiter + L_NAME_FORMAT + delimiter +							//build generic customer string by assembling the pieces
				GPA_FORMAT + delimiter + System.getProperty("line.separator");		//Record size is calculated from the dummy record.
		//final int RECSIZE = s.length();
		final double MIN_GPA = 2.0;
		
		FileChannel fcGoodStanding = null;
		FileChannel fcAcademicStanding = null;
		String idString;
		//int id;
		String f_name;
		String l_name;
		double gpa;
		final String QUIT = "ZZZ";
		
		//createEmptyFile(inStateFile, s);		//method below where blank template files are created. Later, files can be written 
		//createEmptyFile(outOfStateFile, s);			//* Method accepts Path for a file and the String that defines the record format.
		
		try
		{
			fcGoodStanding = (FileChannel)Files.newByteChannel(goodFile,  CREATE, WRITE);
			fcAcademicStanding = (FileChannel)Files.newByteChannel(probFile,  CREATE, WRITE);
			System.out.print("Enter student ID number >> ");
			idString = input.nextLine();
			while(!(idString.equals(QUIT)))
			{
				//id = Integer.parseInt(idString);
				System.out.print("Enter first name of student >> ");
				f_name = input.nextLine();
				//StringBuilder sb= new StringBuilder(f_name);		//use Stringbuilder in this format to establish length of field
				//sb.setLength(F_NAME_LENGTH);
				//f_name = sb.toString();
				System.out.print("Enter last name of student >> ");
				l_name = input.nextLine();
				//StringBuilder sb1= new StringBuilder(l_name);
				//sb1.setLength(L_NAME_LENGTH);
				//l_name = sb1.toString();
				System.out.print("Enter GPA >> ");
				gpa = input.nextDouble();
				input.nextLine();								//absorbs the Enter key value left in the input stream
				DecimalFormat df1 = new DecimalFormat(GPA_FORMAT);		//decimal format will be 

				s = (idString + delimiter + f_name + delimiter + l_name + delimiter + 
						df1.format(gpa) + System.getProperty("line.separator"));
				byte data[] = s.getBytes();						//convert the constructed string above to an array of bytes,
				ByteBuffer buffer = ByteBuffer.wrap(data);			//* then wrap the array into a ByteBuffer
				if(gpa >= MIN_GPA) 
				{
					fcGoodStanding.write(buffer);
				}
				else
				{
					fcAcademicStanding.write(buffer);
				}
				System.out.print("Enter next student ID number or " + QUIT + " to quit >>");
				idString = input.nextLine();
				idString = idString.toUpperCase();
			}
			fcGoodStanding.close();
			fcAcademicStanding.close();
		}
		catch (IOException e)
		{
			System.out.println("Error message: " + e);
		}
		
	}
	/*public static void createEmptyFile(Path file, String s)		//creates blank file template used later for writing
	{
		final int NUMRECS = 1000;		//creates maximum 1000 records
		try 
		{
			OutputStream outputStr = new BufferedOutputStream(Files.newOutputStream(file, CREATE));		//step 1, buffered stream
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStr));				//step 2, buffered writer
			
			for (int count = 0; count < NUMRECS; ++count)
			{
				writer.write(s, 0, s.length());
			}
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Error message: " + e);
		}
		

	}*/

}
