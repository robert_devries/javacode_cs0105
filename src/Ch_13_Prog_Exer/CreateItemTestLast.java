package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

import javax.management.InstanceNotFoundException;

import javafx.scene.chart.PieChart.Data;

import java.nio.channels.FileChannel;
import java.text.*;

/**
 * Class description: Lesson 13, Prog Exer 13-10b 
 * Write a program named CreateItemFile.java that creates a file of items carried by the company (Items.txt). 
 * 		Include a three-digit item number and up to a 20-character description for each item. 
 * 		Issue an error message if the user tries to store an item number that already has been used. 
 * 		Accept user input until 999 is input for the item number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 27 Oct 2021
 */

public class CreateItemTestLast {

	public static void main(String[] args) 
	{
		//Path file = Paths.get("/root/sandbox/Items.txt");			//needed for Cengage homepage access to Items.txt
		Scanner keyboard = new Scanner(System.in);
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Items.txt");
				
		String itemStr = "000";		//* 3 digits long
		int itemNum = 0;
		int finalNum = 0;
		String itemDesc = "                    ";	//* 20 characters long
		int descLength = 20;
		String s = "000,                    " + System.getProperty("line.separator"); //separate line in given format
	    String retrieved = "000,                    " + System.getProperty("line.separator");
		final int RECSIZE = s.length(); 		//length of string in each line of template file

	    //user data into ByteBuffer
		byte [] newData = s.getBytes();
	    ByteBuffer buffer = ByteBuffer.wrap(newData);

	    //retrieved data into ByteBuffer for comparison with user input itemNum
	    String[] retrievedArray = new String[2];
	    
	    //FileChannel used to write new data at random location based on item number
	    FileChannel fc = null;  
	    final int NUMRECS = 10; 		//total accounts
	    String delimiter = ","; 		//use ',' as a separator
	    String retrievedItemNum = "";
	    final String QUIT = "999"; 	//quit if user enters 9999
	    boolean isFound = false;
	   
	    try
	    {	//*Step 1: Create template for Items/Description
	       OutputStream output = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
	       BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output)); //use bufferedWriter to write in file
	       for(int count = 0; count < NUMRECS; ++count) //loop till 10000 all account numbers
	          writer.write(s, 0, s.length()); 	//write template for account details
	       writer.close(); 						//close the writer
	       
	    }
	    catch(Exception e)
	    {
	        System.out.println("Error message: " + e); //catch the exception if try fails
	    }
	    
	    try
	    {	//*Step 2: Get item number 
	    	fc = (FileChannel)Files.newByteChannel(file, READ, WRITE); //file descriptor to write account details
	        
	    	System.out.print("Enter 3-digit item number >> ");
	    	itemStr = keyboard.nextLine(); //enter the account number of user as string
	       
	    	while(!itemStr.equals(QUIT)) //loop till 10000
	    	{
	    		itemNum = Integer.parseInt(itemStr); //convert string to integer
	    		
	    		System.out.print("Enter 20-letter description for item >> ");
	    		itemDesc = keyboard.nextLine(); //enter the name of user
	    		s = itemStr + delimiter + itemDesc;		//+ System.getProperty("line.separator") not required for writing data into file
	    		
	    		if (itemDesc.length() > 20) 
	    		{
	    			itemDesc = itemDesc.substring(0, descLength);
	    		}
	    		
	    		InputStream input1 = new BufferedInputStream(Files.newInputStream(file));
		    	BufferedReader reader1 = new BufferedReader(new InputStreamReader(input1));
		    	retrieved = reader1.readLine();
		    	
	    		while (retrieved != null)
	    		{
	    			retrievedArray = retrieved.split(delimiter);
	    			retrievedItemNum = retrievedArray[0];
	    			if(retrievedItemNum.equals(itemStr))
	    			{
	    				isFound = true;
	    				break;
	    			}
	    			retrieved = reader1.readLine();
	    		}
	    		reader1.close(); //close the file used for confirming duplication does not exist
	    		
	    		
	    		//if item num found, error message, else insert into Item.txt file
	    		if (isFound)
	    		{
	    			System.out.println("Error: Item number previously enterred. Description not changed.");
					System.out.println();
					System.out.print("Enter item number or " + QUIT + " to quit >> ");
					itemStr = keyboard.nextLine();
						
				}
	    		else 
	    		{
	    			fc.read(buffer);
	    			newData = s.getBytes();
	    		    buffer = ByteBuffer.wrap(newData);
	    		    finalNum = itemNum - 1;				//used for placement in FileChannel array location in text file
	    		    fc.position(finalNum * RECSIZE);
	    		    fc.write(buffer);
	    		    System.out.print("Enter item number or " + QUIT + " to quit >> ");
	    		    itemStr = keyboard.nextLine();
				}
	    		isFound = false;		//resets duplicate value - needed at lines 123 & 135
	       }
	       fc.close(); //close the file for writing 
	       
	    }
	    catch (Exception e)
	    {
	        System.out.println("Error message: " + e); //otherwise catch exception here
	    }
	}
}
