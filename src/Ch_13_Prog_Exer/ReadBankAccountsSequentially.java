package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import static java.nio.file.AccessMode.*;


/**
 * Class description: Lesson 13-9, uses CreatebankFile.java to access the file created and read accounts in sequential order
 *   Uses the file created by the user in CreatebankFile.java and displays all existing accounts in account-number order. 
 *   For example, the output should be displayed in the following format:
 * 			ID#3  Smith   $40.0		(2 spaces after ID#3, and 3 spaces after last name)
 * 			ID#50  Johnson   $10000.4
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class ReadBankAccountsSequentially 
{

	public static void main(String[] args) 
	{
		//Path file = Paths.get("/root/sandbox/BankAccounts.txt");
		
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\BankAccounts.txt");
		String s = "";
		String[] array = new String [3];		//BankAccounts.txt is built with 3 fields (account number, name, balance)
		
		String delimiter = ",";
		int acctInt;
		String acctStr;
		String nameStr;
		String balanceStr;
								
		try
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(file));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			
			s = reader.readLine();
			
			while(s != null)
			{
				array = s.split(delimiter);
				acctStr = array[0];
				acctStr = acctStr.trim();		//hopefully, this removes any whitespace that follows the account number which is saved in 
				acctInt = Integer.parseInt(acctStr);	//**BankAccounts.txt with the implementation of StringBuilder to ensure spacing is consistent
				if (acctInt != 0)
				{
					nameStr = array[1];
					nameStr = nameStr.trim();
					balanceStr = array[2];
					balanceStr = balanceStr.trim();
					System.out.println("ID#" + acctStr + " " + nameStr + " $" + balanceStr);
					
				}
				s = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e) 
		{
			System.out.println("IO Exception" + e);
		}
	}
}
