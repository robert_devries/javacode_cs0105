package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;

/**
 * Class description: Lesson 13, Prog Exer 13-7:
 * Write an application named SeekPosition that allows a user to enter a filename and an integer representing a character's position (index).
 * Assume that the file is in the same folder as your executing program. 
 * Access the requested position within the file, and display the next 10 characters there.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  23 Oct 2021
 */
public class SeekPosition2 
{
	public static void main(String[] args) 
	{
		String userFileName = "", userPosNum = "", numCharsToDisplay = "";
		Scanner keyboard = new Scanner(System.in);		
		int userNum = 0, userCharsToDisplay = 0;
		char[] array = new char[1024];		//**Ali added this line - why?
		//final int TEN_CHARS = 10;
		//String tenChars = "";
		//String s2 = "";
		
		try
		{
			System.out.print("Enter a file name >> ");		//use input2.txt as file
			userFileName = keyboard.nextLine();
			System.out.print("Enter a position number >> ");
			userPosNum = keyboard.nextLine();
			userNum = Integer.parseInt(userPosNum);
			System.out.print("Enter number of characters to display >> ");
			numCharsToDisplay = keyboard.nextLine();
			userCharsToDisplay = Integer.parseInt(numCharsToDisplay);
			userFileName = "C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\" + userFileName;
			Path userPath = Paths.get(userFileName);
			
			InputStream inputData = new BufferedInputStream(Files.newInputStream(userPath));
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputData));
			/*
			s2 = reader.readLine();
			byte[] fileContents = s2.getBytes();
			int s2Length = s2.length();
			
			for (int i = 0; i < userCharsToDisplay; ++i)
			{
				char outPut = s2.charAt(userNum + i);
				System.out.print(outPut);
			}
			reader.close();*/
			for(int i = 0; i < userNum; ++i)
			{
				reader.read();
			}
			reader.read(array, 0, userCharsToDisplay);
			System.out.println(array);
			
			reader.close();
			
		} 
		catch (IOException e) 
		{
			System.out.println("Error message: " + e);
		}
	}
}
