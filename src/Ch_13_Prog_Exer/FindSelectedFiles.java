package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.File;
import java.io.IOException;

/**
 * Class description: Lesson 13, Prog Exer 13-4
 * Write an application that determines which, if any, of the following files are stored in the folder 
 * 		where you have saved the exercises created in this chapter: 
 * 		autoexec.bat, CompareFolders.java, FileStatistics.class, and Hello.doc. 
 * 
 * The program should display the number of files found. 
 * For example, if two of the files are found display the message 2 of the files exist.
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class FindSelectedFiles {

	public static void main(String[] args) 
	{
		final int MAX_PATHS = 4;
		int numPaths = 0;
		boolean isPath = false;
		
		/*comment out the below to run the local files (these files belong to Cengage for inputs to Prog Exer
		Path f1 = Paths.get("/root/sandbox/autoexec.bat");
        Path f2 = Paths.get("/root/sandbox/CompareFolders.java");
        Path f3 = Paths.get("/root/sandbox/FileStatistics.class");
        Path f4 = Paths.get("/root/sandbox/Hello.doc");
        */
        //*Uncomment below and run application
		Path f1 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData1.txt");
		Path f2 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData2.txt");
		Path f3 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\TestData3.txt");
		Path f4 = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Test_Dir_1\\TestData3.txt");
		           			        
        try 
        {
        	if (Files.exists(f1))
        		numPaths++;
        	if (Files.exists(f2))
        		numPaths++;
        	if (Files.exists(f3))
        		numPaths++;
        	if (Files.exists(f4))
        		numPaths++;
        	System.out.println(numPaths + " of the files exist");
		}
                
        catch (Exception e) 
        {
			System.out.println("IO exception");
			e.printStackTrace();
		}
	}
}
