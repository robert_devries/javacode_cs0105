package Ch_13_Prog_Exer;

import java.nio.channels.FileChannel;
import java.nio.file.*;		//included in Cengage
import java.io.*;			//included in Cengage
import java.util.Scanner;	//included in Cengage
import java.nio.ByteBuffer;
import static java.nio.file.AccessMode.*;		//included in Cengage
import static java.nio.file.StandardOpenOption.*;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

/**
 * Class description: Lesson 13, Prog Exer 13-5, first creates a blank template, then inserts records according to ID Number
 * 			Step 1:  WriteCustomerList main initial program to create a record.
 * 						Create a program in WriteCustomerList.java that allows a user to input customer records 
 * 						(ID number, first name, last name, and balance owed) and save each record to a file named CustomerList.txt. 
 * 						Have the program accept input until ZZZ is input for the ID number. 
 * 						Write the records to CustomerList.txt in CSV format, for example:
 * 			Step 2:  DisplaySavedCustomerList application reads the file created by this application
 * 			Step 3:  DisplaySelectedCustomer application allows entry of ID Number, reads the customer data file created in Step 1
 * 						Display "No records found" if the ID Number cannot be found in the input file
 * 			Step 4:  DisplaySelectedCustomersByName allows last name entry and displays all the data for customers with the given last name.
 * 						Display "No records found" if the name cannot be found in the input file
 * 			Step 5:  DisplaySelectedCustomersByBalance allows entry of any purchase amt and displays all the data for customers 
 * 						with balances greater than the desired entered value.  	
 * 						Displays message "No records found" if no customers meet criteria		 	
 * 		
 * 
 * 		101,John, Smith,107.5
 * 		41,Jill,Green,20.0
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * @since 15 Oct 2021
 */

public class WriteCustomerList 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		Path customerListFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\CustomerList.txt");
		
		final String ID_FORMAT = "000";						//length 3
		final String FIRST_NAME_FORMAT = "     ";		//length 10
		final String LAST_NAME_FORMAT = "      ";		//length 10
		final String BALANCE_OWED = "000.00";				//length 7
		String delimiter = ",";
		String templateStr = (ID_FORMAT + delimiter + FIRST_NAME_FORMAT + delimiter + LAST_NAME_FORMAT + 
					delimiter + BALANCE_OWED + System.getProperty("line.separator"));		//line.separator is system based line separator
		final int RECORD_SZ = templateStr.length();											//should be 34 bytes long;
		final int NUM_RECORDS = 1000;
		FileChannel fc = null;					//since record based file, use FileChannel
		
		String idString;						//if ZZZ for ID, then QUIT
		int idNum;			
		String f_name;
		String l_name;
		String balanceOwed;		
		final String QUIT = "ZZZ";
				
		//create blank template of CustomerList.txt file for use with future output of data
		try 
		{
			OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(customerListFile, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			
			for (int count = 0; count < NUM_RECORDS; count++)
			{
				writer.write(templateStr, 0, templateStr.length());
			}
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Error message creating path: " + e);
		}
		
		try 
		{
			fc = (FileChannel)Files.newByteChannel(customerListFile, READ, WRITE);
			//fc = (FileChannel)Files.newByteChannel(customerListFile, READ, WRITE);
			System.out.print("Enter ID number for customer or ZZZ to QUIT >> " );
			idString = keyboard.nextLine();
			while (!idString.equals(QUIT))
			{				
				idNum = Integer.parseInt(idString);
				System.out.print("Enter name for customer >> ");
				f_name = keyboard.nextLine();
				System.out.print("Enter last name for customer >> ");
				l_name = keyboard.nextLine();
				System.out.print("Enter balance owing >> ");
				balanceOwed = keyboard.nextLine();
				templateStr = idString + delimiter + f_name + delimiter + l_name + delimiter + balanceOwed;      // + System.getProperty("line.separator");
				byte[] data = templateStr.getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(data);
				fc.position(idNum*RECORD_SZ);
				fc.write(buffer);
				System.out.print("Enter ID for next customer or ZZZ to QUIT>> ");
				idString = keyboard.nextLine();
				idString = idString.toUpperCase();
			}
			fc.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Error in writing user data to customerListFile: " + e);
		}
		

	}

}
