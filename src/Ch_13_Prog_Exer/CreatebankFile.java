package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.text.*;

/**
 * Class description: Lesson 13-9, Prog Exer:  Uses ReadBankAccountsSequentially.java to read text file created by this application
 * The Rochester Bank maintains customer records in a random access file. 
 * Write an application in the CreateBankFile.java file that creates 
 * 		10,000 blank records and then allows the user to enter customer account information, 
 * 			including an account number that is 9998 or less, a last name, and a balance. 
 * Insert each new record into a data file at a location that is equal to the account number. 
 * Assume that the user will not enter invalid account numbers. Force each name to eight characters, padding it with spaces or truncating it if necessary. 
 * Also assume that the user will not enter a bank balance greater than 99,000.00. 
 * Allow records to be input until 9999 is input as the account number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 24 Oct 2021
 */

public class CreatebankFile
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		//Path file = Paths.get("/root/sandbox/BankAccounts.txt");
		
		//Step 1 - create file path to text file
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\BankAccounts.txt");
		
		//Step 2  - create blank file template of 10,000 records
		String recordLayout = "0000,        ,00000.00" + System.getProperty("line.separator");		//max acct numbers 9998 or less, last name is 8 chars, balance max is 99,000.00
		byte[] data = recordLayout.getBytes();
		final int acctNumLength = 4;
		final int nameLength = 8;
		final int accountBalLength = 8;
		final String QUIT = "9999";
		String delimiter = ",";
		String s = "";
		final int NUMRECS = 10000;		//** change to 10,000
		final int RECSIZE = recordLayout.length();		//each record is 4(acctNum) + 1(delimiter) + 8(inputName) + 1(delim) + 8(balance) + 2(line separator)
		FileChannel fc = null;
		String acctStr, inputName, inputBalance;
		int acctNum;
		
		try 	//try/catch to create blank file of 1000 records
		{
			OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			for (int count = 0; count < NUMRECS; ++count)
				writer.write(recordLayout, 0, recordLayout.length());
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.println("Error in creating blank records: " + e);
		}
		
		//Step 3 - open BankAccounts.txt file and insert random accounts at location equal to account number
		try 
		{
			fc = (FileChannel)Files.newByteChannel(file, READ, WRITE);
			System.out.print("Enter account number >> ");
			acctStr = keyboard.nextLine();
			while(!acctStr.equals(QUIT))
			{
				acctNum = Integer.parseInt(acctStr);		
				StringBuilder sbAcctNum = new StringBuilder(acctStr);	//create record on preset length of blank file attributes
				sbAcctNum.setLength(acctNumLength);			//looking for an int value to set length
				acctStr = sbAcctNum.toString();
				
				System.out.print("Enter name >> ");
				inputName = keyboard.nextLine();
				StringBuilder sbInputName = new StringBuilder(inputName);	//create record on preset length of blank file attributes
				sbInputName.setLength(nameLength);			//looking for an int value to set length
				inputName = sbInputName.toString();
				
				System.out.print("Enter balance >> ");
				inputBalance = keyboard.nextLine();
				StringBuilder sbInputBalance = new StringBuilder(inputBalance);		//create record on preset length of blank file attributes
				sbInputBalance.setLength(accountBalLength);		//looking for an int value to set length
				inputBalance = sbInputBalance.toString();
				
				s = acctStr + delimiter + inputName + delimiter + inputBalance + System.getProperty("line.separator");
				byte[] data1 = s.getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(data1);
				fc.position(acctNum * RECSIZE);	
				fc.write(buffer);
				System.out.print("Enter next ID number or " + QUIT + " to quit >> ");
				acctStr = keyboard.nextLine();
			}
			fc.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Error in creating bank account record: " + e);
		}

	}

}
