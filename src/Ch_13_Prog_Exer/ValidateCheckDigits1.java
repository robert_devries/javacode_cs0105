package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;


/**
 * Class description: lesson 13, Prog_Exer 13-6:
 *  AcctNumsIn.txt has been provided and contains a list of at least 15 six-digit account numbers. 
 *  Implement the ValidateCheckDigits program to read in each account number and display whether it is valid. 
 *  An account number is valid only if 
 *  	the last digit is equal to the remainder when the sum of the first five digits is divided by 10. 
 *  	For example, the number 223355 is valid because the sum of the first five digits is 15, 
 *  		the remainder when 15 is divided by 10 is 5, and the last digit is 5. 
 *  Write only valid account numbers to an output file (AcctNumsOut.txt), each on its own line.
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 19 Oct 2021
 */
public class ValidateCheckDigits1 
{

	public static void main(String[] args) 
	{
		//Path fileIn = Paths.get("/root/sandbox/AcctNumsIn.txt");
        //Path fileOut = Paths.get("/root/sandbox/AcctNumsOut.txt");
       
		Path fileIn = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\AccNumsIn.txt");
		Path fileOut = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\AccNumsOut.txt");
		String s1="";
		String s2="";
		int num1, num2, lastDigit=0, total = 0;
						
		try 	//check first file to validate account numbers within file
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(fileIn));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			
			OutputStream outToFile = new BufferedOutputStream(Files.newOutputStream(fileOut, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outToFile));
			
			s1 = reader.readLine();
			while (s1 != null)
			{
				//read & total the first 5 digits and 
				//System.out.println(s1);
				num1 = Integer.parseInt(s1);
				String number = String.valueOf(num1);
				for(int i = 0; i < number.length(); i++)
				{
					if (i < number.length()-1)
					{
						int j = Character.digit(number.charAt(i), 10);
						total += j;
					}
					//capture last digit in 6th spot
					if (i == number.length()-1)
					{
						lastDigit = Character.digit(number.charAt(i), 10);
					}
				}
				
				//divide total by 10 and capture remainder integer
				if (total % 10 == lastDigit)		//is remainder integer equal to last digit of original 6 digits, then print to outToFile
				{
					System.out.println("Number " + s1 + " is a valid account number");
					writer.write(s1, 0, s1.length());
					writer.newLine();
				}
				else
					System.out.println("Number " + s1 + " is not a valid account number");
				//System.out.println(((Object)num1).getClass().getName());
				
				total = 0;	//resets total for next account number verification
				s1 = reader.readLine();
			}
			writer.close();
		} 
		catch (IOException e) 		//for first try block
		{
			System.out.println("Message for InputStream: " + e);
		}
		
		
	}
}
