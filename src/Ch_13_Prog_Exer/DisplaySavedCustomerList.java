package Ch_13_Prog_Exer;

import java.nio.channels.FileChannel;
import java.nio.file.*;		//included in Cengage
import java.io.*;			//included in Cengage
import java.util.Scanner;	//included in Cengage
import java.nio.ByteBuffer;
import static java.nio.file.AccessMode.*;		//included in Cengage

/**
 * Class description: Lesson 13, Prog Exer 13-5,   Step 2 in below coding instructions
 * Done		Step 1:  WriteCustomerList main initial program to create a record.
 * 						Create a program in WriteCustomerList.java that allows a user to input customer records 
 * 						(ID number, first name, last name, and balance owed) and save each record to a file named CustomerList.txt. 
 * 						Have the program accept input until ZZZ is input for the ID number. 
 * 						Write the records to CustomerList.txt in CSV format, for example:
 * Below	Step 2:  DisplaySavedCustomerList application reads the file created by this application
 * 			Step 3:  DisplaySelectedCustomer application allows entry of ID Number, reads the customer data file created in Step 1
 * 						Display "No records found" if the ID Number cannot be found in the input file
 * 			Step 4:  DisplaySelectedCustomersByName allows last name entry and displays all the data for customers with the given last name.
 * 						Display "No records found" if the name cannot be found in the input file
 * 			Step 5:  DisplaySelectedCustomersByBalance allows entry of any purchase amt and displays all the data for customers 
 * 						with balances greater than the desired entered value.  	
 * 						Displays message "No records found" if no customers meet criteria		 	
 *  *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 18 Oct 2021
 */
public class DisplaySavedCustomerList 
{

	public static void main(String[] args) 
	{
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\CustomerList.txt");
		String s = "";
		
		try
		{
			InputStream input = new BufferedInputStream(Files.newInputStream(file));
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			s = reader.readLine();
			while(s != null)
			{
				System.out.println(s);
				s = reader.readLine();
			}
			reader.close();
		}
		catch(Exception e)
		{
			System.out.println("Message: " + e);
		}


	}

}
