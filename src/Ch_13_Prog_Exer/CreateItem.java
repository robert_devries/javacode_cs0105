package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.nio.channels.FileChannel;

/**
 * Class description: Lesson 13, Prog Exer 13-10b 
 * Write a program named CreateItemFile.java that creates a file of items carried by the company (Items.txt). 
 * 		Include a three-digit item number and up to a 20-character description for each item. 
 * 		Issue an error message if the user tries to store an item number that already has been used. 
 * 		Accept user input until 999 is input for the item number.
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 27 Oct 2021
 */

public class CreateItem 
{
	public static void main(String[] args) 
	{
		//Path file = Paths.get("/root/sandbox/Items.txt");			//needed for Cengage homepage access to Items.txt
		Scanner keyboard = new Scanner(System.in);
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\Items.txt");
		String sFormat = "";
		String delimiter = ",";
		String itemStr = "000";
		int itemNum;
		String itemDesc = "                    ";
		int descriptionLength = 20;
		final int QUIT = 999;
		String s = itemStr + delimiter + itemDesc + System.getProperty("line.separator");
		int RECSIZE = s.length();
		int NUM_RCDS = 5;
		
		//variables for InputStream/reader to check if item previously loaded
		String readStr = "";
		String searchItem = "";
		int searchInt = 0;		
	
		final int MAX_FIELDS = 2;
		String[] array = new String[MAX_FIELDS];
		boolean isFound = false;
		boolean textFileEmpty = true;
		
		//Use FileChannel to insert at specified location and not sequentially
		FileChannel fc = null;			//initialize filechannel for accessing random ID location
		byte[] data = s.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(data);
		
		try 
		{
			OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(file, CREATE));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			for (int rcds = 0; rcds < NUM_RCDS; rcds++)
				writer.write(s, 0, RECSIZE);
			writer.close();
		} 
		catch (Exception e) 
		{
			System.out.print("Error in creating blank template of records: " + e);
		}

		try 
		{
			//used to upload user data into file Items.txt
			
			//A_OutputStream output1 = new BufferedOutputStream(Files.newOutputStream(file, CREATE, WRITE));
			//A_BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output1));
			
			fc = (FileChannel)Files.newByteChannel(file, READ, WRITE);
			
			System.out.print("Enter item number >> ");
			itemNum = keyboard.nextInt();
			keyboard.nextLine();		//gets rid of buffer garbage
			
			//InputStream reader to check file (Items.txt) for previous existing item included
			
			while (itemNum != QUIT)
			{
				System.out.print("Enter description for item >> ");						
				itemDesc = keyboard.nextLine();
				if (itemDesc.length() > 20)
					itemDesc = itemDesc.substring(0, descriptionLength);
				sFormat = itemNum + delimiter + itemDesc;

				if (!textFileEmpty)
				{
					InputStream input1 = new BufferedInputStream(Files.newInputStream(file));
					BufferedReader reader1 = new BufferedReader(new InputStreamReader(input1));
					readStr = reader1.readLine();
						
					while (readStr != null)		//**first input, searchStr is null, following inputs need checking
					{
						array = readStr.split(delimiter);
						searchItem = array[0];
						searchInt = Integer.parseInt(searchItem);
						readStr = reader1.readLine();
						
						if (searchInt == itemNum)
						{
							isFound = true;
							reader1.close();
							break;
						}
					}
				}
				if (isFound)
				{
					System.out.println("Error: Item number previously enterred. Description not changed.");
					System.out.println();
					System.out.print("Enter item number or " + QUIT + " to quit >> ");
					itemNum = keyboard.nextInt();
					keyboard.nextLine();
				}
				
				else 
				{
					fc.read(buffer);
					byte[] dataNew = sFormat.getBytes();
					buffer = ByteBuffer.wrap(dataNew);
					fc.position(searchInt * RECSIZE);
					fc.write(buffer);
					System.out.print("Enter item number or " + QUIT + " to quit >> ");
					itemNum = keyboard.nextInt();
					keyboard.nextLine();
					textFileEmpty = false;
				}
				
					/*A_writer.write(sFormat, 0, sFormat.length());
					writer.flush();					//inline with line 49 (CREATE, WRITE), this ln will flush buffer
					writer.newLine();
					System.out.print("Enter item number or " + QUIT + " to quit >> ");
					itemNum = keyboard.nextInt();
					keyboard.nextLine();
								//**** data for stream is in buffer until the close() method runs
					textFileEmpty = false;
						//use the buffer stream to write the data 
									// ** after checking duplication does not exist
				}A_*/
				isFound = false;		//reset boolean for next item to be searched in Items.txt
			}
			//A_writer.close();	
			
		} 
		catch (IOException e) 
		{
			System.out.println("IO Exception in reader/writer " + e);
		}

	}

}
