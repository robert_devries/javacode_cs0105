package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;

/**
 * Class description: Lesson 13-8, Prog Exer which reads files from StudentsStanding.java file execution which creates 2 x text files
 *
 *	Create an application named StudentsStanding2.java that displays each record in the two files created in the StudentsStanding 
 *	application. 
 *
 *	Display a heading to introduce the list produced from each file. 
 *
 *	For each record, display the 
 *		ID number, 
 *		first name, 
 *		last name, 
 *		grade point average, and the 
 *		amount by which the grade point average exceeds or falls short of the 2.0 cutoff. 
 *
 *	Probationary Standing
 *
 *	ID #10  Mike  Green  GPA: 1.9    -0.10000000000000009 from 2.0 cutoff
 *
 *	Good Standing
 *
 *	ID #100  Jill  Green  GPA: 2.0    0.0 from 2.0 cutoff
 *	ID #50  Jane  Doe  GPA: 3.7    1.7000000000000002 from 2.0 cutoff
 *
 *	For example, the output should be formatted as follows (note that the student info may vary):
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date:  23 Oct 2021
 */
public class StudentsStanding2 
{
	public static void main(String[] args) 
	{
		//Path goodFile = Paths.get("/root/sandbox/StudentsGoodStanding.txt");
        //Path probFile = Paths.get("/root/sandbox/StudentsAcademicProbation.txt");
        
		Path goodFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\StudentsGoodStanding.txt");
		Path probFile = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\StudentsAcademicProbation.txt");
		String s1, s2;
		String delimiter = ",";
		String [] array = new String [4];
				
		//review and output data for Probationary Standing from StudentsAcademicProbation.txt
		try 
		{
			InputStream iStream2 = new BufferedInputStream(Files.newInputStream(probFile));
			BufferedReader readerPF = new BufferedReader(new InputStreamReader(iStream2));
			s2 = readerPF.readLine();
			
			System.out.println("Probationary Standing");
			System.out.println("");
			
			while (s2 != null)
			{				
				array = s2.split(delimiter);
				String gpa = array[3];
				String displayData = ("ID #" + array[0] + " " + array[1] + " " + array[2] + "  GPA: " + gpa + "  ");	//spaces needed
				System.out.print(displayData);
				display(gpa);
				s2 = readerPF.readLine();
				
			}
			readerPF.close();
		} 
		catch (Exception e) 
		{
			System.out.print("Reading file error from first block: " + e);
		}
		
		//next file to read and display data for StudentsGoodStanding.txt
		try 
		{
			InputStream iStream1 = new BufferedInputStream(Files.newInputStream(goodFile));
			BufferedReader readerGF = new BufferedReader(new InputStreamReader(iStream1));
			s1 = readerGF.readLine();
			System.out.println("");
			System.out.println("Good Standing");
			System.out.println("");
			
			while (s1 != null)
			{				
				array = s1.split(delimiter);
				String gpa = array[3];
				String displayData = ("ID #" + array[0] + " " + array[1] + " " + array[2] + "  GPA: " + gpa + "  ");	//spaces needed
				System.out.print(displayData);
				display(gpa);
				s1 = readerGF.readLine();
			}
			readerGF.close();
		} 
		catch (IOException e) 
		{
			System.out.print("Reading file error from second block: " + e);
		}
    }
    public static void display(String s) 
    {
    	double gpa = Double.parseDouble(s);
    	final double MIN_GPA = 2.0;
    	System.out.print(gpa - MIN_GPA + " from " + MIN_GPA + " cutoff");
    	System.out.println("");
    }

}
