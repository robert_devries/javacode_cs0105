package Ch_13_Prog_Exer;

import java.nio.file.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.ByteBuffer;
import static java.nio.file.StandardOpenOption.*;
import java.util.Scanner;
import java.net.*;

/**
 * Class description: Lesson 13-9, Prog Exer uses text file (BankAccounts.txt) created by CreatebankFile.java, selects random records based on user selection
 * Create an application named ReadBankAccountsRandomly that uses the file created by app above (BankAccounts.txt) and allows the user 
 * to enter an account number to view the account balance. 
 * Allow the user to view additional account balances until entering an application-terminating value of 9999. 
 * An example program execution is as follows:
 * 
 * 		Enter account number >> 50
 * 		ID #50  50,Johnson,10000.4
 *
 * 		Enter next account number or 9999 to quit >> 3
 * 		ID #3  3,Smith,40.0
 *
 *		Enter next account number or 9999 to quit >> 9999
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */

public class ReadBankAccountsRandomly 
{
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		//Path file = Paths.get("/root/sandbox/BankAccounts.txt");
		
		Path file = Paths.get("C:\\Users\\Gamer\\Documents\\workspace251\\MyFirstProject\\src\\Ch_13_Prog_Exer\\BankAccounts.txt");
		
		String s = "0000,        ,00000.00" + System.getProperty("line.separator");
		final int RECSIZE = s.length();
		byte[] data = s.getBytes();
		ByteBuffer buffer = ByteBuffer.wrap(data);
		String[] array = new String[3];
		String delimiter = ",";
		
		FileChannel fc = null;
		String acctNumStr;
		int acct;
		String acctStr, nameStr = "", balanceStr = "";
		final String QUIT = "9999";
				
		try
		{
			fc = (FileChannel)Files.newByteChannel(file, READ, WRITE);
			System.out.print("Enter account number >> ");
			acctNumStr = keyboard.nextLine();
			while(!acctNumStr.equals(QUIT))
			{
				acct = Integer.parseInt(acctNumStr);
				buffer = ByteBuffer.wrap(data);
				fc.position(acct * RECSIZE);
				fc.read(buffer);
				s = new String(data);
				array = s.split(delimiter);
				acctStr = array[0];
				acctStr = acctStr.trim();
				nameStr = array[1];
				nameStr = nameStr.trim();
				balanceStr = array[2];
				balanceStr = balanceStr.trim();
				System.out.println("ID #" + acctNumStr + "  " + acctStr + delimiter + nameStr + delimiter + balanceStr);
				System.out.print("Enter next account number or " + QUIT + " to quit >> ");
				acctNumStr = keyboard.nextLine();
			}
			fc.close();
		}
		catch(Exception e) 
		{
			System.out.println("Error message: " + e);
		}
		

	}

}
