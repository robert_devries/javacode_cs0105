package Ch8_Figures;

import javax.swing.JOptionPane;

/**
 * Class description: Figure 8-9 in notes, using array to find a price and corresponding item
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 15 July 2021
 */
public class FindPrice_Arrays 
{
	public static void main(String[] args) 
	{
		final int NUMBER_OF_ITEMS = 10;
		int[] validValues = {000, 100, 201, 313, 466, 504, 611, 709, 811, 912};		//used first int value to represent total listing of 10 items
		double[] prices = {0.29, 1.23, 2.50, 3.60, 4.55, 5.99, 6.23, 7.99, 8.99, 9.99};		//first digit used corresponds to index listing (0.29 is index 0)
		String strItem;
		int itemOrdered;
		double itemPrice = 0.0;
		boolean isValidItem = false;
		
		strItem = JOptionPane.showInputDialog(null, "Enter the item number you want to order >> ");
		itemOrdered = Integer.parseInt(strItem);
		
		for(int x = 0; x < NUMBER_OF_ITEMS; ++x)
		{
			if(itemOrdered == validValues[x])
			{
				isValidItem = true;
				itemPrice = prices[x];
			}
		}
		if(isValidItem)
		{
			JOptionPane.showMessageDialog(null,  "The price for item " + itemOrdered + " is $" + itemPrice);
		}
		else
			JOptionPane.showMessageDialog(null,  "Sorry - invalid item entered");
	}
}
