package Ch7_You_Do_It;

import javax.swing.*;

/**
 * Class description:  Ch 7, 7-4a, You Do It 
 * This is a JOptionPane window application that asks user for first and last name
 * returns the same name with all first characters in names in upper-case.
 * 
 * @author Robert deVries Stadelaar (UoOttawa)
 * 25 June 2021
 */
public class RepairName 
{
	public static void main(String[] args) 
	{
		String name, saveOriginalName;
		int stringLength;
		int i;
		char c;
		name = JOptionPane.showInputDialog(null, "Please enter your first and last name");
		saveOriginalName = name;
		stringLength = name.length();
		for(i = 0; i < stringLength; i++)
		{
			c = name.charAt(i);
			if(i == 0)
			{
				c = Character.toUpperCase(c);
				name = c + name.substring(1, stringLength);
			}
			else 
				if(name.charAt(i) == ' ')
				{
					++i;
					c = name.charAt(i);
					c = Character.toUpperCase(c);
					name = name.substring(0, i) + c + name.substring(i + 1, stringLength);
					
				}
		}
		JOptionPane.showMessageDialog(null,  "Original name was " + 
		saveOriginalName + "\nRepaired name is " + name);

	}

}
