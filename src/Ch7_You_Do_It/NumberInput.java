package Ch7_You_Do_It;

import javax.swing.*;

/**
 * Class description:  Ch 7, 7-4b, You Do It 
 * Takes characters from the keyboard, stores the characters in a String, and then converts the String 
 * to an integer that can be used in arithmetic statements. 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  26 Jun 2021
 */
public class NumberInput 
{
	public static void main(String[] args) 
	{
		String inputString;
		int inputNumber;
		int result;
		final int FACTOR = 10;
		
		inputString = JOptionPane.showInputDialog(null, "Enter a number");
		
		inputNumber = Integer.parseInt(inputString);
		result = inputNumber * FACTOR;
		JOptionPane.showMessageDialog(null, inputNumber + " * " + FACTOR + " = " + result);
		

	}

}
