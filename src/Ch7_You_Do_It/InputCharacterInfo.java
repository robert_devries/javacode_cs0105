package Ch7_You_Do_It;

import java.util.*;

/**
 * Class description: Lesson 7 (You Do It) to review the Character Class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 24 Jun 2021
 */
public class InputCharacterInfo 
{
	public static void main(String[] args) 
	{
		char aChar = 'D';
		String userInput = "";								//added lines 16 - 22
		System.out.println("The character is " + aChar);
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter character to search >> ");
		userInput = keyboard.next();
		aChar = userInput.charAt(0);
		
		if(Character.isUpperCase(aChar))
			System.out.println(aChar + " is uppercase");
		else
			System.out.println(aChar + " is not uppercase");
		if(Character.isLowerCase(aChar))
			System.out.println(aChar + " is lowercase");
		else
			System.out.println(aChar + " is not lowercase");
		
		aChar = Character.toLowerCase(aChar);
		System.out.println("After toLowerCase(), aChar is " + aChar);
		aChar = Character.toUpperCase(aChar);
		System.out.println("After toUpperCase(), aChar is " + aChar);
		
		if(Character.isLetterOrDigit(aChar))
			System.out.println(aChar + " is a letter or digit");
		else
			System.out.println(aChar + " is neither a letter or digit");
		if(Character.isWhitespace(aChar))
			System.out.println(aChar + " is whitespace");
		else
			System.out.println(aChar + " is not whitespace");

	}

}
