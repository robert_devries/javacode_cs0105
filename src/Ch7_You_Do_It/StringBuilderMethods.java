package Ch7_You_Do_It;

import java.util.*;

/**
 * Class description: Ch 7, 7-5, You Do It, StringBuilder methods 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 27 Jun 2021
 */
public class StringBuilderMethods {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		StringBuilder str = new StringBuilder("singing");		//new StringBuilder (initialized with "singing")
		System.out.println(str);
		
		str.append(" in the dead of ");							//append
		System.out.println(str);
		
		str.insert(0,  "Black");								//insert at 0
		System.out.println(str);
		str.insert(5,  "bird ");							//insert at 5
		System.out.println(str);
		
		str.append("night");							//append
		System.out.println(str);
		
		char letter = str.charAt(0);						//charAt (index location)
		System.out.println("char at 0 is >> " + letter);
		
		System.out.println("Total string length is >> " + str.length());		//length method
		System.out.println("Total of string capacity is >> " + str.capacity());		//capacity method 
		
		String word1 = "happy";
		String word2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Keyboard entry for boolean expression (type (happy) >> ");
		word2 = sc.nextLine();
		
		boolean bool;
		bool = (word1==word2);
		System.out.println("value of boolean expression word1(happy) and word2(keyboard input) >> " + bool);
		
	
	}

}
