package application;

import java.util.Scanner;

/**
 * Class description: Lesson 2-5a Pitfall with Scanner class retrieval
 * or the next() method before you use the nextLine() method
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  6 May 2021
 */

public class GetUserInfo2 
{

	public static void main(String[] args) 
	{
		String name;
		int age;
		
		Scanner inputDevice = new Scanner(System.in);	// notification is understandable - see line 24 comment
		System.out.print("Please enter your age >> ");
		age = inputDevice.nextInt();
		System.out.print("Please enter your name >> ");		//line never gets executed due to white space, tab or Enter from line 23
		name = inputDevice.nextLine();	// which was left in the keyboard buffer also called type-ahead buffer
		System.out.println("Your name is " + name +
				" and you are " + age + " years old.");
		
		System.out.print("Please enter your name >> ");
		name = inputDevice.nextLine();
		System.out.print("Please enter your age >> ");    //reversed lines 24/25 above lines 22/23
		age = inputDevice.nextInt();
		System.out.println("Your name is " + name +
				" and you are " + age + " years old.");

		inputDevice.close();
		
	}

}
