package application;

/**
 * Class description:  Lesson 4-1 Overloading a Method
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 21 May 2021
 */
public class DemoBlock 
{
	public static void main(String[] args) 
	{
		System.out.println("Demonstrating block scope");    // TODO Auto-generated method stub
		int x =1111;
		System.out.println("In first block x is " + x);
		{
			int y = 2222;
			System.out.println("In second block x is " + x);
			System.out.println("In second block y is " + y);
		}
		{
			//x = 1111_1111;		//runs and overwrites x value
			//int x = 1111_1111;    //will not execute as this is reinitializing x value
			int y = 3333;
			System.out.println("In third block x is " + x);
			System.out.println("In third block y is " + y);
			demoMethod();
			System.out.println("After method call x is " + x);
			System.out.println("After method call x is " + y);
		}
		

		
		System.out.println("At the end x is " + x);
	}
		public static void demoMethod()
		{
			int x = 8888, y = 9999;
			System.out.println("In demoMethod x is " + x);
			System.out.println("In demoMethod block y is " + y);
		}

}
