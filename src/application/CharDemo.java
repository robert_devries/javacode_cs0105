package application;

/**
 * Class description: Lesson 2-4 using the char data type
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 5 May 2021
 */
public class CharDemo 
{
	public static void main(String[] args) 
	{
		char initial = 'A';
		
		System.out.println(initial);
		System.out.print("\t\"abc\\def\bghi\n\njkl\n\n");
		System.out.print("The above code has no errors but has a graphic question mark and doesn't backspace.");

	}

}
