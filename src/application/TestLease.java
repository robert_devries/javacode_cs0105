package application;

import java.util.Scanner;
/**
 * Class description: Lesson 3, Exercise 13
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  17 May 2021
 */
public class TestLease 
{
	public static void main(String[] args) 
	{
		Lease firstLeaseObj;
		Lease secondLeaseObj;
		Lease thirdLeaseObj;
		Lease fourthLeaseObj;
		
		firstLeaseObj = getData();		//gets data from user to populate each Lease
		secondLeaseObj = getData();
		thirdLeaseObj = getData();
		fourthLeaseObj = getData();
				
		showValues(firstLeaseObj);		//without pet fees added
		firstLeaseObj.addPetFee();		//pet fees added to firstLeaseObj
		
		showValues(firstLeaseObj);
		showValues(secondLeaseObj);
		showValues(thirdLeaseObj);
		showvalues(fourthLeaseObj);
		

	}
	// setters to set each leases
	public static Lease getData()
	{
		Lease eachLease = new Lease();
		String name;
		int apartmentNum;
		int leaseInMonths;
		double monthlyRent;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Name >> ");
		name = input.nextLine();
		eachLease.setName(name);
		System.out.println("Apartment Number >> ");
		apartmentNum = input.nextInt();
		eachLease.setApartmentNum(apartmentNum);
		System.out.println("Lease in months >> ");
		leaseInMonths = input.nextInt();
		eachLease.setLeaseInMonths(leaseInMonths);
		System.out.println("Monthly rental cost >> ");
		monthlyRent = input.nextDouble();
		eachLease.setMonthlyRent(monthlyRent);
				
		return eachLease;
	}
	public static void showValues (Lease leaseObj)
	{
		System.out.println("\nValues retrieved for each apartment lease are >> " + 
				"\nOccupant Name:  " + leaseObj.getName() + 
				"\nApartment Number:  " + leaseObj.getApartmentNum() +
				"\nLease in months:  " + leaseObj.getLeaseInMonths() +
				"\nMonthly rent:  " + leaseObj.getMonthlyRent());
	}

}
