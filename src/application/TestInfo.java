package application;

/**
 * Class description: will call displayInfo() from ParadiseInfo class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 10 May 2021
 */
public class TestInfo 
{


	public static void main(String[] args) 
	{
		System.out.println("Calling method from ParadiseInfo.displayInfo class:");
		ParadiseInfo.displayInfo();

	}

}
