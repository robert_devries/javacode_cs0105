package application;

import java.util.Scanner;

/**
 * Class description: demo on integers of various types
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  06 May 2021
 */
public class IntegerDemoInteractive 
{

	public static void main(String[] args) 
	{
		int anInt;
		byte aByte;
		short aShort;
		long aLong;
		
		String name;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please enter an integer >> ");
		anInt = input.nextInt();
		
		System.out.println("Please enter a byte integer >> ");
		aByte = input.nextByte();
		System.out.println("Please enter a short integer >> ");
		aShort = input.nextShort();
		System.out.println("Please enter a long integer >> ");
		aLong = input.nextLong();
		input.nextLine();		// line added to clear input buffer of the 'Enter' key
		System.out.print("Please provide your name >> ");
		name = input.nextLine();
		System.out.println("\nThank you, " + name);

		System.out.println(anInt + " " + aByte + " " + aShort + " " + aLong +
			" " + " " + name );

		System.out.print("\nAll done!");

		input.close();
	}

}
