package application;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class DivideByZero {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int num = 4;
		int denominator = 2;
		int remainder = 0;
		
		// Loop through until number can't be evenly divided 
		while (remainder == 0) {
			remainder = num % denominator;
			
			denominator--;
		}
		
		System.out.printf("The lowest number %d can be divided by is %d. \n", num, denominator);

	}

}
