package application;

/**
 * Class description:  Lesson 3-7a Creating Instance Methods in a Class
 * **** there is no public static main() method in this scripting
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 14 May 2021
 */
public class SpaService 
{
	private String serviceDescription;
	private double price;
	
	public SpaService()		//this is the constructor which initializes the fields to a value 
	{
		serviceDescription = "XXX ";
		price = 0;
	}

	// ***** setters - service and price info for spa
	public void setServiceDescription(String service)
	{
		serviceDescription = service;
	}
	public void setPrice(double servicePrice)
	{
		price = servicePrice;
	}
	
	// ***** getters - service description and price
	public String getServiceDescription()
	{
		return serviceDescription;
	}
	public double getPrice()
	{
		return price;
	}

}
