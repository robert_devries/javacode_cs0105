package application;

/**
 * Class description:  Programming Exercise 4-5 which uses TestGame main, Team class and TestTeam main
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 26 May 2021
 */
public class Game 
{
	private Team team1;
    private Team team2;
    private String time;
    public Game(Team t1, Team t2, String time) 
    {
    	this.team1 = t1;
    	this.team2 = t2;
    	this.time = time;
    }
    public Team getTeam1() 
    {
    	return team1;
    }
    public Team getTeam2() 
    {
    	return team2;
    }
    public String getTime() 
    {
    	return time;
    }

}
