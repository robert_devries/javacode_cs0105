package application;

import java.util.Scanner;

/**
 * Class description: Lesson 3-8a Understanding Data Hiding (in conjunction with SpaService methods class
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 14 May 2021
 */
public class CreateSpaServices 
{

	public static void main(String[] args) 
	{
						
		SpaService firstService = new SpaService();
		SpaService secondService = new SpaService();
		
				
		firstService = getData(firstService);
		secondService = getData(secondService);
		
		System.out.println("First service details: ");
			System.out.println(firstService.getServiceDescription() + 
					"$" + firstService.getPrice());
			
		System.out.println("Second service details: ");
			System.out.println(secondService.getServiceDescription() + 
					"$" + secondService.getPrice());

	}
	
	public static SpaService getData(SpaService service) 
	{
		String services;
		double prices;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter service >> ");
		services = keyboard.nextLine();
		System.out.println("Enter price >> ");
		prices = keyboard.nextDouble();
		keyboard.nextLine();
		
		service.setServiceDescription(services);
		service.setPrice(prices);
		keyboard.close();

		return service;
		
	}
}
