package application;

/**
 * Class description:  Lesson 4-6 Overriding a variable
 * 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class OverridingVariable 
{


	public static void main(String[] args) 
	{
		int aNumber = 10;
		
		System.out.println("In main(), aNumber is " + aNumber);
		firstMethod();
		System.out.println("Back in main(), aNumber is " + aNumber);
		secondMethod(aNumber);
		System.out.println("Back in main(), aNumber is " + aNumber);

	}
	public static void firstMethod()
	{
		int aNumber = 77;
		System.out.println("In firstMethod(), aNumber is " + aNumber);
		
	}
	public static void secondMethod(int aNumber)
	{
		int bNumber = aNumber;
		System.out.println("In secondMethod(), at first " + "aNumber is " + aNumber + " cause it is a parameter in the method" +
				" and was not renamed within the scope of the method.");
		aNumber = 862;
		System.out.println("In secondMethod(), after an assignment " + "aNumber is " + aNumber);

		System.out.println("Me: added this line to show bNumber variable " + 
			"can be used instead of aNumber variable " + bNumber);
	}

}
