package application;
import java.util.Scanner;

public class GetUserInfo2Fix 

{

	public static void main(String[] args) 
	{
		String name;
		int age;
		
		Scanner inputDevice = new Scanner(System.in);
		System.out.print("Please enter your age >> ");
		age = inputDevice.nextInt();
		inputDevice.nextLine();		// this line simply captures the left over Enter key submitted to line 15
		System.out.print("Please enter your name >> ");		
		name = inputDevice.nextLine();	
		System.out.println("Your name is " + name +
				" and you are " + age + " years old.");
		
		inputDevice.close();
	}

}
