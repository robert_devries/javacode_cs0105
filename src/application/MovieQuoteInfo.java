package application;


/** Quotes movie classic statement including:
 * movie title
 * actor/actress 
 * year of movie
 * author:  Robert de Vries Stadelaar
 * course:  Coding for Veterans
 */
public class MovieQuoteInfo 
{
    public static void main(String[] args)
    {
        System.out.println("I\'ll be back");
        System.out.println("Terminator");
        System.out.println("The Terminator");
        System.out.println("1984");
    }
    
}
