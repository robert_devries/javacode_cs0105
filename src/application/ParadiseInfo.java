package application;

/**
 * Class description: Lesson 3-2d Methods
 * first method construction
 * this will be used by TestInfo.java class 
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 10 May 2021
 * 
 */
public class ParadiseInfo 
{

	public static void main(String[] args) 
	{
		displayInfo();

	}
	public static void displayInfo()
	{
		System.out.println("Paradise Day Spa wants to pamper you.");
		System.out.println("We will make you lood good.");
	}
}
