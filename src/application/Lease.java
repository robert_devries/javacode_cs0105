package application;

public class Lease 
{
	public String name;
	public int apartmentNum;
	public int leaseInMonths;	
	public double monthlyRent;
	
	public Lease()		//constructor to initialize. Note parentheses/identifier same as class
	{
		name = "XXX";
		apartmentNum = 0;
		monthlyRent = 1000;
		leaseInMonths = 12;
	}
	
	public void setName(String occupantName)
	{
		name = occupantName;
	}
	public String getName()
	{
		return name;
	}
	public void setApartmentNum(int apartment)
	{
		apartmentNum = apartment;
	}
	public int getApartmentNum()
	{
		return apartmentNum;
	}
	public void setMonthlyRent(double rent)
	{
		monthlyRent = rent;
	}
	public double getMonthlyRent()
	{
		return monthlyRent;
	}
	public void setLeaseInMonths(int inMonths)
	{
		leaseInMonths = inMonths;
	}
	public int getLeaseInMonths()
	{
		return leaseInMonths;
	}
	public void addPetFee()
	{
		double petFee = 10.0;
		explainPetPolicy();
		monthlyRent = monthlyRent + petFee;
	}
	public static void explainPetPolicy()
	{
		System.out.println("\n\nCharges for pet increases monthly rent by $10.00");
	}

}
