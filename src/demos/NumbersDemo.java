package demos;

/**
 * Class description:  Lesson 3 Programming Exercise #4
 * holds integer variables
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class NumbersDemo 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		int var1 = 10;
		int var2 = 50;
		
		displayTwiceTheNumber(var1);
		displayTwiceTheNumber(var2);
		displayNumberPlusFive(var1);
		displayNumberPlusFive(var2);
		displayNumberSquared(var1);
		displayNumberSquared(var2);
		
		

	}
	public static void displayTwiceTheNumber(int num) 
	{
		int num1 = num;
		
		System.out.println(num + " doubled is >> " + (num1 * 2));
	}
	
	public static void displayNumberPlusFive (int num)
	{
		int num1 = num;
		
		System.out.println(num + " plus 5 is >> " + (num1 + 5));
	}
	
	public static void displayNumberSquared (int num)
	{
		int num1 = num;
		System.out.println(num + " squared is >> " + (num1*num1));
	}
}
