package demos;

/**
 * Class description: demo on data types
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date: 06 May 2021
 */
public class DataDemo 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		int aWholeNumber = 315;
		final int STATES_IN_US = 50;
		System.out.print("The number is ");
		System.out.println(aWholeNumber);
		System.out.println
		("The number of states is "+
		STATES_IN_US);

	}

}
