package demos;

import java.util.Scanner;

/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Percentages2
{

	public static void main(String[] args) 
	{
		double var1 = 2.0;
		double var2 = 5.0;
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Enter first number >> ");
		var1 = keyboard.nextDouble();
		System.out.println("Enter second number >> ");
		var2 = keyboard.nextDouble();
		
		computePercent(var1, var2);
		computePercent(var2, var1);

	}
	
	public static void computePercent(double num1, double num2)
	{
		double first = num1;
		double second = num2;
		double result;
		
		result = (num1/num2) * 100;
		System.out.println(num1 + " is " + result + " percent of " + num2);
		
	}
}
