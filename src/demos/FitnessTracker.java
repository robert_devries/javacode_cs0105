package demos;

import java.time.*;

/**
 * Class description:Lesson 4-3AB Programming Exercise, constructor for TestFitnessTracker
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * date:  23 May 2021
 */
public class FitnessTracker 
{
	String activity;
	int minutes;
	LocalDate date;
	
	public FitnessTracker()
	{
		activity = "running";
		minutes = 0;
		date = date.of(2021, 1, 1);
		
	}

}
