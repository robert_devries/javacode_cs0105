package demos;



/**
 * Class description:
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Percentages 
{

	public static void main(String[] args) 
	{
		double var1 = 2.0;
		double var2 = 5.0;
		
		computePercent(var1, var2);
		computePercent(var2, var1);

	}
	
	public static void computePercent(double num1, double num2)
	{
		double first = num1;
		double second = num2;
		double result;
		
		result = (num1/num2) * 100;
		System.out.println(num1 + " is " + result + " percent of " + num2);
		
	}
}
