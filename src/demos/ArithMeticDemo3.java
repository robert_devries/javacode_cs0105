package demos;

import java.util.Scanner;

/**
 * Class description: Lesson 2-7 You Do It Exercise
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 07 May 2021
 */
public class ArithMeticDemo3
{
	public static void main(String[] args) 
	{
		int firstNumber;	//sample number 20
		int secondNumber;	//sample number 19
		int sum;
		byte single = 8;	//added to see if line 45 would error - nope
		int difference;
		double average;		//int changed to double for this demo
		double altAverage;		//expected o/p should be 19.5
		
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter an integer >> ");
		firstNumber = input.nextInt();
		System.out.print("Please enter another integer >>");
		secondNumber = input.nextInt();
		
		sum = firstNumber + secondNumber;
		difference = firstNumber - secondNumber;
		average = (double) sum / 2;		//changed to include type casting (double) to o/p accurate results
		 									//after compilation, changed to include the parantheses (sum / 2)
											//after exe line 30, the result was incorrect (therefore removed brackets)
		altAverage = sum / 2.0;			//in lieu of line 30, chg denom to 2.0 which will cause double value o/p
		
		System.out.println(firstNumber + " + " +
				secondNumber + " is " + sum);
		System.out.println(firstNumber + " - " +
				secondNumber + " is " + difference);
		System.out.println("The average of " + firstNumber +
				" and " + secondNumber + " is " + average);
		System.out.println("The average of changed avg value " + firstNumber +
				" and " + secondNumber + " is " + altAverage);
		
		System.out.println(firstNumber + secondNumber + single + average);	//checking to see if int + int + double would error
	}

}
