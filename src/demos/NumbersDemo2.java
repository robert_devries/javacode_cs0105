package demos;

import java.util.Scanner;

/**
 * Class description:  Lesson 3 Programming Exercise #4
 * holds integer variables
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class NumbersDemo2 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		int var1, var2;
	
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("First number >> ");
		var1 = keyboard.nextInt();
		
		System.out.println("Second number >> ");
		var2 = keyboard.nextInt();
		
		displayTwiceTheNumber(var1);
		displayTwiceTheNumber(var2);
		displayNumberPlusFive(var1);
		displayNumberPlusFive(var2);
		displayNumberSquared(var1);
		displayNumberSquared(var2);
		
	}
	public static void displayTwiceTheNumber(int num) 
	{
		int num1 = num;
		
		System.out.println(num + " doubled is >> " + (num1 * 2));
	}
	
	public static void displayNumberPlusFive (int num)
	{
		int num1 = num;
		
		System.out.println(num + " plus 5 is >> " + (num1 + 5));
	}
	
	public static void displayNumberSquared (int num)
	{
		int num1 = num;
		System.out.println(num + " squared is >> " + (num1*num1));
	}
}
