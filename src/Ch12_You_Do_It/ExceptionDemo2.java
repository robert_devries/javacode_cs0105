package Ch12_You_Do_It;

import javax.swing.*;			//use this for the JOptionPane.showInputDialog() method

/**
 * Class description: Ch 12, Lesson 12-2b (Declaring and Initializing Variables in a try...catch block)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 5 Sep 2021
 */
public class ExceptionDemo2
{
	public static void main(String[] args) 
	{
		int numerator = 0, denominator = 0, result;
		String inputString;
		
		try 				// *** if no catch block is included, application will not compile ***
		{
			inputString = JOptionPane.showInputDialog(null, "Enter a number to be divided");
			numerator = Integer.parseInt(inputString);
			inputString = JOptionPane.showInputDialog(null, "Enter a number to divide into the first number");
			denominator = Integer.parseInt(inputString);
			result = numerator/denominator;
		}
		
		catch(ArithmeticException exception)
		{
			JOptionPane.showMessageDialog(null,  exception.getMessage());		//this will use the system error msgs to display the method getMessage()
			result = 0;
		}
		
		catch (NumberFormatException exception)			//*** this block added as part of You Do It, exercise 12-3 for multiple exceptions (for non-integers)***
		{
			JOptionPane.showMessageDialog(null,  "This application accepts digits only!");
			numerator = 999;
			denominator = 999;
			result = 1;
		}
		
		JOptionPane.showMessageDialog(null,  numerator + " / " + denominator + "\nResult is " + result);

	}

}
