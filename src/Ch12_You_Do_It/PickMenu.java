package Ch12_You_Do_It;

import javax.swing.*;

/**
 * Class description: You_Do_It from Chapter 12-9, Menu class that is used with PickMenu class and application PlanMenu
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 8 Sept 2021
 */
public class PickMenu2 
{
	private Menu briefMenu;
	private String guestChoice = new String();
	
	public PickMenu(Menu theMenu)	//constructor that sets the menu
	{
		briefMenu = theMenu;		//receives the user selected menu item
		setGuestChoice();
	}
	
	public void setGuestChoice() throws MenuException		//*this was added as part of MenuException class to account for throws exception
	{
		JOptionPane.showMessageDialog(null,  "Choose from the following menu:");
		guestChoice = briefMenu.displayMenu();
	}
	
	public String getGuestChoice()
	{
		return (guestChoice);
	}

}
