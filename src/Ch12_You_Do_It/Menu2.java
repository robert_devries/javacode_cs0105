package Ch12_You_Do_It;

import javax.swing.*;

/**
 * Class description:  You_Do_It from Chapter 12-9, MenuException class, which uses Menu class that is used with PickMenu class and application PlanMenu
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class Menu2 

{
	protected String [] entreeChoices = {"Rosemary Chicken", "Beef Wellington", "Maine Lobster"};		//change to protected,VegetarianMenu child class can access
	private String menu ="";
	private int choice;
	protected char initials[] = new char[entreeChoices.length];		//*this added as part of Exception class to record the first char of each input char
	
	public String displayMenu() throws MenuException				//*this added as part of Exception class (code will be added to throw such exception
	{
		for (int x = 0; x < entreeChoices.length; ++x)
		{
			menu = menu + "\n" + (x + 1) + " for " + entreeChoices[x];
			initials[x] = entreeChoices[x].charAt(0);		//*added as part of Exception class. stores 1st character of each element in entreeChoices array
		}														//*either R, B or M
		String input = JOptionPane.showInputDialog(null, "Type your selection, then press Enter." + menu);
		for(int y = 0; y < entreeChoices.length; ++y)				//*this added as part of Exception class, compares first letter of user's choice to each 
		{																//*of the initials of valid menu options
			if(input.charAt(0) == initials[y])						//*if match is found, throw a new instance of the MenuException class that uses corresponding
			{															//entree as its string argument
				throw (new MenuException(entreeChoices[y]));		//*will be caught before the parseInt (line34) before it can cause a NumberFormatException
			}
		}
		choice = Integer.parseInt(input);
		return(entreeChoices[choice - 1]);			//assume the person who input the date picked 1, 2, 3 instead of the index 0, 1, 2 (error may occur here)
	}

}
