package Ch12_You_Do_It;

/**
 * Class description: You_Do_It from Chapter 12-9, Menu class (parent class) used with VegetarianMenu child class, used with PickMenu class and application PlanMenu
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class VegetarianMenu extends Menu
{
	String[] vegEntreeChoices = {"Spinach Lasagna", "Cheese Enchiladas", "Fruit Plate"};
	
	public VegetarianMenu()
	{
		super();
		for(int x = 0; x < vegEntreeChoices.length; ++x)
		{
			entreeChoices[x] = vegEntreeChoices[x];
		}
	}

}
