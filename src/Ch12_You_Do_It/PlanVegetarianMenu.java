package Ch12_You_Do_It;

import javax.swing.*;

/**
 * Class description: You_Do_It from Chapter 12-9, Menu class that is used with PickMenu class and application PlanMenu
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * 8 Sept 2021
 */
public class PlanVegetarianMenu 
{

	public static void main(String[] args) 
	{
		VegetarianMenu briefMenu = new VegetarianMenu();		//Menu is the class with briefMenu the object
		PickMenu entree = null;				//do not want to construct a PickMenu object yet bcuz you want to be able to catch the exception that the
		String guestChoice = new String();		//PickMenu constructor might throw.  wait and construct the PickMenu object within a 'try' block
													//for now declare 'entree' and assign it 'null'
		try												//the guestChoice String is also declared that holds the customer's menu selected
		{
			PickMenu selection = new PickMenu(briefMenu);		//if construction is successful, the next statement assigns a selection to the entree object
			entree = selection;									//since entree is a PickMenu object, it has access to the getGuestChoice () method in PickMenu class
			guestChoice = entree.getGuestChoice();					//and can assign the method's returned value to the guestChoice String
		}
		
		catch(Exception error)						//catch block must immediately follow the 'try' block. when 'try' block fails, guestChoice will not have a valid 
		{												//value, so recover from the exception by assigning a value to guestChoice within the following catch block
			guestChoice = "an invalid vegetarian selection";
		}
		
		JOptionPane.showMessageDialog(null,  "You chose " + guestChoice);	//after catch block, the application continues. 
		JOptionPane.showMessageDialog(null,  "Run this app using value outside array index for exception 'throw' execution. ");																	
	}

}
