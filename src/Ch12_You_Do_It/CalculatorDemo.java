package Ch12_You_Do_It;

import java.util.Scanner;
import java.io.IOException;

/**
 * Class description: Ch 12-10, CalculatorDemo as part of the OSK (On-Screen Keyboard) - Virtual Keyboard
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 * Date: 8 Sep 2021
 */
public class CalculatorDemo 
{
	public static void main(String[] args) throws IOException		//note the inclusion of the throws IOException in the header for main
	{
		Scanner input = new Scanner(System.in);
		Process proc = Runtime.getRuntime().exec 
				("cmd /c C:\\Windows\\System32\\calc.exe");
		
		double num1 = 279.6;
		double num2 = 872.8;
		double answer = num1 + num2;
		double usersAnswer;
		
		System.out.print("What is the sum of " + num1 + " and " + num2 + "? >> ");
		usersAnswer = input.nextDouble();
		if(usersAnswer == answer)
		{
			System.out.println("Correct!");
		}
		else {
			System.out.println("Sorry - the answer is " + answer);
		}

	}

}
