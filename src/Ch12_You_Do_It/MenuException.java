package Ch12_You_Do_It;

/**
 * Class description: Ch 12, Lesson 12-9 (class extends Exception)
 *
 * @author Robert deVries Stadelaar (UoOttawa)
 *
 */
public class MenuException extends Exception
{
	public MenuException (String choice)
	{
		super(choice);
	}

}
